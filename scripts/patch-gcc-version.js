const fs = require('fs');
const path = require('path');
const xcode = require('xcode');
const semver = require('semver');
const configFile = 'config.xml';
const property = 'GCC_C_LANGUAGE_STANDARD';
const value = 'gnu11';

module.exports = context => {
    if (context.opts.cordova.platforms.indexOf('ios') > -1) {
        const projectRoot = context.opts.projectRoot;
        const platformPath = path.join(projectRoot, 'platforms', 'ios');
        const COMMENT_KEY = /_comment$/;
        const config = getConfigParser(context, path.join(projectRoot, configFile));
        const projectName = config.name();
        const pbxprojPath = path.join(platformPath, projectName + '.xcodeproj', 'project.pbxproj');
        const xcodeProject = xcode.project(pbxprojPath);
        xcodeProject.parseSync();
        const buildConfigs = xcodeProject.pbxXCBuildConfigurationSection();
        for (configName in buildConfigs) {
            if (!COMMENT_KEY.test(configName)) {
                const buildConfig = buildConfigs[configName];
                xcodeProject.updateBuildProperty(property, value, buildConfig.name);
                console.log('Update IOS build setting', property, 'to:', value, 'for build configuration', buildConfig.name);
            }
        }
        fs.writeFileSync(pbxprojPath, xcodeProject.writeSync());
    }
};

const getConfigParser = (context, configPath) => {
    let ConfigParser;
    if (semver.lt(context.opts.cordova.version, '5.4.0')) {
        ConfigParser = context.requireCordovaModule('cordova-lib/src/ConfigParser/ConfigParser');
    } else {
        ConfigParser = context.requireCordovaModule('cordova-common/src/ConfigParser/ConfigParser');
    }
    return new ConfigParser(configPath);
};