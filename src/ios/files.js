const folder = './sources/';
const fs = require('fs');
const files = fs.readdirSync(folder);
files.sort();
files.forEach((file) => {
    const ext = file.split('.')[1];
    if (ext === 'xib') {
        console.log('<resource-file src="src/ios/sources/' + file + '" />');
    } else if (ext === 'h') {
        if (file.includes('Bridging')) {
            console.log('<header-file src="src/ios/sources/' + file + '" type="BridgingHeader" />');
        } else {
            console.log('<header-file src="src/ios/sources/' + file + '" />');
        }
    } else {
        console.log('<source-file src="src/ios/sources/' + file + '" />');
    }
});