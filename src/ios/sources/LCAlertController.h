#import <UIKit/UIKit.h>
@class LCAlertController;
typedef void(^LCAlertControllerHandler)(NSInteger index);
@interface LCAlertController : UIAlertController
+ (LCAlertController *)showWithTitle:(NSString *)title
							 message:(NSString *)message
				   cancelButtonTitle:(NSString *)cancelButtonTitle
					otherButtonTitle:(NSString *)otherButtonTitle
							 handler:(LCAlertControllerHandler)handler;
+ (LCAlertController *)showWithTitle:(NSString *)title
							 message:(NSString *)message
				   cancelButtonTitle:(NSString *)cancelButtonTitle
				   otherButtonTitles:(NSArray<NSString *> *)otherButtonTitles
							 handler:(LCAlertControllerHandler)handler;
+ (LCAlertController *)showInViewController:(UIViewController *)vc
									  title:(NSString *)title
									message:(NSString *)message
						  cancelButtonTitle:(NSString *)cancelButtonTitle
						   otherButtonTitle:(NSString *)otherButtonTitle
									handler:(LCAlertControllerHandler)handler;
+ (LCAlertController *)showInViewController:(UIViewController *)vc
									  title:(NSString *)title
									message:(NSString *)message
						  cancelButtonTitle:(NSString *)cancelButtonTitle
						  otherButtonTitles:(NSArray<NSString *> *)otherButtonTitles
									handler:(LCAlertControllerHandler)handler;
+ (void)dismissAnimated:(BOOL)isAnimated;
+ (void)dismiss;
+ (BOOL)isDisplayed;
+ (UIViewController *)topPresentOrRootController;
@end
