#import <Foundation/Foundation.h>
@interface NSData (LeChange)
- (NSData *)lc_AES256Encrypt:(NSString *)key;
- (NSData *)lC_AES256Decrypt:(NSString *)key;
@end
