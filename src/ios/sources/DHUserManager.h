#import <Foundation/Foundation.h>

@interface DHUserManager : NSObject

@property(nonatomic, strong) NSMutableArray<NSDictionary *> *mSSIDMutableArray;

+ (DHUserManager *)shareInstance;

- (void)getUserConfigFile;

- (void)saveUserConfigFile;

- (BOOL)ssidIsSaved:(NSString *)ssid;

- (NSString *)ssidPwdBy:(NSString *)ssid;

- (void)addSSID:(NSString *)ssid ssidPwd:(NSString *)ssidPwd;

- (void)removeSSID:(NSString *)ssid;

- (void)clearCachedSSID;

@end

