#import "LCDeviceSettingArrowCell.h"
#import "LCUIKit.h"
#import "LCBaseDefine.h"

@interface LCDeviceSettingArrowCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet LCButton *rightBtn;
@property (weak, nonatomic) IBOutlet UIImageView *deviceSnap;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLab;
@end

@implementation LCDeviceSettingArrowCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [self addGestureRecognizer:tap];
    self.backgroundColor = [UIColor dhcolor_c43];
}

-(void)tapClick:(UITapGestureRecognizer *)tap{
    if (self.block) {
        self.block(0);
    }
}
- (IBAction)rightBtnClick:(UIButton *)sender {
    if (self.block) {
        self.block(1);
    }
}

-(void)setTitle:(NSString *)title{
    _title = title;
    self.titleLab.text = title;
    self.titleLab.textColor = [UIColor dhcolor_c51];
}

-(void)setSubtitle:(NSString *)subtitle{
    _subtitle = subtitle;
    self.subtitleLab.text = subtitle;
    self.subtitleLab.textColor = [UIColor dhcolor_c51];
}

-(void)setArrowImage:(UIImage *)image{
    [self.rightBtn setImage:image forState:UIControlStateNormal];
}

- (void)loadImage:(NSString *)imageUrl DeviceId:(NSString *)deviceId ChannelId:(NSString *)channelId {
    [self.deviceSnap lc_setThumbImageWithURL:imageUrl placeholderImage:LC_IMAGENAMED(@"common_defaultcover_big") DeviceId:deviceId ChannelId:channelId];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    if ([self.rightBtn pointInside:point withEvent:event]) {
        return self.rightBtn;
    }
    return [super hitTest:point withEvent:event];
}

@end
