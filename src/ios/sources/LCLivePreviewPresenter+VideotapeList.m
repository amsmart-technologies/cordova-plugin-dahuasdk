#import "LCLivePreviewPresenter+VideotapeList.h"
#import "LCBaseDefine.h"
#import "NSString+Dahua.h"
#import "UINavigationController+Push.h"
#import "LCError.h"
#import "OpenApiInterface.h"
#import <objc/runtime.h>
#import "LCVideotapeListViewController.h"
#import "LCApplicationDataManager.h"
static const void *kVideotapeList = @"videotapeHistoryList";
@implementation LCLivePreviewPresenter (VideotapeList)
- (void)loadCloudVideotape {
    weakSelf(self);
    [self.historyView startAnimation];
    [OpenApiInterface getCloudRecordsForDevice:self.videoManager.currentDevice.deviceId channelId:self.videoManager.currentChannelInfo.channelId day:[NSDate new] From:-1 Count:6 success:^(NSMutableArray<LCCloudVideotapeInfo *> *_Nonnull videos) {
        if (videos.count == 0) {
            [self setErrorViewWith:nil];
        } else {
            [weakself willChangeValueForKey:@"videotapeList"];
            weakself.videotapeList = videos;
            [weakself didChangeValueForKey:@"videotapeList"];
        }
    } failure:^(LCError *_Nonnull error) {
        [self setErrorViewWith:error];
    }];
}
- (void)loadLocalVideotape {
    weakSelf(self);
    [self.historyView startAnimation];
    [OpenApiInterface queryLocalRecordsForDevice:self.videoManager.currentDevice.deviceId channelId:self.videoManager.currentChannelInfo.channelId day:[NSDate new] From:1 To:6 success:^(NSMutableArray<LCLocalVideotapeInfo *> *_Nonnull videos) {
        if (videos.count == 0) {
            [self setErrorViewWith:nil];
        } else {
            [weakself willChangeValueForKey:@"videotapeList"];
            weakself.videotapeList = videos;
            [weakself didChangeValueForKey:@"videotapeList"];
        }
    } failure:^(LCError *_Nonnull error) {
        [self setErrorViewWith:error];
    }];
}
- (void)setErrorViewWith:(LCError *)error {
    LCButton *errorBtn = [LCButton lcButtonWithType:LCButtonTypeShadow];
    if (!error) {
        [errorBtn setTitle:@"play_module_none_record".lc_T forState:UIControlStateNormal];
        errorBtn.touchUpInsideblock = ^(LCButton *_Nonnull btn) {
            LCVideotapeListViewController *videotape = [[LCVideotapeListViewController alloc] init];
            videotape.defaultType = (self.historyView.isCurrentCloud ? 0 : 1);
            UIViewController *yourCurrentViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
            while (yourCurrentViewController.presentedViewController){
                yourCurrentViewController = yourCurrentViewController.presentedViewController;
            }
            [videotape setModalPresentationStyle:UIModalPresentationFullScreen];
            [yourCurrentViewController presentViewController:videotape animated:YES completion:nil];
        };
    } else {
        [errorBtn setTitle:error.errorMessage forState:UIControlStateNormal];
    }
    [self.historyView setupErrorView:errorBtn];
}
- (void)setHistoryView:(LCVideoHistoryView *)historyView {
    objc_setAssociatedObject(self, kVideotapeList, historyView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (LCVideoHistoryView *)historyView {
    return objc_getAssociatedObject(self, kVideotapeList);
}
@end
