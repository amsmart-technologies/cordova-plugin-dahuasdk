#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef void(^selectBlock)(NSUInteger index);
@interface LCSegmentController : UIView
@property (strong, nonatomic ,readonly) NSArray <NSString *> *items;
+(instancetype)segmentWithItems:(nonnull NSArray <NSString *> *)items SelectedBlock:(selectBlock)selected;
+(instancetype)segmentWithFrame:(CGRect)frame DefaultSelect:(NSInteger)select Items:(NSArray<NSString *> *)items SelectedBlock:(selectBlock)selected;
-(void)setSelectIndex:(NSInteger)index;
@property (nonatomic) BOOL enable;
@end
NS_ASSUME_NONNULL_END
