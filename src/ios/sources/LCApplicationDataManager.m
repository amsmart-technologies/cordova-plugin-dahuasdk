#import "LCApplicationDataManager.h"
#import "NSString+Verify.h"
#define APPID             @"GLOBAL_CONFIG_APPLICATION_APPID"
#define OVERSEAAPPID      @"GLOBAL_CONFIG_APPLICATION_OVERSEA_APPID"
#define APPSECRET         @"GLOBAL_CONFIG_APPLICATION_APPSECRET"
#define OVERSEAAPPSECRET  @"GLOBAL_CONFIG_APPLICATION_OVERSEA_APPSECRET"
#define HOSTAPI           @"GLOBAL_CONFIG_APPLICATION_HOSTAPI"
#define OVERSEAHOSTAPI    @"GLOBAL_CONFIG_APPLICATION_OVERSEA_HOSTAPI"
#define MANAGERTOKEN      @"GLOBAL_AUTH_MANAGER_TOKEN"
#define USERTOKEN         @"GLOBAL_AUTH_USER_TOKEN"
#define SUBACCOUNTTOKEN   @"GLOBAL_AUTH_SUBACCOUNT_TOKEN"
#define OPENID            @"GLOBAL_AUTH_OPENID"
#define EXPIRETIME        @"GLOBAL_AUTH_EXPIRE_TIME"
#define CURRENTMODE       @"GLOBAL_JOINT_CURRENT_MODE"
#define DEFAULTHOSTAPIOVS @"https://openapi.easy4ip.com:443"
static NSMutableDictionary *serialCachePool;
static NSMutableDictionary *deviceInfosPool;
@implementation LCApplicationDataManager
+ (NSString *)appId {
    return (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:OVERSEAAPPID];
}
+ (NSString *)appSecret {
    return (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:OVERSEAAPPSECRET];
}
+ (NSString *)hostApi {
    NSString *hostApi = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:OVERSEAHOSTAPI];
    hostApi = ((hostApi == nil || [hostApi isNull]) ? (DEFAULTHOSTAPIOVS) : hostApi);
    return hostApi;
}
+ (void)setAppIdWith:(NSString *)appId {
    [[NSUserDefaults standardUserDefaults] setObject:appId forKey:OVERSEAAPPID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setAppSecretWith:(NSString *)appSecret {
    [[NSUserDefaults standardUserDefaults] setObject:appSecret forKey:OVERSEAAPPSECRET];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setHostApiWith:(NSString *)api {
    [[NSUserDefaults standardUserDefaults] setObject:api forKey:OVERSEAHOSTAPI];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setManagerToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:MANAGERTOKEN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USERTOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setUserToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:USERTOKEN];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:MANAGERTOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setSubAccountToken:(NSString *)token {
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:SUBACCOUNTTOKEN];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setOpenid:(NSString *)openid {
    [[NSUserDefaults standardUserDefaults] setObject:openid forKey:OPENID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (void)setExpireTime:(NSInteger)second {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:second];
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:EXPIRETIME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (BOOL)isVaildToken {
    NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:EXPIRETIME];
    NSInteger now = [date timeIntervalSinceNow];
    if (now > 300) {
        return YES;
    }
    return NO;
}
+ (NSString *)token {
    NSString *managerToken = [[NSUserDefaults standardUserDefaults] objectForKey:MANAGERTOKEN];
    NSString *userToken = [[NSUserDefaults standardUserDefaults] objectForKey:USERTOKEN];
    return managerToken ? managerToken : userToken;
}
+ (NSString *)subAccountToken {
    return (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:SUBACCOUNTTOKEN];
}
+ (NSString *)openid {
    return (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:OPENID];
}
+ (NSString *)SDKHost {
    NSString *host = [NSString stringWithFormat:@"%@/openapi",[LCApplicationDataManager hostApi]];
    NSString *textStr = [[[[[[host componentsSeparatedByString:@"//"] objectAtIndex:1] componentsSeparatedByString:@"/"] objectAtIndex:0] componentsSeparatedByString:@":"] objectAtIndex:0];
    return textStr;
}
+ (NSInteger)SDKPort {
    NSString *host = [LCApplicationDataManager hostApi];
    NSString *regex = @"(:[0-9]{1,4})";
    NSRegularExpression *regular = [[NSRegularExpression alloc]initWithPattern:regex options:NSRegularExpressionDotMatchesLineSeparators error:nil];
    NSArray *resultArray = [regular matchesInString:host options:0 range:NSMakeRange(0, host.length)];
    if (resultArray.count == 0) {
        return [host containsString:@"https"]?443:80;
    }
    NSTextCheckingResult *result = resultArray[0];
    NSString *textStr = [[host substringWithRange:result.range] substringFromIndex:1];
    return [textStr integerValue];
}
+ (BOOL)isManagerMode {
    NSString *managerToken = [[NSUserDefaults standardUserDefaults] objectForKey:MANAGERTOKEN];
    return managerToken ? YES : NO;
}
+ (void)setCurrentMode:(LCJointModeType)type {
    [[NSUserDefaults standardUserDefaults] setInteger:type forKey:CURRENTMODE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
#define kRandomLength 32 
+ (NSString *)serial{
    NSString *string = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    NSString *timeStr = [NSString stringWithFormat:@"%.0f", time];
    static const NSString *kRandomAlphabet = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:kRandomLength];
    for (int i = 0; i < kRandomLength; i++) {
        [randomString appendFormat:@"%C", [kRandomAlphabet characterAtIndex:arc4random_uniform((u_int32_t)[kRandomAlphabet length])]];
    }
    NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[string componentsSeparatedByString:@"-"]];
    [array removeLastObject];
    [array addObject:timeStr];
    [array addObject:randomString];
    NSString *serialStr = [array componentsJoinedByString:@"-"];
    if ([serialCachePool objectForKey:serialStr]) {
        return [LCApplicationDataManager serial];
    }
    NSRecursiveLock *lock = [[NSRecursiveLock alloc] init];
    [lock lock];
    for (NSString *key in serialCachePool) {
        NSDate *date = (NSDate *)[serialCachePool objectForKey:key];
        NSTimeInterval timeInterval = [date timeIntervalSinceNow];
        timeInterval = -timeInterval;
        if (timeInterval > 5) {
            [serialCachePool removeObjectForKey:key];
        }
    }
    [serialCachePool setObject:[NSDate new] forKey:serialStr];
    [lock unlock];
    return serialStr;
}
+ (NSString *)getCurrentTimeStamp {
    return [NSString stringWithFormat:@"%.0f", [[NSDate new] timeIntervalSince1970]];
}
+ (NSMutableDictionary *)defaultPool {
    if (!serialCachePool) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            serialCachePool = [NSMutableDictionary dictionary];
        });
    }
    return serialCachePool;
}
+ (NSMutableDictionary *)defaultDeviceInfosPool {
    if (!deviceInfosPool) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            deviceInfosPool = [NSMutableDictionary dictionary];
        });
    }
    return deviceInfosPool;
}
@end
