#import "LCPTZControlView.h"
#import <Masonry/Masonry.h>
#import "LCBaseDefine.h"
@interface LCPTZControlView ()
@property (strong, nonatomic) UIView *handleArea;
@property (strong, nonatomic) UIView *dragView;
@property (strong, nonatomic) UIButton *closeBtn;
@end
@implementation LCPTZControlView
-(instancetype)initWithDirection:(LCPTZControlSupportDirection)direction{
    if (self = [super initWithFrame:CGRectZero]) {
        [self setupPTZViewWithDirection:direction];
    }
    return self;
}
-(void)setupPTZViewWithDirection:(LCPTZControlSupportDirection)direction{
    _panel = [[LCPTZPanel alloc] initWithFrame:CGRectMake(0, 0, 100, 100) style:direction==LCPTZControlSupportFour?LCPTZPanelStyle4Direction:LCPTZPanelStyle8Direction];
    [self addSubview:_panel];
    weakSelf(self);
    [_panel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(weakself).multipliedBy(0.4);
        make.width.mas_equalTo(weakself.panel.mas_height);
        make.top.mas_equalTo(weakself).offset(120);
        make.centerX.mas_equalTo(weakself.mas_centerX);
    }];
}
-(void)layoutSubviews{
    [super layoutSubviews];
}
-(void)closeBtnClicked:(UIButton *)close{
    if (self.close) {
        self.close();
    }
}
@end
