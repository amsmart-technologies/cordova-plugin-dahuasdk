//
//  Copyright © 2020 dahua. All rights reserved.
//  设备详情一级列表

#import "LCDeviceSettingPersenter.h"
#import "LCBasicViewController.h"
typedef void(^FinishBlock)(void);
NS_ASSUME_NONNULL_BEGIN

@interface LCDeviceSettingViewController : LCBasicViewController


@property (nonatomic) LCDeviceSettingStyle style;

@property (nonatomic, copy) FinishBlock finishBlock;


@end

NS_ASSUME_NONNULL_END
