#import <Foundation/Foundation.h>
@interface NSObject (JSON)
- (NSString *)dh_jsonString;
@end
@interface NSString (JSON)
- (id)dh_jsonObject;
- (NSDictionary *)dh_jsonDictionary;
- (NSArray *)dh_jsonArray;
@end
