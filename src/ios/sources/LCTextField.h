#import <UIKit/UIKit.h>
typedef void (^LCTextFieldMsgBlock)(NSString *checkingMsg);
typedef void (^LCTextFieldInputBlock)(NSString *result);
typedef BOOL (^LCTextFieldInputingBlock)(NSString *result,NSString *replace);
typedef void (^LCTextFieldTextChangedBlock)(NSString *text);
typedef enum : NSUInteger {
    LCTextFieldMenuTypeCanNone = 0,
    LCTextFieldMenuTypeCanCopy = 1 << 0,
    LCTextFieldMenuTypeCanPaste = 1 << 1,
    LCTextFieldMenuTypeCanSelect = 1 << 2,
    LCTextFieldMenuTypeCanSelectAll = 1 << 3
} LCTextFieldMenuType;
NS_CLASS_AVAILABLE_IOS(7_0) @interface LCTextField : UITextField <UITextFieldDelegate>
@property (nonatomic, assign) BOOL isRuled;            
@property (nonatomic, assign) BOOL disableEmoji;       
@property (nonatomic, assign) BOOL disableSpace;       
@property (nonatomic, assign) BOOL disableSpecialChar; 
@property (nonatomic, assign) BOOL disableUnderRegEx;
@property (nonatomic, assign) NSUInteger chineseStrLength;
@property (nonatomic, assign) LCTextFieldMenuType menuType;
@property (nonatomic, assign) IBInspectable BOOL isSetOneTimeCode;
@property (nonatomic, assign) BOOL isMenuHidden;
@property (nonatomic, assign) CGFloat textOffsetWhenInAlignmentCenter;
@property (nonatomic, assign) BOOL isChecking;          
@property (nonatomic, assign) BOOL checkOnTextChanged;  
@property (nonatomic, assign) BOOL checkOnResign;       
@property (nonatomic, assign) unsigned int strLengthCustom;  
@property (nonatomic, copy) LCTextFieldTextChangedBlock textChanged;
@property (copy, nonatomic) LCTextFieldInputingBlock inputingBlock;
- (void)lc_setInputRuleWithRegEx:(NSString *)stringRegEx andInputLength:(unsigned int)inputLength; 
- (void)lc_addRegExToCheckTextField:(NSString *)strRegEx withMsgBlock:(LCTextFieldMsgBlock)msgBlock; 
- (void)lc_addConfirmValidationToCheckTextField:(LCTextField *)targetTextField withMsgBlock:(LCTextFieldMsgBlock)msgBlock; 
- (void)lc_setBlankMsgBlock:(LCTextFieldMsgBlock)block; 
- (BOOL)lc_checkIt; 
+ (instancetype)lcTextFieldWithResult:(void(^)(NSString *result))result;
@end
