#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCError : NSObject
@property (nonatomic, strong)   NSString * errorCode;
@property (nonatomic, strong)   NSString * errorMessage;
@property (nonatomic, strong, readonly) NSDictionary *errorInfo;
- (id)init;
+ (instancetype)errorWithCode:(NSString *)errorCode errorMessage:(nullable NSString *)errorMessage errorInfo:(nullable NSDictionary *)userInfo;
+ (BOOL)isAuthenticationFailed:(NSInteger)errorCode;
@end
NS_ASSUME_NONNULL_END
