#import <UIKit/UIKit.h>
#import "VPVideoDefines.h"
typedef NS_ENUM(NSInteger, LCPTZPanelStyle)
{
    LCPTZPanelStyle4Direction = 0,
    LCPTZPanelStyle8Direction,
};
typedef void(^operateResult)(VPDirection direction,double scale,NSTimeInterval timeInterval);
@interface LCPTZPanel : UIView
- (instancetype)initWithFrame:(CGRect)frame style:(LCPTZPanelStyle)ptzStyle;
@property(nonatomic)        operateResult   resultBlock;
-(void)configLandscapeUI;
- (void)updatePanelBackguoundImageWithPT1:(BOOL)hasPT1;
@end
