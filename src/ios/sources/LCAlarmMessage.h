#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCAlarms : NSObject
@property (strong, nonatomic) NSString *alarmId;
@property (strong, nonatomic) NSString *channelId;
@property (strong, nonatomic) NSString *deviceId;
@property (strong, nonatomic) NSString *localDate;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSArray<NSString *> *picurlArray;
@property (strong, nonatomic) NSString *thumbUrl;
@property (nonatomic) NSInteger time;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *type;
@end
@interface LCAlarmMessage : NSObject
@property (nonatomic) NSInteger nextAlarmId;
@property (nonatomic) NSInteger count;
@property (strong, nonatomic) NSArray<LCAlarms *> *alarms;
@end
NS_ASSUME_NONNULL_END
