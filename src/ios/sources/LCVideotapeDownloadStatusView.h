#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef void(^DownloadStatusViewClickBlock)(void);
@interface LCVideotapeDownloadStatusView : UIView
@property (nonatomic) NSInteger size;
@property (nonatomic) NSInteger recieve;
@property (nonatomic) NSInteger totalRevieve;
@property (copy,nonatomic) DownloadStatusViewClickBlock cancleBlock;
+(instancetype)showDownloadStatusInView:(UIView *)view Size:(NSInteger)size;
- (void)dismiss;
@end
NS_ASSUME_NONNULL_END
