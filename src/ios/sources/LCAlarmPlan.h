#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCAlarmPlanRule : NSObject
@property (nonatomic) BOOL enable;
@property (strong, nonatomic) NSString *beginTime;
@property (strong, nonatomic) NSString *period;
@property (strong, nonatomic) NSString *endTime;
@end
@interface LCAlarmPlan : NSObject
@property (strong, nonatomic) NSString *channelId;
@property (strong, nonatomic) NSArray <LCAlarmPlanRule *> *rules;
@end
NS_ASSUME_NONNULL_END
