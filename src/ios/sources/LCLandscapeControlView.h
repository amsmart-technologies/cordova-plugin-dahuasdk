#import <UIKit/UIKit.h>
#import "LCUIKit.h"
#import "LCLivePreviewPresenter.h"
#import "LCLivePreviewPresenter+Control.h"
#import "LCVideotapePlayProcessView.h"
NS_ASSUME_NONNULL_BEGIN
@protocol LCLandscapeControlViewDelegate <NSObject>
- (NSString *)currentTitle;
- (NSMutableArray *)currentButtonItem;
- (void)changePlayOffset:(NSInteger)offsetTime;
- (void)naviBackClick:(LCButton *)btn;
- (void)lockFullScreen:(LCButton *)btn;
@end
@interface LCLandscapeControlView : UIView
@property (strong, nonatomic) LCLivePreviewPresenter *presenter;
@property (weak, nonatomic) id<LCLandscapeControlViewDelegate> delegate;
@property (nonatomic) BOOL isNeedProcess;
@property (strong, nonatomic) LCVideotapePlayProcessView * processView;
-(void)setStartDate:(NSDate *)startDate EndDate:(NSDate *)endDate;
@property (strong,nonatomic)NSDate * currentDate;
@end
NS_ASSUME_NONNULL_END
