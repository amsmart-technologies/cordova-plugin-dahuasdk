#import "LCQRCode.h"
@implementation LCQRCode
- (void)pharseQRCode:(NSString *)qrCode {
	NSString *vaildString = [qrCode stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]] ;
	NSRange snRange = [vaildString rangeOfString:@"SN:"];
	if (snRange.location != NSNotFound) {
		self.deviceSN = [self getValueByKey:@"SN:" validQRCode:vaildString isType:NO];
		self.deviceType = [self getValueByKey:@"DT:" validQRCode:vaildString isType:YES];
		self.identifyingCode = [self getValueByKey:@"RD:" validQRCode:vaildString isType:NO];
		if (self.identifyingCode.length == 0) {
			self.identifyingCode = [self getValueByKey:@"RC:" validQRCode:vaildString isType:NO];
		}
		self.scCode = [self getValueByKey:@"SC:" validQRCode:vaildString isType:NO];
		self.ncCode = [self getValueByKey:@"NC:" validQRCode:vaildString isType:NO];
		self.imeiCode = [self getValueByKey:@"IMEI:" validQRCode:vaildString isType:NO];
        self.iotDeviceType = [self getValueByKey:@"PID:" validQRCode:vaildString isType:NO];
        self.cidCode = [self getValueByKey:@"CID:" validQRCode:vaildString isType:NO];
        self.typeCode = [self getValueByKey:@"TYPE:" validQRCode:vaildString isType:NO];
        self.uidCode = [self getValueByKey:@"UID:" validQRCode:vaildString isType:NO];
	} else {
		if ([vaildString rangeOfString:@":"].location != NSNotFound) {
			NSArray *strarray = [vaildString componentsSeparatedByString:@":"];
			if (strarray.count == 2) {
				self.deviceSN = strarray[0];
				self.deviceType = strarray[1];
			} else if (strarray.count == 3) {
				self.deviceSN = strarray[0];
				self.deviceType = strarray[1];
				self.identifyingCode = strarray[2];
			}
		} else if ([vaildString rangeOfString:@","].location != NSNotFound) {
			NSArray<NSString *> *list = [vaildString componentsSeparatedByString:@","];
			if(list.count >= 2){
				self.deviceType = list[0];
				self.deviceSN = list[1];
			}
		}
		else{
			self.deviceSN = vaildString;
		}
	}
	NSLog(@" %@:: SN:%@, Type:%@, IdentifyCode:%@, SCCdoe:%@, NCCode:%@, IMEI:%@", NSStringFromClass([self class]), self.deviceSN, self.deviceType, self.identifyingCode, self.scCode, self.ncCode, self.imeiCode);
}
- (NSString *)getValueByKey:(NSString *)key
				validQRCode:(NSString *)validQRCode
					 isType:(BOOL)isType {
	NSString *result;
	NSRange range = [validQRCode rangeOfString:key];
	if (range.location != NSNotFound) {
		result = [self getStringFromNSrange:range urlString:validQRCode isType:isType];
	}
	return result;
}
- (NSString*) getStringFromNSrange:(NSRange)range
                         urlString:(NSString*)urlString
                            isType:(BOOL)isType
{
    NSString *validString = [urlString substringFromIndex:range.location+range.length];
    int nIndex = 0;
    for (int i = 0; i < [validString length]; i++)
    {
        NSRange r ;
        r.length = 1;
        r.location = i;
        NSString* c = [validString substringWithRange:r];
        if (isType)
        {
            if ([c rangeOfString:@"^[a-zA-Z0-9-\\/\\\\ ]$" options:NSRegularExpressionSearch].location == NSNotFound)
            {
                nIndex = i;
                break;
            }
        }
        else
        {
            if ([c rangeOfString:@"^([a-z]|[A-Z]|[0-9])$" options:NSRegularExpressionSearch].location == NSNotFound)
            {
                nIndex = i;
                break;
            }
        }
    }
    return [validString substringToIndex:nIndex];
}
@end
