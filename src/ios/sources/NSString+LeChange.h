#import <UIKit/UIKit.h>
typedef enum {
    StringTypeNumber,
    StringTypeLetter,
    StringTypeLetterAndNumber
} StringType;
@interface NSString(LeChange)
@property (nonatomic) BOOL isAbsent; 
- (NSString *)lc_EncryptToServerWithPwd:(NSString *)password;
- (NSString *)lc_DecryptToServerWithPwd:(NSString *)password;
- (id)lc_jsonValue;
- (BOOL)lc_isStringType:(StringType)type;
+ (NSString*)lc_dictionaryToJson:(NSDictionary *)dic;
+ (NSString *)lc_stringWithInt:(NSInteger)intNum;
- (CGSize)lc_sizeWithFont:(UIFont *)font size:(CGSize)size;
- (CGFloat)lc_widthWithFont:(UIFont *)font;
- (CGRect)lc_rectWithFont:(UIFont *)font;
+ (BOOL)lc_isEmpty:(NSString*)content;
- (NSString *)lc_base64String;
- (NSString *)lc_decodeBase64;
- (NSString *)lc_AES256Encrypt:(NSString *)key;
- (NSString *)lc_AES256Decrypt:(NSString *)key;
- (NSString *)lc_phoneNumberWithEncrypt;
- (BOOL)lc_matchTheFormat:(NSString*)format;
- (BOOL)lc_strictContainString:(NSString*)matchString split:(NSString*)splitString;
- (BOOL)lc_isValidIphoneNum;
- (BOOL)lc_isValidEmail;
- (BOOL)lc_isAllNum;
@end
