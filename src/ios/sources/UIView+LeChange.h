#import <UIKit/UIKit.h>
@interface UIView (LeChange)
@property (nonatomic) CGFloat dh_width;
@property (nonatomic) CGFloat dh_height;
@property (nonatomic) CGFloat dh_x;
@property (nonatomic) CGFloat dh_y;
@property (nonatomic) CGFloat dh_top;
@property (nonatomic) CGFloat dh_right;
@property (nonatomic) CGFloat dh_bottom;
@property (nonatomic) CGFloat dh_left;
@property (nonatomic) CGSize dh_size;
@property (nonatomic) CGFloat dh_centerX;
@property (nonatomic) CGFloat dh_centerY;
@property (nonatomic) CGFloat dh_cornerRadius;
- (void)lc_removeAllSubview;
- (void)lc_setBoraderWith:(CGFloat )borderWidth andColor:(UIColor *)borderColor;
- (void)lc_addIconBtnArray:(NSArray *)btnArray;
- (void)lc_addSubviewToCeneter:(UIView *)view;
- (void)lc_addBorderWidth:(CGFloat)width color:(UIColor*)color radius:(CGFloat)radius;
- (void)lc_setRound;
- (void)lc_setRadius:(CGFloat)radius;
- (void)lc_animationWithPath:(NSString *)keyPath transform:(CATransform3D)transform duration:(CFTimeInterval)duration delegate:(id)delegate;
- (void)lc_shakeViewWithRepeatCount:(NSInteger)repeatCount;
@end
