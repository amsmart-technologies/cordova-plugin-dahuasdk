#import "DHUserManager.h"

@implementation DHUserManager

static DHUserManager *userManager = nil;
+ (DHUserManager *)shareInstance{
    if(userManager == nil){
        userManager = [[DHUserManager alloc]init];
        [userManager getUserConfigFile];
    }
    return userManager;
}

- (id)init{
    if (self = [super init]){}
    return self;
}

- (void)getUserConfigFile{
    NSData *desData = [[NSUserDefaults standardUserDefaults] dataForKey:@"ssid_config"];
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:desData];
    _mSSIDMutableArray = dic[@"mSSIDMutableArray"];
}

- (void)saveUserConfigFile{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc] initWithCapacity:0];
    [dic setValue:_mSSIDMutableArray forKey:@"mSSIDMutableArray"];
    NSData *config2Data = [NSKeyedArchiver archivedDataWithRootObject:dic];
    [[NSUserDefaults standardUserDefaults] setObject:config2Data forKey:@"ssid_config"];
}

- (BOOL)ssidIsSaved:(NSString *)ssid{
	if (_mSSIDMutableArray.count == 0){
		return NO;
	}
	BOOL isSaved = NO;
	for (NSDictionary *dict in _mSSIDMutableArray){
		NSString *mSSID = dict[@"mSSID"];
		if ([mSSID isEqualToString:ssid]){
			isSaved = YES;
			break;
		}
	}
	return isSaved;
}

- (NSString *)ssidPwdBy:(NSString *)ssid {
	if (_mSSIDMutableArray.count == 0) {
		return nil;
	}
	NSString *ssidPwd = nil;
	for (NSDictionary *dict in _mSSIDMutableArray){
		NSString *mSSID = dict[@"mSSID"];
		if ([mSSID isEqualToString:ssid]){
			ssidPwd = dict[@"mSSIDPWD"];
			break;
		}
	}
	return ssidPwd;
}

- (void)addSSID:(NSString *)ssid ssidPwd:(NSString *)ssidPwd{
	if (ssid == nil) {
		ssid = @"";
	}
	if (ssidPwd == nil) {
		ssidPwd = @"";
	}
	NSDictionary *dict = @{@"mSSID":ssid, @"mSSIDPWD":ssidPwd};
	if (_mSSIDMutableArray == nil){
		_mSSIDMutableArray = [NSMutableArray arrayWithCapacity:3];
	}
	if ([self ssidPwdBy:ssid]) {
		[self removeSSID:ssid];
	}
	[_mSSIDMutableArray addObject:dict];
	[self saveUserConfigFile];
}

- (void)removeSSID:(NSString *)ssid{
	if (_mSSIDMutableArray.count == 0){
		return;
	}
	NSDictionary *ssidDict = [NSDictionary dictionary];
	for (NSDictionary *dict in _mSSIDMutableArray){
		NSString *mSSID = dict[@"mSSID"];
		if ([mSSID isEqualToString:ssid]){
			ssidDict = dict;
			break;
		}
	}
	[_mSSIDMutableArray removeObject:ssidDict];
	[self saveUserConfigFile];
}

- (void)clearCachedSSID {
	[_mSSIDMutableArray removeAllObjects];
	[self saveUserConfigFile];
}

@end

