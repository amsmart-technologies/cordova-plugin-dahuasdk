#import <UIKit/UIKit.h>
#import "LCVideotapePlayProcessView.h"
typedef enum : NSUInteger {
    LCVideoControlBlackStyle,
    LCVideoControlLightStyle
} LCVideoControlStyle;
NS_ASSUME_NONNULL_BEGIN
@interface LCVideoControlView : UIView
@property (copy, nonatomic) NSMutableArray<UIView *> *items;
@property (nonatomic) LCVideoControlStyle style;
@property (nonatomic) BOOL isNeedProcess;
@property (strong, nonatomic) LCVideotapePlayProcessView *processView;
@end
NS_ASSUME_NONNULL_END
