#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface DHDateFormatter : NSDateFormatter
- (nullable instancetype)initWithGregorianCalendar;
- (nullable instancetype)initWithCalendarIdentifier:(NSCalendarIdentifier)ident;
@end
NS_ASSUME_NONNULL_END
