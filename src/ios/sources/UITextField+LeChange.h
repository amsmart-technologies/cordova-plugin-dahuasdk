#import <UIKit/UIKit.h>
@interface UITextField (LeChange)
@property (nonatomic, assign) BOOL customClearButton;
- (NSRange) selectedRange;
- (void) setSelectedRange:(NSRange) range;
- (void)setSecureTextEntryWithBtn:(id)sender;
@end
