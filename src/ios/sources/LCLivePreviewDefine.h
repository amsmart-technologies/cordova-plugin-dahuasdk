#ifndef LCLivePreviewDefine_h
#define LCLivePreviewDefine_h
typedef enum : NSUInteger {
    LCLiveDefinitionSD,
    LCLiveDefinitionHD
} LCLiveDefinition;
typedef enum : NSInteger {
    LCVideotapeDownloadStatusFail = 0,
    LCVideotapeDownloadStatusBegin = 1,
    LCVideotapeDownloadStatusEnd = 2,
    LCVideotapeDownloadStatusCancle = 3,
    LCVideotapeDownloadStatusSuspend = 4,
    LCVideotapeDownloadStatusTimeout = 5,
    LCVideotapeDownloadStatusKeyError = 6,
    LCVideotapeDownloadStatusPartDownload = 7
} LCVideotapeDownloadState;
typedef NS_ENUM (NSInteger, HLSResultCode) {
    HLS_DOWNLOAD_FAILD = 0,
    HLS_DOWNLOAD_BEGIN = 1,
    HLS_DOWNLOAD_END = 2,
    HLS_SEEK_SUCCESS = 3,
    HLS_SEEK_FAILD = 4,
    HLS_ABORT_DONE = 5,
    HLS_RESUME_DONE = 6,
    HLS_DOWNLOAD_TIMEOUT = 7,
    HLS_KEY_ERROR = 11
};
#endif