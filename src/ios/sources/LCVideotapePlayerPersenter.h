#import "LCUIKit.h"
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_PlayWindow.h>
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_AudioTalk.h>
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_EventListener.h>
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_Define.h>
#import "UIDevice+LeChange.h"
#import "LCLivePreviewDefine.h"
#import "LCDeviceVideotapePlayManager.h"
#import "LCBasicPresenter.h"
NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    LCVideotapePlayerControlPlay,
    LCVideotapePlayerControlClarity,
    LCVideotapePlayerControlTimes,
    LCVideotapePlayerControlVoice,
    LCVideotapePlayerControlFullScreen,
    LCVideotapePlayerControlSnap,
    LCVideotapePlayerControlPVR,
    LCVideotapePlayerControlDownload
} LCVideotapePlayerControlType;
@interface LCVideotapePlayerPersenter : LCBasicPresenter
@property (copy, nonatomic) LCOpenSDK_PlayWindow * playWindow;
@property (strong, nonatomic) UIImageView *loadImageview;
@property (strong, nonatomic) LCDeviceVideotapePlayManager * videoManager;
@property (nonatomic) NSInteger  sssdate;
@property (strong, nonatomic) LCButton *bigPlayBtn;
@property (strong, nonatomic) LCButton * errorBtn;
@property (strong, nonatomic) UILabel * errorMsgLab;
-(NSMutableArray *)getMiddleControlItems;
-(NSMutableArray *)getBottomControlItems;
- (LCButton *)getItemWithType:(LCVideotapePlayerControlType)type;
-(void)stopDownload;
-(void)onActive:(id)sender;
-(void)onResignActive:(id)sender;
-(void)showVideoLoadImage;
-(void)hideVideoLoadImage;
-(void)showPSKAlert;
-(void)downLoad;
-(void)showPlayBtn;
-(void)hidePlayBtn;
-(void)showErrorBtn;
-(void)hideErrorBtn;
-(void)configBigPlay;
-(void)loadStatusView;
@end
NS_ASSUME_NONNULL_END
