#import "LCDeviceSettingViewController.h"
#import "LCBaseDefine.h"
#import <KVOController/KVOController.h>
#import "NSString+Dahua.h"

@interface LCDeviceSettingViewController ()
@property (strong, nonatomic) UITableView *listView;
@property (strong, nonatomic) LCDeviceSettingPersenter *persenter;
@end

@implementation LCDeviceSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.persenter.style = self.style;
    if (self.style == LCDeviceSettingStyleDeviceNameEdit) {
        UIGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTap:)];
        [self.view addGestureRecognizer:tap];
    }
    [self setupView];
}

- (void)viewTap:(UITapGestureRecognizer *)tap{
    [super viewTap:tap];
    self.persenter.endEdit = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.persenter stopCheckUpdate];
    if (self.style == LCDeviceSettingStyleMainPage) {
        self.finishBlock();
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.listView reloadData];
    weakSelf(self);
}

- (LCDeviceSettingPersenter *)persenter {
    if (!_persenter) {
        _persenter = [LCDeviceSettingPersenter new];
        _persenter.container = self;
    }
    return _persenter;
}

- (void)setupView {
    weakSelf(self);
    UIView * topView = [UIView new];
    self.view.backgroundColor = [UIColor dhcolor_c7];
    [self.view addSubview:topView];
    topView.backgroundColor = [UIColor dhcolor_c7];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(kStatusBarHeight);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(kNavBarHeight);
    }];
    UILabel * titleLab = [UILabel new];
    NSString *titleStr = self.persenter.manager.currentDevice.name;
    if (self.persenter.manager.currentChannelInfo != nil) {
        titleStr = self.persenter.manager.currentChannelInfo.channelName;
    }
    titleLab.text = titleStr;
    titleLab.textColor = [UIColor dhcolor_c51];
    [topView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(topView.mas_centerY);
        make.centerX.mas_equalTo(topView.mas_centerX);
    }];
    LCButton * back = [LCButton lcButtonWithType:LCButtonTypeCustom];
    [topView addSubview:back];
    [back setTintColor:[UIColor dhcolor_c51]];
    [back setImage:LC_IMAGENAMED(@"nav_back") forState:UIControlStateNormal];
    [back mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topView.mas_left).offset(15);
        make.centerY.mas_equalTo(topView.mas_centerY);
    }];
    back.touchUpInsideblock = ^(LCButton * _Nonnull btn) {
        [self dismissViewControllerAnimated:true completion:nil];
    };
    if (self.style == LCDeviceSettingStyleDeviceNameEdit) {
        LCButton * submit = [LCButton lcButtonWithType:LCButtonTypeCustom];
        [topView addSubview:submit];
        [submit setTintColor:[UIColor dhcolor_c51]];
        [submit setImage:LC_IMAGENAMED(@"setting_icon_check") forState:UIControlStateNormal];
        [submit mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(topView.mas_right).offset(-15);
            make.centerY.mas_equalTo(topView.mas_centerY);
        }];
        submit.touchUpInsideblock = ^(LCButton * _Nonnull btn) {
            [self.view endEditing:YES];
            [weakself.persenter modifyDevice];
        };
    }
    self.listView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) style:UITableViewStyleGrouped];
    self.listView.backgroundColor = [UIColor dhcolor_c7];
    self.listView.dataSource = self.persenter;
    self.listView.tag = 999;
    self.listView.delegate = self.persenter;
    [self.listView registerClass:NSClassFromString(@"LCDeviceSwitchCell") forCellReuseIdentifier:@"LCDeviceSwitchCell"];
    [self.listView registerClass:NSClassFromString(@"UITableViewCell") forCellReuseIdentifier:@"UITableViewCell"];
    [self.listView registerNib:[UINib nibWithNibName:@"LCDeviceSettingArrowCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LCDeviceSettingArrowCell"];
    [self.listView registerNib:[UINib nibWithNibName:@"LCDeviceSettingSubtitleCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LCDeviceSettingSubtitleCell"];
    [self.view addSubview:self.listView];
    self.listView.rowHeight = UITableViewAutomaticDimension;
    self.listView.estimatedRowHeight = 100.0f;
    [self.listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(kNavBarAndStatusBarHeight);
        make.left.right.mas_equalTo(self.view);
        make.bottom.mas_equalTo(self.view);
    }];
    [self.listView.KVOController observe:self.persenter keyPath:@"needReload" options:NSKeyValueObservingOptionNew block:^(id  _Nullable observer, id  _Nonnull object, NSDictionary<NSString *,id> * _Nonnull change) {
        [weakself.listView reloadData];
    }];
    
    LCButton * deleteBtn = [LCButton lcButtonWithType:LCButtonTypeMinor];
    [self.view addSubview:deleteBtn];
    [deleteBtn setTitle:@"mobile_common_delete".lc_T forState:UIControlStateNormal];
    [deleteBtn setTitleColor:[UIColor dhcolor_c30] forState:UIControlStateNormal];
    [deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self.view);
        make.height.mas_equalTo(45);
        make.bottom.mas_equalTo(self.view.mas_bottom).offset(-20);
    }];
    if (self.style == LCDeviceSettingStyleMainPage) {
        if (self.persenter.manager.isbindFromLeChange) {
            deleteBtn.hidden = YES;
        } else {
            if (self.persenter.manager.currentDevice.channels.count > 1 && self.persenter.manager.currentChannelIndex > -1) {
                deleteBtn.hidden = YES;
            } else {
                deleteBtn.hidden = NO;
            }
        }
    } else {
        deleteBtn.hidden = YES;
    }
    deleteBtn.touchUpInsideblock = ^(LCButton * _Nonnull btn) {
        [LCOCAlertView lc_ShowAlertWith:@"Alert_Title_Notice".lc_T Detail:@"setting_device_delete_alert".lc_T ConfirmTitle:@"Alert_Title_Button_Confirm".lc_T CancleTitle:@"Alert_Title_Button_Cancle".lc_T Handle:^(BOOL isConfirmSelected) {
            if (isConfirmSelected) {
                [weakself.persenter deleteDevice];
            }
        }];
        
    };
}

- (void)dealloc {
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (@available(iOS 13.0, *)) {
        return UIStatusBarStyleDarkContent;
    } else {
        return UIStatusBarStyleDefault;
    }
}

@end
