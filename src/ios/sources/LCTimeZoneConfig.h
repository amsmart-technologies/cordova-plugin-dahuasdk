#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCTimeZoneConfig : NSObject
@property (strong, nonatomic) NSString *deviceId;
@property (strong, nonatomic) NSString *areaIndex;
@property (strong, nonatomic) NSString *timeZone;
@property (strong, nonatomic) NSString *beginSunTime;
@property (strong, nonatomic) NSString *endSunTime;
@end
NS_ASSUME_NONNULL_END