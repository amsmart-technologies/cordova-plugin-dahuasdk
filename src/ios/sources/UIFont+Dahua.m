#import "UIFont+Dahua.h"
#import "DHPubDefine.h"
@implementation UIFont (Dahua)
+ (UIFont *)dhFont_f0Heavy {
    return [UIFont systemFontOfSize:20 weight:UIFontWeightHeavy];
}
+ (UIFont *)dhFont_f1Bold {
    return [UIFont boldSystemFontOfSize:17];
}
+ (UIFont *)dhFont_f1 {
    return [UIFont systemFontOfSize:17];
}
+ (UIFont *)dhFont_f2 {
	return [UIFont systemFontOfSize:15];
}
+ (UIFont *)dhFont_f2Bold {
	return [UIFont boldSystemFontOfSize:15];
}
+ (UIFont *)dhFont_f3 {
	return [UIFont systemFontOfSize:13];
}
+ (UIFont *)dhFont_f4 {
	return [UIFont systemFontOfSize:10];
}
+ (UIFont *)dhFont_f5 {
    return [UIFont systemFontOfSize:9];
}
+ (UIFont *)dhFont_t0
{
    return [UIFont systemFontOfSize:35];
}
+ (UIFont *)dhFont_t1
{
    return [UIFont systemFontOfSize:20];
}
+ (UIFont *)dhFont_t1Bold {
	return [UIFont boldSystemFontOfSize:20];
}
+ (UIFont *)dhFont_t2
{
    return [UIFont systemFontOfSize:17];
}
+ (UIFont *)dhFont_t3
{
    return [UIFont systemFontOfSize:16];
}
+ (UIFont *)dhFont_t3Bold{
    return [UIFont boldSystemFontOfSize:16];
}
+ (UIFont *)dhFont_t2Bold{
    return [UIFont boldSystemFontOfSize:17];
}
+ (UIFont *)dhFont_t4
{
    return [UIFont systemFontOfSize:15];
}
+ (UIFont *)dhFont_t4Bold
{
    return [UIFont boldSystemFontOfSize:15];
}
+ (UIFont *)dhFont_t5
{
    return [UIFont systemFontOfSize:14];
}
+ (UIFont *)dhFont_t5Bold {
	return [UIFont boldSystemFontOfSize:14];
}
+ (UIFont *)dhFont_t6
{
    return [UIFont systemFontOfSize:12];
}
+ (UIFont *)dhFont_t7
{
    return [UIFont systemFontOfSize:9];
}
+ (UIFont *)dhFont_t8
{
    return [UIFont systemFontOfSize:11];
}
+ (UIFont *)lcFont_t0
{
    return [UIFont systemFontOfSize:FontSize(35)];
}
+ (UIFont *)lcFont_t1
{
    return [UIFont systemFontOfSize:FontSize(20)];
}
+ (UIFont *)lcFont_t1Bold {
    return [UIFont boldSystemFontOfSize:FontSize(20)];
}
+ (UIFont *)lcFont_t2
{
    return [UIFont systemFontOfSize:FontSize(17)];
}
+ (UIFont *)lcFont_t3
{
    return [UIFont systemFontOfSize:FontSize(16)];
}
+ (UIFont *)lcFont_t4
{
    return [UIFont systemFontOfSize:FontSize(15)];
}
+ (UIFont *)lcFont_t5
{
    return [UIFont systemFontOfSize:FontSize(14)];
}
+ (UIFont *)lcFont_t5Bold {
    return [UIFont boldSystemFontOfSize:FontSize(14)];
}
+ (UIFont *)lcFont_t6
{
    return [UIFont systemFontOfSize:FontSize(12)];
}
static inline CGFloat FontSize(CGFloat fontSize){
    if (SCREEN_WIDTH==320) {
        return fontSize-2;
    }else if (SCREEN_WIDTH==375){
        return fontSize;
    }else{
        return fontSize+2;
    }
}
@end
