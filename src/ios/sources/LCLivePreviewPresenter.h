#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_PlayRealWindow.h>
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_AudioTalk.h>
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_EventListener.h>
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_TalkerListener.h>
#import "LCLivePreviewViewController.h"
#import "UIDevice+LeChange.h"
#import "LCButton.h"
#import "LCBasicPresenter.h"
#import "LCDeviceVideoManager.h"
NS_ASSUME_NONNULL_BEGIN
typedef void (^ItemHandle)(LCButton * btn);
typedef enum : NSUInteger {
    LCLivePreviewControlPlay,
    LCLivePreviewControlClarity,
    LCLivePreviewControlVoice,
    LCLivePreviewControlFullScreen,
    LCLivePreviewControlPTZ,
    LCLivePreviewControlSnap,
    LCLivePreviewControlAudio,
    LCLivePreviewControlPVR,
    LCLivePreviewControlAlarm,
    LCLivePreviewControlLight,
    LCLivePreviewCallAnswer,
    LCLivePreviewCallRefuse
} LCLivePreviewControlType;
@interface LCLivePreviewControlItem : NSObject
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSString *imageNameSelect;
@property (nonatomic) LCLivePreviewControlType type;
@property (nonatomic) NSUInteger weight;
@property (copy, nonatomic) ItemHandle handle;
@property (strong, nonatomic) LCButton * btn;
@property (strong, nonatomic) id userInfo;
@end
@interface LCLivePreviewPresenter : LCBasicPresenter
@property (strong, nonatomic) LCOpenSDK_PlayRealWindow * playWindow;
@property (strong, nonatomic) LCOpenSDK_AudioTalk *talker;
@property (strong, nonatomic) NSMutableArray *videotapeList;
@property (weak, nonatomic) LCDeviceVideoManager *videoManager;
@property (strong, nonatomic) LCButton *bigPlayBtn;
@property (strong, nonatomic) UIImageView *loadImageview;
@property (weak, nonatomic) LCLivePreviewViewController *liveContainer;
@property (strong, nonatomic) LCButton * errorBtn;
@property (strong, nonatomic) UILabel * errorMsgLab;
@property (strong, nonatomic) UIImageView *defaultImageView;
-(NSMutableArray *)getMiddleControlItems;
-(NSMutableArray *)getBottomControlItems;
-(NSMutableArray *)getBottomCallItems;
-(void)ptzControlWith:(NSString *)direction Duration:(NSTimeInterval)duration;
-(UIView *)getVideotapeView;
-(void)showVideoLoadImage;
-(void)hideVideoLoadImage;
-(void)onActive:(id)sender;
-(void)onResignActive:(id)sender;
-(void)showPSKAlert;
-(void)showPlayBtn;
-(void)hidePlayBtn;
-(void)showErrorBtn;
-(void)hideErrorBtn;
-(void)configBigPlay;
-(void)openSettings;
@end
NS_ASSUME_NONNULL_END
