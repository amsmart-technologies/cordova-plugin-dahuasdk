#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface UIImage (Compress)
- (UIImage *)lc_thumbnailWithImageWithSize:(CGSize)asize;
@end
NS_ASSUME_NONNULL_END
