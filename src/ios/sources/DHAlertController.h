#import <UIKit/UIKit.h>
@class DHAlertController;
typedef void (^DHAlertControllerHandler)(NSInteger index);
@interface DHAlertController : UIAlertController
+ (DHAlertController *)showWithTitle:(NSString *)title
                             message:(NSString *)message
                   cancelButtonTitle:(NSString *)cancelButtonTitle
                    otherButtonTitle:(NSString *)otherButtonTitle
                             handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showWithTitle:(NSString *)title
                             message:(NSString *)message
                   cancelButtonTitle:(NSString *)cancelButtonTitle
                    otherButtonTitle:(NSString *)otherButtonTitle
                     tipsButtonTitle:(NSString *)tipsButtonTitle
               tipsButtonNormalImage:(NSString *)tipsButtonNormalImage
             tipsButtonSelectedImage:(NSString *)tipsButtonSelectedImage
                             handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showWithTitle:(NSString *)title
                             message:(NSString *)message
                   cancelButtonTitle:(NSString *)cancelButtonTitle
                   otherButtonTitles:(NSArray<NSString *> *)otherButtonTitles
                             handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showSheetWithCancelButtonTitle:(NSString *)cancelButtonTitle
                                    otherButtonTitles:(NSArray <NSString *> *)otherButtonTitles
                                              handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showSheetWithTitle:(NSString *)title
                                  message:(NSString *)message
                        cancelButtonTitle:(NSString *)cancelButtonTitle
                         otherButtonTitle:(NSString *)otherButtonTitle
                                  handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showWithTitle:(NSString *)title
                             message:(NSString *)message
                         buttonTitle:(NSString *)cancelButtonTitle
                             handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showWithTitle:(NSString *)title
                          customView:(UIView *)customView
                   cancelButtonTitle:(NSString *)cancelButtonTitle
                    otherButtonTitle:(NSString *)otherButtonTitle
                             handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showInViewController:(UIViewController *)vc
                                      title:(NSString *)title
                                    message:(NSString *)message
                          cancelButtonTitle:(NSString *)cancelButtonTitle
                           otherButtonTitle:(NSString *)otherButtonTitle
                                    handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showInViewController:(UIViewController *)vc
                                      title:(NSString *)title
                                    message:(NSString *)message
                          cancelButtonTitle:(NSString *)cancelButtonTitle
                          otherButtonTitles:(NSArray<NSString *> *)otherButtonTitles
                                    handler:(DHAlertControllerHandler)handler;
+ (DHAlertController *)showInViewController:(UIViewController *)vc
                                      title:(NSString *)title
                                 customView:(UIView *)customView
                          cancelButtonTitle:(NSString *)cancelButtonTitle
                           otherButtonTitle:(NSString *)otherButtonTitle
                                    handler:(DHAlertControllerHandler)handler;
+ (void)dismissAnimated:(BOOL)isAnimated;
+ (void)dismiss;
+ (BOOL)isDisplayed;
+ (UIViewController *)topPresentOrRootController;
@end
