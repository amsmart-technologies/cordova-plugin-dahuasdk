#import <UIKit/UIKit.h>
typedef NS_ENUM(NSUInteger, DraggingType) {
    DraggingTypeDisabled,
    DraggingTypeNormal,
    DraggingTypeRevert,
    DraggingTypePullOver,
    DraggingTypeAdsorb,
};
typedef enum : NSUInteger {
    LCPTZControlDirectionTop,
    LCPTZControlDirectionBottom,
    LCPTZControlDirectionLeft,
    LCPTZControlDirectionRight,
    LCPTZControlDirectionLeftTop,
    LCPTZControlDirectionLeftBottom,
    LCPTZControlDirectionRightTop,
    LCPTZControlDirectionRightBottom,
    LCPTZControlDirectionBlowUp,
    LCPTZControlDirectionshrink,
    LCPTZControlDirectionStop
} LCPTZControlDirection;
@protocol DraggingDelegate <NSObject>
-(void)draggingDidBegan:(UIView *)view;
-(void)draggingDidChanged:(UIView *)view;
-(void)draggingDidEnded:(UIView *)view;
-(void)draggingAngleChanged:(LCPTZControlDirection)direction View:(UIView *)view;
@end
@interface UIView (LCDraggable)<UIGestureRecognizerDelegate>
@property (weak, nonatomic) id<DraggingDelegate> delegate;
@property(nonatomic)DraggingType draggingType;
@property(nonatomic)BOOL draggingInBounds;
-(void)adsorbingAnimated:(BOOL)animated;
-(void)pullOverAnimated:(BOOL)animated;
-(void)revertAnimated:(BOOL)animated;
- (void)setCornerRadius:(CGFloat)value addRectCorners:(UIRectCorner)rectCorner;
@end
