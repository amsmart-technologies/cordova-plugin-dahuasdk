#import "LCNetworkRequestManager.h"
#import <AFNetworking/AFNetworking.h>
#import "LCApplicationDataManager.h"
#import "LCModel.h"
@interface LCNetworkRequestManager ()
@property (strong, nonatomic) AFURLSessionManager *sessionManager;
@end
@implementation LCNetworkRequestManager
+ (instancetype)manager {
    return [[LCNetworkRequestManager alloc] init];
}
- (void)lc_POST:(NSString *)URLString parameters:(id)parameters success:(requestSuccessBlock)success failure:(requestFailBlock)failure {
    [self post:URLString parameters:parameters success:success failure:failure];
}
- (void)post:(NSString *)URLString parameters:(id)parameters success:(requestSuccessBlock)success failure:(requestFailBlock)failure {
    NSDictionary *requestDic = [[LCRequestModel lc_WrapperNetworkRequestPackageWithParams:parameters] mj_keyValues];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDic != nil ? requestDic : [[NSMutableDictionary alloc] init] options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    URLString = [[LCApplicationDataManager.hostApi stringByAppendingString:@"/openapi"] stringByAppendingString:URLString];
    NSLog(@"LCNetworkRequestManager request url: %@", URLString);
    NSLog(@"LCNetworkRequestManager request data: \n%@", requestDic);
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:requestDic error:nil];
    request.timeoutInterval = 60;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"text/plain" forHTTPHeaderField:@"Accept"];
    [request setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSessionDataTask *task = [self.sessionManager dataTaskWithRequest:request uploadProgress:^(NSProgress *_Nonnull uploadProgress) {
        NSLog(@"");
    } downloadProgress:^(NSProgress *_Nonnull downloadProgress) {
        NSLog(@"");
    } completionHandler:^(NSURLResponse *_Nonnull response, id _Nullable responseObject, NSError *_Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                NSDictionary *res = [responseObject mj_JSONObject];
                NSLog(@"LCNetworkRequestManager now recv: %@", URLString);
                NSLog(@"LCNetworkRequestManager recv data: \n%@", res);
                LCResponseModel *respObjc = [LCResponseModel mj_objectWithKeyValues:res];
                if ([respObjc.result.code isEqualToString:@"0"]) {
                    success(respObjc.result.data);
                } else {
                    failure([LCError errorWithCode:respObjc.result.code errorMessage:respObjc.result.msg errorInfo:nil]);
                }
            } else {
                NSLog(@"LCNetworkRequestManager now recv error: %@", error);
                failure([LCError errorWithCode:@"9999" errorMessage:[NSString stringWithFormat:@"mobile_common_net_fail".lc_T, error.code] errorInfo:error.userInfo]);
            }
        });
    }];
    [task resume];
}
- (AFURLSessionManager *)sessionManager {
    if (!_sessionManager) {
        _sessionManager =  [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        _sessionManager.responseSerializer = [[AFCompoundResponseSerializer alloc] init];
    }
    return _sessionManager;
}
@end
