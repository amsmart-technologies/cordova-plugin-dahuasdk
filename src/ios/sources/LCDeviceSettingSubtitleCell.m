#import "LCDeviceSettingSubtitleCell.h"
#import "LCUIKit.h"

@interface LCDeviceSettingSubtitleCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLab;
@property (weak, nonatomic) IBOutlet UILabel *detailLab;

@end

@implementation LCDeviceSettingSubtitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [self addGestureRecognizer:tap];
    self.backgroundColor = [UIColor dhcolor_c43];
}

-(void)tapClick:(UITapGestureRecognizer *)tap{
    if (self.block) {
        self.block();
    }
}

-(void)setTitle:(NSString *)title{
    _title = title;
    self.titleLab.text = title;
    self.titleLab.textColor = [UIColor dhcolor_c51];
}

-(void)setSubtitle:(NSString *)subtitle{
    _subtitle = subtitle;
    self.subTitleLab.text = subtitle;
    self.subTitleLab.textColor = [UIColor dhcolor_c51];
}

-(void)setDetail:(NSString *)detail{
    _detail = detail;
    self.detailLab.text = detail;
    self.detailLab.textColor = [UIColor dhcolor_c51];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
