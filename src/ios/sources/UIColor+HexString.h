#import <UIKit/UIKit.h>
@interface UIColor (HexString)
+ (UIColor *)lc_colorWithHexString:(NSString *)hexString;
+ (UIColor *)lc_colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;
@end
