#import <UIKit/UIKit.h>
@interface DHModuleConfig : NSObject
@property (nonatomic, strong, readonly) NSMutableDictionary *dicConfigs;
+ (instancetype)shareInstance;
@property (nonatomic, assign, readonly) BOOL isLeChange;
@property (nonatomic, assign, readonly) BOOL isShowAllRecord;
@property (nonatomic, assign, readonly) BOOL isShowCollectionMaskView;
@property (nonatomic, assign, readonly) BOOL isShowAllRecordMaskView;
@property (nonatomic, assign, readonly) BOOL isShowMoveAreaMaskView;
@property (nonatomic, assign, readonly) BOOL isShowLinkAgeMaskView;
@property (nonatomic, assign, readonly) BOOL isShowWeatherMaskView;
@property (nonatomic, assign, readonly) BOOL isShowDialogMaskView;
@property (nonatomic, assign, readonly) BOOL isGeneralVersion;
@property (nonatomic, assign, readonly) BOOL isDistributionVersion;
@property (nonatomic, assign, readonly) NSInteger ptzPanelStyle;
@property (nonatomic, strong, readonly) UIColor *playOperateBgColor;
@property (nonatomic, strong, readonly) UIColor *liveMonitorDateBarColor;
@property (nonatomic, strong, readonly) UIColor *themeColor;
@property (nonatomic, strong, readonly) UIColor *themeSecondColor;
@property (nonatomic, strong, readonly) UIColor *navigationBarColor;
@property (nonatomic, strong, readonly) UIColor *navigationTextColor;
@property (nonatomic, strong, readonly) UIColor *messageCalendarColor;
@property (nonatomic, strong, readonly) UIColor *zoomFocusProgressBarColor;
@property (nonatomic, strong, readonly) UIColor *confirmButtonColor;
- (NSDictionary *)shareSheetType;
- (NSDictionary *)commonButtonConfig;
- (NSDictionary *)generateDictionaryContainColor:(NSDictionary *)dicOrigin;
- (CGFloat)commonButtonCornerRadius;
- (UIColor *)commonButtonColor;
- (NSString *)serviceCall;
@end
