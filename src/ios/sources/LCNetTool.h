#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocationManager.h>
NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    LCNetStatusOFFLine,
    LCNetStatusWWAN,
    LCNetStatusWiFi
} LCNetStatus;
@interface LCNetTool : NSObject<CLLocationManagerDelegate>
+(instancetype)shareManager;
-(void)lc_CurrentWiFiName:(void(^)(NSString * ssid))block;
+(LCNetStatus)lc_CurrentNetStatus;
+ (void)lc_ObserveNetStatus:(void (^)(LCNetStatus status))netStatus;
@end
NS_ASSUME_NONNULL_END
