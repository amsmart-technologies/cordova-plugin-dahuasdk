#import <Foundation/Foundation.h>
#import "LCModel.h"
#import "LCDevice.h"
#import "LCClientConfigInfo.h"
#import "LCCheckDeviceBindOrNotInfo.h"
#import "LCDeviceOnlineInfo.h"
#import "OpenApiInterface.h"
#import "LCNetworkRequestManager.h"
#import "TextDefine.h"
#import "LCApplicationDataManager.h"
#import "LCCloudVideotapeInfo.h"
#import "NSString+Verify.h"
NS_ASSUME_NONNULL_BEGIN
@interface OpenApiInterface : NSObject
+ (NSNumber *)totalDevices;
+ (void)accessTokenWithsuccess:(void (^)(LCAuthModel *authInfo))success failure:(void (^)(LCError *error))failure;
+ (void)createSubAccount:(NSString *)token account:(NSString *)account success:(void (^)(LCAuthModel *authInfo))success failure:(void (^)(LCError *error))failure;
+ (void)getOpenIdByAccount:(NSString *)token account:(NSString *)account success:(void (^)(LCAuthModel *authInfo))success failure:(void (^)(LCError *error))failure;
+ (void)getSubAccountToken:(NSString *)token openid:(NSString *)openid success:(void (^)(LCAuthModel *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure;
+ (void)listSubAccount:(NSString *)token pageNo:(NSNumber *)pageNo pageSize:(NSNumber *)pageSize success:(void (^)(NSString *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure;
+ (void)deleteSubAccount:(NSString *)token openid:(NSString *)openid success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)listSubAccountDevice:(NSString *)token openid:(NSString *)openid pageNo:(NSNumber*)pageNo pageSize:(NSNumber*)pageSize success:(void (^)(NSString *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure;
+ (void)subAccountDeviceList:(NSString *)token pageNo:(NSNumber*)pageNo pageSize:(NSNumber*)pageSize success:(void (^)(NSString *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure;
+ (void)listDeviceDetailsByPage:(NSString *)token page:(NSNumber*)page pageSize:(NSNumber*)pageSize success:(void (^)(NSString *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure;
+ (void)addDeviceToSubAccount:(NSString *)token openid:(NSString *)openid policy:(NSString *)policy success:(void (^)(void))success failure:(void (^)(LCError *_Nonnull))failure;
+ (void)getDeviceIntroductionForDeviceModel:(NSString *)token deviceModel:(NSString *)deviceModel success:(void (^)(DHOMSIntroductionInfo * introductions))success failure:(void (^)(LCError *error))failure;
+ (void)checkDeviceIntroductionWithUpdateTime:(NSString *)updateTime success:(void (^)(BOOL isUpdated))success failure:(void (^)(LCError *error))failure;
+ (void)queryAllProductWithDeviceType:(nullable NSString *)deviceModel Success:(void (^)(NSDictionary *productList))success failure:(void (^)(LCError *error))failure;
+ (void)checkDeviceBindOrNotWithDevice:(NSString *)deviceId success:(void (^)(LCCheckDeviceBindOrNotInfo * info))success failure:(void (^)(LCError *error))failure;
+ (void)unBindDeviceInfoForDevice:(NSString *)token deviceId:(NSString *)deviceId DeviceModel:(nullable NSString *)deviceModel DeviceName:(NSString *)deviceName ncCode:(NSString *)ncCode productId:(NSString *)productId success:(void (^)(DHUserDeviceBindInfo * info))success failure:(void (^)(LCError *error))failure;
+ (void)deviceOnlineFor:(nonnull NSString *)deviceId success:(void (^)(LCDeviceOnlineInfo *deviceOnlineInfo))success failure:(void (^)(LCError *error))failure;
+ (void)bindDeviceWithDevice:(NSString *)token deviceId:(nonnull NSString *)deviceId Code:(NSString *)code success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)timeZoneConfigByWeekWithDevice:(nonnull NSString *)deviceId AreaIndex:(NSInteger)areaIndex TimeZone:(NSInteger)timeZone BeginSunTime:(NSString *)beginSunTime EndSunTime:(NSString *)endSunTime success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)timeZoneConfigByDateWithDevice:(nonnull NSString *)deviceId AreaIndex:(NSInteger)areaIndex TimeZone:(NSInteger)timeZone BeginSunTime:(NSString *)beginSunTime EndSunTime:(NSString *)endSunTime success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)setDeviceSnapWithDevice:(NSString *)deviceId Channel:(NSString *)channelId success:(void (^)(NSString *picUrlString))success failure:(void (^)(LCError *error))failure;
+ (void)setDeviceSnapEnhancedWithDevice:(NSString *)deviceId Channel:(NSString *)channelId success:(void (^)(NSString *picUrlString))success failure:(void (^)(LCError *error))failure;
+ (void)controlMovePTZWithDevice:(NSString *)deviceId Channel:(NSString *)channelId Operation:(NSString *)operation Duration:(NSInteger)duration success:(void (^)(NSString *picUrlString))success failure:(void (^)(LCError *error))failure;
+ (void)wifiAroundDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(LCAroundWifiInfo *wifiInfo))success failure:(void (^)(LCError *error))failure;
+ (void)currentDeviceWifiDevice:(NSString *)deviceId success:(void (^)(LCWifiInfo *wifiInfo))success failure:(void (^)(LCError *error))failure;
+ (void)timeZoneQueryByDay:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(LCTimeZoneConfig *config))success failure:(void (^)(LCError *error))failure;
+ (void)timeZoneConfigByDay:(NSString *)token deviceId:(NSString *)deviceId areaIndex:(NSString *)areaIndex timeZone:(NSString *)timeZone beginSunTime:(NSString *)beginSunTime endSunTime:(NSString *)endSunTime success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)setMessageCallback:(NSString *)token status:(NSString *)status callbackUrl:(NSString *)callbackUrl callbackFlag:(NSString *)callbackFlag success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)getMessageCallback:(NSString *)token success:(void (^)(LCMessageCallback *callback))success failure:(void (^)(LCError *error))failure;
+ (void)getAlarmMessage:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId beginTime:(NSString *)beginTime endTime:(NSString *)endTime count:(NSString *)count nextAlarmId:(NSString *)nextAlarmId success:(void (^)(LCAlarmMessage *alarmMessage))success failure:(void (^)(LCError *error))failure;
+ (void)deleteAlarmMessage:(NSString *)token deviceId:(NSString *)deviceId indexId:(NSString *)indexId channelId:(NSString *)channelId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)controlDeviceWifiFor:(NSString *)token deviceId:(NSString *)deviceId ConnestSession:(LCWifiConnectSession *)session success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)upgradeDevice:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)modifyDeviceAlarmStatus:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId enable:(BOOL)enable success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)queryLocalRecordPlan:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId success:(void (^)(LCAlarmPlan *plan))success failure:(void (^)(LCError *error))failure;
+ (void)setLocalRecordPlanRules:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId plan:(LCAlarmPlan *)plan success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)breathingLightStatusForDevice:(NSString *)deviceId success:(void (^)(BOOL status))success failure:(void (^)(LCError *error))failure;
+ (void)modifyBreathingLightForDevice:(NSString *)deviceId Status:(BOOL)open success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)frameReverseStatusForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(NSString *direction))success failure:(void (^)(LCError *error))failure;
+ (void)modifyFrameReverseStatusForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId Direction:(NSString *)direction success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)recoverSDCardForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure;
+ (void)setDeviceOsdForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId enable:(BOOL)open OSD:(NSString *)osd success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)queryDeviceOsdForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(BOOL enable, NSString *osd))success failure:(void (^)(LCError *error))failure;
+ (void)uploadDeviceCoverPictureForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId PictureData:(NSData *)data success:(void (^)(NSString *picUrlString))success failure:(void (^)(LCError *error))failure;
+ (void)devicePTZInfoForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(NSString *h, NSString *v, NSString *z))success failure:(void (^)(LCError *error))failure;
+ (void)refreshDeviceCoverForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)setCollectionForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId Name:(NSString *)name success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)deleteCollectionForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId NameList:(NSMutableArray <NSString *> *)nameList success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)getCollectionForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(NSMutableArray <NSString *> *))success failure:(void (^)(LCError *error))failure;
+ (void)modifyCollectionForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(NSMutableArray <NSString *> *))success failure:(void (^)(LCError *error))failure;
+ (void)queryCloudRecordCallNum:(NSString *)token strategyId:(NSNumber *)strategyId success:(void (^)(CloudRecordCallNum *cloudRecordCallNum))success failure:(void (^)(LCError *error))failure;
+ (void)openCloudRecord:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId strategyId:(NSNumber *)strategyId deviceCloudId:(NSString *)deviceCloudId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)getDeviceCloud:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId success:(void (^)(StorageStrategy *storageStrategy))success failure:(void (^)(LCError *error))failure;
+ (void)unBindDeviceWithDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)modifyDeviceForDevice:(NSString *)token deviceId:(NSString *)deviceId Channel:(nullable NSString *)channelId NewName:(NSString *)name success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)bindDeviceChannelInfoWithDevice:(NSString *)token deviceId:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(LCBindDeviceChannelInfo *info))success failure:(void (^)(LCError *error))failure;
+ (void)deviceVersionForDevices:(NSString *)token devices:(NSArray *)devices success:(void (^)(NSMutableArray <LCDeviceVersionInfo*> *info))success failure:(void (^)(LCError *error))failure;
+ (void)deviceDetailListFromLeChangeWith:(NSInteger)bindId Limit:(int)limit Type:(NSString *)type NeedApInfo:(BOOL)needApInfo success:(void (^)(NSMutableArray <LCDeviceInfo *> *devices))success failure:(void (^)(LCError *error))failure;
+ (void)deviceDetailListFromOpenPlatformWith:(NSInteger)bindId Limit:(int)limit Type:(NSString *)type NeedApInfo:(BOOL)needApInfo success:(void (^)(NSMutableArray <LCDeviceInfo *> *devices))success failure:(void (^)(LCError *error))failure;
+ (void)listSubAccountDevice2:(NSString *)token openid:(NSString *)openid pageNo:(NSNumber*)pageNo pageSize:(NSNumber*)pageSize success:(void (^)(NSMutableArray<LCDeviceInfo *> * _Nonnull))success failure:(void (^)(LCError *_Nonnull))failure;;
+ (void)deviceBaseDetailListFromLeChangeWithSimpleList:(NSMutableArray <LCDeviceInfo *>*)infos  success:(void (^)(NSMutableArray<LCDeviceInfo *> *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure;
+ (void)deviceOpenDetailListFromLeChangeWithSimpleList:(NSMutableArray <LCDeviceInfo *>*)infos  success:(void (^)(NSMutableArray<LCDeviceInfo *> *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure;
+ (void)queryCloudRecordsForDevice:(NSString *)deviceId channelId:(NSString *)channelId day:(NSDate *)day From:(int)start To:(int)end  success:(void (^)(NSMutableArray <LCCloudVideotapeInfo *> * videos))success failure:(void (^)(LCError *error))failure;
+ (void)getCloudRecordsForDevice:(NSString *)deviceId channelId:(NSString *)channelId day:(NSDate *)day From:(long)nextRecordId Count:(long)count success:(void (^)(NSMutableArray<LCCloudVideotapeInfo *> * _Nonnull))success failure:(void (^)(LCError * _Nonnull))failure;
+ (void)queryLocalRecordsForDevice:(NSString *)deviceId channelId:(NSString *)channelId day:(NSDate *)day From:(int)start To:(int)end success:(void (^)(NSMutableArray<LCLocalVideotapeInfo *> * _Nonnull))success failure:(void (^)(LCError * _Nonnull))failure;
+ (void)deleteCloudRecords:(NSString *)recordRegionId success:(void (^)(void))success failure:(void (^)(LCError * _Nonnull))failure;
+ (void)deviceSdcardStatus:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(SDCardStatusData *sdcardStatus))success failure:(void (^)(LCError *error))failure;
+ (void)deviceStorage:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(SDCardStatusData *sdcardStatus))success failure:(void (^)(LCError *error))failure;
+ (void)recoverSDCard:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(SDCardStatusData *sdcardStatus))success failure:(void (^)(LCError *error))failure;
+ (void)setDeviceSnap:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure;
+ (void)restartDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)modifyDevicePwd:(NSString *)token deviceId:(NSString *)deviceId oldPwd:(NSString *)oldPwd newPwd:(NSString *)newPwd success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)upgradeProcessDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure;
+ (void)upgradeDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure;
+ (void)wakeUpDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)deviceOnline:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure;
+ (void)getDevicePowerInfo:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure;
+ (void)doorbellCallAnswer:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)doorbellCallRefuse:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)doorbellCallHangUp:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure;
+ (void)getDeviceCameraStatus:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId enableType:(NSString *)enableType success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure;
+ (void)setDeviceCameraStatus:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId enableType:(NSString *)enableType enable:(BOOL)enable success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure;
@end
NS_ASSUME_NONNULL_END
