#import "NSString+Verify.h"
#define EMOJI          @"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]"
#define SPACE          @"[//s//p{Zs}]"
#define LC_ILLEGALCHAR @"[•€`~!#$%^&*+=|{}()':;',\\[\\]<>/?~！#¥%⋯⋯&*（）+|{}【】‘；：\"”“’。，、？]"
#define LC_NEMECHAR @"[0-9a-zA-Z\u4e00-\u9fa5\\@\\_\\-\\ ]+"
@implementation NSString (Verify)
- (BOOL)isNull {
    if ([self isEqualToString:@""]) {
        return YES;
    }
    return NO;
}
- (BOOL)isVaildPhone {
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|6[6]|7[05-8]|8[0-9]|9[89])\\d{8}$";
    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478]|9[8])\\d{8}$)|(^1705\\d{7}$)";
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|66|7[56]|8[56])\\d{8}$)|(^1709\\d{7}$)";
    NSString *CT = @"(^1(33|53|77|73|8[019]|99)\\d{8}$)|(^1700\\d{7}$)";
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    if (([regextestmobile evaluateWithObject:self] == YES)
        || ([regextestcm evaluateWithObject:self] == YES)
        || ([regextestct evaluateWithObject:self] == YES)
        || ([regextestcu evaluateWithObject:self] == YES)) {
        return YES;
    } else {
        return NO;
    }
}
- (NSString *)vaildDeviceName {
    NSString *vaildStr = @"";
    vaildStr = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    vaildStr = [self disableSpecialCharInString:vaildStr];
    if (vaildStr.length > 20) {
        vaildStr = [vaildStr substringToIndex:20];
    }
    return vaildStr;
}
- (NSString *)disableEmojiInString:(NSString *)text {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:EMOJI options:NSRegularExpressionCaseInsensitive error:nil];
    NSString *modifiedString = [regex stringByReplacingMatchesInString:text
                                                               options:0
                                                                 range:NSMakeRange(0, [text length])
                                                          withTemplate:@""];
    return modifiedString;
}
- (NSString *)disableSpaceInString:(NSString *)text {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:SPACE options:NSRegularExpressionCaseInsensitive error:nil];
    NSString *modifiedString = [regex stringByReplacingMatchesInString:text
                                                               options:0
                                                                 range:NSMakeRange(0, [text length])
                                                          withTemplate:@""];
    return modifiedString;
}
- (NSString *)disableSpecialCharInString:(NSString *)text {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:LC_ILLEGALCHAR options:NSRegularExpressionCaseInsensitive error:nil];
    NSString *modifiedString = [regex stringByReplacingMatchesInString:text
                                                               options:0
                                                                 range:NSMakeRange(0, [text length])
                                                          withTemplate:@""];
    return modifiedString;
}
- (NSString *)filterCharactor:(NSString *)string withRegex:(NSString *)regexStr{
    NSString *searchText = string;
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexStr options:NSRegularExpressionCaseInsensitive error:&error];
    NSString *result = [regex stringByReplacingMatchesInString:searchText options:NSMatchingReportCompletion range:NSMakeRange(0, searchText.length) withTemplate:@""];
    return result;
}
-(BOOL)isCharactor{
    NSString *regex = @"[^\u4e00-\u9fa5]";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL result = [predicate evaluateWithObject:self];
    return result;
}
-(BOOL)isVaildDeviceName{
    NSString *regex = LC_NEMECHAR;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL result = [predicate evaluateWithObject:self];
    return result;
}
- (BOOL)isSafeCode {
    NSString *regex = @"^[A-Za-z0-9]{5,7}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL result = [predicate evaluateWithObject:self];
    return result;
}
- (BOOL)isVaildEmail {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL result = [emailTest evaluateWithObject:self];
    return result;
}
- (BOOL)isVaildSNCode {
    if (self.length < 10 || self.length > 32) {
        return NO;
    }
    NSString *regex = @"^[A-Za-z0-9]{10,32}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL result = [predicate evaluateWithObject:self];
    return result;
}
- (BOOL)isVaildOverseaSNCode {
    return YES;
}
- (BOOL)isFullNumber {
    NSString *regex = @"^[0-9]+$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL result = [predicate evaluateWithObject:self];
    return result;
}
- (BOOL)isFullChar {
    NSString *regex = @"^[A-Za-z]+$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL result = [predicate evaluateWithObject:self];
    return result;
}
- (BOOL)isVaildPasswordBit {
    return [self lc_pwdVerifiers];
}
- (BOOL)isExistBlankSpace {
    NSString *regex = @"^[^ ]+$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL result = [predicate evaluateWithObject:self];
    return result;
}
- (BOOL)isVaildURL {
    NSString *regex = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL result = [predicate evaluateWithObject:self];
    return result;
}
- (BOOL)lc_pwdVerifiers {
    if (self.length < 8 || self.length > 32) {
        return false;
    }
    int numOfType = [self passwordStrength:self];
    if (numOfType < 2) {
        return false;
    }
    if ([self checkInvalidPassword:self]) {
        return false;
    }
    if ([self checkSameCharacter:self]) {
        return false;
    }
    if ([self checkSameCharacter:self]) {
        return false;
    }
    return true;
}
- (int)passwordStrength:(NSString *)pasword
{
    int nRet = 0;
    NSString *smallChar = @"abcdefghijklmnopqrstuvwxyz";
    NSString *bigChar = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSString *numberChar = @"1234567890";
    NSString *otherChar = @" -/:;()$&@\".,?!'[]{}#%^*+=_\\|~<>.,?!'";
    NSString *urlString = pasword;
    NSString *strType[4] = { smallChar, bigChar, numberChar, otherChar };
    for (int j = 0; j < 4; j++) {
        for (int i = 0; i < [urlString length]; i++) {
            NSRange r;
            r.length = 1;
            r.location = i;
            NSString *c = [urlString substringWithRange:r];
            if ([strType[j] rangeOfString:c].location != NSNotFound) {
                nRet++;
                break;
            }
        }
    }
    return nRet;
}
- (BOOL)checkInvalidPassword:(NSString *)password {
    if (password.length > 0) {
        if ([[self initializeInvalidPassword] containsObject:password]) {
            return YES;
        }
    }
    return NO;
}
- (NSArray *)initializeInvalidPassword {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"InvalidPasswordList" ofType:@"plist"];
    NSArray *list = [[NSArray alloc] initWithContentsOfFile:filePath];
    return list;
}
- (BOOL)checkSameCharacter:(NSString *)password {
    if (password.length > 0) {
        NSString *checkerString = @"^.*(.)\\1{5}.*$";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", checkerString];
        return [predicate evaluateWithObject:password];
    }
    return NO;
}
@end
