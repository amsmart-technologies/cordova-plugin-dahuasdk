#import "LCVideotapePlayerPersenter+SDKListener.h"
#import "LCVideotapePlayerPersenter+Control.h"
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_Define.h>
#import "LCBaseDefine.h"
#define HLS_Result_String(enum) [@[ @"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"11"] objectAtIndex:enum]
@implementation LCVideotapePlayerPersenter (SDKListener)
#pragma mark - 播放回调
- (void)onReceiveData:(NSInteger)len Index:(NSInteger)index {
    NSLog(@"");
}
- (void)onPlayerResult:(NSString *)code Type:(NSInteger)type Index:(NSInteger)index {
    weakSelf(self);
    NSLog(@"TEST设备录像回调code = %@, type = %ld", code, (long)type);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (type == RESULT_PROTO_TYPE_OPENAPI) {
            if ([code isEqualToString:@"-1000"]) {
                [self showErrorBtn];
            } else if (![code isEqualToString:@"0"]) {
                [self showErrorBtn];
            }
        }
        if ((type == RESULT_PROTO_TYPE_LCHTTP && [code integerValue] == STATE_LCHTTP_KEY_ERROR) ||
            (type == RESULT_PROTO_TYPE_RTSP && [code integerValue] == STATE_RTSP_KEY_MISMATCH) ||
            (type == RESULT_PROTO_TYPE_HLS && [code integerValue] == STATE_HLS_KEY_MISMATCH)) {
            [weakself showErrorBtn];
            if (![weakself.videoManager.currentPsk isEqualToString:self.videoManager.currentDevice.deviceId]) {
                weakself.videoManager.currentPsk = @"";
                [weakself hideErrorBtn];
                [weakself onPlay:nil];
            }else{
                [weakself showPSKAlert];
                [self.playWindow stopCloud:YES];
                [self.playWindow stopDeviceRecord:YES];
            }
            return;
        }
        if (type == RESULT_PROTO_TYPE_RTSP &&
            ([code integerValue] == STATE_PACKET_COMPONENT_ERROR ||
             [code integerValue] == STATE_PACKET_FRAME_ERROR ||
             [code integerValue] == STATE_RTSP_TEARDOWN_ERROR ||
             [code integerValue] == STATE_RTSP_AUTHORIZATION_FAIL ||
             [code integerValue] == STATE_RTSP_SERVICE_UNAVAILABLE)) {
            [self showErrorBtn];
            return;
        }
        if (type == RESULT_PROTO_TYPE_LCHTTP &&
            ([code integerValue] != 0 &&
             [code integerValue] != STATE_LCHTTP_OK &&
             [code integerValue] != STATE_LCHTTP_PLAY_FILE_OVER &&
             [code integerValue] != STATE_LCHTTP_PAUSE_OK)) {
            [self showErrorBtn];
            return;
        }
        type == RESULT_PROTO_TYPE_HLS ? [weakself onPlayCloudRecordResult:code Type:type] : [weakself onPlayDeviceRecordResult:code Type:type];
    });
    return;
}
- (void)onPlayBegan:(NSInteger)index {
    weakSelf(self);
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakself hideVideoLoadImage];
        [self hideErrorBtn];
        weakself.videoManager.isPlay = YES; 
        weakself.videoManager.playStatus = 1001;
    });
}
- (void)onPlayFinished:(NSInteger)index {
    weakSelf(self);
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakself.playWindow stopCloud:YES];
        [weakself.playWindow stopDeviceRecord:YES];
        weakself.videoManager.playStatus = STATE_RTSP_FILE_PLAY_OVER;
        weakself.videoManager.isPlay = NO;
        weakself.videoManager.pausePlay = YES;
        if ([self.videoManager.currentPlayOffest timeIntervalSinceDate:self.videoManager.cloudVideotapeInfo ? self.videoManager.cloudVideotapeInfo.endDate : self.videoManager.localVideotapeInfo.endDate] < 0) {
            self.videoManager.currentPlayOffest = self.videoManager.cloudVideotapeInfo ? self.videoManager.cloudVideotapeInfo.endDate : self.videoManager.localVideotapeInfo.endDate;
        }
        [weakself hideVideoLoadImage];
        [weakself showPlayBtn];
    });
}
- (void)onPlayerTime:(long)time Index:(NSInteger)index {
    weakSelf(self);
    dispatch_async(dispatch_get_main_queue(), ^{
        NSInteger ooo = time - weakself.sssdate;
        NSLog(@"异常跳针NOR:%ld", ooo);
        if (ooo > 1 && (self.sssdate != 0)) {
            NSLog(@"异常跳针前次：%ld，本次：%ld", self.sssdate, time);
        }
        weakself.sssdate = time;
        weakself.videoManager.currentPlayOffest = [NSDate dateWithTimeIntervalSince1970:time];
    });
}
#pragma mark - 云录像播放回调
- (void)onPlayCloudRecordResult:(NSString *)code Type:(NSInteger)type {
    weakSelf(self);
    [self hideVideoLoadImage];
    if ([HLS_Result_String(HLS_SEEK_SUCCESS) isEqualToString:code]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"定位成功");
        });
        return;
    }
    if ([HLS_Result_String(HLS_SEEK_FAILD) isEqualToString:code]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"定位失败");
            [weakself showErrorBtn];
        });
        return;
    }
    if ([code integerValue] == HLS_KEY_ERROR) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakself showErrorBtn];
            if (![weakself.videoManager.currentPsk isEqualToString:self.videoManager.currentDevice.deviceId]) {
                weakself.videoManager.currentPsk = @"";
                [weakself hideErrorBtn];
                [weakself onPlay:nil];
            } else {
                [weakself showPSKAlert];
                [weakself stopPlay];
            }
        });
        return;
    }
}
#pragma mark - 设备录像播放回调
- (void)onPlayDeviceRecordResult:(NSString *)code Type:(NSInteger)type {
    weakSelf(self);
    [self hideVideoLoadImage];
    if (code.intValue == STATE_PACKET_FRAME_ERROR) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakself showErrorBtn];
        });
        return;
    }
    if (code.intValue == STATE_RTSP_TEARDOWN_ERROR) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakself showErrorBtn];
        });
        return;
    }
    if (code.intValue == STATE_RTSP_AUTHORIZATION_FAIL) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakself showErrorBtn];
        });
        return;
    }
    if (code.intValue == STATE_RTSP_PLAY_READY) {
        dispatch_async(dispatch_get_main_queue(), ^{
        });
        return;
    }
    if (code.intValue == STATE_RTSP_FILE_PLAY_OVER) {
        dispatch_async(dispatch_get_main_queue(), ^{
        });
        return;
    }
    if (code.intValue == STATE_RTSP_KEY_MISMATCH) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakself showErrorBtn];
        });
    }
    if (code.intValue == STATE_RTSP_PAUSE_READY) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakself showErrorBtn];
        });
    }
    if (code.intValue == STATE_RTSP_SERVICE_UNAVAILABLE) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakself showErrorBtn];
        });
    }
    if (code.intValue == STATE_RTSP_USER_INFO_BASE_START) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakself showErrorBtn];
        });
    }
}
@end
