#import <Cordova/CDVPlugin.h>
#import <UIKit/UIKit.h>
#import <NetworkExtension/NetworkExtension.h>

@interface DahuaSDK : CDVPlugin { }

- (void)setEnv:(CDVInvokedUrlCommand *)command;
- (void)getToken:(CDVInvokedUrlCommand *)command;
- (void)initialize:(CDVInvokedUrlCommand *)command;
- (void)parseQRcode:(CDVInvokedUrlCommand *)command;
- (void)createSubAccount:(CDVInvokedUrlCommand *)command;
- (void)getOpenIdByAccount:(CDVInvokedUrlCommand *)command;
- (void)getSubAccountToken:(CDVInvokedUrlCommand *)command;
- (void)listSubAccount:(CDVInvokedUrlCommand *)command;
- (void)listSubAccountDevice:(CDVInvokedUrlCommand *)command;
- (void)subAccountDeviceList:(CDVInvokedUrlCommand *)command;
- (void)listDeviceDetailsByPage:(CDVInvokedUrlCommand *)command;
- (void)addDeviceToSubAccount:(CDVInvokedUrlCommand *)command;
- (void)getDevicesList:(CDVInvokedUrlCommand *)command;
- (void)getSnapShot:(CDVInvokedUrlCommand *)command;
- (void)openDevicePlay:(CDVInvokedUrlCommand *)command;
- (void)openDeviceDetail:(CDVInvokedUrlCommand *)command;
- (void)deviceInfoBeforeBind:(CDVInvokedUrlCommand *)command;
- (void)userDeviceBind:(CDVInvokedUrlCommand *)command;
- (void)deviceLeadingInfo:(CDVInvokedUrlCommand *)command;
- (void)startSearchService:(CDVInvokedUrlCommand *)command;
- (void)stopSearchService:(CDVInvokedUrlCommand *)command;
- (void)getDeviceNetInfo:(CDVInvokedUrlCommand *)command;
- (void)startDevInitByIp:(CDVInvokedUrlCommand *)command;
- (void)checkWifi:(CDVInvokedUrlCommand *)command;
- (void)startSmartConfig:(CDVInvokedUrlCommand *)command;
- (void)stopSmartConfig:(CDVInvokedUrlCommand *)command;
- (void)listenForSearchedDevices:(CDVInvokedUrlCommand *)command;
- (void)getWifiListSoftAp:(CDVInvokedUrlCommand *)command;
- (void)connectDeviceToWifi:(CDVInvokedUrlCommand *)command;
- (void)connectPhoneToWifi:(CDVInvokedUrlCommand *)command;
- (void)modifyDeviceName:(CDVInvokedUrlCommand *)command;
- (void)searchDeviceBLE:(CDVInvokedUrlCommand *)command;
- (void)unbindDevice:(CDVInvokedUrlCommand *)command;
- (void)bindDeviceChannelInfo:(CDVInvokedUrlCommand *)command;
- (void)modifyDeviceAlarmStatus:(CDVInvokedUrlCommand *)command;
- (void)deviceVersionList:(CDVInvokedUrlCommand *)command;
- (void)wifiAround:(CDVInvokedUrlCommand *)command;
- (void)controlDeviceWifi:(CDVInvokedUrlCommand *)command;
- (void)timeZoneQueryByDay:(CDVInvokedUrlCommand *)command;
- (void)timeZoneConfigByDay:(CDVInvokedUrlCommand *)command;
- (void)setMessageCallback:(CDVInvokedUrlCommand *)command;
- (void)getMessageCallback:(CDVInvokedUrlCommand *)command;
- (void)getAlarmMessage:(CDVInvokedUrlCommand *)command;
- (void)deleteAlarmMessage:(CDVInvokedUrlCommand *)command;
- (void)queryCloudRecordCallNum:(CDVInvokedUrlCommand *)command;
- (void)openCloudRecord:(CDVInvokedUrlCommand *)command;
- (void)getDeviceCloud:(CDVInvokedUrlCommand *)command;
- (void)deviceSdcardStatus:(CDVInvokedUrlCommand *)command;
- (void)deviceStorage:(CDVInvokedUrlCommand *)command;
- (void)recoverSDCard:(CDVInvokedUrlCommand *)command;
- (void)setDeviceSnap:(CDVInvokedUrlCommand *)command;
- (void)queryLocalRecordPlan:(CDVInvokedUrlCommand *)command;
- (void)setLocalRecordPlanRules:(CDVInvokedUrlCommand *)command;
- (void)restartDevice:(CDVInvokedUrlCommand *)command;
- (void)modifyDevicePwd:(CDVInvokedUrlCommand *)command;
- (void)upgradeProcessDevice:(CDVInvokedUrlCommand *)command;
- (void)upgradeDevice:(CDVInvokedUrlCommand *)command;
- (void)wakeUpDevice:(CDVInvokedUrlCommand *)command;
- (void)deviceOnline:(CDVInvokedUrlCommand *)command;
- (void)getDevicePowerInfo:(CDVInvokedUrlCommand *)command;
- (void)getDeviceCameraStatus:(CDVInvokedUrlCommand *)command;
- (void)setDeviceCameraStatus:(CDVInvokedUrlCommand *)command;

@end
