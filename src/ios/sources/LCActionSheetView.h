#import "LCUIKit.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCActionSheetView : UIView
+(instancetype)lc_ShowActionView:(NSArray *)itemsList ToView:(UIView *)view ItemColor:(nullable NSString *)itemColor Success:(void(^)(NSInteger index))success Cancle:(void(^)(void))cancle Complete:(void(^)(void))complete;
-(void)dismiss;
@end
NS_ASSUME_NONNULL_END
