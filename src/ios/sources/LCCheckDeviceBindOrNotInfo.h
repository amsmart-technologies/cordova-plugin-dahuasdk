#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCCheckDeviceBindOrNotInfo : NSObject
@property (nonatomic) BOOL isBind;
@property (nonatomic) BOOL isMine;
@end
@interface LCUnBindDeviceInfo : NSObject
@property (strong, nonatomic) NSString *ability;
@property (strong, nonatomic) NSString *deviceType;
@property (strong, nonatomic) NSString *deviceCatalog;
@property (strong, nonatomic) NSString *dt;
@property (strong, nonatomic) NSString *dtName;
@property (strong, nonatomic) NSString *wifiMode;
@property (strong, nonatomic) NSString *userAccount;
@property (strong, nonatomic) NSString *wifiConfigMode;
@end
NS_ASSUME_NONNULL_END
