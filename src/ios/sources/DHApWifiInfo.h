#import <Foundation/Foundation.h>
@interface DHApWifiInfo : NSObject
@property (nonatomic, assign) BOOL autoConnect;
@property (nonatomic, assign) NSInteger encryptionAuthority;
@property (nonatomic, assign) BOOL selected;
@property (nonatomic, assign) NSInteger linkQuality;
@property (nonatomic, copy) NSString *wlanSSID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *netcardName; 
@end
