#import <UIKit/UIKit.h>
#import "UIViewController+LCNavigationBar.h"
#import "UIColor+LeChange.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCBasicViewController : UIViewController
- (void)fixlayoutConstant:(UIView *)view;
- (void)onActive:(id)sender;
- (void)onResignActive:(id)sender;
- (void)viewTap:(UITapGestureRecognizer *)tap;
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer;
@end
NS_ASSUME_NONNULL_END
