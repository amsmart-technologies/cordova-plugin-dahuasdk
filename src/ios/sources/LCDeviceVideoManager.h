#import <Foundation/Foundation.h>
#import "LCLivePreviewDefine.h"
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_Define.h>
#import "LCDeviceInfo.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#define RTSP_Result_String(enum) [@[ @"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"99", @"100" ] objectAtIndex:enum]
NS_ASSUME_NONNULL_BEGIN
@interface LCDeviceVideoManager : NSObject
+(instancetype)manager;
-(void)playDoorBellSound;
@property (nonatomic) BOOL isPlay;
@property (nonatomic) BOOL pausePlay;
@property (nonatomic) RTSP_STATE playStatus;
@property (nonatomic) NSInteger playSpeed;
@property (nonatomic) BOOL isSD;
@property (nonatomic) BOOL isbindFromLeChange;
@property (nonatomic) BOOL isSoundOn;
@property (nonatomic) BOOL isFullScreen;
@property (nonatomic) BOOL isOpenCloudStage;
@property (nonatomic) BOOL isOpenAudioTalk;
@property (nonatomic) BOOL isOpenRecoding;
@property (nonatomic) BOOL isLockFullScreen;
@property (nonatomic) BOOL playbackEnabled;
@property (nonatomic) BOOL callBellEvent;
@property (nonatomic) BOOL callAnswered;
@property (nonatomic) BOOL answerbuttonPressed;
@property (strong) AVAudioPlayer *audioPlayer;
@property (nonatomic) NSInteger currentChannelIndex;
@property (strong,nonatomic) LCDeviceInfo * currentDevice;
@property (strong,nonatomic,readonly) LCChannelInfo * currentChannelInfo;
@property (strong,nonatomic) NSString * currentPsk;
@property (nonatomic, strong) LCCIResolutions *currentResolution;
@end
NS_ASSUME_NONNULL_END
