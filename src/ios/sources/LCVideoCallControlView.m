#import "LCVideoCallControlView.h"
#import "LCUIKit.h"
@implementation LCVideoCallControlView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
    }
    return self;
}
- (void)setItems:(NSMutableArray<UIView *> *)items {
    _items = items;
    [self setupView];
}
- (void)setupView {
    for (int a = 0; a < self.items.count; a++) {
        UIView *tempView = self.items[a];
        [self addSubview:tempView];
        [tempView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(self);
            make.bottom.mas_equalTo(self);
            make.height.mas_equalTo(100);
            make.width.mas_equalTo(100);
        }];
    }
    self.backgroundColor = [UIColor dhcolor_c43];
    [self.items mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedItemLength:100 leadSpacing:40 tailSpacing:40];
}
@end
