#import <UIKit/UIKit.h>
#import "LCButton.h"
typedef enum : NSUInteger {
    LCNAVIGATION_STYLE_DEFAULT,
    LCNAVIGATION_STYLE_CLEAR,
    LCNAVIGATION_STYLE_CLEARWITHLINE,
    LCNAVIGATION_STYLE_LIGHT,
    LCNAVIGATION_STYLE_DEVICELIST,
    LCNAVIGATION_STYLE_LIVE,
    LCNAVIGATION_STYLE_CLEAR_YELLOW,
    LCNAVIGATION_STYLE_SUBMIT
} LCNAVIGATION_STYLE;
typedef void(^NavigationBtnClickBlock)(NSInteger index);
NS_ASSUME_NONNULL_BEGIN
@interface UIViewController (LCNavigationBar)
- (void)lcCreatNavigationBarWith:(LCNAVIGATION_STYLE)style buttonClickBlock:(nullable NavigationBtnClickBlock)block;
- (LCButton *)lc_getRightBtn;
@end
NS_ASSUME_NONNULL_END
