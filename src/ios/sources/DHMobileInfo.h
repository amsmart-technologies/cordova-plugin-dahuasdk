#import <UIKit/UIKit.h>
#import "DHPubDefine.h"
@interface DHMobileInfo : NSObject
+ (instancetype)sharedInstance;
@property (strong, nonatomic) NSString *WIFISSID;
@property (strong, nonatomic) NSString *WIFIBSSID;
@property (strong, nonatomic) NSString *UUIDString;
@property (nonatomic) CGRect mainScreenRect;
@end
