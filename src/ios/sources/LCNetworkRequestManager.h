#import <Foundation/Foundation.h>
#import "LCModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef void (^requestSuccessBlock)(id objc);
typedef void (^requestFailBlock)(LCError *error);
@interface LCNetworkRequestManager : NSObject
+ (instancetype)manager;
@property (nonatomic) NSTimeInterval timeoutInterval;
- (void)lc_POST:(NSString *)URLString parameters:(id)parameters success:(requestSuccessBlock)success failure:(requestFailBlock)failure;
@end
NS_ASSUME_NONNULL_END
