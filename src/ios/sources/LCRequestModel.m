#import "LCRequestModel.h"
#import "LCApplicationDataManager.h"
@implementation LCRequestSystemModel
-(instancetype)init{
    if (self = [super init]) {
        self.nonce = [LCApplicationDataManager serial];
        self.time = [LCApplicationDataManager getCurrentTimeStamp];
        self.ver = @"1.0";
        self.appId = [LCApplicationDataManager appId];
        self.sign = [self getSign];
    }
    return self;
}
-(NSString *)getSign{
    NSString * signStr = [NSString stringWithFormat:@"time:%@,nonce:%@,appSecret:%@",self.time,self.nonce,[LCApplicationDataManager appSecret]];
    return [signStr lc_MD5Digest];
}
@end
@implementation LCRequestModel
+(instancetype)lc_WrapperNetworkRequestPackageWithParams:(id)params{
    LCRequestModel * model = [[LCRequestModel alloc] initWithParams:params];
    return model;
}
-(instancetype)initWithParams:(id)params{
    if (self = [super init]) {
        self.identifier = [LCApplicationDataManager serial];
        self.system = [LCRequestSystemModel new];
        _params = params;
    }
    return self;
}
+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{ @"identifier":@"id" };
}
@end