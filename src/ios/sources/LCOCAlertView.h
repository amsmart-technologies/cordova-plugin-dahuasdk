#import <UIKit/UIKit.h>
#import "LCUIKit.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^LCAlertViewResultBlock)(BOOL isConfirmSelected);
@interface LCOCAlertView : UIView
+(void)lc_ShowAlertWith:(NSString * )title Detail:(NSString *)detail ConfirmTitle:(nullable NSString *)confirmTitle CancleTitle:(nullable NSString *)cancleTitle Handle:(nullable LCAlertViewResultBlock)block;
+(void)lc_ShowAlertWithContent:(NSString *)content;
+(void)lc_showTextFieldAlertTextFieldWithTitle:(NSString *)title Detail:(NSString *)detail Placeholder:(NSString *)placeholder
ConfirmTitle:(NSString *)confirmTitle CancleTitle:(NSString *)cancleTitle Handle:(void (^)(BOOL isConfirmSelected,NSString * inputContent))block;
@end
NS_ASSUME_NONNULL_END
