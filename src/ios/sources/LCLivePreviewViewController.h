#import "LCBasicViewController.h"
typedef void(^FinishBlock)(void);
NS_ASSUME_NONNULL_BEGIN
@interface LCLivePreviewViewController : LCBasicViewController
-(void)showPtz;
-(void)hidePtz;
-(void)configFullScreenUI;
@property (nonatomic, copy) FinishBlock finishBlock;
@end
NS_ASSUME_NONNULL_END
