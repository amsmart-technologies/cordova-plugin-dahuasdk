#import "LCVideotapePlayerPersenter.h"
#import "LCVideotapePlayerPersenter+Control.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCVideotapePlayerPersenter (LandscapeControlView)
-(NSMutableArray *)getLandscapeBottomControlItems;
@end
NS_ASSUME_NONNULL_END
