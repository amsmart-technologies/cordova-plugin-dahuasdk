#import <Foundation/Foundation.h>
@interface NSData (Base64)
- (NSString *)base64String;
@end
