#import "DHTextField.h"
#import "UITextField+LeChange.h"
#import "UIColor+LeChange.h"
#import "UIFont+Dahua.h"
@interface DHTextField ()<UITextFieldDelegate>
@property (nonatomic, copy) DHTextFieldInputBlock result;
@property (strong, nonatomic) NSString *resultString;
@end
@implementation DHTextField
@synthesize placeholder = _placeholder;
+ (instancetype)lcTextFieldWithResult:(void (^)(NSString *result))result {
    DHTextField *textField = [DHTextField new];
    textField.result = result;
    return textField;
}
- (void)setInputingBlock:(DHTextFieldInputingBlock)inputingBlock{
    self.delegate = self;
    _inputingBlock = inputingBlock;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.font = [UIFont dhFont_t4];
        self.backgroundColor = [UIColor dhcolor_c43];
        self.textColor = [UIColor dhcolor_c51];
        self.delegate = self;
    }
    return self;
}
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 0);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 10, 0);
}
- (void)setPlaceholder:(NSString *)placeholder {
    self.attributedPlaceholder = [[NSAttributedString alloc]initWithString:placeholder attributes:@{ NSFontAttributeName: [UIFont dhFont_t4], NSForegroundColorAttributeName: [UIColor dhcolor_c41] }];
}
-(BOOL)textFieldShouldClear:(UITextField *)textField{
    if (self.result) {
        self.result(@"");
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason {
    if (self.result) {
        self.result(textField.text);
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.inputingBlock) {
        return self.inputingBlock(textField.text,string);
    }
    if ([self hasEmoji:string]) {
        return NO;
    }
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}
- (BOOL)hasEmoji:(NSString *)string;
{
    NSString *pattern = @"[^\\u0020-\\u007E\\u00A0-\\u00BE\\u2E80-\\uA4CF\\uF900-\\uFAFF\\uFE30-\\uFE4F\\uFF00-\\uFFEF\\u0080-\\u009F\\u2000-\\u201f\r\n]";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:string];
    return isMatch;
}
- (BOOL)isChinese:(NSString *)str
{
    NSString *match = @"(^[\u4e00-\u9fa5]+$)";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF matches %@", match];
    return [predicate evaluateWithObject:str];
}
@end
