#import <Foundation/Foundation.h>
#import "NSString+MD5.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCRequestSystemModel : NSObject
@property (strong, nonatomic) NSString *ver;
@property (strong, nonatomic) NSString *sign;
@property (strong, nonatomic) NSString *appId;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *nonce;
@end
@interface LCRequestModel : NSObject
@property (strong, nonatomic) LCRequestSystemModel *system;
@property (strong, nonatomic,readonly) id params;
@property (strong, nonatomic) NSString * identifier;
+(instancetype)lc_WrapperNetworkRequestPackageWithParams:(id)params;
@end
NS_ASSUME_NONNULL_END
