#ifndef DHPubDefine_h
#define DHPubDefine_h
typedef NSInteger Index;
typedef int64_t LongIndex;
typedef NS_ENUM(NSInteger,DHServerPortType)
{
    DHServerPortTypeHttp = 80, 
    DHServerPortTypeHttps = 443 
};
typedef NS_ENUM(NSInteger, DeviceCatalog)
{
    DeviceCatalogCamera, 
    DeviceCatalogBox,   
    DeviceCatalogBoxPart, 
    DeviceCatalogNVR,    
    DeviceCatalogNVRChannel, 
    DeviceCatalogG1, 
    DeviceCatalogG1Part  
};
typedef NS_ENUM(NSInteger, DHChannelListType)
{
    DHChannelListTypeCoverFlow,
    DHChannelListTypeList,
    DHChannelListTypeRecord
} ;
typedef NS_ENUM(NSInteger, SDCardStatus)
{
    SDCardStatusException  = 0,
    SDCardStatusNormal     = 1,
    SDCardStatusNoCard     = 2,
    SDCardStatusFormatting = 3,
    SDCardStatusError      = 4 
};
typedef NS_ENUM(NSInteger, HlsStatus)
{
    HlsStatusDownloadFail,	 
    HlsStatusDownloadBegin,  
    HlsStatusDownloadEnd,  
    HlsStatusSeekSuccess,  
    HlsStatusSeekFail, 
    HlsStatusAbortDone, 
    HlsStatusResumeDone,
    HlsStatusKeyError = 11, 
};
typedef NS_ENUM(NSInteger, DHLoadingStatus)
{
	DHLoadingStatusUnknown,
	DHLoadingStatusLoading,
	DHLoadingStatusSuccess,
	DHLoadingStatusFail,
};
#define SCREEN_WIDTH        [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT       [[UIScreen mainScreen] bounds].size.height
#define DH_SCREEN_SIZE_WIDTH     MIN([[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)
#define DH_SCREEN_SIZE_HEIGHT    MAX([[UIScreen mainScreen]bounds].size.width, [[UIScreen mainScreen]bounds].size.height)
#define DH_StatusBarHeight  ([UIApplication  sharedApplication].statusBarFrame.size.height)
#define DHL_NavigationBarHeight  44.f
#define DH_TabbarHeight    (DH_IS_IPHONEX ? (49.f+34.f) : 49.f)
#define DH_StatusBarAndNavigationBarHeight  (44.f + [UIApplication  sharedApplication].statusBarFrame.size.height)
#define DH_ViewSafeAreInsets(view) ({UIEdgeInsets insets; if(@available(iOS 11.f, *)) {insets = view.safeAreaInsets;} else {insets = UIEdgeInsetsZero;} insets;})
#define DH_BottomSafeMargin (DH_IS_IPHONEX ? 34.f : 0.f)
#define DH_TopSafeMargin (DH_IS_IPHONEX ? 24.f : 0.f)
#define DH_LandscapeSafeMargin (DH_IS_IPHONEX ? 44.f : 0.f)
#define DH_LandscapeVideoSafeMargin (DH_IS_IPHONEX ? 64.f : 0.f)
#define DH_NavViewHeight (DH_IS_IPHONEX ? 84.f : 64.f)
#define DH_ScreenSafeHeight (DH_IS_IPHONEX ? (DH_SCREEN_SIZE_HEIGHT - 58.f) : DH_SCREEN_SIZE_HEIGHT)
#define DH_SIZE_SCALE            (1.f / [UIScreen mainScreen].scale)
#define DH_AnimationDuration 0.4f
#define DH_LEGAL_PASSWORD_REGEX @"^[A-Za-z0-9!#%,<=>@_~`\\-\\.\\/\\(\\)\\*\\+\\?\\$\\[\\]\\\\\\^\\{\\}\\|]$"
#define DH_IMAGENAMED(imageName)  [UIImage imageNamed:imageName]
#define DH_KEY_WINDOW ([UIApplication sharedApplication].delegate).window
#define DH_IOS_VERSION ([[[UIDevice currentDevice] systemVersion] floatValue]) //iOS版本号
#define DH_CLIENT_OS_VERSION ([NSString stringWithFormat:@"iOS%@", [[UIDevice currentDevice] systemVersion]]) //iOS系统版本号
#define DH_iOS11_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
#define DH_iOS10_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define DH_IS_IPHONE5 (DH_SCREEN_SIZE_WIDTH == 320.0f && DH_SCREEN_SIZE_HEIGHT == 568.0f)
#define DH_IS_IPHONE6_6s (DH_SCREEN_SIZE_WIDTH == 375.0f && DH_SCREEN_SIZE_HEIGHT == 667.0f)
#define DH_IS_IPHONEX ((DH_SCREEN_SIZE_WIDTH == 375.0f && DH_SCREEN_SIZE_HEIGHT == 812.0f) || ((DH_SCREEN_SIZE_WIDTH == 414 && DH_SCREEN_SIZE_HEIGHT == 896)))
#define DH_IS_IPHONE6Plus_6sPlus (DH_SCREEN_SIZE_WIDTH == 414.0f && DH_SCREEN_SIZE_HEIGHT == 736.0f)
#define DH_RETURN_IF_TRUE(checkVal) {if(checkVal) \
{NSLog(@"####:%s,Check value true,return...", __FUNCTION__);return;}}   //判断为真返回
#define DH_RETURN_IF_FALSE(checkVal) {if(!(checkVal)) \
{ NSLog(@"####:%s,Check value false,return...", __FUNCTION__); return;}} //判断为假返回
#define DH_RETURN_NIL_IF_TRUE(checkVal) {if(checkVal) \
{NSLog(@" ####:%s,Check value true,return...", __FUNCTION__);return nil;}}   //判断为真返回空值
#define DH_RETURN_NIL_IF_FALSE(checkVal) {if(!(checkVal)) \
{ NSLog(@" ####:%s,Check value false,return...", __FUNCTION__); return nil;}} //判断为假返回空值
#define kAutoHideBarTime    (4.0f)  //悬浮条自动隐藏时间
#define kPictureHideBarTime    (5.0f)  //抓图录制图片自动隐藏时间
#define kFormatTimeLocal        (@"yyyyMMdd'T'HHmmss")
#define DEVICE_NAME_MAX_LIMIT     32
#define CHANNEL_NAME_MAX_LIMIT    32
#define DH_LEGAL_PASSWORD_OLD   @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~`!@#$%^&*()-_+={[]}|\\:;'\"<,>.?/ "
#define DH_LEGAL_PASSWORD       @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!#$%()*+,-./<=>?@[\\]^_`{|}~"
#define DH_LEGAL_ACCOUNT        @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890@_-."
#define DH_LEGAL_ACCOUNT_WITHOUT_LIMIT       @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!#$%()*+,-./<=>?@[\\]^_`{|}~&'"
#define DH_LEGAL_NUMBER         @"1234567890"
#define DH_LEGAL_NUMBERnABC     @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
#define DH_PASSWORD_MIN_LENGTH      8   //密码最小长度
#define DH_PASSWORD_MAX_LENGTH      32  //密码最大长度
#define DH_USERNAME_MAX_LENGTH      11  //登录名最大长度

#define SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(__CLASSNAME__)    \
\
+ (__CLASSNAME__*) sharedInstance;    \

#define SYNTHESIZE_SINGLETON_FOR_CLASS(__CLASSNAME__) \
\
+ (__CLASSNAME__ *)sharedInstance { \
static __CLASSNAME__ *shared##__CLASSNAME__ = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##__CLASSNAME__ = [[self alloc] init]; \
}); \
return shared##__CLASSNAME__; \
}
#define TT_FIX_CATEGORY_BUG(name) @interface TT_FIX_CATEGORY_BUG_##name:NSObject @end \
@implementation TT_FIX_CATEGORY_BUG_##name @end
#endif
