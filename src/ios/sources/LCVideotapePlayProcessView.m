#import "LCVideotapePlayProcessView.h"
#import "LCUIKit.h"
#import "LCToolKit.h"
@interface LCVideotapePlayProcessView ()
@property (strong,nonatomic)UISlider * silder;
@property (strong,nonatomic)UILabel * startLab;
@property (strong,nonatomic)UILabel * endLab;
@property (strong,nonatomic)NSDate * endDate;
@property (strong,nonatomic)NSDate * startDate;
@end
@implementation LCVideotapePlayProcessView
-(void)setStartDate:(NSDate *)startDate EndDate:(NSDate *)endDate{
    self.endDate = endDate;
    self.startDate = startDate;
    self.startLab.text = [self.startDate stringWithFormat:@"HH:mm:ss"];
    self.endLab.text = [self.endDate stringWithFormat:@"HH:mm:ss"];
    NSTimeInterval during = [self.endDate timeIntervalSinceDate:self.startDate];
    self.silder.maximumValue = during;
}
-(void)configFullScreenUI{
     self.backgroundColor = [UIColor dhcolor_c00];
    [self.startLab setHidden:YES];
    [self.endLab setHidden:YES];
    [self.silder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(self);
        make.centerY.mas_equalTo(self);
    }];
}
-(void)configPortraitScreenUI{
   self.backgroundColor = [UIColor dhcolor_c50];
    [self.startLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.mas_equalTo(self);
        make.width.mas_equalTo(45);
    }];
    [self.endLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.mas_equalTo(self);
        make.width.mas_equalTo(45);
    }];
    [self.silder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.startLab.mas_right).offset(5);
        make.right.mas_equalTo(self.endLab.mas_left).offset(-5);
        make.centerY.mas_equalTo(self);
    }];
}
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupView];
        self.canRefreshSlider = YES;
    }
    return self;
}
-(void)setupView{
    self.startLab = [UILabel new];
    [self addSubview:self.startLab];
    self.startLab.textAlignment = NSTextAlignmentCenter;
    self.startLab.textColor = [UIColor dhcolor_c43];
    self.startLab.adjustsFontSizeToFitWidth = YES;
    self.endLab = [UILabel new];
    self.endLab.textAlignment = NSTextAlignmentCenter;
    self.endLab.textColor = [UIColor dhcolor_c43];
    [self addSubview:self.endLab];
    self.endLab.adjustsFontSizeToFitWidth = YES;
    self.silder = [UISlider new];
    [self.silder addTarget:self action:@selector(sliderChangeValue:) forControlEvents:UIControlEventValueChanged];
    [self.silder addTarget:self action:@selector(sliderEndChangeValue:) forControlEvents:UIControlEventTouchUpOutside];
    [self.silder addTarget:self action:@selector(sliderEndChangeValue:) forControlEvents:UIControlEventTouchUpInside];
    [self.silder addTarget:self action:@selector(sliderEndChangeValue:) forControlEvents:UIControlEventTouchCancel];
    [self.silder setThumbImage:LC_IMAGENAMED(@"common_icon_slider_thumb") forState:UIControlStateNormal];
    self.silder.minimumValue = 0;
    [self addSubview:self.silder];
    [self.silder setMinimumTrackTintColor:[UIColor dhcolor_c43]];
}
-(void)sliderChangeValue:(UISlider *)slider{
    self.canRefreshSlider = NO;
    self.startLab.text = [[self.startDate dateByAddingSeconds:(NSInteger)slider.value] stringWithFormat:@"HH:mm:ss"];
    if (self.valueChangeBlock) {
        self.valueChangeBlock(self.silder.value, [self.startDate dateByAddingSeconds:self.silder.value]);
    }
}
-(void)sliderEndChangeValue:(UISlider *)slider{
    NSTimeInterval offest = [self.endDate timeIntervalSinceDate:self.startDate];
    if (self.silder.value == offest) {
        [self.silder setValue:(offest-3)];
    }
    if (self.valueChangeEndBlock) {
        self.valueChangeEndBlock(self.silder.value, [self.startDate dateByAddingSeconds:self.silder.value]);
    }
    self.canRefreshSlider = YES;
}
-(void)setCurrentDate:(NSDate *)currentDate{
    _currentDate = currentDate;
    if (self.canRefreshSlider) {
        NSTimeInterval offest = [currentDate timeIntervalSinceDate:self.startDate];
        NSLog(@"异常跳针OFF:%f",offest);
        self.startLab.text = [currentDate stringWithFormat:@"HH:mm:ss"];
        [self.silder setValue:offest];
    }
}
@end
