#import "LCDeviceVideoManager.h"
static LCDeviceVideoManager * manager = nil;
@implementation LCDeviceVideoManager
+(instancetype)manager{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [LCDeviceVideoManager new];
        manager.isPlay = NO;
        manager.pausePlay = NO;
        manager.isSD = NO;
        manager.isSoundOn = YES;
        manager.isFullScreen = NO;
        manager.isOpenCloudStage = NO;
        manager.isOpenAudioTalk = NO;
        manager.isOpenRecoding = NO;
        manager.isLockFullScreen = NO;
        manager.playSpeed = 1;
    });
    return manager;
}
- (LCChannelInfo *)currentChannelInfo {
    if (self.currentChannelIndex == -1) {
        if (self.currentDevice.channels.count == 1) {
            return self.currentDevice.channels[0];
        }
        return nil;
    }
    if (self.currentDevice.channels.count > 0) {
        LCChannelInfo *channel = self.currentDevice.channels[self.currentChannelIndex];
        return channel;
    }
    return nil;
}
-(NSString *)currentPsk{
    if (!_currentPsk || _currentPsk.length==0) {
       return self.currentDevice.deviceId;
    }
    return _currentPsk;
}
-(void)setIsFullScreen:(BOOL)isFullScreen{
    _isFullScreen = isFullScreen;
    self.getCurrentVC.navigationController.interactivePopGestureRecognizer.enabled = !isFullScreen;
}
-(void)playDoorBellSound{
    [self.audioPlayer stop];
    NSString *soundFilePath = [NSString stringWithFormat:@"%@/doorbell.mp3",[[NSBundle mainBundle] resourcePath]];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    self.audioPlayer.numberOfLoops = -1; //Infinite
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: nil];
    [self.audioPlayer play];
}
- (UIViewController *)getCurrentVC
{
    UIViewController *rootViewController = [self getViewControllerWindow].rootViewController;
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    return currentVC;
}
- (UIWindow*)getViewControllerWindow{
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [UIApplication sharedApplication].windows;
        for (UIWindow *target in windows) {
            if (target.windowLevel == UIWindowLevelNormal) {
                window = target;
                break;
            }
        }
    }
    return window;
}
- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC
{
    UIViewController *currentVC;
    if ([rootVC presentedViewController]) {
        while ([rootVC presentedViewController]) {
            rootVC = [rootVC presentedViewController];
        }
    }
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
    } else {
        currentVC = rootVC;
    }
    return currentVC;
}
- (void)setCurrentResolution:(LCCIResolutions *)currentResolution {
    _currentResolution = currentResolution;
}
@end
