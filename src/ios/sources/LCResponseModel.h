#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCResponseResultModel : NSObject
@property (strong, nonatomic) id data;
@property (strong, nonatomic) NSString *code;
@property (strong, nonatomic) NSString *msg;
@end
@interface LCResponseModel : NSObject
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) LCResponseResultModel *result;
@end
NS_ASSUME_NONNULL_END
