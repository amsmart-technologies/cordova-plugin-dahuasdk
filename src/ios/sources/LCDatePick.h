#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCDatePickResult : NSObject
@property (nonatomic) NSInteger year;
@property (nonatomic) NSInteger month;
@property (nonatomic) NSInteger weekOfMonth;
@property (nonatomic) NSInteger weekOfYear;
@property (nonatomic) NSInteger weekDay;
@property (nonatomic) NSInteger day;
@property (nonatomic) NSInteger hour;
@property (nonatomic) NSInteger minute;
@property (nonatomic) NSInteger second;
@end
@interface LCDatePick : UIView
#pragma mark - display
+(LCDatePick*(^)(void))initialize;
-(LCDatePick *(^)(void))start;
-(LCDatePick *(^)(void))dismiss;
#pragma mark - config
-(LCDatePick*(^)(void))addYear;
-(LCDatePick*(^)(void))addMonth;
-(LCDatePick*(^)(void))addWeekOfYear;
-(LCDatePick*(^)(void))addWeekOfMonth;
-(LCDatePick*(^)(void))addWeekDay;
-(LCDatePick*(^)(void))addDay;
-(LCDatePick*(^)(void))addHour;
-(LCDatePick*(^)(void))addMinute;
-(LCDatePick*(^)(void))addSecond;
-(LCDatePick*(^)(NSString * cancleTitle))cancleTitle;
-(LCDatePick*(^)(NSString * confirmTitle))confirmTitle;
-(LCDatePick*(^)(NSString * title))title;
-(LCDatePick*(^)(NSInteger min))minYear;
-(LCDatePick*(^)(NSInteger max))maxYear;
#pragma mark - action
-(LCDatePick*)cancleHandle:(void(^)(void))resultBlock;
-(LCDatePick*)confirmHandle:(void(^)(LCDatePickResult * result))resultBlock;
-(LCDatePick*)dismissHandle:(void(^)(void))resultBlock;
@end
NS_ASSUME_NONNULL_END
