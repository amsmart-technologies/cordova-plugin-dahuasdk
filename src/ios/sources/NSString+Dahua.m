#import "NSString+Dahua.h"
#import "NSString+MD5.h"
@implementation NSString(Dahua)
- (NSString*)lc_T {
    return NSLocalizedStringFromTable(self, @"LCLanguage", nil);
}
+ (NSString *_Nonnull)dh_currentLanguageCode
{
    NSString *currentLanguage = [NSLocale preferredLanguages].firstObject;
    if ([currentLanguage hasPrefix:@"zh"]) {
        currentLanguage = [currentLanguage containsString:@"zh-Hant"] ? @"zh-TW" : @"zh-CN";
    }
    return currentLanguage;
}
- (BOOL)isSupportCurrentLanguage {
	NSString *currentLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
	NSArray *supportLanguages = @[@"en", @"zh-Hans", @"zh-Hant", @"ko", @"es", @"vi", @"pt", @"nl", @"cs", @"bg", @"de", @"ru", @"it", @"sr", @"da", @"nb", @"sv", @"fi", @"tr", @"pl", @"hu", @"fr", @"no", @"ja", @"tw", @"sk", @"th", @"ro", @"ar", @"uk"];
	for (NSString *supportLanguage in supportLanguages) {
		if ([currentLanguage hasPrefix:supportLanguage]) {
			return true;
		}
	}
	return false;
}
+ (NSString *)isoLocalizeLanguageString {
	NSLocale *local = [NSLocale autoupdatingCurrentLocale];
	NSString *area = [local objectForKey:NSLocaleCountryCode];
	NSString *language = [NSLocale preferredLanguages].firstObject;
	NSString *currentLanguage = @"";
	if ([language containsString:@"zh-Hant"]) {
		currentLanguage = @"zh_TW";
	} else if(area != nil) {
		currentLanguage = [language componentsSeparatedByString:@"-"].firstObject;
		currentLanguage = [currentLanguage stringByAppendingFormat:@"_%@",area];
	} else {
		currentLanguage = language;
	}
	return currentLanguage;
}
+ (BOOL)dh_pwdVerifiers:(NSString *)password {
    int numOfType = [self passwordStrength:password];
    if (numOfType < 2) {
        return false;
    }
    if ([self checkInvalidPassword:password]) {
        return false;
    }
    if ([self checkSameCharacter:password]) {
        return false;
    }
    return true;
}
+ (int)passwordStrength:(NSString *)pasword
{
    int nRet = 0;
    NSString *smallChar = @"abcdefghijklmnopqrstuvwxyz";
    NSString *bigChar = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSString *numberChar = @"1234567890";
    NSString *otherChar = @" -/:;()$&@\".,?!'[]{}#%^*+=_\\|~<>.,?!'";
    NSString *urlString = pasword;
    NSString *strType[4] = {smallChar, bigChar, numberChar, otherChar};
    for (int j=0;j<4;j++){
        for (int i = 0; i < [urlString length]; i++){
            NSRange r ;
            r.length = 1;
            r.location = i;
            NSString* c = [urlString substringWithRange:r];
            if ([strType[j] rangeOfString:c].location != NSNotFound){
                nRet++;
                break;
            }
        }
    }
    return nRet;
}
+ (BOOL)checkInvalidPassword:(NSString *)password {
    if (password.length > 0) {
        if ([[self initializeInvalidPassword] containsObject:password]) {
            return YES;
        }
    }
    return NO;
}
+ (NSArray *)initializeInvalidPassword {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"InvalidPasswordList" ofType:@"plist"];
    NSArray *list = [[NSArray alloc] initWithContentsOfFile:filePath];
    return list;
}
+ (BOOL)checkSameCharacter:(NSString *)password {
    if (password.length > 0) {
        NSString *checkerString = @"^.*(.)\\1{5}.*$";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", checkerString];
        return [predicate evaluateWithObject:password];
    }
    return NO;
}
- (NSString *_Nullable)lc_stringByTrimmingCharactersInSet:(NSCharacterSet *_Nullable)characterSet
{
    NSString *newStr = [self stringByTrimmingCharactersInSet:characterSet];
    return newStr;
}
@end
