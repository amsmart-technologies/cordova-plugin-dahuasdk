#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,TipsType) {
    TipsTypeNone,               
    TipsTypeDeviceShareNone,    
    TipsTypeMessageNone,        
    TipsTypeDeviceNone,         
    TipsTypeDeviceAPNone,       
    TipsTypeAlarmNone,          
    TipsTypeWifiNone,           
    TipsTypeCloudNone,          
    TipsTypeDeviceOffline,      
    TipsTypeLiveListNone,       
    TipsTypeCommentListNone,    
    TipsTypeFail,               
    TipsTypeNetError,           
    TipsTypeUpdate,             
    TipsTypeNoVideoMsg,         
	TipsTypeNoVideotape,        
	TipsTypeNoAuthority,        
	TipsTypeNoSdCard,           
    TipsTypeNoCollection,       
    TipsTypeNoOneDayVideo,      
    TipsTypeNoFriendMsg,        
    TipsTypeNoSearchResult,     
    TipsTypeVideoMsgNone,       
    TipsTypeDefault,
};
@interface UIScrollView(Tips)
@property (strong, nonatomic) UIButton *tapBtn;
@property (strong, nonatomic) UIImageView *tapImageView;
- (void)lc_addTipsView:(TipsType)type;
- (void)lc_clearTipsView;
- (UIView *)lc_getTipsView:(TipsType)type;
- (void)lc_addTipsViewModifyFrame;
- (void)lc_setEmyptImageName:(NSString *)imageName andDescription:(id )description;
- (void)lc_setEmyptImageName:(NSString *)imageName andDescription:(id )description ClickedBlock:(void(^)(void))block;
- (void)lc_setEmyptImageName:(NSString *)imageName andDescription:(id )description andButtonTitle:(NSString *)buttonTitle withButtonClickedBlock:(void(^)(void))block;
- (void)lc_setEmyptImageName:(NSString *)imageName emptyTitle:(NSString *)title emptyDescription:(NSString *)description;
- (void)lc_setEmyptImageName:(NSString *)imageName emptyDescription:(NSString *)description customView:(UIView *)customView;
- (void)lc_setEmptyImage:(UIImage *)image description:(id)description;
- (void)lc_setEmyptImageName:(NSString *)imageName andDescription:(id )description userInteractionEnabled:(BOOL)enable;
@end
