#import "DHNetWorkHelper.h"
#import "DHAlertController.h"
#import "DHPubDefine.h"
#import "LCProgressHUD.h"
#import "NSString+Dahua.h"
#import <SystemConfiguration/CaptiveNetwork.h>
@interface DHNetWorkHelper()
@property (nonatomic, strong) DHAlertController	*alertVc;
@end
@implementation DHNetWorkHelper
+ (instancetype)sharedInstance {
	static dispatch_once_t once;
	static DHNetWorkHelper *sharedInstance;
	dispatch_once(&once, ^ {
		sharedInstance = [[self alloc] init];
	});
	return sharedInstance;
}
- (instancetype)init
{
	if (self = [super init])
	{
		_bShouldShowFlowTip = NO; 
		_bShouldShowFlowTipWhenLoadVideo = YES;
		_bShouldShowFlowTipWhenVideoShare  = YES;
		_interfaceQueue = dispatch_queue_create("interfaceQueue", DISPATCH_QUEUE_SERIAL);
		CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(),
										NULL,
										systemNetworkChanged,
										CFSTR("com.apple.system.config.network_change"),
										(__bridge const void *)(self),
										CFNotificationSuspensionBehaviorHold);
	}
	return self;
}
- (NSString *)networkType {
    if (_emNetworkStatus == AFNetworkReachabilityStatusReachableViaWiFi) {
        return @"WiFi";
    }else if (_emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN) {
        return @"WWAN";
    }
    return @"Unknown";
}
static NSString *dh_lastWifiName = nil;
void systemNetworkChanged(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo)
{
	dispatch_queue_t queue = DHNetWorkHelper.sharedInstance.interfaceQueue;
	dispatch_async(queue, ^{
		NSString *wifiName = @"";
		NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
		for (NSString *ifnam in ifs) {
			NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
			NSLog(@"systemNetworkChanged wifiInfo = %@", info);
			if (info[@"SSID"]) {
				wifiName = info[@"SSID"];
			}
		}
		NSLog(@"28614-*-* systemNetworkChanged wifi name: %@ - %@", dh_lastWifiName, wifiName);
		if(wifiName.length && [dh_lastWifiName isEqualToString:wifiName]){
			return;
		}
		dispatch_async(dispatch_get_main_queue(), ^{
			NSLog(@"28614-*-* LCNotificationWifiNetWorkChange111111  %ld",[DHNetWorkHelper sharedInstance].emNetworkStatus);
			NSLog(@"28614-*-* SSID  %@",wifiName);
			if (wifiName.length > 0) {
				[[DHNetWorkHelper sharedInstance] configureByStatus:AFNetworkReachabilityStatusReachableViaWiFi];
			}
			[[NSNotificationCenter defaultCenter] postNotificationName:@"LCNotificationWifiNetWorkDidSwitch" object:wifiName];
		});
		dh_lastWifiName = wifiName;
	});
}
- (NSString *)fetchSSIDInfo
{
    NSArray *ifs = (__bridge id)CNCopySupportedInterfaces();
    NSLog(@"%s: Supported interfaces: %@", __func__, ifs);
    id info = nil;
    for (NSString *ifnam in ifs)
    {
        info = (__bridge id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info && [info count])
        {
            break;
        }
    }
    NSString *ssid = [[info objectForKey:@"SSID"] copy];
    return ssid;
}
- (void)checkNetwork
{
	AFNetworkReachabilityManager *reachalilityManager = [AFNetworkReachabilityManager sharedManager];
	self.emNetworkStatus = reachalilityManager.networkReachabilityStatus;
	__weak typeof(self) weakSelf = self;
	static dispatch_once_t predicate;
	dispatch_once(&predicate, ^{
		[reachalilityManager startMonitoring];
		[reachalilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status){
			[weakSelf configureByStatus:status];
		}];
	});
}
- (void)configureByStatus:(AFNetworkReachabilityStatus)status {
	NSLog(@"DHNetWorkHelper::Before Network Sattus:%ld", (long)self.emNetworkStatus);
	if (self.emNetworkStatus == status) {
		return;
	}
	self.emNetworkStatus = status;
	BOOL firstLaunched = [[NSUserDefaults standardUserDefaults] boolForKey:@"DHNetWorkTipHaveLaunched"];
	NSLog(@"DHNetWorkHelper::Current Network Sattus:%ld", (long)status);
	if (status == AFNetworkReachabilityStatusReachableViaWWAN)
	{
		_bShouldShowFlowTip = NO;
		_bShouldShowFlowTipWhenLoadVideo = YES;
		_bShouldShowFlowTipWhenVideoShare = YES;
	}
	else if(status == AFNetworkReachabilityStatusReachableViaWiFi)
	{
		_bShouldShowFlowTip = NO;
		_bShouldShowFlowTipWhenLoadVideo = NO;
		_bShouldShowFlowTipWhenVideoShare = NO;
		[self dissmissAlert:YES];
	}
	else if(status == AFNetworkReachabilityStatusNotReachable && firstLaunched == YES)
	{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LCNotificationWifiNoAvailableNetWork" object:@""];
		[self dissmissAlert:YES];
	}
	[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DHNetWorkTipHaveLaunched"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"LCNotificationWifiNetWorkChange" object:@""];
}
#pragma mark - 检查当前网络状态下，是否允许播放视频
- (BOOL)isPermittedToPlayVideoWithTip:(NSString *)tip
{
	BOOL permittedToPlay = YES;
	if ([DHNetWorkHelper sharedInstance].bShouldShowFlowTip &&
		[DHNetWorkHelper sharedInstance].emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN)
	{
		permittedToPlay = NO;
	}
	else if ([DHNetWorkHelper sharedInstance].emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN)
	{
		permittedToPlay = YES;
		if (tip.length) {
			[LCProgressHUD showMsg:tip];
		}
		[self dissmissAlert:YES];
	}
	return permittedToPlay;
}
- (BOOL)showPermittedToVC:(UIViewController *)vc playVideoAlert:(dispatch_block_t)confirmAction withTip:(NSString *)tip
{
	if ([DHNetWorkHelper sharedInstance].bShouldShowFlowTip &&
		[DHNetWorkHelper sharedInstance].emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN)
	{
		if ([vc.presentedViewController isKindOfClass:[UIAlertController class]]) {
			return NO;
		}
		[self dissmissAlert:NO];
		_alertVc = [DHAlertController showInViewController:vc
													 title:@"mobile_common_play_on_cellular_data".lc_T
												   message:@"mobile_common_allow_play_on_cellular_data".lc_T
										 cancelButtonTitle:@"common_cancel".lc_T
										 otherButtonTitles: @[@"mobile_common_always".lc_T, @"mobile_common_once".lc_T]
												   handler:^(NSInteger index)
					{
						if (index == 1) {
							[DHNetWorkHelper sharedInstance].bShouldShowFlowTip = NO;
							if (confirmAction) {
								confirmAction();
							}
						}
						else if(index == 2)
						{
							if (confirmAction) {
								confirmAction();
							}
						}
						[self dissmissAlert:YES];
					}];
		return NO;
	}
	else if ([DHNetWorkHelper sharedInstance].emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN &&
			 tip.length && [MBProgressHUD HUDForView:DH_KEY_WINDOW] == nil)
	{
		[LCProgressHUD showMsg:tip];
		[self dissmissAlert:YES];
	}
	return YES;
}
- (BOOL)showPermittedToPlayVideoAlert:(dispatch_block_t)confirmAction withTip:(NSString *)tip
{
	if ([DHNetWorkHelper sharedInstance].bShouldShowFlowTip &&
		[DHNetWorkHelper sharedInstance].emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN)
	{
		UIViewController *topVc = [DHAlertController topPresentOrRootController];
		if ([topVc.presentedViewController isKindOfClass:[UIAlertController class]]) {
			return NO;
		}
		[self dissmissAlert:NO];
		_alertVc = [DHAlertController showWithTitle:@"mobile_common_play_on_cellular_data".lc_T
											message:@"mobile_common_allow_play_on_cellular_data".lc_T
								  cancelButtonTitle:@"common_cancel".lc_T
								  otherButtonTitles:@[@"mobile_common_always".lc_T,@"mobile_common_once".lc_T]
											handler:^(NSInteger index)
					{
						if (index == 1) {
							[DHNetWorkHelper sharedInstance].bShouldShowFlowTip = NO;							
							if (confirmAction) {
								confirmAction();
							}
						}
						else if(index == 2)
						{
							if (confirmAction) {
								confirmAction();
							}
						}
						[self dissmissAlert:YES];
					}];
		return NO;
	}
	else if ([DHNetWorkHelper sharedInstance].emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN &&
			 tip.length && [MBProgressHUD HUDForView:DH_KEY_WINDOW] == nil)
	{
		[LCProgressHUD showMsg:tip];
		[self dissmissAlert:YES];
	}
	return YES;
}
- (void)showPermittedToDownloadAlert:(dispatch_block_t)confirmAction withTip:(NSString *)tip
{
	if ([DHNetWorkHelper sharedInstance].bShouldShowFlowTip &&
		[DHNetWorkHelper sharedInstance].emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN)
	{
		[self dissmissAlert:NO];
		_alertVc = [DHAlertController showWithTitle:@"mobile_common_play_on_cellular_data".lc_T
											message:@"mobile_common_allow_play_on_cellular_data".lc_T
								  cancelButtonTitle:@"common_cancel".lc_T
								  otherButtonTitles:@[@"mobile_common_always".lc_T,@"mobile_common_once".lc_T]
											handler:^(NSInteger index)
					{
						if (index == 1) {
							[DHNetWorkHelper sharedInstance].bShouldShowFlowTip = NO;							
							if (confirmAction) {
								confirmAction();
							}
						}
						else if(index == 2)
						{
							if (confirmAction) {
								confirmAction();
							}
						}
						[self dissmissAlert:YES];
					}];
	}
	else if ([DHNetWorkHelper sharedInstance].emNetworkStatus == AFNetworkReachabilityStatusReachableViaWWAN &&
			 tip.length && [MBProgressHUD HUDForView:DH_KEY_WINDOW] == nil)
	{
		[LCProgressHUD showMsg:tip];
		[self dissmissAlert:YES];
	}
}
- (void)dissmissAlert:(BOOL)animated
{
	if (_alertVc) {
		[_alertVc dismissViewControllerAnimated:animated completion:nil];
		_alertVc = nil;
	}
}
- (void)dissmissAlert {
	[self dissmissAlert:YES];
}
@end
