#import <UIKit/UIKit.h>
#import "LCUIKit.h"
NS_ASSUME_NONNULL_BEGIN
typedef void(^videoDateControlBlock)(NSDate * date);
@interface LCVideotapeListDateControl : UIView
@property (strong, nonatomic)LCButton *lastDay;
@property (strong, nonatomic)UILabel *dateLab;
@property (strong, nonatomic)LCButton *nextDay;
@property (copy, nonatomic) videoDateControlBlock result;
@property (strong, nonatomic) NSDate *nowDate;
@property (nonatomic) BOOL enable;
@end
NS_ASSUME_NONNULL_END
