#import "LCLivePreviewPresenter.h"
#import "LCVideoHistoryView.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCLivePreviewPresenter (VideotapeList)
@property (strong, nonatomic) LCVideoHistoryView *historyView;
-(void)loadCloudVideotape;
-(void)loadLocalVideotape;
@end
NS_ASSUME_NONNULL_END
