#import <Foundation/Foundation.h>
@interface LCPermissionHelper : NSObject
+ (void)requestAudioPermission:(void (^)(BOOL granted))completion;
+ (void)requestCameraPermission:(void (^)(BOOL granted))completion;
+ (void)requestAlbumPermission:(void (^)(BOOL granted))completion;
- (void)requestAlwaysLocationPermissions:(BOOL)always completion:(void (^)(BOOL granted))completion;
@end
