#import <UIKit/UIKit.h>
#import "LCPTZPanel.h"
#import "UIView+LCDraggable.h"
typedef enum : NSUInteger {
    LCPTZControlSupportFour,
    LCPTZControlSupportEight
} LCPTZControlSupportDirection;
typedef void(^PTZClose)(void);
NS_ASSUME_NONNULL_BEGIN
@interface LCPTZControlView : UIView
-(instancetype)initWithDirection:(LCPTZControlSupportDirection)direction;
@property (copy, nonatomic) PTZClose close;
@property (strong, nonatomic) LCPTZPanel * panel;
@end
NS_ASSUME_NONNULL_END
