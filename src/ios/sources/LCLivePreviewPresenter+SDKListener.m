#import "LCLivePreviewPresenter+SDKListener.h"
#import "UIImageView+Surface.h"
#import "LCLivePreviewPresenter+Control.h"
#import "LCBaseDefine.h"
#import "LCProgressHUD.h"
#import "NSString+Dahua.h"
@implementation LCLivePreviewPresenter (SDKListener)
- (void)onReceiveData:(NSInteger)len Index:(NSInteger)index {
    NSLog(@"");
}
- (void)onPlayerResult:(NSString *)code Type:(NSInteger)type Index:(NSInteger)index {
    weakSelf(self);
    NSLog(@"LIVE_PLAY-CODE:%@,TYPE:%ld", code, type);
    dispatch_async(dispatch_get_main_queue(), ^{
        if (99 == type) {
            if ([code isEqualToString:@"-1000"]) {
                self.videoManager.isPlay = NO;
                [self showErrorBtn];
            } else if (![code isEqualToString:@"0"]) {
                self.videoManager.isPlay = NO;
                [self showErrorBtn];
            } else {
            }
        }
        if (type == 5) {
            if ([code integerValue] == STATE_LCHTTP_KEY_ERROR) {
                [self showErrorBtn];
                if (![self.videoManager.currentPsk isEqualToString:self.videoManager.currentDevice.deviceId]) {
                    self.videoManager.currentPsk = @"";
                    [self hideErrorBtn];
                    [self onPlay:nil];
                } else {
                    [self showPSKAlert];
                }
            }
            if ([code integerValue] != 0 && [code integerValue] != STATE_LCHTTP_OK) {
                [self showErrorBtn];
            }
        }
        if (type == 0) {
            self.videoManager.playStatus = [code integerValue];
            if ([RTSP_Result_String(STATE_RTSP_DESCRIBE_READY) isEqualToString:code]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                });
                return;
            }
            if ([RTSP_Result_String(STATE_RTSP_PLAY_READY) isEqualToString:code]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                });
                return;
            }
            if ([RTSP_Result_String(STATE_RTSP_KEY_MISMATCH) isEqualToString:code]) {
                [weakself showErrorBtn];
                [weakself showPSKAlert];
            } else {
                [weakself showErrorBtn];
            }
        }
    });
    return;
}
- (void)onPlayBegan:(NSInteger)index {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.videoManager.playStatus = 1001;
        [self saveThumbImage];
        [self hideVideoLoadImage];
    });
}
- (void)onTalkResult:(NSString *)error TYPE:(NSInteger)type
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"开启对讲回调error = %@, type = %ld", error, (long)type);
        weakSelf(self);
        [LCProgressHUD hideAllHuds:nil];
        if (99 == type) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakself.videoManager.isOpenAudioTalk = NO;
                [LCProgressHUD showMsg:@"play_module_video_preview_talk_failed".lc_T];
            });
            return;
        }
        if (nil != error && [RTSP_Result_String(STATE_RTSP_DESCRIBE_READY) isEqualToString:error]) {
            dispatch_async(dispatch_get_main_queue(), ^{
            });
            return;
        }
        if (nil != error && [RTSP_Result_String(STATE_RTSP_PLAY_READY) isEqualToString:error]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.videoManager.isOpenAudioTalk = YES;
                [LCProgressHUD showMsg:@"device_mid_open_talk_success".lc_T];
            });
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [LCProgressHUD showMsg:@"play_module_video_preview_talk_failed".lc_T];
            weakself.videoManager.isOpenAudioTalk = NO;
        });
    });
}
- (void)saveThumbImage {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,
                                                         NSUserDomainMask, YES);
    NSString *libraryDirectory = [paths objectAtIndex:0];
    NSString *myDirectory =
        [libraryDirectory stringByAppendingPathComponent:@"lechange"];
    NSString *picDirectory =
        [myDirectory stringByAppendingPathComponent:@"picture"];
    NSDateFormatter *dataFormat = [[NSDateFormatter alloc] init];
    [dataFormat setDateFormat:@"yyyyMMddHHmmss"];
    NSString *strDate = [dataFormat stringFromDate:[NSDate date]];
    NSString *datePath = [picDirectory stringByAppendingPathComponent:strDate];
    NSString *picPath = [datePath stringByAppendingString:@".jpg"];
    NSLog(@"test jpg name[%@]\n", picPath);
    NSFileManager *fileManage = [NSFileManager defaultManager];
    NSError *pErr;
    BOOL isDir;
    if (NO == [fileManage fileExistsAtPath:myDirectory isDirectory:&isDir]) {
        [fileManage createDirectoryAtPath:myDirectory
              withIntermediateDirectories:YES
                               attributes:nil
                                    error:&pErr];
    }
    if (NO == [fileManage fileExistsAtPath:picDirectory isDirectory:&isDir]) {
        [fileManage createDirectoryAtPath:picDirectory
              withIntermediateDirectories:YES
                               attributes:nil
                                    error:&pErr];
    }
    [self.playWindow snapShot:picPath];
    UIImage *image = [UIImage imageWithContentsOfFile:picPath];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIImageView new] lc_storeImage:image ForDeviceId:self.videoManager.currentDevice.deviceId ChannelId:self.videoManager.currentChannelInfo.channelId];
    });
}
@end
