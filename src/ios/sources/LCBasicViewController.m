#import "LCBasicViewController.h"
#import "DHPubDefine.h"
@interface LCBasicViewController ()<UIGestureRecognizerDelegate>
@end
@implementation LCBasicViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor dhcolor_c54];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushToLogin) name:@"NEEDLOGIN" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onResignActive:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    });
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewTap:(UITapGestureRecognizer *)tap {
    [self.view endEditing:YES];
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}
- (void)fixlayoutConstant:(UIView *)view {
    for (NSLayoutConstraint *constraint in view.constraints) {
        constraint.constant = constraint.constant / 375.0 * SCREEN_WIDTH;
    }
    for (UIView *subview in view.subviews) {
        [self fixlayoutConstant:subview];
    }
}
- (BOOL)shouldAutorotate {
    return NO;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (void)onActive:(id)sender{
}
- (void)onResignActive:(id)sender{
}
@end
