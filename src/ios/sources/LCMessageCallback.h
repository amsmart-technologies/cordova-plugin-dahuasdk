#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCMessageCallback : NSObject
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *callbackUrl;
@property (strong, nonatomic) NSString *callbackFlag;
@end
NS_ASSUME_NONNULL_END