#import "LCVideotapePlayerPersenter.h"
#import "LCVideotapePlayerPersenter+SDKListener.h"
#import "LCVideotapePlayerPersenter+Control.h"
#import "LCBaseDefine.h"
#import <KVOController/KVOController.h>
#import "NSString+Dahua.h"
#import <SDWebImage/SDWebImage.h>
@interface LCVideotapePlayerPersenter ()
@property (strong, nonatomic) NSMutableArray *middleControlList;
@property (strong, nonatomic) NSMutableArray *bottomControlList;
@end
@implementation LCVideotapePlayerPersenter
- (void)stopDownload {
    [self.videoManager cancleDownload:self.videoManager.currentDownloadInfo.recordId];
}
- (NSMutableArray *)getMiddleControlItems {
    NSMutableArray *middleControlList = [NSMutableArray array];
    [middleControlList addObject:[self getItemWithType:LCVideotapePlayerControlPlay] ];
    [middleControlList addObject:[self getItemWithType:LCVideotapePlayerControlTimes] ];
    [middleControlList addObject:[self getItemWithType:LCVideotapePlayerControlVoice] ];
    [middleControlList addObject:[self getItemWithType:LCVideotapePlayerControlFullScreen]];
    self.middleControlList = middleControlList;
    return middleControlList;
}
- (NSMutableArray *)getBottomControlItems {
    NSMutableArray *bottomControlList = [NSMutableArray array];
    [bottomControlList addObject:[self getItemWithType:LCVideotapePlayerControlSnap] ];
    [bottomControlList addObject:[self getItemWithType:LCVideotapePlayerControlPVR]];
    self.bottomControlList = bottomControlList;
    return bottomControlList;
}
- (LCButton *)getItemWithType:(LCVideotapePlayerControlType)type {
    weakSelf(self);
    LCButton *item = [LCButton lcButtonWithType:LCButtonTypeCustom];
    item.tag = type;
    switch (type) {
        case LCVideotapePlayerControlPlay: {
            [item setImage:LC_IMAGENAMED(@"live_video_icon_play") forState:UIControlStateNormal];
            item.touchUpInsideblock = ^(LCButton *_Nonnull btn) {
                [weakself onPlay:btn];
            };
            [item.KVOController observe:self.videoManager keyPath:@"isPlay" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                if ([change[@"new"] boolValue]) {
                    [item setImage:LC_IMAGENAMED(@"live_video_icon_pause") forState:UIControlStateNormal];
                } else {
                    [item setImage:LC_IMAGENAMED(@"live_video_icon_play") forState:UIControlStateNormal];
                }
            }];
        };
            break;
        case LCVideotapePlayerControlTimes: {
            [item setImage:LC_IMAGENAMED(@"video_1x") forState:UIControlStateNormal];
            item.touchUpInsideblock = ^(LCButton *_Nonnull btn) {
                [weakself onSpeed:btn];
            };
            [item.KVOController observe:self.videoManager keyPath:@"playSpeed" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                NSInteger speed = [change[@"new"] integerValue];
                CGFloat speedTime = 1.0;
                if (speed == 1) {
                    speedTime = 1.0;
                    [item setImage:LC_IMAGENAMED(@"video_1x") forState:UIControlStateNormal];
                } else if (speed == 2) {
                    speedTime = 4.0;
                    [item setImage:LC_IMAGENAMED(@"video_4x") forState:UIControlStateNormal];
                } else if (speed == 3) {
                    speedTime = 8.0;
                    [item setImage:LC_IMAGENAMED(@"video_8x") forState:UIControlStateNormal];
                } else if (speed == 4) {
                    speedTime = 16.0;
                    [item setImage:LC_IMAGENAMED(@"video_16x") forState:UIControlStateNormal];
                } else if (speed == 5) {
                    speedTime = 32.0;
                    [item setImage:LC_IMAGENAMED(@"video_32x") forState:UIControlStateNormal];
                }
                [self.playWindow setPlaySpeed:speedTime];
            }];
            [item.KVOController observe:self.videoManager keyPath:@"isPlay" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                item.enabled = NO;
            }];
            [item.KVOController observe:self.videoManager keyPath:@"playStatus" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                if ([change[@"new"] integerValue] == 1001) {
                    item.enabled = YES;
                }
            }];
        };
            break;
        case LCVideotapePlayerControlVoice: {
            [item setImage:LC_IMAGENAMED(@"live_video_icon_sound_on") forState:UIControlStateNormal];
            [item.KVOController observe:self.videoManager keyPath:@"isSoundOn" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                if ([change[@"new"] boolValue]) {
                    [item setImage:LC_IMAGENAMED(@"live_video_icon_sound_on") forState:UIControlStateNormal];
                } else {
                    [item setImage:LC_IMAGENAMED(@"live_video_icon_sound_off") forState:UIControlStateNormal];
                }
            }];
            [item.KVOController observe:self.videoManager keyPath:@"isPlay" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                item.enabled = NO;
            }];
            [item.KVOController observe:self.videoManager keyPath:@"playStatus" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                if ([change[@"new"] integerValue] == 1001) {
                    item.enabled = YES;
                }
            }];
            item.touchUpInsideblock = ^(LCButton *_Nonnull btn) {
                [weakself onAudio:btn];
            };
        }
        break;
        case LCVideotapePlayerControlFullScreen: {
            [item setImage:LC_IMAGENAMED(@"live_video_icon_fullscreen") forState:UIControlStateNormal];
            item.touchUpInsideblock = ^(LCButton *_Nonnull btn) {
                [weakself onFullScreen:btn];
            };
        }
        break;
        case LCVideotapePlayerControlSnap: {
            [item setImage:LC_IMAGENAMED(@"play_module_livepreview_icon_screenshot") forState:UIControlStateNormal];
            item.enabled = NO;
            [item.KVOController observe:self.videoManager keyPath:@"isPlay" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                item.enabled = NO;
            }];
            [item.KVOController observe:self.videoManager keyPath:@"playStatus" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                if ([change[@"new"] integerValue] == 1001) {
                    item.enabled = YES;
                }
            }];
            item.touchUpInsideblock = ^(LCButton *_Nonnull btn) {
                [weakself onSnap:btn];
            };
        }
        break;
        case LCVideotapePlayerControlPVR: {
            [item setImage:LC_IMAGENAMED(@"play_module_livepreview_icon_video") forState:UIControlStateNormal];
            item.enabled = NO;
            [item.KVOController observe:self.videoManager keyPath:@"isOpenRecoding" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                if ([change[@"new"] boolValue]) {
                    [item setImage:LC_IMAGENAMED(@"play_module_livepreview_icon_video_ing") forState:UIControlStateNormal];
                } else {
                    [item setImage:LC_IMAGENAMED(@"play_module_livepreview_icon_video") forState:UIControlStateNormal];
                }
            }];
            [item.KVOController observe:self.videoManager keyPath:@"isPlay" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                item.enabled = NO;
            }];
            [item.KVOController observe:self.videoManager keyPath:@"playStatus" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                if ([change[@"new"] integerValue] == 1001) {
                    item.enabled = YES;
                }
            }];
            item.touchUpInsideblock = ^(LCButton *_Nonnull btn) {
                [weakself onRecording:btn];
            };
        }
        break;
        case LCVideotapePlayerControlDownload: {
            [item setImage:LC_IMAGENAMED(@"video_icon_download") forState:UIControlStateNormal];
            [item setTitle:@"mobile_common_data_download".lc_T forState:UIControlStateNormal];
            [item setTitleColor:[UIColor dhcolor_c51] forState:UIControlStateNormal];
            if (self.videoManager.localVideotapeInfo) {
                item.enabled = NO;
            }
            [self.KVOController observe:self.videoManager keyPath:@"isFullScreen" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                if ([change[@"new"] boolValue]) {
                    item.hidden = YES;
                } else {
                    item.hidden = self.videoManager.cloudVideotapeInfo == nil ? YES : NO;
                }
            }];
            [item.KVOController observe:self.videoManager keyPath:@"downloadQueue" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
                LCVideotapeDownloadInfo *info = [self.videoManager currentDownloadInfo];
                if (self.videoManager.localVideotapeInfo || ![info.recordId isEqualToString:self.videoManager.currentVideotapeId]) {
                    return;
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (info.donwloadStatus == LCVideotapeDownloadStatusEnd) {
                        item.selected = NO;
                        [item setTitle:@"mobile_common_data_download_success".lc_T forState:UIControlStateNormal];
                        [item setImage:[UIImage new] forState:UIControlStateNormal];
                    } else if (info.donwloadStatus != LCVideotapeDownloadStatusBegin && info.donwloadStatus != LCVideotapeDownloadStatusPartDownload) {
                        item.selected = YES;
                        [item setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
                        [item setTitle:@"mobile_common_data_download_fail".lc_T forState:UIControlStateNormal];
                        [item setImage:[UIImage new] forState:UIControlStateNormal];
                    }
                });
            }];
            item.touchUpInsideblock = ^(LCButton *_Nonnull btn) {
                [weakself.videoManager startDeviceDownload];
                btn.enabled = NO;
            };
        }
        break;
        default:
            break;
    }
    return item;
}
- (LCOpenSDK_PlayWindow *)playWindow {
    if (!_playWindow) {
        _playWindow = [[LCOpenSDK_PlayWindow alloc] initPlayWindow:CGRectMake(50, 50, 30, 30) Index:0];
        [_playWindow setSurfaceBGColor:[UIColor blackColor]];
        [_playWindow setWindowListener:self];
    }
    return _playWindow;
}
- (LCDeviceVideotapePlayManager *)videoManager {
    if (!_videoManager) {
        _videoManager = [LCDeviceVideotapePlayManager manager];
    }
    return _videoManager;
}
- (void)loadStatusView {
    UIView *tempView = [self.playWindow getWindowView];
    UIImageView *defaultImageView = [UIImageView new];
    // [defaultImageView sd_setImageWithURL:[NSURL URLWithString:self.videoManager.currentDevice.channels[self.videoManager.currentChannelIndex].picUrl] placeholderImage:LC_IMAGENAMED(@"common_defaultcover_big")];
    [tempView addSubview:defaultImageView];
    [defaultImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.mas_equalTo(tempView);
    }];
    [defaultImageView.KVOController observe:self.videoManager keyPath:@"isPlay" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
        defaultImageView.hidden = NO;
    }];
    [defaultImageView.KVOController observe:self.videoManager keyPath:@"pausePlay" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
        if ([change[@"new"] boolValue]) {
            defaultImageView.hidden = YES;
        }
    }];
    [defaultImageView.KVOController observe:self.videoManager keyPath:@"playStatus" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([change[@"new"] integerValue] == 1001) defaultImageView.hidden = YES;
        });
    }];
    self.loadImageview = [UIImageView new];
    self.loadImageview.contentMode = UIViewContentModeCenter;
    [tempView addSubview:self.loadImageview];
    [self.loadImageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.mas_equalTo(tempView);
    }];
}
- (void)onActive:(id)sender {
}
- (void)onResignActive:(id)sender {
    if (self.playWindow) {
        [self.playWindow stopRtspReal:YES];
        self.videoManager.isPlay = NO;
        [self.playWindow stopAudio];
    }
}
- (void)checkDownloadStatus:(LCButton *)btn DonwloadStatus:(LCVideotapeDownloadState)status {
    if (status == -1) {
        [btn setImage:LC_IMAGENAMED(@"video_icon_download") forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor dhcolor_c54]];
        [btn setBorderWithStyle:LC_BORDER_DRAW_TOP borderColor:[UIColor dhcolor_c53] borderWidth:1];
    }
}
- (void)showVideoLoadImage {
    self.loadImageview.hidden = NO;
    [self.loadImageview loadGifImageWith:@[@"video_waiting_gif_1", @"video_waiting_gif_2", @"video_waiting_gif_3", @"video_waiting_gif_4"] TimeInterval:0.3 Style:LCIMGCirclePlayStyleCircle];
}
- (void)hideVideoLoadImage {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.loadImageview.hidden = YES;
        [self.loadImageview releaseImgs];
    });
}
- (void)configBigPlay {
    UIView *tempView = [self.playWindow getWindowView];
    self.errorBtn = [LCButton lcButtonWithType:LCButtonTypeVertical];
    [self.errorBtn setImage:LC_IMAGENAMED(@"videotape_icon_replay") forState:UIControlStateNormal];
    [self.container.view addSubview:self.errorBtn];
    [self.errorBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(tempView.mas_centerX);
        make.centerY.mas_equalTo(tempView.mas_centerY);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(60);
    }];
    self.errorBtn.hidden = YES;
    self.errorMsgLab = [UILabel new];
    [self.container.view addSubview:self.errorMsgLab];
    self.errorMsgLab.textColor = [UIColor whiteColor];
    self.errorMsgLab.font = [UIFont lcFont_t3];
    self.errorMsgLab.textAlignment = NSTextAlignmentCenter;
    [self.errorMsgLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.errorBtn.mas_bottom).offset(10);
        make.width.mas_equalTo(SCREEN_WIDTH);
        make.height.mas_equalTo(30);
    }];
    self.errorMsgLab.hidden = YES;
    self.errorMsgLab.text = @"play_module_video_replay_description".lc_T;
    self.bigPlayBtn = [LCButton lcButtonWithType:LCButtonTypeVertical];
    [self.bigPlayBtn setImage:LC_IMAGENAMED(@"videotape_icon_play_big") forState:UIControlStateNormal];
    [self.container.view addSubview:self.bigPlayBtn];
    [self.bigPlayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(tempView.mas_centerX);
        make.centerY.mas_equalTo(tempView.mas_centerY);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(60);
    }];
    self.bigPlayBtn.hidden = YES;
    [self.bigPlayBtn addTarget:self action:@selector(onPlay:) forControlEvents:UIControlEventTouchUpInside];
    [self.errorBtn addTarget:self action:@selector(onPlay:) forControlEvents:UIControlEventTouchUpInside];
}
- (void)showPlayBtn {
    self.bigPlayBtn.hidden = NO;
    self.errorBtn.hidden = YES;
    self.errorMsgLab.hidden = YES;
}
- (void)hidePlayBtn {
    self.bigPlayBtn.hidden = YES;
    self.errorBtn.hidden = YES;
    self.errorMsgLab.hidden = YES;
}
- (void)showErrorBtn {
    self.bigPlayBtn.hidden = YES;
    self.errorBtn.hidden = NO;
    self.errorMsgLab.hidden = NO;
    [self hideVideoLoadImage];
    self.videoManager.isPlay = NO;
}
- (void)hideErrorBtn {
    self.bigPlayBtn.hidden = YES;
    self.errorBtn.hidden = YES;
    self.errorMsgLab.hidden = YES;
}
- (void)showPSKAlert {
    weakSelf(self);
    [LCOCAlertView lc_showTextFieldAlertTextFieldWithTitle:@"Alert_Title_Notice".lc_T Detail:@"mobile_common_input_video_password_tip".lc_T Placeholder:@"" ConfirmTitle:@"Alert_Title_Button_Confirm".lc_T CancleTitle:@"Alert_Title_Button_Cancle".lc_T Handle:^(BOOL isConfirmSelected, NSString *_Nonnull inputContent) {
        if (isConfirmSelected) {
            weakself.videoManager.currentPsk = inputContent;
            [weakself onPlay:nil];
        }
    }];
}
@end
