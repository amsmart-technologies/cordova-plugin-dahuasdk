#import <Foundation/Foundation.h>
@interface NSData (AES)
- (NSData *)lc_AES256CBCEncryptWithKey:(NSString *)key iv:(NSString *)iv;
- (NSData *)lc_AES256CBCDecryptWithKey:(NSString *)key iv:(NSString *)iv;
@end
