#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCVideoCallControlView : UIView
@property (copy, nonatomic) NSMutableArray<UIView *> *items;
@end
NS_ASSUME_NONNULL_END
