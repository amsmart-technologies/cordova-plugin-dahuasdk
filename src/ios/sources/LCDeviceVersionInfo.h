#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCDeviceUpgradeInfo : NSObject
@property (strong, nonatomic) NSString *version;
@property (strong, nonatomic) NSString *LcDescription;
@property (strong, nonatomic) NSString *packageUrl;
@end
@interface LCDeviceVersionInfo : NSObject
@property (strong, nonatomic) NSString *deviceId;
@property (strong, nonatomic) NSString *version;
@property (nonatomic) BOOL canBeUpgrade;
@property (strong, nonatomic) LCDeviceUpgradeInfo *upgradeInfo;
-(void)testInfo;
@end
NS_ASSUME_NONNULL_END
