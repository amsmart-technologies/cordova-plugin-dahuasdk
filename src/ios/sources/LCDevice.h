#import <Foundation/Foundation.h>
#import "LCEncryptInfo.h"
@class LCDevice;
typedef NS_ENUM(NSInteger, DHChannelPicType ) {
    DHChannelPicTypeUnkown,        
    DHChannelPicTypeAuto,        
    DHChannelPicTypeCustom,        
};
typedef NS_ENUM(NSInteger, DHOnlineStatus) {
    DHOnlineStatusOnline,        
    DHOnlineStatusOffline,        
    DHOnlineStatusSleep,        
    DHOnlineStatusClose,        
    DHOnlineStatusUpgrading,    
};
@interface LCBasicDevice : NSObject
@end
@interface LCDevice : LCBasicDevice<NSCopying, NSCoding>
@property (nonatomic, copy)   NSString        *deviceID;
@property (nonatomic, copy)   NSString        *bindId;
@property (nonatomic, assign) BOOL            isOnline;
@property (nonatomic, assign) int             status;
@property (nonatomic, assign) int             channelNum;
@property (nonatomic, copy)   NSString        *baseline;
@property (nonatomic, assign) int             encryptMode;
@property (nonatomic, copy)   NSString        *deviceModel;
@property (nonatomic, copy)   NSString        *deviceModelName;
@property (nonatomic, copy)   NSString        *deviceCatalog;
@property (nonatomic, copy)   NSString        *deviceBrand;
@property (nonatomic, copy)   NSString        *deviceVersion;
@property (nonatomic, copy)   NSString        *deviceName;
@property (nonatomic, copy)   NSString        *deviceUsername;
@property (nonatomic, copy)   NSString        *devicePassword;
@property (nonatomic, copy)   NSString        *dmsIP;
@property (nonatomic, copy)   NSString        *ability;
@property (nonatomic, assign) BOOL            isNeedUpdate;
@property (nonatomic, assign) BOOL            isSharedTo;
@property (nonatomic, assign) BOOL            isSharedFrom;
@property (nonatomic, assign) int             shareState;
@property (nonatomic, assign) int             beShareToState;
@property (nonatomic, copy)   NSString        *ownerUsername;
@property (nonatomic, copy)   NSString        *ownerNickname;
@property (nonatomic, copy)   NSString        *urlShareUser;
@property (nonatomic, copy)   NSString        *urlDeviceLogo;
@property (nonatomic, copy)   NSString        *urlPano;
@property (nonatomic, strong) NSMutableArray  *channelList;
@property (nonatomic, strong) NSMutableArray  *apList;
@property (nonatomic, strong) NSMutableArray  *airDetectionList;
@property (nonatomic, assign) BOOL tlsEnable;
@property (nonatomic, strong) NSString *tlsPrivatePort;
@property (nonatomic, assign) int64_t  shareTime;
@property (nonatomic, copy)   NSString *deviceCategory;
@property (nonatomic, strong) NSMutableArray *zbList;
@property (nonatomic, assign) int agEnableState; 
@property (nonatomic, assign) int paasFlag; 
@end
@interface LCChannel : LCBasicDevice<NSCopying, NSCoding>
@property (nonatomic, copy)   NSString  *picurl;
@property (nonatomic, assign) int       channelID;
@property (nonatomic, copy)   NSString  *channelName;
@property (nonatomic, copy)   NSString  *deviceID;
@property (nonatomic, copy)   NSString  *functions;
@property (nonatomic, copy)   NSString  *channelAbility;
@property (nonatomic, assign) int       belong;
@property (nonatomic, copy)   NSString  *lastOffLineTime;
@property (nonatomic, assign) BOOL      isSharedTo;
@property (nonatomic, assign) int       beShareToState;
@property (nonatomic, assign) BOOL      isOnline;
@property (nonatomic, assign) int       sdCardStatus;
@property (nonatomic, assign) int       csStatus;
@property (nonatomic, copy) NSString  *csExpireTime;
@property (nonatomic, assign) int       csType;
@property (nonatomic, assign) int       alarmStatus;
@property (nonatomic, assign) int       remindStatus;
@property (nonatomic, assign) int64_t   publicExpire;
@property (nonatomic, copy)   NSString  *publicToken;
@property (nonatomic, strong) LCContentEncryptInfo *encryptInfo;
@property (nonatomic, strong) NSMutableArray *mdRules;
@property (nonatomic, assign) int isCloseCamera;
@property (nonatomic, assign) DHChannelPicType picType;
@property (nonatomic, strong) NSMutableDictionary *dh_userInfo;
@property (nonatomic, assign) BOOL      isFrameReversed;
@property (nonatomic, assign) int       lc_shareState;
@property (nonatomic, assign) int       lc_dialogStatus;
@property (nonatomic, copy)   NSString *lc_temperature;
@property (nonatomic, copy)   NSString *lc_weather;
@property (nonatomic, copy)   NSString *lc_region;
- (id)lc_generateDHChannel;
@property (nonatomic, strong) LCDevice *lc_pConvertDevice;
@end
@interface LCZBDevicePowerConsumptionMessage: NSObject
@property (nonatomic, copy)     NSString        *zbDeviceId;
@property (nonatomic, copy)     NSString        *name;
@property (nonatomic, copy)     NSString        *channelId;
@property (nonatomic, assign)   double          total;
@property (nonatomic, assign)   double          monthToatl;
@property (nonatomic, strong)   NSMutableArray  *month;
@end
@interface LCDevicePowerConsumptionMessage : NSObject
@property (nonatomic, copy)     NSString        *deviceID;
@property (nonatomic, strong)   NSMutableArray  *zbDevicePowerList;
@end
@interface LCUserPowerConsumptionMessage : NSObject
@property (nonatomic, assign)   double   total;
@property (nonatomic, strong)   NSMutableArray  *month;
@end
@interface LCDeviceShareInfo : NSObject
@property (nonatomic, assign) int      allowShareCount;
@property (nonatomic, assign) int      leftShareCount;
@property (nonatomic, strong)  NSArray *shareInfos;
@end
@interface LCDeviceSharer : NSObject
@property (nonatomic, copy)   NSString  *username;
@property (nonatomic, copy)   NSString  *userId;
@property (nonatomic, copy)   NSString  *nickname;
@property (nonatomic, copy)   NSString  *remarkName;
@property (nonatomic, copy)   NSString  *userIcon;
@property (nonatomic, copy)   NSString  *functions;
@property (nonatomic, assign) int64_t   activeTime;
@property (nonatomic, assign) int       operation;
@end
@interface LCDeviceUpdateVersionList : NSObject
@property (nonatomic, copy)     NSString  *description;  
@property (nonatomic, copy)     NSString  *deviceId;   
@property (nonatomic, copy)     NSString  *version;   
@property (nonatomic, copy)     NSString  *url;   
@end
@interface LCDeviceWifiInfo : NSObject
@property (nonatomic, copy)     NSString    *deviceID;
@property (nonatomic, assign)   BOOL        enabled;
@property (nonatomic, strong)   NSMutableArray  *wifiStatusList;
@end
@interface LCDeviceWifiStatus : NSObject
@property (nonatomic, copy)     NSString    *BSSID;
@property (nonatomic, copy)     NSString    *auth;
@property (nonatomic, copy)     NSString    *SSID;
@property (nonatomic, assign)     int       linkStatus;
@property (nonatomic, assign)     int       intensity;
@end
@interface LCChannelAlarmPlan : NSObject
@property (nonatomic, copy)     NSString    *channelID;
@property (nonatomic, strong)   NSMutableArray  *alarmRluleList;
@end
@interface LCAlarmRule : NSObject <NSCopying, NSMutableCopying>
@property (nonatomic, assign)   BOOL        enable;
@property (nonatomic, copy)     NSString    *period;
@property (nonatomic, copy)     NSString    *beginTime;
@property (nonatomic, copy)     NSString    *endTime;
@property (nonatomic, assign)   BOOL        bPlus;
@end
@interface LCAlarmMode : NSObject
@property (nonatomic, copy)     NSString    *AlarmMode;
@property (nonatomic, assign)   int         TimeLimit;
@end
@interface DeviceModelInfo : NSObject
@property (nonatomic, copy)     NSString    *logoUrl; 
@property (nonatomic, copy)     NSString    *deviceCatalog; 
@property (nonatomic, assign)   int         type; 
@property (nonatomic, copy)     NSString    *modelName; 
@property (nonatomic, strong)   NSArray     *wifiConfigMode; 
@property (nonatomic, strong)   NSArray     *faqs;
@property (nonatomic, copy)     NSString    *brand;
@property (nonatomic, strong)   NSArray     *moreDesc;
@property (nonatomic, copy)     NSString *wifiTransferMode;
@end
@interface PublicLiveInfo : NSObject
@property(nonatomic, copy)   NSString  *url;
@property(nonatomic, copy)   NSString  *page;
@property(nonatomic, copy)   NSString  *token;
@property(nonatomic, assign) int64_t   publicExpire;
@end
@interface PublicLiveStream : NSObject
@property(nonatomic,copy)NSString *url;
@property(nonatomic,copy)NSString *page;
@end
@interface VideoParameter : NSObject
@property (nonatomic, assign)   int       iFrameIntv;   
@property (nonatomic, assign)   int       streamId;   
@property (nonatomic, assign)   int       fps;   
@property (nonatomic, assign)   int       bitRate;   
@property (nonatomic, copy)     NSString  *resolution;   
@end
@interface LCPermission : NSObject
@property (nonatomic, copy)   NSString *type;
@property (nonatomic, assign) BOOL     flag;
@end
@interface LCWifiAutoPairInfo : NSObject
@property (nonatomic,assign) int index;
@property (nonatomic,strong) NSString *typeString;
@property (nonatomic,strong) NSString *resultString;
@property (nonatomic,strong) NSString *startTimeString;
@property (nonatomic,strong) NSString *endTimeString;
@property (nonatomic,strong) NSString *deviceSNString;
@property (nonatomic,strong) NSString *deviceTypeString;
@property (nonatomic,strong) NSString *phoneTypeString;
@property (nonatomic,strong) NSString *phoneVerString;
@property (nonatomic,strong) NSString *userNameString;
@property (nonatomic,assign) BOOL getDevRsp;
@property (nonatomic,assign) BOOL interruption;
@property (nonatomic,strong) NSString *routeInfoString;
@property (nonatomic,strong) NSString *dataString;
@end
@interface LCWeatherInfo : NSObject
@property (nonatomic,copy) NSString *date;
@property (nonatomic,copy) NSString *dayWeather;
@property (nonatomic,copy) NSString *nightWeather;
@property (nonatomic,copy) NSString *dayTemperature;
@property (nonatomic,copy) NSString *nightTemperature;
@end
@interface LCMotionDetectRulesInfo : NSObject
@property (nonatomic,copy) NSString *period;
@property (nonatomic,copy) NSString *beginTime;
@property (nonatomic,copy) NSString *endTime;
@end
@interface LCUnbindDeviceApplyListInfo : NSObject<NSCopying, NSCoding>
@property (nonatomic,copy) NSString *deviceCode; 
@property (nonatomic,assign) int64_t applyID; 
@property (nonatomic, assign) int status; 
@property (nonatomic, assign) int64_t startTime; 
@property (nonatomic, assign) int64_t updateTime; 
@end
@interface LCUnbindDeviceApplyInfo : NSObject
@property (nonatomic,copy   )    NSString      *deviceCode;    
@property (nonatomic, assign)    int           status;         
@property (nonatomic, assign)    int64_t       createTime;     
@property (nonatomic, copy  )    NSString      *statusExplain; 
@property (nonatomic, assign)    BOOL          isExpired;     
@end
@interface LCUnbindDeviceApplicationInfo : NSObject
@property (nonatomic,copy) NSString *applicantName;  
@property (nonatomic,copy) NSString *phoneNumber;    
@property (nonatomic,copy) NSString *deviceCode;     
@property (nonatomic,copy) NSString *devicePicUrl;   
@property (nonatomic,copy) NSString *idFrontPicUrl;  
@property (nonatomic,copy) NSString *idBackPicUrl;   
@property (nonatomic,copy) NSString *signPicUrl;     
@end
@interface LCGetDevModelInfo : NSObject<NSCoding>
@property (nonatomic, assign) int64_t modelId; 
@property (nonatomic, copy) NSString *deviceModel; 
@property (nonatomic, copy) NSString *modelName; 
@property (nonatomic, copy) NSString *logoUrl; 
@end
@interface LCGetDevModelInfoList : NSObject<NSCoding>
@property (nonatomic,strong) NSArray *modelsArray; 
@property (nonatomic, assign) int64_t timeStamp;      
@end
@interface LCPanoUrlInfo : NSObject
@property (nonatomic, copy) NSString *url;
@property (nonatomic, strong) LCContentEncryptInfo *encryptInfo;
@end
@interface LCDeviceCuriseInfo: NSObject
@property (nonatomic, copy) NSString *period;
@property (nonatomic, copy) NSString *beginTime;
@property (nonatomic, copy) NSString *endTime;
@end
@interface LCDeviceCollectionInfo: NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger stayTime;
@property (nonatomic, copy) NSString *imageString;
@property (nonatomic, copy) NSString *imagePath;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *deviceId;
@end
@interface LCDeviceCuriseConfig: NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *mode;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, strong) NSArray<LCDeviceCollectionInfo *> *collectionInfos;
@end
@interface LCShareStrategy :NSObject
@property (nonatomic) int64_t strategyId;       
@property (nonatomic, copy) NSString *name;     
@property (nonatomic) double fee;        
@property (nonatomic) int64_t validTime;  
@property (nonatomic, copy) NSString *desc;     
@property (nonatomic, copy) NSString *picUrl;   
@end
@interface LCDevShareStrategy :NSObject
@property (nonatomic) int64_t strategyListId;       
@property (nonatomic) int64_t beginTime;        
@property (nonatomic) int64_t endTime;          
@property (nonatomic, copy) NSString *name;     
@property (nonatomic, copy) NSString *desc;     
@property (nonatomic) int64_t shareNum;         
@property (nonatomic, copy) NSString *backgroudPicUrl;     
@property (nonatomic) int64_t defaultNum;       
@property (nonatomic, copy) NSString *status;   
@end
@interface LCReportStatisticNode :NSObject
@property (nonatomic, copy) NSString *time;   
@property (nonatomic, strong) NSArray<NSNumber *> *numberArray; 
@end
@interface LCReportStatisticData :NSObject
@property (nonatomic) int64_t reportId;
@property (nonatomic, copy) NSString *reportName;   
@property (nonatomic, copy) NSString *strategyType; 
@property (nonatomic, copy) NSString *updateTime;   
@property (nonatomic) int64_t todayNum;     
@property (nonatomic) int64_t yesterdayNum; 
@property (nonatomic) int64_t weekNum;      
@property (nonatomic) int64_t lastWeekNum;  
@property (nonatomic) int64_t monthNum;     
@property (nonatomic) int64_t lastMonthNum; 
@property (nonatomic) int64_t yearNum;     
@property (nonatomic) int64_t lastYearNum; 
@property (nonatomic, strong) NSArray<LCReportStatisticNode *> *dataList; 
@end
@interface LCReportStrategy:NSObject
@property (nonatomic) int64_t strategyId;     
@property (nonatomic, copy) NSString *name;   
@property (nonatomic) double price;           
@property (nonatomic) NSInteger type;         
@property (nonatomic, copy) NSString *picUrl;   
@property (nonatomic, copy) NSString *describe; 
@property (nonatomic) NSInteger validTime; 
@end
@interface LCStrategyDetail:NSObject
@property (nonatomic, copy) NSString *strategyType; 
@property (nonatomic, copy) NSString *startTime;    
@property (nonatomic, copy) NSString *endTime;      
@end
@interface LCOneDayStrategy:NSObject
@property (nonatomic, copy) NSString* type;         
@property (nonatomic) int64_t strategyId;           
@property (nonatomic, copy) NSString* status;       
@property (nonatomic, copy) NSString* beginTime;    
@property (nonatomic, copy) NSString* endTime;      
@property (nonatomic, copy) NSString* name;      
@end
@interface LCDevCloudStrategy:NSObject
@property (nonatomic, copy) NSString* type;         
@property (nonatomic, copy) NSString* status;       
@property (nonatomic, copy) NSString* beginTime;    
@property (nonatomic, copy) NSString* endTime;      
@property (nonatomic, copy) NSString* name;      
@end
@interface LCSnapKeyInfo:NSObject
@property (nonatomic, copy) NSString *keyId;     
@property (nonatomic, copy) NSString *snapKey;     
@property (nonatomic, copy) NSString *name;     
@property (nonatomic, copy) NSString *status;     
@property (nonatomic, copy) NSString *createUtcTime;     
@property (nonatomic, copy) NSString *createLocalTime;     
@property (nonatomic, copy) NSString *localTime;     
@property (nonatomic, copy) NSString *utcTime;    
@end
@interface LCKeyEffectPeriod:NSObject
@property (nonatomic, copy) NSString *period;
@property (nonatomic, copy) NSString *beginTime;
@property (nonatomic, copy) NSString *endTime;
@end
@interface LCSecretKeyInfo:NSObject
@property (nonatomic, copy) NSString *type; 
@property (nonatomic, copy) NSString *keyId; 
@property (nonatomic, copy) NSString *name;         
@property (nonatomic) BOOL bManager;        
@property (nonatomic) int effectTime;         
@property (nonatomic) BOOL bHijackAlarm;         
@property (nonatomic, copy) NSString *location;         
@property (nonatomic, copy) NSString *phone;         
@property (nonatomic, strong) NSArray<LCKeyEffectPeriod *> *effectPeriod;         
@end
@interface LCHoveringAlarmInfo:NSObject
@property (nonatomic, copy) NSString *hoveringAlarmStatus; 
@property (nonatomic, assign) NSInteger stayTime; 
@end
@interface LCDevicePowerInfo:NSObject
@property (nonatomic, copy) NSString *type; 
@property (nonatomic, assign) int electric; 
@property (nonatomic, assign) int alkElec;  
@property (nonatomic, assign) int litElec;  
@end
@interface LCDeviceFlushInfo:NSObject
@property (nonatomic, assign) NSInteger ringIndex;
@property (nonatomic, copy) NSArray *list;
@end
@interface LCDeviceFlushCellInfo:NSObject
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, copy) NSString *name;
@end
@interface LCDeviceGearInfo:NSObject
@property (nonatomic, assign) NSInteger value;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, assign) NSInteger gear;
@end
@interface NVMMode: NSObject
@property (nonatomic, copy  ) NSString *model;   
@property (nonatomic, copy  ) NSArray<NSString *> *models; 
@end
@interface NVMChannelMode: NSObject
@property (nonatomic, copy  ) NSString *chan;   
@property (nonatomic, copy  ) NSString *sn;     
@property (nonatomic, copy  ) NSString *mode;
@end
@interface LCDeviceMotionDetectInfo:NSObject
@property (nonatomic, assign) NSInteger stall;
@property (nonatomic, assign) NSInteger row;
@property (nonatomic, assign) NSInteger column;
@property (nonatomic, assign) NSInteger sensitive;
@property (nonatomic, assign) NSInteger threshold;
@property (nonatomic, copy) NSString *region;
@end
@interface DHQuerySirenStateResultObject : NSObject
@property (nonatomic, assign) int time;
@property (nonatomic, copy) NSString *whiteLight;
@property (nonatomic, copy) NSString *searchLight;
@property (nonatomic, copy) NSString *clientLocalTime;
@end
@interface LCDeviceZoomFocusInfo : NSObject
@property (nonatomic, assign) int channelId;
@property (nonatomic, assign) double zoomFocus;
@end
@interface LCDeviceWifiStateFromServer : NSObject
@property (nonatomic, assign) BOOL linkEnable; 
@property (nonatomic, copy) NSString *intensity; 
@property (nonatomic, copy) NSString *sigStrength; 
@property (nonatomic, copy) NSString *ssid; 
@end
@interface LCDeviceWifiForRemoteDevice : NSObject
@property (nonatomic, assign) int channelId; 
@property (nonatomic, assign) BOOL linkEnable; 
@property (nonatomic, assign) int intensity; 
@property (nonatomic, copy) NSString *sigStrength; 
@property (nonatomic, copy) NSString *ssid; 
@end
@interface LCDeviceBatteryElectric : NSObject
@property (nonatomic, copy) NSString *type; 
@property (nonatomic, copy) NSString *electric; 
@property (nonatomic, copy) NSString *alkElec; 
@property (nonatomic, copy) NSString *litElec; 
@end
@interface LCDeviceRemoteDeviceElectric: NSObject
@property (nonatomic, assign) int channelId; 
@property (nonatomic, copy) NSString *type; 
@property (nonatomic, assign) int electric; 
@property (nonatomic, assign) int alkElec; 
@property (nonatomic, assign) int litElec; 
@end
@interface LCMotionDetectParamInfo : NSObject
@property (nonatomic, assign) NSInteger stall;    
@property (nonatomic, assign) NSInteger row;      
@property (nonatomic, assign) NSInteger column;   
@property (nonatomic, assign) NSInteger sensitive;
@property (nonatomic, assign) NSInteger threshold;
@property (nonatomic, copy) NSString *region;
@end
@interface LCAirDetection : LCBasicDevice <NSCopying, NSCoding>
@property (nonatomic, copy) NSString *type; 
@property (nonatomic, copy) NSString *value; 
@property (nonatomic, copy) NSString *qualityType;
@property (nonatomic, copy) NSString *unit;
@end
@interface LCAirDetectReportData : LCBasicDevice <NSCopying, NSCoding>
@property (nonatomic, copy) NSString *utcTime; 
@property (nonatomic, copy) NSString *value; 
@property (nonatomic, copy) NSString *minValue;
@property (nonatomic, copy) NSString *maxValue;
@property (nonatomic, copy) NSString *qualityType;
@property (nonatomic, copy) NSString *type;
@end
@interface LCAirDetectAllData : LCBasicDevice <NSCopying, NSCoding>
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *minRange;
@property (nonatomic, copy) NSString *maxRange;
@property (nonatomic, copy) NSString *percision;
@property (nonatomic, copy) NSString *unit;
@property (nonatomic, copy) NSString *mode;
@property (nonatomic, strong) NSMutableArray *space;
@end
@interface DHSirenTimeInfo: NSObject
@property (nonatomic, copy) NSString *currentIndex;
@property (nonatomic, copy) NSString *index;
@property (nonatomic, copy) NSString *time;
@end
@interface DHUserDeviceBindInfo: NSObject
@property (nonatomic, copy) NSString *deviceExist; 
@property (nonatomic, copy) NSString *bindStatus; 
@property (nonatomic, copy) NSString *userAccount; 
@property (nonatomic, copy) NSString *wifiConfigMode; 
@property (nonatomic) BOOL support; 
@property (nonatomic, copy) NSString *wifiTransferMode; 
@property (nonatomic, copy) NSString *status; 
@property (nonatomic, copy) NSString *deviceModel; 
@property (nonatomic, copy) NSString *ability; 
@property (nonatomic, copy) NSString *catalog; 
@property (nonatomic) BOOL wifiConfigModeOptional;  
@property (nonatomic, copy) NSString *accessType; 
@property (nonatomic, copy) NSString *brand; 
@property (nonatomic, copy) NSString *family; 
@property (nonatomic, copy) NSString *modelName;
@property (nonatomic, copy) NSString *deviceCodeModel;
@property (nonatomic, copy) NSString *deviceModelName;
@property (nonatomic, copy) NSString *type; 
@property (nonatomic, copy) NSString *channelNum; 
@property (nonatomic, copy) NSString *watchSetupVideoUrl;
@property (nonatomic, copy) NSString *port; 
@property (nonatomic, copy) NSString *httpPort; 
@property (nonatomic, copy) NSString *rtspPort; 
@property (nonatomic, copy) NSString *tlsPrivatePort; 
@property (nonatomic, copy) NSString *privateMediaPort ; 
- (BOOL)isDeviceExist;
@property (strong, nonatomic) NSString *deviceType;
@property (strong, nonatomic) NSString *deviceCatalog;
@property (strong, nonatomic) NSString *dt;
@property (strong, nonatomic) NSString *dtName;
@property (strong, nonatomic) NSString *wifiMode;
@property (nonatomic, copy) NSString *deviceImageURI ; 
@end
@interface DHBindDeviceInfo: NSObject
@property (nonatomic, copy) NSString *deviceId; 
@property (nonatomic, copy) NSString *code; 
@property (nonatomic, copy) NSString *deviceKey; 
@property (nonatomic, copy) NSString *longitude; 
@property (nonatomic, copy) NSString *latitude; 
@property (nonatomic, copy) NSString *deviceUsername; 
@property (nonatomic, copy) NSString *devicePassword; 
@property (nonatomic, copy) NSString *imeiCode;     
@end
@interface DHBindDeviceSuccess: NSObject
@property (nonatomic, copy) NSString *deviceName; 
@property (nonatomic, copy) NSString *bindStatus; 
@property (nonatomic, copy) NSString *userAccount; 
@property (nonatomic, copy) NSString *recordSaveDays; 
@property (nonatomic, copy) NSString *streamType; 
@property (nonatomic, copy) NSString *seviceTime; 
@end
@interface DHDeviceTimeZone: NSObject
@property (nonatomic, copy) NSString *area; 
@property (nonatomic, copy) NSString *timeZone; 
@property (nonatomic, copy) NSString *beginSunTime; 
@property (nonatomic, copy) NSString *endSunTime; 
@property (nonatomic, copy) NSString *offset;
@property (nonatomic, assign, readonly) NSInteger areaIndex;
@property (nonatomic, assign, readonly) NSInteger timeZoneIndex;
+ (NSString *)dstTimeFormat;
@end
@interface DHDeviceTimeZoneQueryInfo: NSObject
@property (nonatomic, copy) NSString *endWeekSunTime;
@property (nonatomic, copy) NSString *beginSunTime;
@property (nonatomic, assign) int timeZone;
@property (nonatomic, copy) NSString *beginWeekSunTime;
@property (nonatomic, copy) NSString *mode;
@property (nonatomic, copy) NSString *endSunTime;
@property (nonatomic, copy) NSString *areaIndex;
@property (nonatomic, copy) NSString *offset;
-(BOOL)isDayModel;
-(BOOL)isWeekModel;
@end
@interface LCSearchLightModel: NSObject
@property (nonatomic, assign) NSInteger index; 
@property (nonatomic, copy) NSString *mode; 
@end
@interface LCSearchLightWorkMode: NSObject
@property (nonatomic, assign) NSInteger index; 
@property (nonatomic, strong) NSArray<LCSearchLightModel *> *models;
@end
@interface LCLightTimeModel: NSObject
@property (nonatomic, assign) NSInteger index; 
@property (nonatomic, strong) NSString *time; 
@end
@interface LCLightTimeWorkMode: NSObject
@property (nonatomic, assign) NSInteger index; 
@property (nonatomic, strong) NSArray<LCLightTimeModel *> *models;
@end
@interface LCSirenContentModel: NSObject
@property (nonatomic, copy) NSString *clientLocalTime;  
@property (nonatomic, strong) NSArray *channels;          
@end
@interface LCSirenChannelModel: NSObject
@property (nonatomic, copy) NSString *channelId;    
@property (nonatomic, assign) int time;             
@property (nonatomic, copy) NSString *whiteLight;   
@property (nonatomic, copy) NSString *searchLight;  
@end
@interface LCSirenResponseModel: NSObject
@property (nonatomic, copy) NSString *clientLocalTime;  
@property (nonatomic, strong) NSArray <LCSirenChannelModel *>*channels;     
@end
@interface LCBellContentModel: NSObject
@property (nonatomic, assign) BOOL isMultiChannel;  
@property (nonatomic, assign) int index;            
@property (nonatomic, copy) NSString *sn;           
@property (nonatomic, copy) NSString *chan;         
@property (nonatomic, copy) NSString *relateType;   
@property (nonatomic, copy) NSString *name;         
@property (nonatomic, copy) NSString *url;          
@property (nonatomic, copy) NSString *type;         
@end
@interface DHIntelligentlockNotesInfo: NSObject
@property (nonatomic, copy) NSString *name;          
@property (nonatomic, copy) NSString *keyType;       
@property (nonatomic, copy) NSString *localKeyType;       
@property (nonatomic, copy) NSString *operateType;   
@property (nonatomic, copy) NSString *localOperateType;    
@property (nonatomic, copy) NSString *time;          
@property (nonatomic, copy) NSString *localTime;    
@end
