#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCAuthModel : NSObject
@property (strong, nonatomic) NSString *accessToken;
@property (strong, nonatomic) NSString *userToken;
@property (strong, nonatomic) NSString *openid;
@property (nonatomic) NSUInteger expireTime;
@end
NS_ASSUME_NONNULL_END
