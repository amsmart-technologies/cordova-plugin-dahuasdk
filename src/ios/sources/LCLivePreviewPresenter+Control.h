#import "LCLivePreviewPresenter.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCLivePreviewPresenter (Control)
-(NSMutableArray *)getMiddleControlItems;
-(NSMutableArray *)getBottomControlItems;
- (void)onFullScreen:(LCButton *)btn;
- (void)onAudio:(LCButton *)btn;
- (void)onAudioTalk:(LCButton *)btn;
- (void)onPlay:(nullable LCButton *)btn;
- (void)onPtz:(LCButton *)btn;
- (void)onSnap:(LCButton *)btn;
- (void)onRecording:(LCButton *)btn;
- (void)onCallAnswer:(LCButton *)btn;
- (void)onCallRefuse:(LCButton *)btn;
- (void)onQuality:(LCButton *)btn;
- (void)stopPlay;
- (void)startPlay;
-(void)onLockFullScreen:(LCButton *)btn;
@end
NS_ASSUME_NONNULL_END
