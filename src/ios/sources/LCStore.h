#import <Foundation/Foundation.h>
@interface LCStore : NSObject
@property (nonatomic, strong, readonly) NSDictionary *dicStore;
- (instancetype)initWithLocalPath:(NSString *)path;
- (id)getObjByKey:(const NSString *)key;
- (void)saveObj:(id)value withKey:(const NSString *)key;
- (void)saveAppended:(NSDictionary *)dictionary;
@end
