#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCDeviceOnlineInfoChannelInfo : NSObject
@property (nonatomic) int channelId;
@property (strong, nonatomic) NSString *onLine;
@end
@interface LCDeviceOnlineInfo : NSObject
@property (strong, nonatomic) NSString *deviceId;
@property (strong, nonatomic) NSString *onLine;
@property (strong, nonatomic) LCDeviceOnlineInfoChannelInfo *channels;
@end
NS_ASSUME_NONNULL_END
