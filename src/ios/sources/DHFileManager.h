#import <Foundation/Foundation.h>
typedef void(^LCFileRenameSuccessBlock)(NSString *filepath, NSString *thumbnailPah, NSString *fileName);
@interface DHFileManager : NSObject
+ (NSString *)capturesFolder;
+ (NSString *)userthumbsFolderPath;
+ (NSString *)thumbFilePathWithChannel:(NSString *)deviceId channelId:(NSString *)channelId;
+ (NSString *)userAndServiceConfigFilePath;
+ (NSString *)configFilePath;
+ (NSString *)userFolder;
+ (NSString *)userConfigFilePath;
+ (NSString *)userGuideFilePath;
+ (NSString *)screenshotFilePath:(NSString*)devcieId;
+ (NSString *)screenshotThumbFilePath:(NSString*)devcieId;
+ (NSString *)videotapeFilePath:(NSString*)devcieId;
+ (NSString *)videotapeThumbFilePath:(NSString*)devcieId;
+ (NSString *)collectionThumbFilePath;
+ (NSString *)supportFolder;
+ (NSString *)myFileImageDir;
+ (NSString *)myFileVideoDir;
+ (NSString *)videoNameWithPath:(NSString *)filepath;
#pragma mark - 缓存相关
+ (NSArray *)thumbFolderPaths;
+ (void)clearCache;
+ (BOOL)removeFileAtPath:(NSString *)path;
@end
