@interface LCEncryptValidInfo : NSObject
@property (nonatomic, assign) BOOL isValid;
@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, assign) int64_t expiresTime;
@end
@interface LCContentEncryptInfo : NSObject
@property (nonatomic, copy) NSString *encryptionMode;
@property (nonatomic, copy) NSString *ruleVerison;
@property (nonatomic, copy) NSString *keyMode;
@end
