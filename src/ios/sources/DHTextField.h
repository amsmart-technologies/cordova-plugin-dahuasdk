#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef void(^DHTextFieldInputBlock)(NSString * result);
typedef BOOL(^DHTextFieldInputingBlock)(NSString * result,NSString * replace);
@interface DHTextField : UITextField
@property (copy, nonatomic) DHTextFieldInputingBlock inputingBlock;
@property (nonatomic, copy,nullable) NSString *placeholder;
+(instancetype)lcTextFieldWithResult:(void(^)(NSString *result))result;
@end
NS_ASSUME_NONNULL_END
