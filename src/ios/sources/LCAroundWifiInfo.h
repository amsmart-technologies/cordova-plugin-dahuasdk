#import <Foundation/Foundation.h>
typedef enum : NSUInteger {
    LinkStatusNoConnect,
    LinkStatusConnecting,
    LinkStatusConnected 
} LCWiFiLinkStatus;
typedef enum : NSUInteger {
    LinkDisconnectHandle,
    LinkConnectHandle
} LCLinkHandle;
NS_ASSUME_NONNULL_BEGIN
@interface LCWifiConnectSession : NSObject
@property (strong, nonatomic) NSString *ssid;
@property (strong, nonatomic) NSString *bssid;
@property (nonatomic) LCLinkHandle linkEnable;
@property (strong, nonatomic) NSString *password;
@property (nonatomic) int intensity;
@end
@interface LCWifiInfo : NSObject
@property (strong, nonatomic) NSString *ssid;
@property (strong, nonatomic) NSString *bssid;
@property (nonatomic) LCWiFiLinkStatus linkStatus;
@property (strong, nonatomic) NSString *auth;
@property (nonatomic) int intensity;
@end
@interface LCAroundWifiInfo : NSObject
@property (nonatomic) BOOL enable;
@property (strong, nonatomic) NSArray<LCWifiInfo *> *wLan;
@end
NS_ASSUME_NONNULL_END
