#define SEC_MINUTE      60
#define SEC_HOUR		(60*SEC_MINUTE)
#define SEC_DAY         (24*SEC_HOUR)
#define SEC_YEAR        (365*SEC_DAY)
typedef struct
{
    int year;
    int month;
    int day;
    int week;
    int hour;
    int minute;
    int second;
}Time_Info;
#import <Foundation/Foundation.h>
@interface NSDate (LeChange)
+ (NSCalendar *) lc_currentCalendar;
+ (void)lc_setCurrentCanlendar:(NSCalendarIdentifier)identifier;
@property (readonly) NSInteger hour;
@property (readonly) NSInteger minute;
@property (readonly) NSInteger seconds;
@property (readonly) NSInteger day;
@property (readonly) NSInteger month;
@property (readonly) NSInteger weekOfYear;
@property (readonly) NSInteger weekday;
@property (readonly) NSInteger year;
+ (NSDate *)lc_dateOfString:(NSString*)dateString withFormat:(NSString *)format;
+ (NSString*)lc_timeByLength:(NSInteger)time;
- (NSString *)lc_dateDescription;
+ (NSDate *)lc_stringToDate:(NSString *)timeString format:(NSString*)format;
+ (NSString *)lc_stringOfTimeInterval:(NSTimeInterval)timeInterval format:(NSString*)format;
- (NSDate *)lc_dateAtStartOfDay;
- (NSDate *)lc_dateAtEndOfDay;
- (NSDate *)lc_dateBeforeDay;
- (NSDate *)lc_dateAfterDay;
- (NSString *)lc_stringRepresentation;
- (NSString *)lc_stringOfDateWithFormator:(NSString *)formator;
- (NSString *)lc_stringDateAtStartOfDay;
- (NSString *)lc_stringDateAtEndOfDay;
- (BOOL)lc_isEqualToDateIgnoringTime:(NSDate *)compareDate;
- (BOOL)lc_isToday;
- (BOOL)lc_isYesterday;
- (BOOL)lc_isInFuture;
- (BOOL)lc_isInPast;
#pragma mark - PubFun 分离
+ (NSString *)lc_currentTimeString;
- (Time_Info)lc_timeInfo;
+ (NSDate *)lc_dateFromString:(NSString *)string;
+ (NSDate *)lc_dateFromString:(NSString *)string format:(NSString *)format;
+ (NSDate *)lc_todayFromString:(NSString *)string;
+ (NSString *)lc_stringOfDate:(NSDate *)date format:(NSString *)format;
+ (NSDate *)lc_dateOfTimeInfo:(Time_Info)timeInfo;
+ (NSString *)lc_stringDateBeginWithHour:(NSString *)dateString;
+ (NSString *)lc_nextDayStringWithString:(NSString *)string;
+ (BOOL)lc_isLaterThanCurrentTimeByHour:(int)hour minute:(int)minute;
+ (NSString *)lc_stringDate:(NSDate *)date;
@end
