#import <UIKit/UIKit.h>
#import <MBProgressHUD/MBProgressHUD.h>
@interface LCProgressHUD : UIView
+ (void)hideAllHuds:(UIView *)view;
+ (void)hideAllHuds:(UIView *)view animated:(BOOL)animated;
+ (void)showMsg:(NSString*)msg;
+ (void)showMsg:(NSString*)msg duration:(NSTimeInterval)duration;
+ (void)showMsg:(NSString*)msg inView:(UIView *)view;
+ (MBProgressHUD *)showHudOnView:(UIView *)view tip:(NSString*)tip animated:(BOOL)animated;
+ (MBProgressHUD *)showHudOnView:(UIView *)view tip:(NSString*)tip;
+ (MBProgressHUD *)showHudOnView:(UIView *)view animated:(BOOL)animated;
+ (MBProgressHUD *)showHudOnView:(UIView *)view animated:(BOOL)animated isInteract:(BOOL)isInteract;
+ (MBProgressHUD *)showHudOnView:(UIView *)view;
+ (MBProgressHUD *)showHubOnView:(UIView *)view duration:(NSTimeInterval)duration;
+ (MBProgressHUD *)showHudOnView:(UIView *)view isInteract:(BOOL)isInteract;
+ (MBProgressHUD *)showHudOnView:(UIView *)view bgColor:(UIColor*)bgColor;
+ (MBProgressHUD *)showHudOnLowerView:(UIView *)view;
+ (MBProgressHUD *)showHudOnLowerView:(UIView *)view tip:(NSString*)tip;
+ (void)showHudWithTip:(NSString *)tip image:(UIImage *)image onView:(UIView *)view;
+ (UIView *)keyWindow;
@end
