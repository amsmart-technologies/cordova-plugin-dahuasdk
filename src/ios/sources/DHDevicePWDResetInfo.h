#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface DHDevicePWDResetInfo : NSObject
@property (nonatomic, assign) BOOL isSuccess;
@property (nonatomic, copy) NSString *errorStr;
@end
NS_ASSUME_NONNULL_END
