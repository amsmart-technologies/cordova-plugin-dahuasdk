#import "LCLivePreviewPresenter+Control.h"
#import "LCLivePreviewDefine.h"
#import <objc/runtime.h>
#import "PHAsset+Lechange.h"
#import "UIImage+LeChange.h"
#import "LCLivePreviewPresenter+SDKListener.h"
#import "LCBaseDefine.h"
#import "NSString+Dahua.h"
#import "LCApplicationDataManager.h"
#import "LCProgressHUD.h"
#import "UIDevice+LeChange.h"
#import "LCPermissionHelper.h"
#import "OpenApiInterface.h"
static const void *kLCLivePreviewPresenterSavePath = @"LCLivePreviewPresenterSavePath";
@interface LCLivePreviewPresenter ()
@property (strong, nonatomic) NSString *savePath;
@end
@implementation LCLivePreviewPresenter (Control)
- (void)onFullScreen:(LCButton *)btn {
    self.videoManager.isFullScreen = !self.videoManager.isFullScreen;
    self.videoManager.isLockFullScreen = NO;
    [UIDevice lc_setRotateToSatusBarOrientation];
}
- (void)onAudio:(LCButton *)btn {
    if (self.videoManager.isOpenAudioTalk) {
        [LCProgressHUD showMsg:@"livepreview_result_close_talk_first".lc_T];
    }
    if (self.videoManager.isSoundOn) {
        [self.playWindow stopAudio];
    } else {
        [self.playWindow playAudio];
    }
    self.videoManager.isSoundOn = !self.videoManager.isSoundOn;
}
- (void)onPlay:(LCButton *)btn {
    if (self.videoManager.isPlay) {
        [self stopPlay];
    } else {
        [self startPlay];
    }
    NSLog(@"%@", [NSThread currentThread]);
}
- (void)stopPlay {
    [self saveThumbImage];
    if (self.videoManager.isOpenCloudStage) {
        [self onPtz:nil];
    }
    [self hideVideoLoadImage];
    [self showPlayBtn];
    self.videoManager.isPlay = NO;
    self.videoManager.isSoundOn = YES;
    [self.talker stopTalk];
    self.videoManager.isOpenAudioTalk = NO;
    [self.playWindow stopRtspReal:NO];
    [self.playWindow stopAudio];
}
- (void)startPlay {
    [self showVideoLoadImage];
    [self hidePlayBtn];
    [self.playWindow stopRtspReal:NO];
    [self.playWindow stopAudio];
    LCOpenSDK_ParamReal *param = [[LCOpenSDK_ParamReal alloc]init];
    param.isOpt = YES;
    param.useTLS = self.videoManager.currentDevice.tlsEnable;
    param.accessToken = [LCApplicationDataManager token];
    param.deviceID = self.videoManager.currentDevice.deviceId;
    param.productId = self.videoManager.currentDevice.productId;
    param.channel = [self.videoManager.currentChannelInfo.channelId integerValue];
    param.psk = self.videoManager.currentPsk;
    param.playToken = self.videoManager.currentDevice.playToken;
    param.isOpenAudio = self.videoManager.isSoundOn;
    if ([self.videoManager.currentChannelInfo.resolutions count] > 0) {
        LCCIResolutions *resolutions = self.videoManager.currentResolution;
        if (!resolutions) {
            resolutions = [self.videoManager.currentChannelInfo.resolutions firstObject];
            self.videoManager.currentResolution = resolutions;
        }
        param.defiMode = [resolutions.streamType integerValue];
        param.imageSize = resolutions.imageSize;
    } else {
        param.defiMode = self.videoManager.isSD ? DEFINITION_MODE_SD : DEFINITION_MODE_HG;
    }
    NSInteger inde = [self.playWindow playRtspReal:param];
    if (inde != 0) {}
    self.videoManager.isPlay = YES;
    if(self.videoManager.callBellEvent){
        [self.playWindow stopAudio];
        [self.videoManager playDoorBellSound];
    }
}
- (void)onQuality:(LCButton *)btn {
    [self.playWindow stopRtspReal:NO];
    [self hideVideoLoadImage];
    NSInteger definition = 0;
    if (self.videoManager.isSD) {
        definition = DEFINITION_MODE_HG;
        self.videoManager.isSD = NO;
    } else {
        definition = DEFINITION_MODE_SD;
        self.videoManager.isSD = YES;
    }
    [self showVideoLoadImage];
    LCOpenSDK_ParamReal *param = [[LCOpenSDK_ParamReal alloc]init];
    param.defiMode = definition;
    param.isOpt = YES;
    param.accessToken = LCApplicationDataManager.token;
    param.deviceID = self.videoManager.currentDevice.deviceId;
    param.channel = [self.videoManager.currentChannelInfo.channelId integerValue];
    param.psk = self.videoManager.currentPsk;
    param.playToken = self.videoManager.currentDevice.playToken;
    [self.playWindow playRtspReal:param];
}
- (void)onPtz:(LCButton *)btn {
    if (self.videoManager.isOpenCloudStage) {
        [self.liveContainer hidePtz];
        self.videoManager.isOpenCloudStage = NO;
    } else {
        [self.liveContainer showPtz];
        self.videoManager.isOpenCloudStage = YES;
    }
}
- (void)onSnap:(LCButton *)btn {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,
                                                         NSUserDomainMask, YES);
    NSString *libraryDirectory = [paths objectAtIndex:0];
    NSString *myDirectory =
    [libraryDirectory stringByAppendingPathComponent:@"lechange"];
    NSString *picDirectory =
    [myDirectory stringByAppendingPathComponent:@"picture"];
    NSDateFormatter *dataFormat = [[NSDateFormatter alloc] init];
    [dataFormat setDateFormat:@"yyyyMMddHHmmss"];
    NSString *strDate = [dataFormat stringFromDate:[NSDate date]];
    NSString *datePath = [picDirectory stringByAppendingPathComponent:strDate];
    NSString *picPath = [datePath stringByAppendingString:@".jpg"];
    NSLog(@"test jpg name[%@]\n", picPath);
    NSFileManager *fileManage = [NSFileManager defaultManager];
    NSError *pErr;
    BOOL isDir;
    if (NO == [fileManage fileExistsAtPath:myDirectory isDirectory:&isDir]) {
        [fileManage createDirectoryAtPath:myDirectory
              withIntermediateDirectories:YES
                               attributes:nil
                                    error:&pErr];
    }
    if (NO == [fileManage fileExistsAtPath:picDirectory isDirectory:&isDir]) {
        [fileManage createDirectoryAtPath:picDirectory
              withIntermediateDirectories:YES
                               attributes:nil
                                    error:&pErr];
    }
    [self.playWindow snapShot:picPath];
    UIImage *image = [UIImage imageWithContentsOfFile:picPath];
    NSURL *imgURL = [NSURL fileURLWithPath:picPath];
    [PHAsset deleteFormCameraRoll:imgURL success:^{
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    }];
    [PHAsset saveImageToCameraRoll:image url:imgURL success:^(void) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [LCProgressHUD showMsg:@"livepreview_localization_success".lc_T];
        });
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [LCProgressHUD showMsg:@"livepreview_localization_fail".lc_T];
        });
    }];
}
- (void)onAudioTalk:(LCButton *)btn {
    if (!self.videoManager.isOpenAudioTalk) {
        [self.talker stopTalk];
        [LCProgressHUD showHudOnView:nil];
        LCOpenSDK_ParamTalk *param = [[LCOpenSDK_ParamTalk alloc]init];
        self.videoManager.currentDevice = [LCDeviceVideoManager manager].currentDevice;
        param.isOpt = YES;
        param.accessToken = LCApplicationDataManager.token;
        param.deviceID = self.videoManager.currentChannelInfo.deviceId == nil ? self.videoManager.currentDevice.deviceId : self.videoManager.currentChannelInfo.deviceId ;
        param.channel = (self.videoManager.currentDevice.channels.count > 1 || [self.videoManager.currentDevice.catalog isEqualToString:@"NVR"]) ? [self.videoManager.currentChannelInfo.channelId intValue] : -1;
        param.psk = self.videoManager.currentPsk;
        param.playToken = self.videoManager.currentDevice.playToken;
        // NSLog(@"%@", self.videoManager.currentChannelInfo.deviceId);
        NSInteger result = [self.talker playTalk:param];
        if (result == 0) {
            self.videoManager.isOpenAudioTalk = true;
        }else{
            [LCProgressHUD hideAllHuds:nil];
            [LCProgressHUD showMsg:@"play_module_video_close_talk".lc_T];
            self.videoManager.isOpenAudioTalk = false;
        }
    } else {
        NSInteger result = [self.talker stopTalk];
        [LCProgressHUD showMsg:@"play_module_video_close_talk".lc_T];
        self.videoManager.isOpenAudioTalk = false;
        [self.talker setListener:nil];
        self.talker = nil;
    }
    if (self.videoManager.isSoundOn) {
        [self.playWindow playAudio];
    }
    // self.videoManager.isOpenAudioTalk = !self.videoManager.isOpenAudioTalk;
}
- (void)onLockFullScreen:(LCButton *)btn {
    self.videoManager.isLockFullScreen = !self.videoManager.isLockFullScreen;
}
- (void)onRecording:(LCButton *)btn {
    if (!self.videoManager.isOpenRecoding) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *libraryDirectory = [paths objectAtIndex:0];
        NSString *myDirectory = [libraryDirectory stringByAppendingPathComponent:@"lechange"];
        NSString *davDirectory = [myDirectory stringByAppendingPathComponent:@"video"];
        NSLog(@"test name[%@]\n", davDirectory);
        NSDateFormatter *dataFormat = [[NSDateFormatter alloc] init];
        [dataFormat setDateFormat:@"yyyyMMddHHmmss"];
        NSString *strDate = [dataFormat stringFromDate:[NSDate date]];
        NSString *datePath = [davDirectory stringByAppendingPathComponent:strDate];
        self.savePath = [datePath stringByAppendingFormat:@"_video_%@.mp4", @"asdasdadd"];
        NSFileManager *fileManage = [NSFileManager defaultManager];
        NSError *pErr;
        BOOL isDir;
        if (NO == [fileManage fileExistsAtPath:myDirectory isDirectory:&isDir]) {
            [fileManage createDirectoryAtPath:myDirectory
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:&pErr];
        }
        if (NO == [fileManage fileExistsAtPath:davDirectory isDirectory:&isDir]) {
            [fileManage createDirectoryAtPath:davDirectory
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:&pErr];
        }
        NSInteger result = [self.playWindow startRecord:self.savePath recordType:1];
        if (result != 0) {
            [LCProgressHUD showMsg:@"play_module_media_play_record_failed".lc_T];
        } else {
            [LCProgressHUD showMsg:@"play_module_video_media_start_record".lc_T];
        }
    } else {
        weakSelf(self);
        [self.playWindow stopRecord];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSURL *davURL = [NSURL fileURLWithPath:weakself.savePath];
            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(weakself.savePath)) {
                [PHAsset deleteFormCameraRoll:davURL success:^{
                } failure:^(NSError *error) {
                }];
                [PHAsset saveVideoAtURL:davURL success:^(void) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [LCProgressHUD showMsg:@"livepreview_localization_success".lc_T];
                    });
                } failure:^(NSError *error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [LCProgressHUD showMsg:@"livepreview_localization_fail".lc_T];
                    });
                }];
            } else {
                [LCProgressHUD showMsg:@"livepreview_localization_fail".lc_T];
            }
        });
    }
    self.videoManager.isOpenRecoding = !self.videoManager.isOpenRecoding;
}
- (void)onCallAnswer:(LCButton *)btn {
    NSLog(@"onCallAnswer");
    self.videoManager.answerbuttonPressed = YES;
    [self.videoManager.audioPlayer stop];
    [LCPermissionHelper requestAudioPermission:^(BOOL granted) {
        if (granted) {
            [OpenApiInterface doorbellCallAnswer:[LCApplicationDataManager token] deviceId:self.videoManager.currentDevice.deviceId success:^{
                NSLog(@"doorbellCallAnswer success");
                self.videoManager.callAnswered = YES;
                [self onAudioTalk:btn];
            } failure:^(LCError * _Nonnull error) {
                NSLog(@"doorbellCallAnswer fail");
                NSLog(@"%@, %@, %@", error.errorCode, error.errorMessage, error.errorInfo);
            }];
        }
    }];
}
- (void)onCallRefuse:(LCButton *)btn {
    NSLog(@"onCallRefuse");
    [self.videoManager.audioPlayer stop];
    if(self.videoManager.callAnswered){
        [OpenApiInterface doorbellCallHangUp:[LCApplicationDataManager token] deviceId:self.videoManager.currentDevice.deviceId success:^{
            NSLog(@"doorbellCallHangUp success");
        } failure:^(LCError * _Nonnull error) {
            NSLog(@"doorbellCallHangUp fail");
            NSLog(@"%@, %@, %@", error.errorCode, error.errorMessage, error.errorInfo);
        }];
    }else{
        [OpenApiInterface doorbellCallRefuse:[LCApplicationDataManager token] deviceId:self.videoManager.currentDevice.deviceId success:^{
            NSLog(@"doorbellCallRefuse success");
        } failure:^(LCError * _Nonnull error) {
            NSLog(@"doorbellCallRefuse fail");
            NSLog(@"%@, %@, %@", error.errorCode, error.errorMessage, error.errorInfo);
        }];
    }
    [self.playWindow uninitPlayWindow];
    UIViewController *yourCurrentViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (yourCurrentViewController.presentedViewController){
        yourCurrentViewController = yourCurrentViewController.presentedViewController;
    }
    [yourCurrentViewController dismissViewControllerAnimated:true completion:nil];
}
- (NSString *)savePath {
    return objc_getAssociatedObject(self, kLCLivePreviewPresenterSavePath);
}
- (void)setSavePath:(NSString *)savePath {
    objc_setAssociatedObject(self, kLCLivePreviewPresenterSavePath, savePath, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end
