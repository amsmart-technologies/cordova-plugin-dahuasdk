#import "LCVideotapeListHeardView.h"
#import "LCUIKit.h"
@interface LCVideotapeListHeardView ()
@property (strong, nonatomic) CAShapeLayer *lineLayer;
@property (strong, nonatomic) CAShapeLayer *ovalLayer;
@property (strong, nonatomic) UILabel *timeLab;
@end
@implementation LCVideotapeListHeardView
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupView];
    }
    return self;
}
- (void)setupView {
    self.timeLab = [[UILabel alloc] initWithFrame:CGRectMake(30,0,100, self.bounds.size.height)];
    self.timeLab.textColor = [UIColor dhcolor_c10];
    self.timeLab.font = [UIFont lcFont_t5];
    [self addSubview:self.timeLab];
}
- (void)setTime:(NSString *)time {
    _time = time;
    self.timeLab.text = time;
    [self drawLine];
}
- (void)drawRect:(CGRect)rect {
    NSLog(@"VIEW_JIA%@",NSStringFromCGRect(rect));
}
- (void)drawLine {
    [self.lineLayer removeFromSuperlayer];
    [self.ovalLayer removeFromSuperlayer];
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    [linePath moveToPoint:CGPointMake(20,self.frame.size.height)];
    if (self.index==0) {
        [linePath addLineToPoint:CGPointMake(20, self.bounds.size.height / 2.0)];
    }else{
        [linePath addLineToPoint:CGPointMake(20, 0)];
    }
    CAShapeLayer *lineLayer = [CAShapeLayer layer];
    lineLayer.lineWidth = 2;
    lineLayer.strokeColor = [UIColor dhcolor_c10].CGColor;
    lineLayer.path = linePath.CGPath;
    self.lineLayer = lineLayer;
    [self.layer addSublayer:lineLayer];
    UIBezierPath *ovalPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(15,self.bounds.size.height /2.0 - 5, 10, 10)];
    CAShapeLayer *ovalLayer = [CAShapeLayer layer];
    ovalLayer.lineWidth = 3;
    ovalLayer.fillColor = [UIColor dhcolor_c10].CGColor;
    ovalLayer.strokeColor = [UIColor dhcolor_c20].CGColor;
    ovalLayer.path = ovalPath.CGPath;
    self.ovalLayer = ovalLayer;
    [self.layer addSublayer:ovalLayer];
}
@end
