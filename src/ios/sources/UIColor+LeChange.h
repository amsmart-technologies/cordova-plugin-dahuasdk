#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface UIColor(LeChange)
#pragma mark 设备融合新增规范色值
+ (UIColor *)dhcolor_c0;
+ (UIColor *)dhcolor_c1;
+ (UIColor *)dhcolor_c2;
+ (UIColor *)dhcolor_c3;
+ (UIColor *)dhcolor_c4;
+ (UIColor *)dhcolor_c5;
+ (UIColor *)dhcolor_c6;
+ (UIColor *)dhcolor_c7;
+ (UIColor *)dhcolor_c8;
+ (UIColor *)dhcolor_c9;
+ (UIColor *)dhcolor_c10;
+ (UIColor *)dhcolor_c11;
+ (UIColor *)dhcolor_c12;
+ (UIColor *)dhcolor_c13;
+ (UIColor *)dhcolor_c15;
+ (UIColor *)dhcolor_c16;
+ (UIColor *)dhcolor_c00;
+ (UIColor *)dhcolor_c20;
+ (UIColor *)dhcolor_c21;
+ (UIColor *)dhcolor_c22;
+ (UIColor *)dhcolor_c30;
+ (UIColor *)dhcolor_c31;
+ (UIColor *)dhcolor_c32;
+ (UIColor *)dhcolor_c33;
+ (UIColor *)dhcolor_c34;
+ (UIColor *)dhcolor_c35;
+ (UIColor *)dhcolor_c40;
+ (UIColor *)dhcolor_c41;
+ (UIColor *)dhcolor_c42;
+ (UIColor *)dhcolor_c43;
+ (UIColor *)dhcolor_c44;
+ (UIColor *)dhcolor_c50;
+ (UIColor *)dhcolor_c51;
+ (UIColor *)dhcolor_c52;
+ (UIColor *)dhcolor_c53;
+ (UIColor *)dhcolor_c54;
+ (UIColor *)dhcolor_c55;
+ (UIColor *)dhcolor_c56;
+ ( UIColor *)dhcolor_c57;
+ (UIColor *)dhcolor_c58;
+ (UIColor *)dhcolor_c59;
+ (UIColor *)dhcolor_c60;
+ (UIColor *)dhcolor_c61;
+ (UIColor *)dhcolor_c62;
+ (UIColor *)dhcolor_c63;
+ (UIColor *)dhcolor_c64;
+ (UIColor *)dhcolor_c65;
#pragma mark Special Color
+ (UIColor *)dhcolor_confirm;
+ (UIColor *)dhcolor_progressBackgroundNormal;
+ (UIColor *)dhcolor_progressBackgroundHilighted;
@end
NS_ASSUME_NONNULL_END
