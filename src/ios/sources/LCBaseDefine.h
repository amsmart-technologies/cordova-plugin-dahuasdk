#ifndef LCBaseDefine_h
#define LCBaseDefine_h
#define SCREEN_BOUNDS       [UIScreen mainScreen].bounds
#define SCREEN_WIDTH        [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT       [[UIScreen mainScreen] bounds].size.height
#define MAINSCREEN_WIDTH    MIN(SCREEN_WIDTH,SCREEN_HEIGHT)
#define MAINSCREEN_HEIGHT   MAX(SCREEN_WIDTH,SCREEN_HEIGHT)
#define IOSVERSION7         ([[UIDevice currentDevice].systemVersion floatValue] > 6.9)
#define IOSVERSION8         ([[UIDevice currentDevice].systemVersion floatValue] > 7.9)
#define IOSVERSION9         ([[UIDevice currentDevice].systemVersion floatValue] > 8.9)
#define IOSVERSION10         ([[UIDevice currentDevice].systemVersion floatValue] > 9.9)
#define IOSVERSION11         ([[UIDevice currentDevice].systemVersion floatValue] > 10.9)
#define IOSVERSION13         ([[UIDevice currentDevice].systemVersion floatValue] > 12.9)
#define IPHONE5             (MAINSCREEN_HEIGHT > 480)
#define IPHONE6             (MAINSCREEN_HEIGHT > 570)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)
#define weakSelf(type)  __weak typeof(type) weak##type = type;
#define strongSelf(type)  __strong typeof(type) type = weak##type;
#define IS_IPHONE_4_OR_LESS (IS_IPHONE && MAINSCREEN_HEIGHT < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && MAINSCREEN_HEIGHT == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && MAINSCREEN_HEIGHT == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && MAINSCREEN_HEIGHT == 736.0)
#define PAGE_CONTENT_HEIGHT (MAINSCREEN_HEIGHT - 64)
#define IMAGE_CROP_SIZE CGSizeMake(125.0, 125.0)
#define kIs_iphone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define kIs_iPhoneX SCREEN_HEIGHT >=375.0f && SCREEN_HEIGHT >=812.0f&& kIs_iphone
#define kWidthRatio      (SCREEN_WIDTH / 375.0)
#define kHeightRatio     (SCREEN_HEIGHT / 667.0)
#define LC_KEY_WINDOW ([UIApplication sharedApplication].delegate).window
#define LC_IMAGENAMED(imageName)  [UIImage imageNamed:imageName]
#define LC_IMAGERATIO(image)  (image.size.height/image.size.width)
#define kStatusBarHeight (CGFloat)(kIs_iPhoneX?(44.0):(20.0))
#define kNavBarHeight (44)
#define kNavBarAndStatusBarHeight (CGFloat)(kIs_iPhoneX?(88.0):(64.0))
#define kTabBarHeight (CGFloat)(kIs_iPhoneX?(49.0 + 34.0):(49.0))
#define kTopBarSafeHeight (CGFloat)(kIs_iPhoneX?(44.0):(0))
#define kBottomSafeHeight (CGFloat)(kIs_iPhoneX?(34.0):(0))
#define kTopBarDifHeight (CGFloat)(kIs_iPhoneX?(24.0):(0))
#define kNavAndTabHeight (kNavBarAndStatusBarHeight + kTabBarHeight)
#define LC_LEGAL_DEVICENAME       @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!#$%()*+,-./<=>?@[\\]^_`{|}~"
#define SuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)
#endif
