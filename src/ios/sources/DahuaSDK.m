#import <Cordova/CDV.h>

#import "DahuaSDK.h"

#import "LCDeviceNetInfo.h"
#import "DHMobileInfo.h"
#import "LCNetSDKInitialManager.h"
#import "LCNetSDKSearchManager.h"
#import "DHNetWorkHelper.h"
#import "LCNetSDKHelper.h"
#import "OpenApiInterface.h"
#import "LCApplicationDataManager.h"
#import "LCDeviceSettingViewController.h"
#import "LCDeviceVideoManager.h"
#import "LCLivePreviewViewController.h"
#import "LCPermissionHelper.h"
#import "LCQRCode.h"
#import "LCSmartConfig.h"
#import "LCTimeZoneConfig.h"
#import "LCMessageCallback.h"
#import "LCAlarmMessage.h"
#import "CloudRecordCallNum.h"
#import "StorageStrategy.h"
#import "SDCardStatusData.h"
#import "UIImageView+Surface.h"
#import "UIDevice+LeChange.h"

#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_Api.h>
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_Bluetooth.h>

@interface DahuaSDK ()
@property(nonatomic, strong) NSTimer *timer;
@property(nonatomic, assign) NSInteger tickCount;
@property(nonatomic, copy) NSString *callbackId;
@property(nonatomic, copy) NSString *deviceId;
@property(strong, nonatomic) LCPermissionHelper *helper;
@end

@implementation DahuaSDK

static NSString *callback;

- (void)setEnv:(CDVInvokedUrlCommand *)command {
    NSLog(@"setEnv");
    
    NSString *appId = [command.arguments objectAtIndex:0];
    [LCApplicationDataManager setAppIdWith:appId];
    
    NSString *appSecret = [command.arguments objectAtIndex:1];
    [LCApplicationDataManager setAppSecretWith:appSecret];
    
    NSString *url = [command.arguments objectAtIndex:2];
    [LCApplicationDataManager setHostApiWith:url];

    CDVPluginResult *pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getToken:(CDVInvokedUrlCommand *)command {
    NSLog(@"getToken");
    self.helper = [LCPermissionHelper new];
    [LCPermissionHelper requestAlbumPermission:^(BOOL granted){ }];
    [LCPermissionHelper requestAudioPermission:^(BOOL granted){ }];
    [self.helper requestAlwaysLocationPermissions:YES completion:^(BOOL granted){ }];
    
    [[DHNetWorkHelper sharedInstance] checkNetwork];
    
    [OpenApiInterface accessTokenWithsuccess:^(LCAuthModel *_Nonnull authInfo) {
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:authInfo.accessToken];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } failure:^(LCError *_Nonnull error) {
        NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)initialize:(CDVInvokedUrlCommand *)command {
    NSLog(@"initialize");
    NSString *token = [command.arguments objectAtIndex:0];
    [LCApplicationDataManager setManagerToken:token];
    
    LCOpenSDK_ApiParam *param = [LCOpenSDK_ApiParam new];
    param.procotol = [[LCApplicationDataManager hostApi] containsString:@"https"] ? PROCOTOL_TYPE_HTTPS : PROCOTOL_TYPE_HTTP;
    param.addr = [LCApplicationDataManager SDKHost];
    param.port = [LCApplicationDataManager SDKPort];
    param.token = token;
    [[LCOpenSDK_Api shareMyInstance] initOpenApi:param];
    
    CDVPluginResult *pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)parseQRcode:(CDVInvokedUrlCommand *)command {
    NSLog(@"parseQRcode");
    NSString *text = [command.arguments objectAtIndex:0];
    [self.commandDelegate runInBackground:^{
        LCQRCode *code = [LCQRCode new];
        [code pharseQRCode:text];
        NSString *str = [NSString stringWithFormat: @"{\"sn\":\"%@\",\"mode\":\"%@\",\"sc\":\"%@\",\"nc\":\"%@\",\"productId\":\"%@\"}", code.deviceSN, code.deviceType, code.scCode, code.ncCode, code.iotDeviceType];
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:str];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)createSubAccount:(CDVInvokedUrlCommand *)command {
    NSLog(@"createSubAccount");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *account = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface createSubAccount:token account:account success:^(LCAuthModel *_Nonnull authInfo) {
            CDVPluginResult *pluginResult = nil;
            NSString *str = [NSString stringWithFormat:@"{\"openid\":\"%@\"}", authInfo.openid];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getOpenIdByAccount:(CDVInvokedUrlCommand *)command {
    NSLog(@"getOpenIdByAccount");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *account = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface getOpenIdByAccount:token account:account success:^(LCAuthModel *_Nonnull authInfo) {
            CDVPluginResult *pluginResult = nil;
            NSString *str = [NSString stringWithFormat:@"{\"openid\":\"%@\"}", authInfo.openid];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getSubAccountToken:(CDVInvokedUrlCommand *)command {
    NSLog(@"getSubAccountToken");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *openid = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [LCApplicationDataManager setOpenid:openid];
        [OpenApiInterface getSubAccountToken:token openid:openid success:^(LCAuthModel *_Nonnull authInfo) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:authInfo.accessToken];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)listSubAccount:(CDVInvokedUrlCommand *)command {
    NSLog(@"listSubAccount");
    NSString *token = [command.arguments objectAtIndex:0];
    NSNumber *pageNo = [command.arguments objectAtIndex:1];
    NSNumber *pageSize = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface listSubAccount:token pageNo:pageNo pageSize:pageSize success:^(NSString *_Nonnull accounts) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:accounts];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)deleteSubAccount:(CDVInvokedUrlCommand *)command {
    NSLog(@"deleteSubAccount");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *openid = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface deleteSubAccount:token openid:openid success:^ {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)listSubAccountDevice:(CDVInvokedUrlCommand *)command {
    NSLog(@"listSubAccountDevice");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *openid = [command.arguments objectAtIndex:1];
    NSNumber *pageNo = [command.arguments objectAtIndex:2];
    NSNumber *pageSize = [command.arguments objectAtIndex:3];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface listSubAccountDevice:token openid:openid pageNo:pageNo pageSize:pageSize success:^(NSString *_Nonnull devices) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:devices];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)subAccountDeviceList:(CDVInvokedUrlCommand *)command {
    NSLog(@"subAccountDeviceList");
    NSString *token = [command.arguments objectAtIndex:0];
    NSNumber *pageNo = [command.arguments objectAtIndex:1];
    NSNumber *pageSize = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface subAccountDeviceList:token pageNo:pageNo pageSize:pageSize success:^(NSString *_Nonnull devices) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:devices];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)listDeviceDetailsByPage:(CDVInvokedUrlCommand *)command {
    NSLog(@"listDeviceDetailsByPage");
    NSString *token = [command.arguments objectAtIndex:0];
    NSNumber *page = [command.arguments objectAtIndex:1];
    NSNumber *pageSize = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface listDeviceDetailsByPage:token page:page pageSize:pageSize success:^(NSString *_Nonnull devices) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:devices];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)addDeviceToSubAccount:(CDVInvokedUrlCommand *)command {
    NSLog(@"addDeviceToSubAccount");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *openid = [command.arguments objectAtIndex:1];
    NSString *policy = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface addDeviceToSubAccount:token openid:openid policy:policy success:^{
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getDevicesList:(CDVInvokedUrlCommand *)command {
    NSLog(@"getDevicesList");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *openid = [command.arguments objectAtIndex:1];
    NSNumber *pageNo = [command.arguments objectAtIndex:2];
    NSNumber *pageSize = [command.arguments objectAtIndex:3];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface listSubAccountDevice2:token openid:openid pageNo:pageNo pageSize:pageSize success:^(NSMutableArray<LCDeviceInfo *> *_Nonnull devices) {
            NSNumber *totalDevices = [OpenApiInterface totalDevices];
            NSArray *dictArray = [LCDeviceInfo mj_keyValuesArrayWithObjectArray:devices];
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictArray != nil ? dictArray : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            jsonString = [@"" stringByAppendingString: [NSString stringWithFormat:@"{\"total\":%@,\"deviceList\":%@}",totalDevices, jsonString]];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getSnapShot:(CDVInvokedUrlCommand *)command {
    NSLog(@"getSnapShot");
    NSString *deviceString = [command.arguments objectAtIndex:0];
    LCDeviceInfo *device = [LCDeviceInfo mj_objectWithKeyValues:deviceString];
    NSString *channelId = [command.arguments objectAtIndex:1];
    UIImage *image = [[UIImageView new] lc_getThumbImageForDeviceId:device.deviceId ChannelId:channelId];
    NSString *base64 = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:nil];
    CDVPluginResult *pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:base64 ? base64 : @"null"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)openDevicePlay:(CDVInvokedUrlCommand *)command {
    NSLog(@"openDevicePlay");
    NSString *deviceString = [command.arguments objectAtIndex:0];
    NSString *channelId = [command.arguments objectAtIndex:1];
    BOOL playbackEnabled = [[command.arguments objectAtIndex:2] boolValue];
    BOOL callBellEvent = [[command.arguments objectAtIndex:3] boolValue];
    NSInteger currentChannelIndex = -1;
    LCDeviceInfo *device = [LCDeviceInfo mj_objectWithKeyValues:deviceString];
    LCCIResolutions *currentResolution;
    int index = 0;
    for (LCChannelInfo *channel in device.channels) {
        if (channel.channelId == channelId) {
            currentChannelIndex = index;
            if ([channel.resolutions count] > 0) {
                currentResolution = [channel.resolutions firstObject];
            }
        }
        index++;
    }
    [LCDeviceVideoManager manager].currentDevice = device;
    [LCDeviceVideoManager manager].currentChannelIndex = currentChannelIndex;
    [LCDeviceVideoManager manager].playbackEnabled = playbackEnabled;
    [LCDeviceVideoManager manager].callBellEvent = callBellEvent;
    [LCDeviceVideoManager manager].currentResolution = currentResolution;
    LCLivePreviewViewController *livePreview = [(LCLivePreviewViewController *)[NSClassFromString(@"LCLivePreviewViewController") alloc] init];
    livePreview.finishBlock = ^{
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    };
    [livePreview setModalPresentationStyle:UIModalPresentationFullScreen];
    [self.viewController presentViewController:livePreview animated:YES completion:nil];
}

- (void)openDeviceDetail:(CDVInvokedUrlCommand *)command {
    NSLog(@"openDeviceDetail");
    NSString *deviceString = [command.arguments objectAtIndex:0];
    LCDeviceInfo *device = [LCDeviceInfo mj_objectWithKeyValues:deviceString];
    [LCDeviceVideoManager manager].currentDevice = device;
    [LCDeviceVideoManager manager].currentChannelIndex = -1;
    LCDeviceSettingViewController *deviceSetting = [[LCDeviceSettingViewController alloc] init];
    deviceSetting.style = LCDeviceSettingStyleMainPage;
    deviceSetting.finishBlock = ^{
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    };
    [deviceSetting setModalPresentationStyle:UIModalPresentationFullScreen];
    [self.viewController presentViewController:deviceSetting animated:YES completion:nil];
}

- (void)deviceInfoBeforeBind:(CDVInvokedUrlCommand *)command {
    NSLog(@"deviceInfoBeforeBind");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *deviceCodeModel = [command.arguments objectAtIndex:2];
    NSString *deviceModelName = [command.arguments objectAtIndex:3];
    NSString *ncCode = [command.arguments objectAtIndex:4];
    NSString *productId = [command.arguments objectAtIndex:5];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface unBindDeviceInfoForDevice:token deviceId:deviceId DeviceModel:deviceCodeModel DeviceName:deviceModelName ncCode:ncCode productId:productId success:^(DHUserDeviceBindInfo *_Nonnull info) {
            NSDictionary *statusDict = info.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)userDeviceBind:(CDVInvokedUrlCommand *)command {
    NSLog(@"userDeviceBind");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *code = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface bindDeviceWithDevice:token deviceId:deviceId Code:code success:^{
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)deviceLeadingInfo:(CDVInvokedUrlCommand *)command {
    NSLog(@"deviceLeadingInfo");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceModel = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface getDeviceIntroductionForDeviceModel:token deviceModel:deviceModel success:^(DHOMSIntroductionInfo *_Nonnull introductions) {
            NSDictionary *statusDict = introductions.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError *_Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)startSearchService:(CDVInvokedUrlCommand *)command {
    NSLog(@"startSearchService");
    [[LCNetSDKSearchManager sharedInstance] startSearch];
    CDVPluginResult *pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)stopSearchService:(CDVInvokedUrlCommand *)command {
    NSLog(@"stopSearchService");
    [[LCNetSDKSearchManager sharedInstance] stopSearch];
    CDVPluginResult *pluginResult = nil;
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getDeviceNetInfo:(CDVInvokedUrlCommand *)command {
    NSLog(@"getDeviceNetInfo");
    NSString *deviceId = [command.arguments objectAtIndex:0];
    LCDeviceNetInfo *deviceNetInfo = [[LCNetSDKSearchManager sharedInstance] getNetInfoByID:deviceId];
    if (deviceNetInfo != nil) {
        NSDictionary *statusDict = deviceNetInfo.mj_keyValues;
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    } else {
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"null"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void)startDevInitByIp:(CDVInvokedUrlCommand *)command {
    NSLog(@"startDevInitByIp");
    NSString *deviceId = [command.arguments objectAtIndex:0];
    NSString *password = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        LCDeviceNetInfo *deviceNetInfo = [[LCNetSDKSearchManager sharedInstance] getNetInfoByID:deviceId];
        if (deviceNetInfo != nil) {
            [[LCNetSDKInitialManager sharedInstance] setDeviceID:deviceId];
            [[LCNetSDKInitialManager sharedInstance] initialDeviceWithPassWord:password isSoftAp:NO deviceNetInfo:deviceNetInfo withSuccessBlock:^{
                CDVPluginResult *pluginResult = nil;
                pluginResult =  [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            } withFailureBlock:^{
                CDVPluginResult *pluginResult = nil;
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }];
        } else {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"null"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}

- (void)checkWifi:(CDVInvokedUrlCommand *)command {
    NSLog(@"checkWifi");
    [self.commandDelegate runInBackground:^{
        NSString *wifi = [DHMobileInfo sharedInstance].WIFISSID;
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:wifi ? wifi : @"null"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)startSmartConfig:(CDVInvokedUrlCommand *)command {
    NSLog(@"startSmartConfig");
    NSString *deviceId = [command.arguments objectAtIndex:0];
    NSString *ssid = [command.arguments objectAtIndex:1];
    NSString *password = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        self.deviceId = deviceId;
        [[LCSmartConfig shareInstance] startConfigWithDevice:deviceId ssid:ssid password:password security:@"" fskMode:0];
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)stopSmartConfig:(CDVInvokedUrlCommand *)command {
    NSLog(@"stopSmartConfig");
    [self.commandDelegate runInBackground:^{
        [[LCSmartConfig shareInstance] stopConfig];
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)listenForSearchedDevices:(CDVInvokedUrlCommand *)command {
    NSLog(@"listenForSearchedDevices");
    NSString *deviceId = [command.arguments objectAtIndex:0];
    self.deviceId = deviceId;
    self.callbackId = command.callbackId;
    if (self.timer != nil) {
        [self.timer fire];
        [self.timer invalidate];
        self.timer = nil;
    }
    NSInteger tick = 4;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:tick target:self selector:@selector(timeTick) userInfo:nil repeats:YES];
    [self timeTick];
}

- (void)timeTick {
    LCDeviceNetInfo *deviceNetInfo = [[LCNetSDKSearchManager sharedInstance] getNetInfoByID:self.deviceId];
    if (deviceNetInfo != nil) {
        if (self.timer != nil) {
            [self.timer invalidate];
            self.timer = nil;
        }
        NSDictionary *statusDict = deviceNetInfo.mj_keyValues;
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
        [pluginResult setKeepCallbackAsBool:NO];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackId];
    } else {
        CDVPluginResult *pluginResult = nil;
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"null"];
        [pluginResult setKeepCallbackAsBool:YES];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackId];
    }
}

- (void)getWifiListSoftAp:(CDVInvokedUrlCommand *)command {
    NSLog(@"getWifiListSoftAp");
    [self.commandDelegate runInBackground:^{
        NSString *devicePwd = [command.arguments objectAtIndex:0];
        BOOL hasSc = [command.arguments objectAtIndex:2];
        LCDeviceNetInfo *deviceNetInfo = [[LCNetSDKSearchManager sharedInstance] getNetInfoByID:self.deviceId];
        NSString *deviceIp = deviceNetInfo.deviceIP;
        NSString *gatewayIp = UIDevice.lc_getRouterAddress;
        int port = deviceNetInfo.port;
        if (deviceIp.length == 0) {
            deviceIp = gatewayIp;
        }
        [LCNetSDKHelper getSoftApWifiList:deviceIp port:port devicePassword:devicePwd isSC:hasSc success:^(NSArray<LCOpenSDK_WifiInfo *> * _Nullable list) {
            NSArray *dictArray = [LCWifiInfo mj_keyValuesArrayWithObjectArray:list];
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictArray != nil ? dictArray : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(NSInteger code, NSString * _Nullable describe) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%ld\",\"msg\":\"%@\"}", (long)code, @"ERROR"];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)connectDeviceToWifi:(CDVInvokedUrlCommand *)command {
    NSLog(@"connectDeviceToWifi");
    [self.commandDelegate runInBackground:^{
        NSString *wifiName = [command.arguments objectAtIndex:0];
        NSString *wifiPwd = [command.arguments objectAtIndex:1];
        NSString *deviceId = [command.arguments objectAtIndex:2];
        NSString *devicePwd = [command.arguments objectAtIndex:3];
        BOOL hasSc = [command.arguments objectAtIndex:4];
        NSNumber *wifiEncry = [command.arguments objectAtIndex:5];
        LCDeviceNetInfo *deviceNetInfo = [[LCNetSDKSearchManager sharedInstance] getNetInfoByID:deviceId];
        NSString *deviceIp = deviceNetInfo.deviceIP;
        [LCNetSDKHelper startSoftAPConfig:wifiName wifiPwd:wifiPwd wifiEncry:[wifiEncry intValue] netcardName:@"" deviceIp:deviceIp devicePwd:devicePwd isSC:hasSc handler:^(NSInteger result) {
            if (result == 0) {
                [self connectToWifi:wifiName wifiPwd:wifiPwd complete:^(NSString *success) { }];
                CDVPluginResult *pluginResult = nil;
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            } else {
                NSString *str = [NSString stringWithFormat:@"{\"code\":\"%ld\",\"msg\":\"%@\"}", 1, @"ERROR"];
                CDVPluginResult *pluginResult = nil;
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }
        } timeout: 5000 * 2];
    }];
}

- (void)connectPhoneToWifi:(CDVInvokedUrlCommand *)command {
    NSLog(@"connectPhoneToWifi");
    [self.commandDelegate runInBackground:^{
        NSString *wifiName = [command.arguments objectAtIndex:0];
        NSString *wifiPwd = [command.arguments objectAtIndex:1];
        [self connectToWifi:wifiName wifiPwd:wifiPwd complete:^(NSString *success) {
            if ([success isEqual: @"CONNECTED"]) {
                CDVPluginResult *pluginResult = nil;
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            } else {
                NSString *str = [NSString stringWithFormat:@"{\"code\":\"%d\",\"msg\":\"%@\"}", 1, success];
                CDVPluginResult *pluginResult = nil;
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
                [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }
        }];
    }];
}

- (void)connectToWifi:(NSString *)wifiName wifiPwd:(NSString *)wifiPwd complete:(void (^)(NSString *))complete{
    if (@available (iOS 11.0, *)) {
        NEHotspotConfiguration * configuration = [[NEHotspotConfiguration alloc] initWithSSID:wifiName passphrase:wifiPwd isWEP:NO];
        configuration.joinOnce = NO;
        [[NEHotspotConfigurationManager sharedManager] applyConfiguration: configuration completionHandler: ^ (NSError * _Nullable error) {
            if (nil == error) {
                complete(@"CONNECTED");
            } else {
                NSLog(@"%@", error);
                complete(@"NOT_CONNECTED");
            }}];
    } else {
        complete(@"NOT_GT_IOS11");
    }
}

- (void)modifyDeviceName:(CDVInvokedUrlCommand *)command {
    NSLog(@"modifyDeviceName");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    NSString *name = [command.arguments objectAtIndex:3];
    if([channelId isEqual: @"null"]){
        channelId = nil;
    }
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface modifyDeviceForDevice:token deviceId:deviceId Channel:channelId NewName:name success:^{
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)searchDeviceBLE:(CDVInvokedUrlCommand *)command {
    NSLog(@"searchDeviceBLE");
    NSString *wifiName = [command.arguments objectAtIndex:0];
    NSString *wifiPwd = [command.arguments objectAtIndex:1];
    NSString *deviceId = [command.arguments objectAtIndex:2];
    NSString *deviceProductId = [command.arguments objectAtIndex:3];
    [LCOpenSDK_Bluetooth startAsyncBLEConfig:wifiName wifiPwd:wifiPwd productId:deviceProductId deviceId:deviceId finshed:^(BOOL success, NSString * _Nullable errorMessage) {
        if (success) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } else {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%d\",\"msg\":\"%@\"}", 1, errorMessage.description];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    }];
}

- (void)unbindDevice:(CDVInvokedUrlCommand *)command {
    NSLog(@"unbindDevice");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface unBindDeviceWithDevice:token deviceId:deviceId success:^{
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)bindDeviceChannelInfo:(CDVInvokedUrlCommand *)command {
    NSLog(@"bindDeviceChannelInfo");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface bindDeviceChannelInfoWithDevice:token deviceId:deviceId ChannelId:channelId success:^(LCBindDeviceChannelInfo * _Nonnull info) {
            NSDictionary *statusDict = info.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)modifyDeviceAlarmStatus:(CDVInvokedUrlCommand *)command {
    NSLog(@"modifyDeviceAlarmStatus");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    NSString *enable = [command.arguments objectAtIndex:3];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface modifyDeviceAlarmStatus:token deviceId:deviceId channelId:channelId enable:enable success:^{
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)deviceVersionList:(CDVInvokedUrlCommand *)command {
    NSLog(@"deviceVersionList");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface deviceVersionForDevices:token devices:@[deviceId] success:^(NSMutableArray<LCDeviceVersionInfo *> * _Nonnull info) {
            NSArray *dictArray = [LCDeviceVersionInfo mj_keyValuesArrayWithObjectArray:info];
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictArray != nil ? dictArray : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            jsonString = [@"" stringByAppendingString: [NSString stringWithFormat:@"{\"deviceVersionList\":%@}", jsonString]];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)wifiAround:(CDVInvokedUrlCommand *)command {
    NSLog(@"wifiAround");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface wifiAroundDevice:token deviceId:deviceId success:^(LCAroundWifiInfo * _Nonnull wifiInfo) {
            NSDictionary *statusDict = wifiInfo.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)controlDeviceWifi:(CDVInvokedUrlCommand *)command {
    NSLog(@"controlDeviceWifi");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *ssid = [command.arguments objectAtIndex:2];
    NSString *bssid = [command.arguments objectAtIndex:3];
    NSString *password = [command.arguments objectAtIndex:4];
    [self.commandDelegate runInBackground:^{
        LCWifiConnectSession *session = [LCWifiConnectSession new];
        session.bssid = bssid;
        session.ssid = ssid;
        session.password = password;
        session.linkEnable = YES;
        [OpenApiInterface controlDeviceWifiFor:token deviceId:deviceId ConnestSession:session success:^{
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)timeZoneQueryByDay:(CDVInvokedUrlCommand *)command {
    NSLog(@"timeZoneQueryByDay");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface timeZoneQueryByDay:token deviceId:deviceId success:^(LCTimeZoneConfig * _Nonnull config) {
            NSDictionary *statusDict = config.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)timeZoneConfigByDay:(CDVInvokedUrlCommand *)command {
    NSLog(@"timeZoneConfigByDay");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *areaIndex = [command.arguments objectAtIndex:2];
    NSString *timeZone = [command.arguments objectAtIndex:3];
    NSString *beginSunTime = [command.arguments objectAtIndex:4];
    NSString *endSunTime = [command.arguments objectAtIndex:5];
    if([beginSunTime isEqual: @"null"]){
        beginSunTime = nil;
    }
    if([endSunTime isEqual: @"null"]){
        endSunTime = nil;
    }
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface timeZoneConfigByDay:token deviceId:deviceId areaIndex:areaIndex timeZone:timeZone beginSunTime:beginSunTime endSunTime:endSunTime success:^{
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)setMessageCallback:(CDVInvokedUrlCommand *)command {
    NSLog(@"setMessageCallback");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *status = [command.arguments objectAtIndex:1];
    NSString *callbackUrl = [command.arguments objectAtIndex:2];
    NSString *callbackFlag = [command.arguments objectAtIndex:3];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface setMessageCallback:token status:status callbackUrl:callbackUrl callbackFlag:callbackFlag success:^ {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getMessageCallback:(CDVInvokedUrlCommand *)command {
    NSLog(@"getMessageCallback");
    NSString *token = [command.arguments objectAtIndex:0];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface getMessageCallback:token success:^(LCMessageCallback * _Nonnull callback) {
            NSDictionary *statusDict = callback.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getAlarmMessage:(CDVInvokedUrlCommand *)command {
    NSLog(@"getAlarmMessage");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    NSString *beginTime = [command.arguments objectAtIndex:3];
    NSString *endTime = [command.arguments objectAtIndex:4];
    NSString *count = [command.arguments objectAtIndex:5];
    NSString *nextAlarmId = [command.arguments objectAtIndex:6];
    if([count isEqual: @"null"]){
        count = nil;
    }
    if([nextAlarmId isEqual: @"null"]){
        nextAlarmId = nil;
    }
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface getAlarmMessage:token deviceId:deviceId channelId:channelId beginTime:beginTime endTime:endTime count:count nextAlarmId:nextAlarmId success:^(LCAlarmMessage * _Nonnull alarmMessage) {
            NSDictionary *statusDict = alarmMessage.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)deleteAlarmMessage:(CDVInvokedUrlCommand *)command {
    NSLog(@"deleteAlarmMessage");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *indexId = [command.arguments objectAtIndex:2];
    NSString *channelId = [command.arguments objectAtIndex:3];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface deleteAlarmMessage:token deviceId:deviceId indexId:indexId channelId:channelId success:^ {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)queryCloudRecordCallNum:(CDVInvokedUrlCommand *)command {
    NSLog(@"queryCloudRecordCallNum");
    NSString *token = [command.arguments objectAtIndex:0];
    NSNumber *strategyId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface queryCloudRecordCallNum:token strategyId:strategyId success:^(CloudRecordCallNum * _Nonnull cloudRecordCallNum) {
            NSDictionary *statusDict = cloudRecordCallNum.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)openCloudRecord:(CDVInvokedUrlCommand *)command {
    NSLog(@"openCloudRecord");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    NSNumber *strategyId = [command.arguments objectAtIndex:3];
    NSString *deviceCloudId = [command.arguments objectAtIndex:4];
    if([deviceCloudId isEqual: @"null"]){
        deviceCloudId = nil;
    }
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface openCloudRecord:token deviceId:deviceId channelId:channelId strategyId:strategyId deviceCloudId:deviceCloudId success:^ {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getDeviceCloud:(CDVInvokedUrlCommand *)command {
    NSLog(@"getDeviceCloud");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface getDeviceCloud:token deviceId:deviceId channelId:channelId success:^(StorageStrategy * _Nonnull storageStrategy) {
            NSDictionary *statusDict = storageStrategy.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)deviceSdcardStatus:(CDVInvokedUrlCommand *)command {
    NSLog(@"deviceSdcardStatus");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface deviceSdcardStatus:token deviceId:deviceId success:^(SDCardStatusData * _Nonnull sdcardStatus) {
            NSDictionary *statusDict = sdcardStatus.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)deviceStorage:(CDVInvokedUrlCommand *)command {
    NSLog(@"deviceStorage");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface deviceStorage:token deviceId:deviceId success:^(SDCardStatusData * _Nonnull sdcardStatus) {
            NSDictionary *statusDict = sdcardStatus.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)recoverSDCard:(CDVInvokedUrlCommand *)command {
    NSLog(@"recoverSDCard");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface recoverSDCard:token deviceId:deviceId success:^(SDCardStatusData * _Nonnull sdcardStatus) {
            NSDictionary *statusDict = sdcardStatus.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)setDeviceSnap:(CDVInvokedUrlCommand *)command {
    NSLog(@"setDeviceSnap");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface setDeviceSnap:token deviceId:deviceId channelId:channelId success:^(NSString * _Nonnull result) {
            NSString *str = [NSString stringWithFormat:@"{\"url\":\"%@\"}", result];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)queryLocalRecordPlan:(CDVInvokedUrlCommand *)command {
    NSLog(@"queryLocalRecordPlan");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface queryLocalRecordPlan:token deviceId:deviceId channelId:channelId success:^(LCAlarmPlan * _Nonnull plan) {
            NSDictionary *statusDict = plan.mj_keyValues;
            NSError *error = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:statusDict != nil ? statusDict : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)setLocalRecordPlanRules:(CDVInvokedUrlCommand *)command {
    NSLog(@"setLocalRecordPlanRules");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    NSString *rules = [command.arguments objectAtIndex:3];
    NSString *str = [NSString stringWithFormat:@"{\"channelId\":\"%@\",\"rules\":\%@}", channelId, rules];
    [self.commandDelegate runInBackground:^{
        LCAlarmPlan *plan = [LCAlarmPlan mj_objectWithKeyValues:str];
        [OpenApiInterface setLocalRecordPlanRules:token deviceId:deviceId channelId:channelId plan:plan success:^ {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)restartDevice:(CDVInvokedUrlCommand *)command {
    NSLog(@"restartDevice");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface restartDevice:token deviceId:deviceId success:^ {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)modifyDevicePwd:(CDVInvokedUrlCommand *)command {
    NSLog(@"modifyDevicePwd");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *oldPwd = [command.arguments objectAtIndex:2];
    NSString *newPwd = [command.arguments objectAtIndex:3];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface modifyDevicePwd:token deviceId:deviceId oldPwd:oldPwd newPwd:newPwd success:^ {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)upgradeProcessDevice:(CDVInvokedUrlCommand *)command {
    NSLog(@"upgradeProcessDevice");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface upgradeProcessDevice:token deviceId:deviceId success:^(NSString * _Nonnull result) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)upgradeDevice:(CDVInvokedUrlCommand *)command {
    NSLog(@"upgradeDevice");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface upgradeDevice:token deviceId:deviceId success:^(NSString * _Nonnull result) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)wakeUpDevice:(CDVInvokedUrlCommand *)command {
    NSLog(@"wakeUpDevice");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface wakeUpDevice:token deviceId:deviceId success:^ {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)deviceOnline:(CDVInvokedUrlCommand *)command {
    NSLog(@"deviceOnline");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface deviceOnline:token deviceId:deviceId success:^(NSString * _Nonnull result) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getDevicePowerInfo:(CDVInvokedUrlCommand *)command {
    NSLog(@"getDevicePowerInfo");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface getDevicePowerInfo:token deviceId:deviceId success:^(NSString * _Nonnull result) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)getDeviceCameraStatus:(CDVInvokedUrlCommand *)command {
    NSLog(@"getDeviceCameraStatus");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    NSString *enableType = [command.arguments objectAtIndex:3];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface getDeviceCameraStatus:token deviceId:deviceId channelId:channelId enableType:enableType success:^(NSString * _Nonnull result) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

- (void)setDeviceCameraStatus:(CDVInvokedUrlCommand *)command {
    NSLog(@"setDeviceCameraStatus");
    NSString *token = [command.arguments objectAtIndex:0];
    NSString *deviceId = [command.arguments objectAtIndex:1];
    NSString *channelId = [command.arguments objectAtIndex:2];
    NSString *enableType = [command.arguments objectAtIndex:3];
    BOOL *enable = [[command.arguments objectAtIndex:4] boolValue];
    [self.commandDelegate runInBackground:^{
        [OpenApiInterface setDeviceCameraStatus:token deviceId:deviceId channelId:channelId enableType:enableType enable:enable success:^(NSString * _Nonnull result) {
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } failure:^(LCError * _Nonnull error) {
            NSString *str = [NSString stringWithFormat:@"{\"code\":\"%@\",\"msg\":\"%@\"}", error.errorCode, error.errorMessage];
            CDVPluginResult *pluginResult = nil;
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:str];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    }];
}

@end
