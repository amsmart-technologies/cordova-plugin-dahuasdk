#import "OpenApiInterface.h"
@implementation OpenApiInterface
static NSNumber *totalDevices = 0;
+ (NSNumber *)totalDevices{
    return totalDevices;
}
+ (void)accessTokenWithsuccess:(void (^)(LCAuthModel *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/accessToken" parameters:@{} success:^(id _Nonnull objc) {
        LCAuthModel *model = [LCAuthModel mj_objectWithKeyValues:objc];
        if (success) {
            success(model);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)createSubAccount:(NSString *)token account:(NSString *)account success:(void (^)(LCAuthModel *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/createSubAccount" parameters:@{ KEY_TOKEN: token, KEY_ACCOUNT: account } success:^(id _Nonnull objc) {
        LCAuthModel *model = [LCAuthModel mj_objectWithKeyValues:objc];
        if (success) {
            success(model);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)getOpenIdByAccount:(NSString *)token account:(NSString *)account success:(void (^)(LCAuthModel *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/getOpenIdByAccount" parameters:@{ KEY_TOKEN: token, KEY_ACCOUNT: account } success:^(id _Nonnull objc) {
        LCAuthModel *model = [LCAuthModel mj_objectWithKeyValues:objc];
        if (success) {
            success(model);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)getSubAccountToken:(NSString *)token openid:(NSString *)openid success:(void (^)(LCAuthModel *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/subAccountToken" parameters:@{ KEY_TOKEN: token, KEY_OPENID: openid } success:^(id _Nonnull objc) {
        LCAuthModel *model = [LCAuthModel mj_objectWithKeyValues:objc];
        [LCApplicationDataManager setSubAccountToken:model.accessToken];
        if (success) {
            success(model);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)listSubAccount:(NSString *)token pageNo:(NSNumber *)pageNo pageSize:(NSNumber *)pageSize success:(void (^)(NSString *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/listSubAccount" parameters:@{ KEY_TOKEN: token, KEY_PAGENO: pageNo, KEY_PAGESIZE: pageSize } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)deleteSubAccount:(NSString *)token openid:(nonnull NSString *)openid success:(void (^)(void))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/deleteSubAccount" parameters:@{ KEY_TOKEN: token, KEY_OPENID:openid } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)listSubAccountDevice:(NSString *)token openid:(NSString *)openid pageNo:(NSNumber*)pageNo pageSize:(NSNumber*)pageSize success:(void (^)(NSString *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/listSubAccountDevice" parameters:@{ KEY_TOKEN: token, KEY_OPENID: openid, KEY_PAGENO: pageNo, KEY_PAGESIZE: pageSize } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)subAccountDeviceList:(NSString *)token pageNo:(NSNumber*)pageNo pageSize:(NSNumber*)pageSize success:(void (^)(NSString *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/subAccountDeviceList" parameters:@{ KEY_TOKEN: token, KEY_PAGENO: pageNo, KEY_PAGESIZE: pageSize } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)listDeviceDetailsByPage:(NSString *)token page:(NSNumber*)page pageSize:(NSNumber*)pageSize success:(void (^)(NSString *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/listDeviceDetailsByPage" parameters:@{ KEY_TOKEN: token, KEY_PAGE: page, KEY_PAGESIZE: pageSize } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)addDeviceToSubAccount:(NSString *)token openid:(NSString *)openid policy:(NSString *)policy success:(void (^)(void))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/addPolicy" parameters:@{ KEY_TOKEN: token, KEY_OPENID: openid, KEY_POLICY: policy } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)getDeviceIntroductionForDeviceModel:(NSString *)token deviceModel:(NSString *)deviceModel success:(void (^)(DHOMSIntroductionInfo *introductions))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceAddingProcessGuideInfoGet" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_MODEL_NAME: deviceModel } success:^(id _Nonnull objc) {
        DHOMSIntroductionInfo *introductions = [DHOMSIntroductionInfo mj_objectWithKeyValues:objc];
        if (introductions.updateTime == nil) {
            introductions.updateTime = @"";
        }
        if (success) {
            success(introductions);
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)checkDeviceIntroductionWithUpdateTime:(NSString *)updateTime success:(void (^)(BOOL isUpdated))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceAddingProcessGuideInfoCheck" parameters:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_UPDATETIME: updateTime } success:^(id _Nonnull objc) {
        BOOL update = [[objc objectForKey:@"isUpdated"] boolValue];
        if (success) {
            success(update);
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)queryAllProductWithDeviceType:(NSString *)deviceModel Success:(void (^)(NSDictionary *productList))success failure:(void (^)(LCError *error))failure {
    if (deviceModel == nil || deviceModel.isNull) {
        deviceModel = @"Camera";
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceModelList" parameters:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_DEVICE_TYPE: deviceModel } success:^(id _Nonnull objc) {
        NSMutableArray <DHOMSDeviceType *> *omsModel = [DHOMSDeviceType mj_objectArrayWithKeyValuesArray:[objc objectForKey:@"configList"]];
        NSArray <DHOMSDeviceType *> *modelArr = [NSArray arrayWithArray:omsModel];
        NSString *updateTimeStr = @"";
        if ([objc objectForKey:@"updateTime"] != nil) {
            updateTimeStr = [objc objectForKey:@"updateTime"];
        }
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:modelArr,@"deviceTypeConfigs",updateTimeStr,@"updateTime", nil];
        if (success) {
            success(dic);
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)checkDeviceBindOrNotWithDevice:(NSString *)deviceId success:(void (^)(LCCheckDeviceBindOrNotInfo *))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/checkDeviceBindOrNot" parameters:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        LCCheckDeviceBindOrNotInfo *info = [LCCheckDeviceBindOrNotInfo mj_objectWithKeyValues:objc];
        if (success) {
            success(info);
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)unBindDeviceInfoForDevice:(NSString *)token deviceId:(NSString *)deviceId DeviceModel:(nullable NSString *)deviceModel DeviceName:(NSString *)deviceName ncCode:(NSString *)ncCode productId:(NSString *)productId success:(void (^)(DHUserDeviceBindInfo * info))success failure:(void (^)(LCError *error))failure {
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    [dic setObject:token forKey:KEY_TOKEN];
    [dic setObject:deviceId forKey:KEY_DEVICE_ID];
    if (deviceModel && ![@"" isEqualToString:deviceModel]) {
        [dic setObject:deviceModel forKey:KEY_DT];
    }
    if (deviceName && ![@"" isEqualToString:deviceName]) {
        [dic setObject:deviceName forKey:KEY_DEVICE_MODEL_NAME];
    }
    if (ncCode && ![@"" isEqualToString:ncCode]) {
        [dic setObject:ncCode forKey:KEY_NC_CODE];
    }
    if (productId && ![@"" isEqualToString:productId]) {
        [dic setObject:productId forKey:KEY_PRODUCT_ID];
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/unBindDeviceInfo" parameters:dic success:^(id _Nonnull objc) {
        DHUserDeviceBindInfo *info = [DHUserDeviceBindInfo mj_objectWithKeyValues:objc];
        if (success) {
            success(info);
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)deviceOnlineFor:(NSString *)deviceId success:(void (^)(LCDeviceOnlineInfo *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceOnline" parameters:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        LCDeviceOnlineInfo *info = [LCDeviceOnlineInfo mj_objectWithKeyValues:objc];
        if (success) {
            success(info);
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)bindDeviceWithDevice:(NSString *)token deviceId:(nonnull NSString *)deviceId Code:(NSString *)code success:(void (^)(void))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/bindDevice" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_CODE: code } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)timeZoneConfigByWeekWithDevice:(nonnull NSString *)deviceId AreaIndex:(NSInteger)areaIndex TimeZone:(NSInteger)timeZone BeginSunTime:(NSString *)beginSunTime EndSunTime:(NSString *)endSunTime success:(void (^)(void))success
                               failure:(void (^)(LCError *error))failure {
    NSMutableDictionary *res = [NSMutableDictionary dictionaryWithDictionary:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_DEVICE_ID: deviceId, KEY_AREAINDEX: [NSString stringWithFormat:@"%ld", (long)areaIndex], KEY_TIMEZONE: [NSString stringWithFormat:@"%ld", (long)timeZone] }];
    if (beginSunTime) {
        beginSunTime = [beginSunTime stringByAppendingString:@":00"];
        [res setObject:beginSunTime forKey:KEY_BEGIN_SUMMERTIME];
    }
    if (endSunTime) {
        endSunTime = [endSunTime stringByAppendingString:@":00"];
        [res setObject:endSunTime forKey:KEY_END_SUMMERTIME];
    }
    if (timeZone==0 && areaIndex == 0) {
        if (success) {
            success();
        }
        return;
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/timeZoneConfigByWeek" parameters:res success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)timeZoneConfigByDateWithDevice:(nonnull NSString *)deviceId AreaIndex:(NSInteger)areaIndex TimeZone:(NSInteger)timeZone BeginSunTime:(NSString *)beginSunTime EndSunTime:(NSString *)endSunTime success:(void (^)(void))success
                               failure:(void (^)(LCError *error))failure {
    NSMutableDictionary *res = [NSMutableDictionary dictionaryWithDictionary:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_DEVICE_ID: deviceId, KEY_AREAINDEX: [NSString stringWithFormat:@"%ld", (long)areaIndex], KEY_TIMEZONE: [NSString stringWithFormat:@"%ld", (long)timeZone] }];
    if (beginSunTime) {
        beginSunTime = [beginSunTime stringByAppendingString:@":00"];
        [res setObject:beginSunTime forKey:KEY_BEGIN_SUMMERTIME];
    }
    if (endSunTime) {
        endSunTime = [endSunTime stringByAppendingString:@":00"];
        [res setObject:endSunTime forKey:KEY_END_SUMMERTIME];
    }
    if (timeZone==0 && areaIndex == 0) {
        if (success) {
            success();
        }
        return;
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/timeZoneConfigByDay" parameters:res success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)setDeviceSnapWithDevice:(NSString *)deviceId Channel:(NSString *)channelId success:(void (^)(NSString *picUrlString))success
                        failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/setDeviceSnap" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        NSDictionary *dic = objc;
        if ([dic objectForKey:KEY_URL] && success) {
            success(dic[KEY_URL]);
        } else {
            success(@"");
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)setDeviceSnapEnhancedWithDevice:(NSString *)deviceId Channel:(NSString *)channelId success:(void (^)(NSString *picUrlString))success
                                failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/setDeviceSnapEnhanced" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        NSDictionary *dic = objc;
        if ([dic objectForKey:KEY_URL] && success) {
            success(dic[KEY_URL]);
        } else {
            success(@"");
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+(void)controlMovePTZWithDevice:(NSString *)deviceId Channel:(NSString *)channelId Operation:(NSString *)operation Duration:(NSInteger)duration success:(void (^)(NSString * _Nonnull))success failure:(void (^)(LCError * _Nonnull))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/controlMovePTZ" parameters:@{ KEY_DEVICE_ID: deviceId ,KEY_TOKEN:[LCApplicationDataManager token],KEY_CHANNEL_ID:channelId,KEY_OPERATION:operation,KEY_DURATION:@(duration)} success:^(id _Nonnull objc) {
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)wifiAroundDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(LCAroundWifiInfo *wifiInfo))success
                 failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/wifiAround" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_TOKEN: token } success:^(id _Nonnull objc) {
        LCAroundWifiInfo *info = [LCAroundWifiInfo mj_objectWithKeyValues:objc];
        if (success) {
            success(info);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)currentDeviceWifiDevice:(NSString *)deviceId success:(void (^)(LCWifiInfo *wifiInfo))success
                        failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/currentDeviceWifi" parameters:@{ KEY_DEVICE_ID: deviceId,KEY_TOKEN:[LCApplicationDataManager token] } success:^(id _Nonnull objc) {
        LCWifiInfo *info = [LCWifiInfo mj_objectWithKeyValues:objc];
        info.linkStatus = LinkStatusConnected;
        if (success) {
            success(info);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)timeZoneQueryByDay:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(LCTimeZoneConfig *config))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/timeZoneQueryByDay" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_TOKEN: token } success:^(id _Nonnull objc) {
        LCTimeZoneConfig *info = [LCTimeZoneConfig mj_objectWithKeyValues:objc];
        if (success) {
            success(info);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)timeZoneConfigByDay:(NSString *)token deviceId:(NSString *)deviceId areaIndex:(NSString *)areaIndex timeZone:(NSString *)timeZone beginSunTime:(NSString *)beginSunTime endSunTime:(NSString *)endSunTime success:(void (^)(void))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/timeZoneConfigByDay" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_AREAINDEX:areaIndex, KEY_TIMEZONE:timeZone, KEY_BEGIN_SUMMERTIME:beginSunTime, KEY_END_SUMMERTIME:endSunTime } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)setMessageCallback:(NSString *)token status:(NSString *)status callbackUrl:(NSString *)callbackUrl callbackFlag:(NSString *)callbackFlag success:(void (^)(void))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/setMessageCallback" parameters:@{ KEY_TOKEN: token, KEY_STATUS: status, KEY_CALLBACKURL:callbackUrl, KEY_CALLBACKFLAG:callbackFlag} success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)getMessageCallback:(NSString *)token success:(void (^)(LCMessageCallback *callback))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/getMessageCallback" parameters:@{ KEY_TOKEN: token } success:^(id _Nonnull objc) {
        LCMessageCallback *info = [LCMessageCallback mj_objectWithKeyValues:objc];
        if (success) {
            success(info);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)getAlarmMessage:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId beginTime:(NSString *)beginTime endTime:(NSString *)endTime count:(NSString *)count nextAlarmId:(NSString *)nextAlarmId success:(void (^)(LCAlarmMessage *alarmMessage))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/getAlarmMessage" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_TOKEN: token, KEY_CHANNEL_ID: channelId, KEY_BEGIN_TIME:beginTime, KEY_END_TIME:endTime, KEY_COUNT: count, KEY_NEXT_ALARM_ID:nextAlarmId } success:^(id _Nonnull objc) {
        LCAlarmMessage *info = [LCAlarmMessage mj_objectWithKeyValues:objc];
        if (success) {
            success(info);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)deleteAlarmMessage:(NSString *)token deviceId:(NSString *)deviceId indexId:(NSString *)indexId channelId:(NSString *)channelId success:(void (^)(void))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/deleteAlarmMessage" parameters:@{ KEY_TOKEN: token,KEY_DEVICE_ID: deviceId, KEY_INDEX_ID:indexId, KEY_CHANNEL_ID:channelId } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)controlDeviceWifiFor:(NSString *)token deviceId:(NSString *)deviceId ConnestSession:(LCWifiConnectSession *)session success:(void (^)(void))success
                     failure:(void (^)(LCError *error))failure {
    NSMutableDictionary *sessionDic = [session mj_keyValues];
    [sessionDic removeObjectForKey:@"intensity"];
    [sessionDic setValue:[NSNumber numberWithBool:YES] forKey:@"linkEnable"];
    [sessionDic setValue:deviceId forKey:KEY_DEVICE_ID];
    [sessionDic setValue:token forKey:KEY_TOKEN];
    [[LCNetworkRequestManager manager] lc_POST:@"/controlDeviceWifi" parameters:sessionDic success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)upgradeDevice:(NSString *)deviceId success:(void (^)(void))success
              failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/upgradeDevice" parameters:@{ KEY_TOKEN:[LCApplicationDataManager token],KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)modifyDeviceAlarmStatus:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId enable:(BOOL)enable success:(void (^)(void))success
                        failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/modifyDeviceAlarmStatus" parameters:@{ KEY_TOKEN:token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId, KEY_ENABLE: @(enable) } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)queryLocalRecordPlan:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId success:(void (^)(LCAlarmPlan *plan))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/queryLocalRecordPlan" parameters:@{ KEY_TOKEN:token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        LCAlarmPlan *plan = [LCAlarmPlan mj_objectWithKeyValues:objc];
        if (success) {
            success(plan);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)setLocalRecordPlanRules:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId plan:(LCAlarmPlan *)plan success:(void (^)(void))success failure:(void (^)(LCError *error))failure {
    NSDictionary *planDic = [plan mj_keyValues];
    [planDic setValue:token forKey:KEY_TOKEN];
    [planDic setValue:deviceId forKey:KEY_DEVICE_ID];
    [planDic setValue:channelId forKey:KEY_CHANNEL_ID];
    [[LCNetworkRequestManager manager] lc_POST:@"/setLocalRecordPlanRules" parameters:planDic success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)breathingLightStatusForDevice:(NSString *)deviceId success:(void (^)(BOOL status))success
                              failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/breathingLightStatus" parameters:@{ KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        BOOL status = NO;
        if ([[objc objectForKey:KEY_STATUS] isEqualToString:@"on"]) {
            status = YES;
        }
        if (success) {
            success(status);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)modifyBreathingLightForDevice:(NSString *)deviceId Status:(BOOL)open success:(void (^)(void))success
                              failure:(void (^)(LCError *error))failure {
    NSString *status = open ? @"on" : @"off";
    [[LCNetworkRequestManager manager] lc_POST:@"/modifyBreathingLight" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_STATUS: status } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)frameReverseStatusForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(NSString *direction))success
                            failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/frameReverseStatus" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        BOOL status = NO;
        if ([[objc objectForKey:KEY_STATUS] isEqualToString:@"on"]) {
        }
        if (success) {
            success([objc objectForKey:KEY_DIRECTION]);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)modifyFrameReverseStatusForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId Direction:(NSString *)direction success:(void (^)(void))success
                                  failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/modifyFrameReverseStatus" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId, KEY_DIRECTION: direction } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)recoverSDCardForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(NSString *result))success
                       failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/recoverSDCard" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        if (success) {
            success([objc objectForKey:KEY_RESULT]);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)setDeviceOsdForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId enable:(BOOL)open OSD:(NSString *)osd success:(void (^)(void))success
                      failure:(void (^)(LCError *error))failure {
    NSString *enableStr = open ? @"on" : @"off";
    [[LCNetworkRequestManager manager] lc_POST:@"/setDeviceOsd" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId, KEY_ENABLE: enableStr, KEY_OSD: osd } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)queryDeviceOsdForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(BOOL enable, NSString *osd))success
                        failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/queryDeviceOsd" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        if (success) {
            BOOL tempEnable = [@"on" isEqualToString:[objc objectForKey:KEY_ENABLE]] ? YES : NO;
            success(tempEnable, [objc objectForKey:KEY_OSD]);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)uploadDeviceCoverPictureForDevice:(NSString *)deviceId ChannelId:(NSString *)channelId PictureData:(NSData *)data success:(void (^)(NSString *picUrlString))success
                                  failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/uploadDeviceCoverPicture" parameters:@{ KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId, KEY_PICTURE_DATA: data } success:^(id _Nonnull objc) {
        if (success) {
            success([objc objectForKey:KEY_URL]);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)queryCloudRecordCallNum:(NSString *)token strategyId:(NSNumber *)strategyId success:(void (^)(CloudRecordCallNum *cloudRecordCallNum))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/queryCloudRecordCallNum" parameters:@{ KEY_TOKEN: token, KEY_STRATEGYID: strategyId } success:^(id _Nonnull objc) {
        CloudRecordCallNum *cloudRecordCallNum = [CloudRecordCallNum mj_objectWithKeyValues:objc];
        if (success) {
            success(cloudRecordCallNum);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)openCloudRecord:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId strategyId:(NSNumber *)strategyId deviceCloudId:(NSString *)deviceCloudId success:(void (^)(void))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/openCloudRecord" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId, KEY_STRATEGYID: strategyId, KEY_DEVICECLOUDID: deviceCloudId } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)getDeviceCloud:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId success:(void (^)(StorageStrategy *storageStrategy))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/getDeviceCloud" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        StorageStrategy *storageStrategy = [StorageStrategy mj_objectWithKeyValues:objc];
        if (success) {
            success(storageStrategy);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)unBindDeviceWithDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/unBindDevice" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)modifyDeviceForDevice:(NSString *)token deviceId:(NSString *)deviceId Channel:(NSString *)channelId NewName:(NSString *)name success:(void (^)(void))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/modifyDeviceName" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId ? channelId : @"", KEY_NAME: name } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)bindDeviceChannelInfoWithDevice:(NSString *)token deviceId:(NSString *)deviceId ChannelId:(NSString *)channelId success:(void (^)(LCBindDeviceChannelInfo *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/bindDeviceChannelInfo" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        LCBindDeviceChannelInfo *info = [LCBindDeviceChannelInfo mj_objectWithKeyValues:objc];
        if (success) {
            success(info);
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)deviceVersionForDevices:(NSString *)token devices:(NSArray *)devices success:(void (^)(NSMutableArray<LCDeviceVersionInfo *> *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    NSString * str = @"";
    for (NSString * device in devices) {
        str = [str stringByAppendingString:device];
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceVersionList" parameters:@{ KEY_TOKEN: token, KEY_DEVICES: str } success:^(id _Nonnull objc) {
        NSMutableArray<LCDeviceVersionInfo *> *infos = [LCDeviceVersionInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:@"deviceVersionList"]];
        if (success) {
            success(infos);
        }
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)deviceDetailListFromLeChangeWith:(NSInteger)bindId Limit:(int)limit Type:(NSString *)type NeedApInfo:(BOOL)needApInfo success:(void (^)(NSMutableArray<LCDeviceInfo *> *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceBaseList" parameters:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_BINDID: @(bindId), KEY_LIMIT: @(limit), KEY_TYPE: type, KEY_NEEDAPINFO: @(needApInfo) } success:^(id _Nonnull objc) {
        NSMutableArray <LCDeviceInfo *> *infos = [LCDeviceInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:KEY_DEVICE_LIST]];
        [OpenApiInterface deviceBaseDetailListFromLeChangeWithSimpleList:infos success:success failure:failure];
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)deviceBaseDetailListFromLeChangeWithSimpleList:(NSMutableArray <LCDeviceInfo *>*)infos  success:(void (^)(NSMutableArray<LCDeviceInfo *> *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure{
    int index = 0;
    NSMutableArray * requestList = [NSMutableArray array];
    while (index<infos.count) {
        LCDeviceInfo * info = [infos objectAtIndex:index];
        NSString * channels = @"";
        for (LCChannelInfo * channel in info.channels) {
            channels = [channels stringByAppendingString:[NSString stringWithFormat:@"%@,",channel.channelId]];
        }
        [requestList addObject:@{KEY_DEVICE_ID:info.deviceId,KEY_CHANNELS:channels,KEY_APLIST:@""}];
        index++;
    }
    if (requestList.count==0) {
        if (success) {
            success([NSMutableArray array]);
        }
        return;
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceBaseDetailList" parameters:@{KEY_TOKEN:[LCApplicationDataManager token],KEY_DEVICE_LIST:requestList} success:^(id  _Nonnull objc) {
        NSMutableArray <LCDeviceInfo *> *tempInfos = [LCDeviceInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:KEY_DEVICE_LIST]];
        for (LCDeviceInfo * oldInfo in infos) {
            for (LCDeviceInfo * newInfo in tempInfos) {
                if ([oldInfo.deviceId isEqualToString:newInfo.deviceId]) {
                    newInfo.bindId = oldInfo.bindId;
                }
            }
        }
        if (success) {
            success(tempInfos);
        }
    } failure:^(LCError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)deviceDetailListFromOpenPlatformWith:(NSInteger)bindId Limit:(int)limit Type:(NSString *)type NeedApInfo:(BOOL)needApInfo success:(void (^)(NSMutableArray<LCDeviceInfo *> * _Nonnull))success failure:(void (^)(LCError * _Nonnull))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceOpenList" parameters:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_BINDID: @(bindId), KEY_LIMIT: @(limit), KEY_TYPE: type, KEY_NEEDAPINFO: @(needApInfo) } success:^(id _Nonnull objc) {
        NSMutableArray <LCDeviceInfo *> *infos = [LCDeviceInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:@"deviceList"]];
        [OpenApiInterface deviceOpenDetailListFromLeChangeWithSimpleList:infos success:success failure:failure];
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)listSubAccountDevice2:(NSString *)token openid:(NSString *)openid pageNo:(NSNumber*)pageNo pageSize:(NSNumber*)pageSize success:(void (^)(NSMutableArray<LCDeviceInfo *> * _Nonnull))success failure:(void (^)(LCError * _Nonnull))failure{
    totalDevices = 0;
    [[LCNetworkRequestManager manager] lc_POST:@"/listSubAccountDevice" parameters:@{ KEY_TOKEN: token, KEY_OPENID: openid, KEY_PAGENO: pageNo, KEY_PAGESIZE: pageSize } success:^(id _Nonnull objc) {
        NSMutableArray <LCDeviceInfo *> *infos = [LCDeviceInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:@"policy"]];
        int index = 0;
        NSNumber * total = [objc objectForKey:@"total"];
        totalDevices = total;
        NSString * devicesIds = @"";
        while (index < infos.count) {
            LCDeviceInfo * info = [infos objectAtIndex:index];
            devicesIds = [devicesIds stringByAppendingString:[NSString stringWithFormat:@"%@,",info.deviceId]];
            index++;
        }
        if([devicesIds isEqual: @""]){
            if (success) {
                success([NSMutableArray array]);
            }
            return;
        }
        [[LCNetworkRequestManager manager] lc_POST:@"/queryOpenDeviceChannelInfo" parameters:@{ KEY_TOKEN: [LCApplicationDataManager token], KEY_DEVICEIDS: devicesIds } success:^(id _Nonnull objc) {
            NSMutableArray <LCDeviceInfo *> *infos = [LCDeviceInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:@"devices"]];
            [OpenApiInterface deviceOpenDetailListFromLeChangeWithSimpleList:infos success:success failure:failure];
        } failure:^(LCError *_Nonnull error) {
            if (failure) {
                failure(error);
            }
        }];
    } failure:^(LCError *_Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)deviceOpenDetailListFromLeChangeWithSimpleList:(NSMutableArray <LCDeviceInfo *>*)infos  success:(void (^)(NSMutableArray<LCDeviceInfo *> *_Nonnull))success failure:(void (^)(LCError *_Nonnull))failure{
    int index = 0;
    NSMutableArray * requestList = [NSMutableArray array];
    while (index<infos.count) {
        LCDeviceInfo * info = [infos objectAtIndex:index];
        NSString * channels = @"";
        for (LCChannelInfo * channel in info.channels) {
            channels = [channels stringByAppendingString:[NSString stringWithFormat:@"%@,",channel.channelId]];
        }
        [requestList addObject:@{KEY_DEVICE_ID:info.deviceId,KEY_CHANNELS:channels,KEY_APLIST:@""}];
        index++;
    }
    if (requestList.count==0) {
        if (success) {
            success([NSMutableArray array]);
        }
        return;
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceOpenDetailList" parameters:@{KEY_TOKEN:[LCApplicationDataManager token],KEY_DEVICE_LIST:requestList} success:^(id  _Nonnull objc) {
        NSMutableArray <LCDeviceInfo *> *tempInfos = [LCDeviceInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:KEY_DEVICE_LIST]];
        for (LCDeviceInfo * oldInfo in infos) {
            for (LCDeviceInfo * newInfo in tempInfos) {
                if ([oldInfo.deviceId isEqualToString:newInfo.deviceId]) {
                    newInfo.bindId = oldInfo.bindId;
                }
            }
        }
        if (success) {
            success(tempInfos);
        }
    } failure:^(LCError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+(void)queryCloudRecordsForDevice:(NSString *)deviceId channelId:(NSString *)channelId day:(NSDate *)day From:(int)start To:(int)end success:(void (^)(NSMutableArray<LCCloudVideotapeInfo *> * _Nonnull))success failure:(void (^)(LCError * _Nonnull))failure{
    NSString * query = [NSString stringWithFormat:@"%d-%d",start,end];
    NSDateFormatter * dataFormatter = [[NSDateFormatter alloc] init];
    dataFormatter.dateFormat = @"yyyy-MM-dd";
    NSString * startStr = [NSString stringWithFormat:@"%@ 00:00:00",[dataFormatter stringFromDate:day]];
    NSString * endStr = @"";
    if (![[NSCalendar currentCalendar] isDateInToday:day]) {
        endStr = [NSString stringWithFormat:@"%@ 23:59:59",[dataFormatter stringFromDate:day]];
    }else{
        NSDateFormatter * dataFormatterEnd = [[NSDateFormatter alloc] init];
        dataFormatterEnd.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        endStr = [dataFormatterEnd stringFromDate:day];
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/queryCloudRecords" parameters:@{KEY_TOKEN:[LCApplicationDataManager token],KEY_DEVICE_ID:deviceId,KEY_CHANNEL_ID:channelId,KEY_BEGIN_TIME:startStr,KEY_END_TIME:endStr,KEY_QUERYRANGE:query} success:^(id  _Nonnull objc) {
        NSMutableArray <LCCloudVideotapeInfo *> *infos = [LCCloudVideotapeInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:@"records"]];
        if (success) {
            success(infos);
        }
    } failure:^(LCError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+(void)queryLocalRecordsForDevice:(NSString *)deviceId channelId:(NSString *)channelId day:(NSDate *)day From:(int)start To:(int)end success:(void (^)(NSMutableArray<LCLocalVideotapeInfo *> * _Nonnull))success failure:(void (^)(LCError * _Nonnull))failure{
    NSString * query = [NSString stringWithFormat:@"%d-%d",start,end];
    NSDateFormatter * dataFormatter = [[NSDateFormatter alloc] init];
    dataFormatter.dateFormat = @"yyyy-MM-dd";
    NSString * startStr = [NSString stringWithFormat:@"%@ 00:00:00",[dataFormatter stringFromDate:day]];
    NSString * endStr = @"";
    if (![[NSCalendar currentCalendar] isDateInToday:day]) {
        endStr = [NSString stringWithFormat:@"%@ 23:59:59",[dataFormatter stringFromDate:day]];
    }else{
        NSDateFormatter * dataFormatterEnd = [[NSDateFormatter alloc] init];
        dataFormatterEnd.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        endStr = [dataFormatterEnd stringFromDate:[NSDate new]];
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/queryLocalRecords" parameters:@{KEY_TOKEN:[LCApplicationDataManager token],KEY_DEVICE_ID:deviceId,KEY_CHANNEL_ID:channelId,KEY_BEGIN_TIME:startStr,KEY_END_TIME:endStr,KEY_QUERYRANGE:query} success:^(id  _Nonnull objc) {
        NSMutableArray <LCLocalVideotapeInfo *> *infos = [LCLocalVideotapeInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:@"records"]];
        if (success) {
            success(infos);
        }
    } failure:^(LCError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+(void)getCloudRecordsForDevice:(NSString *)deviceId channelId:(NSString *)channelId day:(NSDate *)day From:(long)nextRecordId Count:(long)count success:(void (^)(NSMutableArray<LCCloudVideotapeInfo *> * _Nonnull))success failure:(void (^)(LCError * _Nonnull))failure{
    NSDateFormatter * dataFormatter = [[NSDateFormatter alloc] init];
    dataFormatter.dateFormat = @"yyyy-MM-dd";
    NSString * startStr = [NSString stringWithFormat:@"%@ 00:00:00",[dataFormatter stringFromDate:day]];
    NSString * endStr = @"";
    if (![[NSCalendar currentCalendar] isDateInToday:day]) {
        endStr = [NSString stringWithFormat:@"%@ 23:59:59",[dataFormatter stringFromDate:day]];
    }else{
        NSDateFormatter * dataFormatterEnd = [[NSDateFormatter alloc] init];
        dataFormatterEnd.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        endStr = [dataFormatterEnd stringFromDate:day];
    }
    [[LCNetworkRequestManager manager] lc_POST:@"/getCloudRecords" parameters:@{KEY_TOKEN:[LCApplicationDataManager token],KEY_DEVICE_ID:deviceId,KEY_CHANNEL_ID:channelId,KEY_BEGIN_TIME:startStr,KEY_END_TIME:endStr,@"nextRecordId":@(nextRecordId),KEY_COUNT:@(count)} success:^(id  _Nonnull objc) {
        NSMutableArray <LCCloudVideotapeInfo *> *infos = [LCCloudVideotapeInfo mj_objectArrayWithKeyValuesArray:[objc objectForKey:@"records"]];
        if (success) {
            success(infos);
        }
    } failure:^(LCError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+(void)deleteCloudRecords:(NSString *)recordRegionId success:(void (^)(void))success failure:(void (^)(LCError * _Nonnull))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/deleteCloudRecords" parameters:@{KEY_TOKEN:[LCApplicationDataManager token],@"recordRegionId":recordRegionId} success:^(id  _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)deviceSdcardStatus:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(SDCardStatusData *sdcardStatus))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceSdcardStatus" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        SDCardStatusData *sdcardStatus = [SDCardStatusData mj_objectWithKeyValues:objc];
        if (success) {
            success(sdcardStatus);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)deviceStorage:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(SDCardStatusData *sdcardStatus))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceStorage" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        SDCardStatusData *sdcardStatus = [SDCardStatusData mj_objectWithKeyValues:objc];
        if (success) {
            success(sdcardStatus);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)recoverSDCard:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(SDCardStatusData *sdcardStatus))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/recoverSDCard" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        SDCardStatusData *sdcardStatus = [SDCardStatusData mj_objectWithKeyValues:objc];
        if (success) {
            success(sdcardStatus);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)setDeviceSnap:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/setDeviceSnapEnhanced" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId } success:^(id _Nonnull objc) {
        NSDictionary *dic = objc;
        if ([dic objectForKey:KEY_URL] && success) {
            success(dic[KEY_URL]);
        } else {
            success(@"");
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)restartDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError * _Nonnull))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/restartDevice" parameters:@{KEY_TOKEN:token, KEY_DEVICE_ID: deviceId} success:^(id  _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)modifyDevicePwd:(NSString *)token deviceId:(NSString *)deviceId oldPwd:(NSString *)oldPwd newPwd:(NSString *)newPwd success:(void (^)(void))success failure:(void (^)(LCError * _Nonnull))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/modifyDevicePwd" parameters:@{KEY_TOKEN:token, KEY_DEVICE_ID: deviceId, KEY_OLDPWD:oldPwd, KEY_NEWPWD:newPwd} success:^(id  _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void)upgradeProcessDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure {
    [[LCNetworkRequestManager manager] lc_POST:@"/upgradeProcessDevice" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)upgradeDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/upgradeDevice" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)wakeUpDevice:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/wakeUpDevice" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)deviceOnline:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/deviceOnline" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)getDevicePowerInfo:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/getDevicePowerInfo" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)doorbellCallAnswer:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/doorbellCallAnswer" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)doorbellCallRefuse:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/doorbellCallRefuse" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)doorbellCallHangUp:(NSString *)token deviceId:(NSString *)deviceId success:(void (^)(void))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/doorbellCallHangUp" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId } success:^(id _Nonnull objc) {
        if (success) {
            success();
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)getDeviceCameraStatus:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId enableType:(NSString *)enableType success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/getDeviceCameraStatus" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId, KEY_ENABLE_TYPE: enableType } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
+ (void)setDeviceCameraStatus:(NSString *)token deviceId:(NSString *)deviceId channelId:(NSString *)channelId enableType:(NSString *)enableType enable:(BOOL)enable success:(void (^)(NSString *result))success failure:(void (^)(LCError *error))failure{
    [[LCNetworkRequestManager manager] lc_POST:@"/setDeviceCameraStatus" parameters:@{ KEY_TOKEN: token, KEY_DEVICE_ID: deviceId, KEY_CHANNEL_ID: channelId, KEY_ENABLE_TYPE: enableType, KEY_ENABLE: @(enable) } success:^(id _Nonnull objc) {
        NSError* error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:objc != nil ? objc : [[NSMutableDictionary alloc] init] options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        if (success) {
            success(jsonString);
        }
    } failure:^(LCError *error) {
        failure(error);
    }];
}
@end
