#import "LCBasicViewController.h"
#import "LCSegmentController.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCVideotapeListViewController : LCBasicViewController
@property (nonatomic,strong)UICollectionView * cloudVideoList;
@property (nonatomic,strong)UICollectionView * localVideoList;
@property (nonatomic,strong)LCSegmentController * segment;
@property (nonatomic)NSInteger defaultType;
@end
NS_ASSUME_NONNULL_END
