#import <Foundation/Foundation.h>
typedef enum : NSUInteger {
    EncryptModeDefault,
    EncryptModeUser
} EncryptMode;
@interface LCLocalVideotapeInfo : NSObject
@property (strong,nonatomic) NSString * recordId;
@property (nonatomic) long  fileLength;
@property (strong,nonatomic) NSString * channelID;
@property (strong,nonatomic) NSString * beginTime;
@property (strong,nonatomic) NSString * endTime;
@property (strong,nonatomic) NSString * type;
@property (strong,nonatomic) NSDate * beginDate;
@property (strong,nonatomic) NSDate * endDate;
-(NSString *)durationTime;
@end
NS_ASSUME_NONNULL_BEGIN
@interface LCCloudVideotapeInfo : NSObject
@property (strong,nonatomic) NSString * recordId;
@property (strong,nonatomic) NSString * recordRegionId;
@property (strong,nonatomic) NSString * deviceId;
@property (strong,nonatomic) NSString * channelId;
@property (strong,nonatomic) NSString * beginTime;
@property (strong,nonatomic) NSString * endTime;
@property (strong,nonatomic) NSString * size;
@property (strong,nonatomic) NSString * thumbUrl;
@property (nonatomic) EncryptMode  encryptMode;
@property (nonatomic) NSInteger type;
@property (strong,nonatomic) NSIndexPath * index;
@property (strong,nonatomic) NSDate * beginDate;
@property (strong,nonatomic) NSDate * endDate;
-(NSString *)durationTime;
@end
NS_ASSUME_NONNULL_END
