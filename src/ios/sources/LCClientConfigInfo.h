#import <Foundation/Foundation.h>
@interface DHOMSDeviceModelItem: NSObject<NSCopying, NSCoding>
@property (nonatomic, copy) NSString *deviceModelName; 
@property (nonatomic, copy) NSString *deviceImageURI; 
@end
@interface DHOMSDeviceType: NSObject
@property (nonatomic, copy) NSString *deviceType ; 
@property (nonatomic, strong) NSMutableArray<DHOMSDeviceModelItem *> *modelItems; 
@end
@interface DHOMSIntroductionImageItem: NSObject<NSCopying, NSCoding>
@property (nonatomic, copy) NSString *imageName ; 
@property (nonatomic, copy) NSString *imageURI ; 
@end
@interface DHOMSIntroductionContentItem: NSObject<NSCopying, NSCoding>
@property (nonatomic, copy) NSString *introductionName; 
@property (nonatomic, copy) NSString *introductionContent; 
@end
@interface DHOMSIntroductionInfo: NSObject<NSCopying, NSCoding>
@property (nonatomic, copy) NSString *updateTime ; 
@property (nonatomic, strong) NSMutableArray<DHOMSIntroductionImageItem *> *images; 
@property (nonatomic, strong) NSMutableArray<DHOMSIntroductionContentItem *> *contens; 
@end
@interface DHTabbarIconInfo: NSObject<NSCopying, NSCoding>
@property (nonatomic, copy) NSString *iconNameDefault ;
@property (nonatomic, copy) NSString *iconName ; 
@property (nonatomic, copy) NSString *iconPic;  
@property (nonatomic, copy) NSString *iconPicSelected ; 
@end
