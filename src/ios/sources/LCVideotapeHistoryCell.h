#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef void(^LCVideotapeHistoryCellClickBlock)(void);
@interface LCVideotapeHistoryCell : UICollectionViewCell
@property (copy,nonatomic) LCVideotapeHistoryCellClickBlock selectedBlock;
@property (strong, nonatomic) NSString *detail;
@property (nonatomic) BOOL isMore;
-(void)loadVideotapImage:(nullable NSString *)url DeviceId:(NSString *)deviceId Key:(NSString *)key;
@end
NS_ASSUME_NONNULL_END
