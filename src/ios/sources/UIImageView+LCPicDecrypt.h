#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface UIImageView (LCPicDecrypt)
-(void)lc_setImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholder DeviceId:(NSString *)deviceId Key:(NSString *)key;
@end
NS_ASSUME_NONNULL_END
