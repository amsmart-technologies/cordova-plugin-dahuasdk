#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface SDCardStatusData : NSObject
@property (nonatomic) int64_t totalBytes;
@property (nonatomic) int64_t usedBytes;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *result;
@end
NS_ASSUME_NONNULL_END
