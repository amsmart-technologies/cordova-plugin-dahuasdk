#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#define LCWwanAlertTip @"mobile_common_media_play_mobile_network_tip_title".lc_T
#define LCNoNetworkTip @"net_error_and_check".lc_T
@interface DHNetWorkHelper : NSObject
+ (instancetype)sharedInstance;
@property (nonatomic, assign) AFNetworkReachabilityStatus         emNetworkStatus;
@property (nonatomic, assign) BOOL                  bShouldShowFlowTip;         
@property (nonatomic, assign) BOOL                  bShouldShowFlowTipWhenLoadVideo;         
@property (nonatomic, assign) BOOL                  bShouldShowFlowTipWhenVideoShare;         
@property (nonatomic, copy) NSString *networkType; 
@property (nonatomic, copy, readonly) dispatch_queue_t interfaceQueue;  
- (void)checkNetwork;
- (BOOL)isPermittedToPlayVideoWithTip:(NSString *)tip;
- (BOOL)showPermittedToVC:(UIViewController *)vc playVideoAlert:(dispatch_block_t)confirmAction withTip:(NSString *)tip;
- (BOOL)showPermittedToPlayVideoAlert:(dispatch_block_t)confirmAction withTip:(NSString *)tip;
- (void)showPermittedToDownloadAlert:(dispatch_block_t)confirmAction withTip:(NSString *)tip;
- (void)dissmissAlert;
- (NSString *)fetchSSIDInfo;
@end
