#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef void(^LCVideotapePlayProcessViewValueChangeBlock)(float offset,NSDate * currentStartTiem);
typedef void(^LCVideotapePlayProcessViewValueChangeEndBlock)(float offset,NSDate * currentStartTiem);
@interface LCVideotapePlayProcessView : UIView
@property (copy,nonatomic) LCVideotapePlayProcessViewValueChangeBlock valueChangeBlock;
@property (copy,nonatomic) LCVideotapePlayProcessViewValueChangeEndBlock valueChangeEndBlock;
@property (nonatomic) BOOL canRefreshSlider;
@property (strong,nonatomic)NSDate * currentDate;
-(void)configFullScreenUI;
-(void)configPortraitScreenUI;
-(void)setStartDate:(NSDate *)startDate EndDate:(NSDate *)endDate;
@end
NS_ASSUME_NONNULL_END
