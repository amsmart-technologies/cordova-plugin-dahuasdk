#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCVideotapeListCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *picImgview;
@property (weak, nonatomic) IBOutlet UIImageView *selectImg;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *durationTimeLab;
@property (strong,nonatomic)id model;
@end
NS_ASSUME_NONNULL_END
