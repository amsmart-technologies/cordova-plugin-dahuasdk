#import <UIKit/UIKit.h>
#import "LCUIKit.h"
typedef enum : NSUInteger {
    LCTEXTFIELD_STYLE_TITLE,
    LCTEXTFIELD_STYLE_PHONE,
    LCTEXTFIELD_STYLE_WIFI,
    LCTEXTFIELD_STYLE_PASSWORD,
    LCTEXTFIELD_STYLE_CODE
} LCTEXTFIELD_STYLE;
NS_ASSUME_NONNULL_BEGIN
@interface LCInputTextField : UIView
@property (nonatomic) LCTEXTFIELD_STYLE style;
@property (strong, nonatomic) UILabel *titleLable;
@property (strong, nonatomic) DHTextField *textField;
@property (strong, nonatomic) LCButton *sendCodeBtn;
@property (copy, nonatomic) NSString *result;
+(instancetype)creatTextFieldWithResult:(void(^)(NSString * result))result;
@end
NS_ASSUME_NONNULL_END
