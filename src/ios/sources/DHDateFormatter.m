#import "DHDateFormatter.h"
@implementation DHDateFormatter
- (instancetype)initWithGregorianCalendar
{
    self = [super init];
    if (self) {
        [self setCalendar:[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]];
        [self setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    }
    return self;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setCalendar:[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]];
        [self setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US"]];
    }
    return self;
}
- (nullable instancetype)initWithCalendarIdentifier:(NSCalendarIdentifier)ident;
{
    self = [super init];
    if (self) {
        [self setCalendar:[[NSCalendar alloc] initWithCalendarIdentifier:ident]];
    }
    return self;
}
@end
