#import <Foundation/Foundation.h>
#define WAKEUP_TIME             5
#define LOADING_TIME_OUT        40
#define HIDE_BAR_TIME           5.0f
#define TAP_TIMEINTERVAL        200
#define PAN_TIMEINTERVAL        30000
typedef NSInteger Index;
typedef NS_ENUM(NSInteger, VPSourceType) {
    VPSourceTypeLive,
    VPSourceTypeRecord,
    VPSourceTypeFile,
    VPSourceTypeHls,
};
typedef NS_ENUM(NSUInteger, VPVideotapePlayingType) {
    VPVideotapePlayingTypeUnknow = 0,
    VPVideotapePlayingTypeCloud = 1,
    VPVideotapePlayingTypeDevice = 2,
};
typedef NS_ENUM(NSInteger, VPPercentType) {
	VPPercentTypeDevice,
	VPPercentTypeCloud,
	VPPercentTypeLocal,
};
typedef NS_ENUM(NSInteger, VPWindowMode) {
    VPWindowModeOne,
    VPWindowModeQuarter,
	VPWindowModeNinth,
	VPWindowModeSixteenth,
} ;
typedef NS_ENUM(NSInteger, VPVideoError) {
    VPVideoErrorUnknown,
    VPVideoErrorPlayFail,
    VPVideoErrorVideoDecryptFail,
    VPVideoErrorDeviceDecryptFail,
    VPVideoErrorAuthFail,
    VPVideoErrorAccountLock,
    VPVideoErrorStreamLimit,
	VPVideoErrorUserOrPswWrong,
	VPVideoErrorLoginTimeout,
	VPVideoErrorUserLocked,
	VPVideoErrorUserInBlackList,
	VPVideoErrorConnectFail,
	VPVideoErrorDeviceSleep,
    VPVideoErrorExtractFailed,
    VPVideoErrorWrongFormat,
	VPVideoErrorCamSleep,
	VPVideoErrorDeviceLocked,
    VPVideoErrorDHHTTPTimeout,
};
typedef NS_ENUM(NSInteger, VPPlayStatus) {
    VPPlayStatusReady,
    VPPlayStatusIfLoad,
    VPPlayStatusWillLoad,
    VPPlayStatusLoading,
    VPPlayStatusPlaying,
    VPPlayStatusStop,
    VPPlayStatusPause,
    VPPlayStatusEnd,
    VPPlayStatusError,
    VPPlayStatusVideoDecryptFail,
    VPPlayStatusDeviceDecryptFail,
    VPPlayStatusStreamLimit,
	VPPlayStatusUserOrPswWrong,
	VPPlayStatusLoginTimeout,
	VPPlayStatusUserLocked,
	VPPlayStatusUserInBlackList,
	VPPlayStatusConnectFail,
    VPPlayStatusExtractFailed,
    VPPlayStatusWrongFormat,
	VPPlayStatusCamSleep,
	VPPlayStatusDeviceLocked,
    VPPlayStatusDHHTTPTimeout,
};
typedef NS_ENUM(NSInteger, VPMaskStatus) {
	VPMaskStatusClose = 0,
	VPMaskStatusOpen = 1,
	VPMaskStatusOpening = -1,
	VPMaskStatusClosing = -2,
};
typedef NS_ENUM(NSInteger, VPVideoStreamType) {
	VPVideoStreamTypeRtsp = 0,
	VPVideoStreamTypeHls = 1,
	VPVideoStreamTypeFile = 2,
	VPVideoStreamTypeNetsdk = 3,
	VPVideoStreamTypeSip = 4,
	VPVideoStreamTypeRest = 99,
};
typedef NS_ENUM(NSInteger, VPPlayControl) {
    VPPlayControlUnknown,
    VPPlayControlPlay,
    VPPlayControlRefresh,
    VPPlayControlReplay,
} ;
typedef NS_ENUM(NSInteger, VPStreamType) {
    VPStreamTypeMain = 0,
    VPStreamTypeAided,
    VPStreamTypeAided2,
    VPStreamTypeAided3,
} ;
typedef NS_ENUM(NSInteger, VPStreamOrientation) {
	VPStreamOrientationDefault,
	VPStreamOrientationVertical,
	VPStreamOrientationHorizontal,
} ;
typedef NS_ENUM(NSInteger, VPDirection) {
    VPDirectionUp,
    VPDirectionDown,
    VPDirectionLeft,
    VPDirectionRight,
    VPDirectionLeftUp,
    VPDirectionLeftDown,
    VPDirectionRightUp,
    VPDirectionRightDown,
    VPDirectionUnknown = 10,
};
typedef NS_ENUM(NSInteger, VPGesture) {
    VPGestureDefault,
    VPGestureTap,
    VPGestureDblclick,
    VPGesturePanUp,
    VPGesturePanDown,
    VPGesturePanLeft,
    VPGesturePanRight,
    LVPGestureZoomOut,
    VPGestureZoomIn,
};
typedef enum : NSUInteger {
    DHPtzDirectionUnknown = 0,
    DHPtzDirectionLeft ,
    DHPtzDirectionTop ,
    DHPtzDirectionRight ,
    DHPtzDirectionBottom ,
} DHPtzDirection;
