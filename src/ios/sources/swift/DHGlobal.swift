import UIKit
import AFNetworking

public typealias dh_closure = () -> ()
public typealias dh_boolClosure = (Bool) -> ()
public typealias dh_stringClosure = (String) -> ()
public typealias dh_intClosure = (Int) -> ()

public let dh_animDuratuion: TimeInterval = 0.4

public let dh_legalPassword = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!#$%()*+,-./<=>?@[\\]^_`{|}~"

public let dh_legalPasswordRegEx = "^[A-Za-z0-9!#%,<=>@_~`\\-\\.\\/\\(\\)\\*\\+\\?\\$\\[\\]\\\\\\^\\{\\}\\|]$"

public let dh_legalValidCode = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

public var dh_keyWindow: UIWindow? {
    return UIApplication.shared.keyWindow
}

public var dh_isPhone5s: Bool {
    return dh_screenWidth <= dh_screenWidth_5s
}

public var dh_screenWidth_5s: CGFloat {
    return 320
}

public var dh_screenWidth: CGFloat {
    return min(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
}

public var dh_screenHeight: CGFloat {
    return max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
}

public var dh_statusBarHeight: CGFloat {
    return dh_isiPhoneX ? 44 : 20
}

public let dh_navBarHeight: CGFloat = 44

public var dh_navViewHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.height + dh_navBarHeight
}

public var dh_tabBarHeight: CGFloat {
    return dh_isiPhoneX ? (49 + 34) : 49
}

public var dh_bottomSafeMargin: CGFloat {
    return dh_isiPhoneX ? 34 : 0
}

public var dh_landscapeSafeMargin: CGFloat {
    return dh_isiPhoneX ? 44 : 0
}

public var dh_landscapeVideoSafeMargin: CGFloat {
    return dh_isiPhoneX ? 64 : 0
}

public var dh_landscapeBottomSafeMargin: CGFloat {
    return dh_isiPhoneX ? 21 : 0
}

public var dh_topSafeMargin: CGFloat {
    return dh_isiPhoneX ? 24 : 0
}

public var dh_screenOrientation: UIInterfaceOrientation {
    return UIApplication.shared.statusBarOrientation
}

public var dh_isAppProtrait: Bool {
    let orientation = UIApplication.shared.statusBarOrientation
    return UIInterfaceOrientationIsPortrait(orientation)
}

public var dh_isAppLandscape: Bool {
    let orientation = UIApplication.shared.statusBarOrientation
    return UIInterfaceOrientationIsLandscape(orientation)
}

public var dh_nameSpace: String? {
    return Bundle.main.infoDictionary!["CFBundleExecutable"] as? String
}

public var dh_appName: String? {
    return Bundle.main.infoDictionary!["CFBundleDisplayName"] as? String
}

public var dh_networkStatus: AFNetworkReachabilityStatus {
    return DHNetWorkHelper.sharedInstance().emNetworkStatus
}

public var heightScare: CGFloat {
    return dh_isPhone5s ? 0.75 : 1
}

public func  dh_for<T>(_ items: [T]?, closure: (T, Int) -> Void) {
    if items == nil {
        return
    }
    var index = 0
    for item in items! {
        closure(item, index)
        index += 1
    }  
}

public func  dh_delay(_ seconds: Double, closure: @escaping dh_closure) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: closure)
}

public func  dh_synchronizd(lock: AnyObject, closure:() -> ()) {
    objc_sync_enter(lock)
    closure()
    objc_sync_exit(lock)
}

public func  dh_scaleSize(_ size: CGFloat) -> CGFloat {
    return dh_screenHeight / 667 * size
}

public func  dh_scaleMinSize(_ size: CGFloat) -> CGFloat {
    return min(size, dh_scaleSize(size))
}

public func  dh_scaleMaxSize(_ size: CGFloat) -> CGFloat {
    return max(size, dh_scaleSize(size))
}
