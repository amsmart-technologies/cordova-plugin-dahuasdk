import Foundation

protocol IDHWiFiConfigPresenter {
    func setContainer(container: IDHWiFiConfigContainer)
    func loadWiFiList()
    func connectWifi(indexPath: IndexPath)
    func connectHideWifi()
    func explain5GInfo()
    func explainWifiInfo()
    func hasConfigedWifi() -> Bool
    func sectionNumber() -> Int
    func numberOfRowsInSection(section: Int) -> Int
    func configCell(cell: UITableViewCell, indexPath: IndexPath)
    func refresh()
}

protocol IDHWiFiConfigContainer: class {
    func refreshEnable(isEnable: Bool)
    func navigationVC() -> UINavigationController?
    func mainView() -> UIView
    func mainController() -> UIViewController
    func table() -> UITableView
}
