import Foundation
import NetworkExtension

@objc public class DHAddDeviceManager: NSObject {
    @objc public static let sharedInstance = DHAddDeviceManager()
    @objc public var deviceId: String = ""
    @objc public var deviceModel: String = ""
    public var isSupport5GWifi: Bool = false
    @objc public var wifiSSID: String? = ""
    @objc public var wifiPassword: String? = ""
    var isSupportSC: Bool = false
    public override init() {
        super.init()
    }
}