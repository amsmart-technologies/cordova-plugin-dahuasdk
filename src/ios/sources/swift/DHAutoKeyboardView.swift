import UIKit

@objc public class DHAutoKeyboardView: UIView {
    
    public var isFullScreen: Bool = false
    
    public weak var relatedView: UIView?
    
    private var lastKeyboardHeight: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addObserver()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addObserver()
        self.addTapGesture()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap))
        self.addGestureRecognizer(tapGesture)
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        guard relatedView != nil else {
            return
        }
        if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardFrame = value.cgRectValue
            lastKeyboardHeight = keyboardFrame.height
            keyboardShow(height: keyboardFrame.height)
        }
    }
    
    func keyboardShow(height: CGFloat, offset: CGFloat = 0) {
        var distanceToBottom = UIScreen.main.bounds.height - UIApplication.shared.statusBarFrame.height - self.relatedView!.frame.maxY
        if !isFullScreen {
            distanceToBottom -= 44
        }
        var transform: CGAffineTransform
        if distanceToBottom < height {
            transform = CGAffineTransform(translationX: 0, y: distanceToBottom - height - offset)
        } else {
            transform = CGAffineTransform.identity
        }
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.transform = transform
        }, completion: nil)
    }
    
    @objc func autoAdjust(offset: CGFloat = 0) {
        guard lastKeyboardHeight > 0 else {
            return
        }
        keyboardShow(height: lastKeyboardHeight, offset: offset)
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        lastKeyboardHeight = 0
        guard relatedView != nil else {
            return
        }
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    @objc func tap() {
        self.endEditing(true)
    }
}
