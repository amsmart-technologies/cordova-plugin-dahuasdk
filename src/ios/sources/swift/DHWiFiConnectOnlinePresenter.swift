import Foundation

class DHWiFiConnectOnlinePresenter: IDHWiFiConnectOnlinePresenter {
    
    public var successHandler: (() -> (Void))?
    var wifiStatus: LCWifiInfo?
    var deviceId: String?
    weak var container: DHWifiConnectOnlineVC?
    
    func setContainer(container: DHWifiConnectOnlineVC) {
        self.container = container
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init(connectWifiInfo: LCWifiInfo, deviceId: String) {
        self.wifiStatus = connectWifiInfo
        self.deviceId = deviceId
    }
    
    func updateContainerViewByWifiInfo() {
        if let wifiName = wifiStatus?.ssid {
            self.container?.nextButton.dh_enable = true
            self.container?.wifiNameLabel.text = wifiName
            if let password = DHUserManager.shareInstance().ssidPwd(by: self.container?.wifiNameLabel.text), password.count > 0 {
                self.container?.checkButton.isSelected = true
                self.container?.passwordInputView.textField.text = password
            } else {
                self.container?.checkButton.isSelected = false
                self.container?.passwordInputView.textField.text = nil
            }
        } else {
            self.container?.nextButton.dh_enable = false
            self.container?.wifiNameLabel.text = ""
            self.container?.passwordInputView.textField.text = ""
        }
    }
    
    func setupSupportView() {
        container?.supportView.isHidden = true
        container?.checkWidthConstraint.constant = 250
    }
    
    func nextStepAction(wifiSSID: String, wifiPassword: String?) {
        var password: String?
        if let text = self.container?.passwordInputView.textField.text,
           text.count > 0 {
            password = self.container?.passwordInputView.textField.text
        }
        let connectSession = LCWifiConnectSession()
        connectSession.bssid = wifiStatus?.bssid ?? ""
        connectSession.ssid = wifiStatus?.ssid ?? ""
        connectSession.linkEnable = LCLinkHandle(rawValue: LCLinkHandle.RawValue(truncating: NSNumber(booleanLiteral: true)))
        connectSession.password = password ?? ""
        LCProgressHUD.show(on: self.container?.view)
        OpenApiInterface.controlDeviceWifi(for: LCApplicationDataManager.token(), deviceId: deviceId ?? "", connestSession: connectSession, success: {
            LCProgressHUD.hideAllHuds(self.container?.view)
            LCProgressHUD.showMsg("livepreview_localization_success".lc_T, duration: 3.0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.container?.dismiss(animated: true, completion: nil)
            }
        }) { (error) in
            LCProgressHUD.hideAllHuds(self.container?.view)
            LCProgressHUD.showMsg(error.errorMessage, duration: 3.0)
        }
    }
}
