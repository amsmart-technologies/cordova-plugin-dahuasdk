#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface UINavigationController (Push)
- (void)pushToModeSelect;
- (void)pushToUserModeLogin;
- (void)pushToUserModeRegist;
- (void)pushToLeChanegMainPage;
- (void)pushToLivePreview;
- (void)pushToUserModeIntroduce;
- (void)pushToManagerModeIntroduce;
- (void)pushToVideotapeListPageWithType:(NSInteger)type;
- (void)pushToVideotapePlay;
- (void)pushToCloudService;
- (void)pushToDeviceSettingPage;
- (void)pushToDeviceSettingDeploy;
- (void)pushToWifiSettings:(NSString *)deviceId;
- (void)pushToDeviceSettingDeviceDetail;
- (void)pushToDeviceSettingVersion;
- (void)pushToDeviceSettingEditName;
- (void)pushToDeviceSettingEditSnap;
- (void)pushToAddDeviceScanPage;
- (void)pushToSerialNumberPage;
- (void)pushToProductChoosePage;
- (void)removeViewController:(UIViewController *)VC;
@end
NS_ASSUME_NONNULL_END
