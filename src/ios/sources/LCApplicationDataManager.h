#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    LCJointModeChinaMainland,
    LCJointModeOversea
} LCJointModeType;
@interface LCApplicationDataManager : NSObject
+ (NSString *)appId;
+ (NSString *)appSecret;
+ (NSString *)hostApi;
+ (void)setAppIdWith:(NSString *)appId;
+ (void)setAppSecretWith:(NSString *)appSecret;
+ (void)setHostApiWith:(NSString *)api;
+ (void)setManagerToken:(NSString *)token;
+ (void)setUserToken:(NSString *)token;
+ (void)setSubAccountToken:(NSString *)token;
+ (void)setOpenid:(NSString *)openid;
+ (void)setExpireTime:(NSInteger)second;
+ (BOOL)isVaildToken;
+ (NSString *)token;
+ (NSString *)subAccountToken;
+ (NSString *)openid;
+ (BOOL)isManagerMode;
+ (void)setCurrentMode:(LCJointModeType)type;
+ (NSString *)SDKHost;
+ (NSInteger)SDKPort;
+ (NSString *)serial;
+ (NSString *)getCurrentTimeStamp;
@end
NS_ASSUME_NONNULL_END
