#import "NSString+ScanResultAnalysis.h"
#import "LCQRCode.h"
#import <objc/runtime.h>
static const void *KEY_CODE_ANALYSIS = @"KEY_CODE_ANALYSIS";
@interface NSString ()
@property (strong, nonatomic) LCQRCode *codeAnalysis;
@end
@implementation NSString (ScanResultAnalysis)
- (NSString *)SNCode {
    return self.codeAnalysis.deviceSN;
}
- (NSString *)SCCode {
    return self.codeAnalysis.scCode;
}
- (NSString *)RC8Code {
    return self.codeAnalysis.identifyingCode;
}
- (NSString *)DTCode {
    return self.codeAnalysis.deviceType;
}
- (NSString *)RCCode {
    return self.codeAnalysis.identifyingCode;
}
- (NSString *)NCCode {
    return self.codeAnalysis.ncCode;
}
- (NSInteger)numberWithHexString:(NSString *)hexString {
    const char *hexChar = [hexString cStringUsingEncoding:NSUTF8StringEncoding];
    int hexNumber;
    sscanf(hexChar, "%x", &hexNumber);
    return (NSInteger)hexNumber;
}
- (BOOL)isNetConfigSupportNewSound {
    NSString *ncCode = [self NCCode];
    if (!ncCode) {
        return NO;
    }
    NSInteger ncInt =  [self numberWithHexString:ncCode];
    if ((ncInt & LC_NC_CODE_TYPE_NEW_SOUND) != 0) {
        return YES;
    }
    return NO;
}
- (BOOL)isNetConfigSupportOldSound {
    NSString *ncCode = [self NCCode];
    if (!ncCode) {
        return NO;
    }
    NSInteger ncInt =  [self numberWithHexString:ncCode];
    if ((ncInt & LC_NC_CODE_TYPE_OLD_SOUND) != 0) {
        return YES;
    }
    return NO;
}
- (BOOL)isNetConfigSupportSmartConfig {
    NSString *ncCode = [self NCCode];
    if (!ncCode) {
        return NO;
    }
    NSInteger ncInt =  [self numberWithHexString:ncCode];
    if ((ncInt & LC_NC_CODE_TYPE_SMARTCONFIG) != 0) {
        return YES;
    }
    return NO;
}
- (BOOL)isNetConfigSupportSoftAP {
    NSString *ncCode = [self NCCode];
    if (!ncCode) {
        return NO;
    }
    NSInteger ncInt =  [self numberWithHexString:ncCode];
    if ((ncInt & LC_NC_CODE_TYPE_SOFTAP) != 0) {
        return YES;
    }
    return NO;
}
- (BOOL)isNetConfigSupportLAN {
    NSString *ncCode = [self NCCode];
    if (!ncCode) {
        return NO;
    }
    NSInteger ncInt =  [self numberWithHexString:ncCode];
    if ((ncInt & LC_NC_CODE_TYPE_LAN) != 0) {
        return YES;
    }
    return NO;
}
- (BOOL)isNetConfigSupportBLE {
    NSString *ncCode = [self NCCode];
    if (!ncCode) {
        return NO;
    }
    NSInteger ncInt =  [self numberWithHexString:ncCode];
    if ((ncInt & LC_NC_CODE_TYPE_BLE) != 0) {
        return YES;
    }
    return NO;
}
- (BOOL)isNetConfigSupportQRCode {
    NSString *ncCode = [self NCCode];
    if (!ncCode) {
        return NO;
    }
    NSInteger ncInt =  [self numberWithHexString:ncCode];
    if ((ncInt & LC_NC_CODE_TYPE_QRCODE) != 0) {
        return YES;
    }
    return NO;
}
- (LCQRCode *)codeAnalysis {
    id obj = objc_getAssociatedObject(self, KEY_CODE_ANALYSIS);
    if (!obj) {
        LCQRCode * code = [LCQRCode new];
        [code pharseQRCode:self];
        [self setCodeAnalysis:code];
        obj = code;
    }
    return obj;
}
- (void)setCodeAnalysis:(LCQRCode *)codeAnalysis {
    objc_setAssociatedObject(self, KEY_CODE_ANALYSIS, codeAnalysis, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end
