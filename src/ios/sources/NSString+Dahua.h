#import <Foundation/Foundation.h>
@interface NSString(Dahua)
@property(nonatomic, copy, nonnull, readonly) NSString *lc_T;
+ (NSString *_Nonnull)isoLocalizeLanguageString;
+ (NSString *_Nonnull)dh_currentLanguageCode;
- (NSString *_Nullable)dh_decryptSK;
+ (BOOL)dh_pwdVerifiers:(NSString *_Nullable)password;
- (NSString *_Nullable)lc_stringByTrimmingCharactersInSet:(NSCharacterSet *_Nullable)characterSet;
@end
