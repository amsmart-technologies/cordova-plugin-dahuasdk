#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCVideotapeListHeardView : UICollectionReusableView
@property (nonatomic) NSInteger index;
@property (strong, nonatomic) NSString *time;
@end
NS_ASSUME_NONNULL_END
