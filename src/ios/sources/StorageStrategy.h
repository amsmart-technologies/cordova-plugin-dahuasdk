#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface StorageStrategy : NSObject
@property (nonatomic) BOOL hasDefault;
@property (nonatomic) int strategyId;
@property (strong, nonatomic) NSString *name;
@property (nonatomic) int strategyStatus;
@property (strong, nonatomic) NSString *beginTime;
@property (strong, nonatomic) NSString *endTime;
@end
NS_ASSUME_NONNULL_END
