#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef void(^LCVideoHistoryViewDatasourceChange)(NSInteger datatType);
typedef void(^LCVideoHistoryViewClickBlock)(id userInfo,NSInteger index);
@interface LCVideoHistoryView : UIView
@property (copy,nonatomic)LCVideoHistoryViewDatasourceChange dataSourceChange;
@property (copy,nonatomic)LCVideoHistoryViewClickBlock historyClickBlock;
@property (nonatomic) BOOL isCurrentCloud;
-(void)reloadData:(NSMutableArray *)dataArys;
-(void)setupErrorView:(UIView *)errorView;
-(void)startAnimation;
-(void)stopAnimation;
@end
NS_ASSUME_NONNULL_END
