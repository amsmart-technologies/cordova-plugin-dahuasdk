#import <Foundation/Foundation.h>
typedef enum : Byte {
    LC_NC_CODE_TYPE_NEW_SOUND = 0x01,
    LC_NC_CODE_TYPE_OLD_SOUND = 0x02,
    LC_NC_CODE_TYPE_SMARTCONFIG = 0x04,
    LC_NC_CODE_TYPE_SOFTAP = 0x08,
    LC_NC_CODE_TYPE_LAN = 0x10,
    LC_NC_CODE_TYPE_BLE = 0x20,
    LC_NC_CODE_TYPE_QRCODE = 0x40,
} LC_NC_CODE_TYPE;
NS_ASSUME_NONNULL_BEGIN
@interface NSString (ScanResultAnalysis)
-(NSString *)SNCode;
-(NSString *)SCCode;
-(NSString *)RCCode;
-(NSString *)DTCode;
-(NSString *)NCCode;
-(BOOL)isNetConfigSupportNewSound;
-(BOOL)isNetConfigSupportOldSound;
-(BOOL)isNetConfigSupportSmartConfig;
-(BOOL)isNetConfigSupportSoftAP;
-(BOOL)isNetConfigSupportLAN;
-(BOOL)isNetConfigSupportBLE;
-(BOOL)isNetConfigSupportQRCode;
@end
NS_ASSUME_NONNULL_END
