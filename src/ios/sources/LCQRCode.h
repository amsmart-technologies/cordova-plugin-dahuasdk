#import <Foundation/Foundation.h>
@interface LCQRCode : NSObject
@property(nonatomic, copy) NSString *deviceSN;
@property(nonatomic, copy) NSString *deviceType;
@property(nonatomic, copy) NSString *identifyingCode;
@property(nonatomic, copy) NSString *scCode;
@property(nonatomic, copy) NSString *ncCode;
@property(nonatomic, copy) NSString *imeiCode;
@property(nonatomic, copy) NSString *iotDeviceType;
@property(nonatomic, copy) NSString *cidCode;
@property(nonatomic, copy) NSString *typeCode;
@property(nonatomic, copy) NSString *uidCode;
- (void)pharseQRCode:(NSString *)qrCode;
@end
