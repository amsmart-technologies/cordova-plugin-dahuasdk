#import "LCVideotapePlayerPersenter.h"
#import <LCOpenSDKDynamic/LCOpenSDK/LCOpenSDK_Download.h>
NS_ASSUME_NONNULL_BEGIN
@interface LCVideotapePlayerPersenter (Control)
- (void)onFullScreen:(LCButton *)btn;
- (void)onSpeed:(LCButton *)btn;
- (void)onAudio:(LCButton *)btn;
- (void)onPlay:(nullable LCButton *)btn;
- (void)onSnap:(LCButton *)btn;
- (void)onRecording:(LCButton *)btn;
-(void)onLockFullScreen:(LCButton *)btn;
- (void)onDownload:(LCButton *)btn;
-(void)onChangeOffset:(NSInteger)offsetTime;
-(void)startPlay:(NSInteger)offsetTime;
- (void)stopPlay;
@end
NS_ASSUME_NONNULL_END
