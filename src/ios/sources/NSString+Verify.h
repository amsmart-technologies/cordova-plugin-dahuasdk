#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN
@interface NSString (Verify)
- (BOOL)isNull;
- (BOOL)isVaildPhone;
-(NSString *)vaildDeviceName;
-(BOOL)isSafeCode;
-(BOOL)isVaildSNCode;
-(BOOL)isFullNumber;
-(BOOL)isFullChar;
-(BOOL)isVaildPasswordBit;
-(BOOL)isExistBlankSpace;
-(BOOL)isVaildOverseaSNCode;
-(BOOL)isVaildURL;
-(BOOL)isCharactor;
- (BOOL)isVaildEmail;
-(BOOL)isVaildDeviceName;
- (NSString *)filterCharactor:(NSString *)string withRegex:(NSString *)regexStr;
@end
NS_ASSUME_NONNULL_END
