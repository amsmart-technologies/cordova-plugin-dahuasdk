#import "LCVideotapePlayerViewController.h"
#import "LCVideotapePlayerPersenter.h"
#import "LCVideotapePlayerPersenter+Control.h"
#import "LCVideoControlView.h"
#import "LCLandscapeControlView.h"
#import "LCVideotapePlayProcessView.h"
#import "LCVideotapeDownloadStatusView.h"
#import "LCBaseDefine.h"
#import "DHAlertController.h"
#import "NSString+Dahua.h"
#import <KVOController/KVOController.h>
@interface LCVideotapePlayerViewController ()
@property (strong, nonatomic) LCVideotapePlayerPersenter *persenter;
@end
@implementation LCVideotapePlayerViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *titleStr = self.persenter.videoManager.currentDevice.name;
    if (self.persenter.videoManager.currentChannelInfo != nil) {
        titleStr = self.persenter.videoManager.currentChannelInfo.channelName;
    }
    self.title = titleStr;
    [self setupView];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.persenter.videoManager.isPlay = NO;
    [self.persenter onPlay:nil];
    weakSelf(self);
    [self lcCreatNavigationBarWith:LCNAVIGATION_STYLE_DEFAULT buttonClickBlock:^(NSInteger index) {
        if (index == 0) {
            if (self.persenter.videoManager.cloudVideotapeInfo) {
                if ([self isDownLoadVideo]) {
                    [DHAlertController showWithTitle:@"Alert_Title_Notice".lc_T message:@"video_tape_download_warnning".lc_T cancelButtonTitle:@"common_cancel".lc_T otherButtonTitle:@"common_confirm".lc_T handler:^(NSInteger index) {
                        if (index == 1) {
                            if ([self isDownLoadVideo]) {
                                [self.persenter stopDownload];
                                [self willChangeValueForKey:@"downloadQueue"];
                                self.persenter.videoManager.currentDownloadInfo.donwloadStatus = LCVideotapeDownloadStatusCancle;
                                [self didChangeValueForKey:@"downloadQueue"];
                            }
                            [weakself.navigationController popViewControllerAnimated:YES];
                        }
                    }];
                } else {
                    [weakself.navigationController popViewControllerAnimated:YES];
                }
            } else {
                [weakself.navigationController popViewControllerAnimated:YES];
            }
        } else {
        }
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(netChange:) name:@"NETCHANGE" object:nil];
}
- (void)netChange:(NSNotification *)notic {
    [self.persenter startPlay:0];
}
- (BOOL)isDownLoadVideo {
    LCVideotapeDownloadState status = self.persenter.videoManager.currentDownloadInfo.donwloadStatus;
    if (status == LCVideotapeDownloadStatusBegin || status == LCVideotapeDownloadStatusPartDownload) {
        return YES;
    } else {
        return NO;
    }
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.persenter.videoManager.isPlay = YES;
    [self.persenter stopPlay];
    self.persenter.videoManager.playSpeed = 1;
    self.persenter = nil;
}
- (LCVideotapePlayerPersenter *)persenter {
    if (!_persenter) {
        _persenter = [LCVideotapePlayerPersenter new];
        _persenter.container = self;
    }
    return _persenter;
}
- (void)setupView {
    weakSelf(self);
    UIView * topView = [UIView new];
    [self.view addSubview:topView];
    topView.backgroundColor = [UIColor dhcolor_c43];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(kStatusBarHeight);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(kNavBarHeight);
    }];
    LCButton * back = [LCButton lcButtonWithType:LCButtonTypeCustom];
    [topView addSubview:back];
    [back setTintColor:[UIColor dhcolor_c51]];
    [back setImage:LC_IMAGENAMED(@"nav_back") forState:UIControlStateNormal];
    [back mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topView.mas_left).offset(15);
        make.centerY.mas_equalTo(topView.mas_centerY);
    }];
    back.touchUpInsideblock = ^(LCButton * _Nonnull btn) {
        if (self.persenter.videoManager.cloudVideotapeInfo) {
            if ([self isDownLoadVideo]) {
                [DHAlertController showWithTitle:@"Alert_Title_Notice".lc_T message:@"video_tape_download_warnning".lc_T cancelButtonTitle:@"common_cancel".lc_T otherButtonTitle:@"common_confirm".lc_T handler:^(NSInteger index) {
                    if (index == 1) {
                        if ([self isDownLoadVideo]) {
                            [self.persenter stopDownload];
                            [self willChangeValueForKey:@"downloadQueue"];
                            self.persenter.videoManager.currentDownloadInfo.donwloadStatus = LCVideotapeDownloadStatusCancle;
                            [self didChangeValueForKey:@"downloadQueue"];
                        }
                        [self.persenter.playWindow uninitPlayWindow];
                        [self dismissViewControllerAnimated:true completion:nil];
                    }
                }];
            } else {
                [self.persenter.playWindow uninitPlayWindow];
                [self dismissViewControllerAnimated:true completion:nil];
            }
        } else {
            [self.persenter.playWindow uninitPlayWindow];
            [self dismissViewControllerAnimated:true completion:nil];
        }
    };
    UIView *tempView = [self.persenter.playWindow getWindowView];
    [self.view addSubview:tempView];
    [tempView updateConstraintsIfNeeded];
    [tempView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(kNavBarAndStatusBarHeight);
        make.left.mas_equalTo(self.view);
        make.width.mas_equalTo(self.view.mas_width);
        make.height.mas_equalTo(211);
    }];
    [self.persenter loadStatusView];
    LCVideoControlView *middleView = [LCVideoControlView new];
    middleView.isNeedProcess = YES;
    middleView.tag = 1;
    [self.view addSubview:middleView];
    [middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo([self.persenter.playWindow getWindowView].mas_bottom);
        make.right.left.mas_equalTo(self.view);
    }];
    middleView.items = [self.persenter getMiddleControlItems];
    [middleView.processView setStartDate:self.persenter.videoManager.cloudVideotapeInfo ? self.persenter.videoManager.cloudVideotapeInfo.beginDate : self.persenter.videoManager.localVideotapeInfo.beginDate EndDate:self.persenter.videoManager.cloudVideotapeInfo ? self.persenter.videoManager.cloudVideotapeInfo.endDate : self.persenter.videoManager.localVideotapeInfo.endDate];
    middleView.processView.valueChangeEndBlock = ^(float offset, NSDate *_Nonnull currentStartTiem) {
        [weakself.persenter onChangeOffset:offset];
    };
    LCButton *snapBtn = [self.persenter getItemWithType:LCVideotapePlayerControlSnap];
    [self.view addSubview:snapBtn];
    [snapBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view).offset(77);
        make.top.mas_equalTo(middleView.mas_bottom).offset(100);
    }];
    LCButton *pvrBtn = [self.persenter getItemWithType:LCVideotapePlayerControlPVR];
    [self.view addSubview:pvrBtn];
    [pvrBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view).offset(-77);
        make.top.mas_equalTo(snapBtn);
    }];
    LCButton *downBtn = [self.persenter getItemWithType:LCVideotapePlayerControlDownload];
    [self.view addSubview:downBtn];
    [downBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(self.view);
        make.height.mas_equalTo(kIs_iPhoneX ? 85 : 55);
    }];
    downBtn.hidden = self.persenter.videoManager.cloudVideotapeInfo == nil ? YES : NO;
    LCVideotapeDownloadStatusView *statusView = [LCVideotapeDownloadStatusView showDownloadStatusInView:self.view Size:self.persenter.videoManager.cloudVideotapeInfo ? [self.persenter.videoManager.cloudVideotapeInfo.size integerValue] : self.persenter.videoManager.localVideotapeInfo.fileLength];
    statusView.alpha = 0;
    statusView.cancleBlock = ^{
        [weakself.persenter stopDownload];
    };
    [statusView.KVOController observe:self.persenter.videoManager keyPath:@"downloadQueue" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
        LCVideotapeDownloadInfo *info = [weakself.persenter.videoManager currentDownloadInfo];
        if (weakself.persenter.videoManager.localVideotapeInfo ||![info.recordId isEqualToString:weakself.persenter.videoManager.currentVideotapeId]) {
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            downBtn.enabled = YES;
            NSLog(@"下载信息%@", info);
            if (info.donwloadStatus != -1) {
                NSLog(@"下载状态:%ld", info.donwloadStatus);
            }
            if (info.donwloadStatus == LCVideotapeDownloadStatusFail || info.donwloadStatus == LCVideotapeDownloadStatusCancle || info.donwloadStatus == LCVideotapeDownloadStatusTimeout || info.donwloadStatus == LCVideotapeDownloadStatusKeyError || info.donwloadStatus == LCVideotapeDownloadStatusEnd) {
                [statusView dismiss];
                statusView.recieve = 0;
                statusView.totalRevieve = 0;
                NSLog(@"隐藏下载进度条%ld", info.donwloadStatus);
            } else {
                NSLog(@"展示下载进度条");
                NSLog(@"下载状态:%ld", info.donwloadStatus);
                statusView.alpha = 1;
                statusView.recieve = info.recieve;
            }
        });
    }];
    LCLandscapeControlView *landscapeControlView = [LCLandscapeControlView new];
    landscapeControlView.delegate = self.persenter;
    landscapeControlView.isNeedProcess = YES;
    [self.view addSubview:landscapeControlView];
    [landscapeControlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.mas_equalTo(self.view);
    }];
    landscapeControlView.hidden = YES;
    landscapeControlView.presenter = self.persenter;
    [landscapeControlView setStartDate:self.persenter.videoManager.cloudVideotapeInfo ? self.persenter.videoManager.cloudVideotapeInfo.beginDate : self.persenter.videoManager.localVideotapeInfo.beginDate EndDate:self.persenter.videoManager.cloudVideotapeInfo ? self.persenter.videoManager.cloudVideotapeInfo.endDate : self.persenter.videoManager.localVideotapeInfo.endDate];
    [self.persenter configBigPlay];
    [self.KVOController observe:self.persenter.videoManager keyPath:@"currentPlayOffest" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
        [middleView.processView setCurrentDate:change[@"new"]];
        [landscapeControlView setCurrentDate:change[@"new"]];
    }];
    [self.KVOController observe:self.persenter.videoManager keyPath:@"isFullScreen" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
        if ([change[@"new"] boolValue]) {
            landscapeControlView.hidden = NO;
            snapBtn.hidden = YES;
            pvrBtn.hidden = YES;
            middleView.hidden = YES;
            [weakself configFullScreenUI];
            self.navigationController.navigationBar.hidden = YES;
        } else {
            self.navigationController.navigationBar.hidden = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                               [weakself configPortraitScreenUI];
                           });
            landscapeControlView.hidden = YES;
            snapBtn.hidden = NO;
            pvrBtn.hidden = NO;
            middleView.hidden = NO;
        }
    }];
}
- (BOOL)shouldAutorotate {
    if (self.persenter.videoManager.isFullScreen && self.persenter.videoManager.isLockFullScreen) {
        return NO;
    }
    if (!self.persenter.videoManager.isFullScreen && [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft) {
        self.persenter.videoManager.isFullScreen = !self.persenter.videoManager.isFullScreen;
        self.persenter.videoManager.isLockFullScreen = NO;
    }
    if (self.persenter.videoManager.isFullScreen && [[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait) {
        self.persenter.videoManager.isFullScreen = !self.persenter.videoManager.isFullScreen;
        self.persenter.videoManager.isLockFullScreen = NO;
    }
    return YES;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}
- (void)configFullScreenUI {
    UIView *playWindow =  [self.persenter.playWindow getWindowView];
    [self.view updateConstraintsIfNeeded];
    [playWindow mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.left.mas_equalTo(self.view);
        make.height.mas_equalTo(self.view);
        make.width.mas_equalTo(self.view);
    }];
}
- (void)configPortraitScreenUI {
    UIView *playWindow =  [self.persenter.playWindow getWindowView];
    [self.view updateConstraintsIfNeeded];
    [playWindow mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(kNavBarAndStatusBarHeight);
        make.left.mas_equalTo(self.view);
        make.width.mas_equalTo(self.view);
        make.height.mas_equalTo(211);
    }];
}
- (void)onActive:(id)sender {
    if ([[self.navigationController.viewControllers lastObject] isKindOfClass:[self class]]) {
        [self.persenter onActive:sender];
    }
}
- (void)onResignActive:(id)sender {
    [self.persenter onResignActive:sender];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.persenter.playWindow setWindowFrame:[self.persenter.playWindow getWindowView].frame];
    });
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    if (@available(iOS 13.0, *)) {
        return UIStatusBarStyleDarkContent;
    } else {
        return UIStatusBarStyleDefault;
    }
}
@end
