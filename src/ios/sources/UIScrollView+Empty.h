#import <UIKit/UIKit.h>
@interface UIScrollView (Empty)
@property (strong, nonatomic)UIImage *lc_emptyImage;
@property (strong, nonatomic)UIView *lc_emptyView;
- (void)lc_clearEmptyViewInfo; 
@end
