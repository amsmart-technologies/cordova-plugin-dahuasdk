#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface UINavigationController (Pop)
-(void)lc_popToViewController:(NSString *)vc Filter:(nullable NSInteger(^)(NSArray * vcs))filter animated:(BOOL)animated;
@end
NS_ASSUME_NONNULL_END
