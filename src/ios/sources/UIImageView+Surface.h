#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
@interface UIImageView (Surface)
-(void)lc_setThumbImageWithURL:(NSString *)url placeholderImage:(UIImage *)placeholder DeviceId:(NSString *)deviceId ChannelId:(NSString *)chanelId;
-(void)lc_storeImage:(UIImage *)image ForDeviceId:(NSString *)deviceId ChannelId:(NSString *)chanelId;
- (nullable UIImage *)lc_getThumbImageForDeviceId:(NSString *)deviceId ChannelId:(NSString *)chanelId;
-(BOOL)lc_deskCacheIsExistThumbImageForDeviceId:(NSString *)deviceId ChannelId:(NSString *)chanelId;
-(void)lc_deleteThumbImageWithDeviceId:(NSString *)deviceId ChannelId:(NSString *)chanelId;
@end
NS_ASSUME_NONNULL_END
