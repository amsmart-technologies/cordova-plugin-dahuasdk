#import "LCLivePreviewViewController.h"
#import "LCVideoControlView.h"
#import "LCVideoCallControlView.h"
#import "LCLivePreviewPresenter.h"
#import "LCPTZControlView.h"
#import "LCLandscapeControlView.h"
#import "LCLivePreviewPresenter+LandscapeControlView.h"
#import "UIImageView+Surface.h"
#import "LCVideoHistoryView.h"
#import "LCDeviceVideotapePlayManager.h"
#import "LCBaseDefine.h"
#import "NSString+AbilityAnalysis.h"
#import <KVOController/KVOController.h>
@interface LCLivePreviewViewController ()
@property (strong, nonatomic) LCLivePreviewPresenter *persenter;
@property (strong, nonatomic) UIView *ptzControl;
@end
@implementation LCLivePreviewViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self.persenter.videoManager.currentDevice.status isEqualToString:@"online"]) {
        self.persenter.videoManager.isPlay = NO;
        [self.persenter onPlay:nil];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    weakSelf(self);
    NSString *titleStr = self.persenter.videoManager.currentDevice.name;
    if (self.persenter.videoManager.currentChannelInfo != nil) {
        titleStr = self.persenter.videoManager.currentChannelInfo.channelName;
    }
    self.title = titleStr;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(netChange:) name:@"NETCHANGE" object:nil];
}
- (void)netChange:(NSNotification *)notic {
    [self.persenter startPlay];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.persenter stopPlay];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.finishBlock();
}
- (LCLivePreviewPresenter *)persenter {
    if (!_persenter) {
        _persenter = [LCLivePreviewPresenter new];
        _persenter.liveContainer  = self;
        _persenter.container = self;
    }
    return _persenter;
}
- (void)setupView {
    weakSelf(self);
    UIView * topView = [UIView new];
    [self.view addSubview:topView];
    topView.backgroundColor = [UIColor dhcolor_c43];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self).offset(kStatusBarHeight);
        make.left.right.mas_equalTo(self);
        make.height.mas_equalTo(kNavBarHeight);
    }];
    UILabel * titleLab = [UILabel new];
    NSString *titleStr = self.persenter.videoManager.currentDevice.name;
    if (self.persenter.videoManager.currentChannelInfo != nil) {
        titleStr = self.persenter.videoManager.currentChannelInfo.channelName;
    }
    titleLab.text = titleStr;
    titleLab.textColor = [UIColor dhcolor_c51];
    [topView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(topView.mas_centerY);
        make.centerX.mas_equalTo(topView.mas_centerX);
    }];
    LCButton * back = [LCButton lcButtonWithType:LCButtonTypeCustom];
    [topView addSubview:back];
    [back setTintColor:[UIColor dhcolor_c51]];
    [back setImage:LC_IMAGENAMED(@"nav_back") forState:UIControlStateNormal];
    [back mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(topView.mas_left).offset(15);
        make.centerY.mas_equalTo(topView.mas_centerY);
    }];
    back.touchUpInsideblock = ^(LCButton * _Nonnull btn) {
        [weakself.persenter.playWindow uninitPlayWindow];
        [self dismissViewControllerAnimated:true completion:nil];
    };
    // LCButton * settings = [LCButton lcButtonWithType:LCButtonTypeCustom];
    // [topView addSubview:settings];
    // [settings setTintColor:[UIColor dhcolor_c51]];
    // [settings setImage:LC_IMAGENAMED(@"home_icon_device_setting") forState:UIControlStateNormal];
    // [settings mas_makeConstraints:^(MASConstraintMaker *make) {
    //     make.right.mas_equalTo(topView.mas_right).offset(-15);
    //     make.centerY.mas_equalTo(topView.mas_centerY);
    // }];
    // settings.touchUpInsideblock = ^(LCButton * _Nonnull btn) {
    //     [self.persenter openSettings];
    // };
    UIView * tempView = [self.persenter.playWindow getWindowView];
    [self.view addSubview:tempView];
    [tempView updateConstraintsIfNeeded];
    [tempView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(kNavBarAndStatusBarHeight);
        make.left.mas_equalTo(self.view);
        make.width.mas_equalTo(self.view.mas_width);
        make.height.mas_equalTo(211);
    }];
    LCVideoControlView * middleView = [LCVideoControlView new];
    middleView.isNeedProcess = NO;
    middleView.tag = 1;
    [self.view addSubview:middleView];
    [middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo([self.persenter.playWindow getWindowView].mas_bottom);
        make.right.left.mas_equalTo(self.view);
    }];
    middleView.items = [self.persenter getMiddleControlItems];
    LCVideoControlView * bottomView = [LCVideoControlView new];
    bottomView.isNeedProcess = NO;
    bottomView.tag = 2;
    bottomView.style = LCVideoControlLightStyle;
    if(!self.persenter.videoManager.callBellEvent){
        [self.view addSubview:bottomView];
    }
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(middleView.mas_bottom);
        make.width.mas_equalTo(self.view.mas_width);
        make.left.mas_equalTo(self.view);
    }];
    bottomView.items = [self.persenter getBottomControlItems];
    LCVideoCallControlView * bottomCallView = [LCVideoCallControlView new];
    bottomCallView.tag = 2;
    if(self.persenter.videoManager.callBellEvent){
        self.persenter.videoManager.callAnswered = NO;
        self.persenter.videoManager.isSoundOn = NO;
        [self.persenter.videoManager playDoorBellSound];
        [self.view addSubview:bottomCallView];
    }
    [bottomCallView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(middleView.mas_bottom);
        make.width.mas_equalTo(self.view.mas_width);
        make.left.mas_equalTo(self.view);
    }];
    bottomCallView.items = [self.persenter getBottomCallItems];
    [bottomCallView.items.KVOController observe:[LCDeviceVideoManager manager] keyPath:@"answerbuttonPressed" options:NSKeyValueObservingOptionNew block:^(id _Nullable observer, id _Nonnull object, NSDictionary<NSString *, id> *_Nonnull change) {
        if ([change[@"new"] boolValue]) {
            [bottomCallView.items mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(bottomCallView.mas_centerX);
            }];
        }
    }];
    LCPTZControlView * ptzControlView = [[LCPTZControlView alloc] initWithDirection:self.persenter.videoManager.currentDevice.ability.isSupportPTZ?LCPTZControlSupportEight:(self.persenter.videoManager.currentDevice.ability.isSupportPT?LCPTZControlSupportEight:LCPTZControlSupportFour)];
    ptzControlView.tag = 999;
    ptzControlView.backgroundColor = [UIColor dhcolor_c43];
    ptzControlView.alpha = 0;
    [self.view addSubview:ptzControlView];
    [ptzControlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(bottomView.mas_bottom);
        make.left.mas_equalTo(self.view.mas_left);
        make.width.mas_equalTo(self.view.mas_width);
        make.bottom.mas_equalTo(self.view.mas_bottom);
    }];
    ptzControlView.panel.resultBlock = ^(VPDirection direction, double scale, NSTimeInterval timeInterval) {
        [weakself.persenter ptzControlWith:[NSString stringWithFormat:@"%ld",direction] Duration:timeInterval];
    };
    LCLandscapeControlView * landscapeControlView = [LCLandscapeControlView new];
    landscapeControlView.delegate = self.persenter;
    landscapeControlView.isNeedProcess = NO;
    [self.view addSubview:landscapeControlView];
    [landscapeControlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.mas_equalTo(self.view);
    }];
    landscapeControlView.hidden = YES;
    landscapeControlView.presenter = self.persenter;
    [self.persenter configBigPlay];
    LCPTZPanel * landscapePtz = [[LCPTZPanel alloc] initWithFrame:CGRectMake(0, 0, 100, 100) style:self.persenter.videoManager.currentDevice.ability.isSupportPTZ?LCPTZPanelStyle8Direction:(self.persenter.videoManager.currentDevice.ability.isSupportPT?LCPTZPanelStyle8Direction:LCPTZPanelStyle4Direction)];
    landscapePtz.tag = 998;
    [landscapePtz configLandscapeUI];
    landscapePtz.alpha = 0;
    [landscapeControlView addSubview:landscapePtz];
    [landscapePtz mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(150);
        make.left.mas_equalTo(landscapeControlView.mas_left).offset(15);
        make.centerY.mas_equalTo(landscapeControlView.mas_centerY);
    }];
    landscapePtz.resultBlock = ^(VPDirection direction, double scale, NSTimeInterval timeInterval) {
        [weakself.persenter ptzControlWith:[NSString stringWithFormat:@"%ld",direction] Duration:timeInterval];
    };
    if(self.persenter.videoManager.playbackEnabled && !self.persenter.videoManager.callBellEvent){
        UIView * videoHistoryView = [self.persenter getVideotapeView];
        [self.view addSubview:videoHistoryView];
        [videoHistoryView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(bottomView.mas_bottom).offset(5);
            make.right.left.mas_equalTo(self.view);
        }];
    }
    [self.KVOController observe:self.persenter.videoManager keyPath:@"isFullScreen" options:NSKeyValueObservingOptionNew block:^(id  _Nullable observer, id  _Nonnull object, NSDictionary<NSString *,id> * _Nonnull change) {
        if ([change[@"new"] boolValue]) {
            landscapeControlView.hidden = NO;
            topView.hidden = YES;
            bottomView.hidden = YES;
            middleView.hidden = YES;
            [weakself configFullScreenUI];
            weakself.navigationController.navigationBar.hidden = YES;
        }else{
            self.navigationController.navigationBar.hidden = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [weakself configPortraitScreenUI];
            });
            landscapeControlView.hidden = YES;
            topView.hidden = NO;
            bottomView.hidden = NO;
            middleView.hidden = NO;
        }
    }];
}
- (void)showPtz {
    UIView * ptzP = [self.view viewWithTag:998];
    UIView * ptzL = [self.view viewWithTag:999];
    [UIView animateWithDuration:0.2 animations:^{
        ptzP.alpha = 1.0;
        ptzL.alpha = 1.0;
    }];
}
- (void)hidePtz {
    UIView * ptzP = [self.view viewWithTag:998];
    UIView * ptzL = [self.view viewWithTag:999];
    [UIView animateWithDuration:0.2 animations:^{
        ptzP.alpha = 0;
        ptzL.alpha = 0;
    }];
}
- (BOOL)shouldAutorotate {
    if (self.persenter.videoManager.isFullScreen && self.persenter.videoManager.isLockFullScreen) {
        return NO;
    }
    if (!self.persenter.videoManager.isFullScreen && [[UIApplication sharedApplication] statusBarOrientation]==UIInterfaceOrientationLandscapeLeft) {
        self.persenter.videoManager.isFullScreen = !self.persenter.videoManager.isFullScreen;
        self.persenter.videoManager.isLockFullScreen = NO;
    }
    if (self.persenter.videoManager.isFullScreen && [[UIApplication sharedApplication] statusBarOrientation]==UIInterfaceOrientationPortrait) {
        self.persenter.videoManager.isFullScreen = !self.persenter.videoManager.isFullScreen;
        self.persenter.videoManager.isLockFullScreen = NO;
    }
    return YES;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}
- (void)configFullScreenUI {
    UIView * playWindow =  [self.persenter.playWindow getWindowView];
    [self.view updateConstraintsIfNeeded];
    [playWindow mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(self.view);
        make.height.mas_equalTo(SCREEN_HEIGHT > SCREEN_WIDTH ? SCREEN_WIDTH : SCREEN_HEIGHT);
        make.width.mas_equalTo(SCREEN_HEIGHT > SCREEN_WIDTH ? SCREEN_HEIGHT : SCREEN_WIDTH);
    }];
}
- (void)configPortraitScreenUI {
    UIView * playWindow =  [self.persenter.playWindow getWindowView];
    [self.view updateConstraintsIfNeeded];
    [playWindow mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view).offset(kNavBarAndStatusBarHeight);
        make.left.mas_equalTo(self.view);
        make.width.mas_equalTo(self.view);
        make.height.mas_equalTo(211);
    }];
}
- (void)onResignActive:(id)sender {
    [self.persenter onResignActive:sender];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    weakSelf(self);
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakself.persenter.playWindow setWindowFrame:[weakself.persenter.playWindow getWindowView].frame];
    });
}
- (void)dealloc {
}
- (UIStatusBarStyle)preferredStatusBarStyle {
    if (@available(iOS 13.0, *)) {
        return UIStatusBarStyleDarkContent;
    } else {
        return UIStatusBarStyleDefault;
    }
}
@end
