#import "LCNetTool.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <AFNetworking/AFNetworking.h>
@interface LCNetTool ()
@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic, copy) void (^ netToolSSID)(NSString *ssid);
@end
static LCNetTool *tool = nil;
@implementation LCNetTool
+ (instancetype)shareManager {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tool = [LCNetTool new];
    });
    return tool;
}
- (CLLocationManager *)locManager {
    if (!_locManager) {
        _locManager = [[CLLocationManager alloc] init];
    }
    return _locManager;
}
- (void)lc_CurrentWiFiName:(void (^)(NSString *ssid))block {
    self.netToolSSID = block;
    [self locManager];
    if (@available(iOS 13.0, *)) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            return;
        }
        if (![CLLocationManager locationServicesEnabled] || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            [self.locManager requestWhenInUseAuthorization];
            return;
        }
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
            [self getSSID];
            return;
        }
    } else {
        [self getSSID];
    }
}
- (void)getSSID {
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info && [info count]) {
            break;
        }
    }
    NSString * ssid = info[@"SSID"];
    if (self.netToolSSID) {
        self.netToolSSID(ssid ? ssid : @"");
    }
}
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse ||
        status == kCLAuthorizationStatusAuthorizedAlways) {
        [self getSSID];
    }
}
+ (LCNetStatus)lc_CurrentNetStatus {
    AFNetworkReachabilityStatus status = [[AFNetworkReachabilityManager sharedManager] networkReachabilityStatus];
    return (LCNetStatus)status;
}
+ (void)lc_ObserveNetStatus:(void (^)(LCNetStatus status))netStatus {
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [manager startMonitoring];
        [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            switch (status) {
                case AFNetworkReachabilityStatusUnknown:
                case AFNetworkReachabilityStatusNotReachable: {
                    netStatus(LCNetStatusOFFLine);
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"NETCHANGE" object:@(LCNetStatusOFFLine) userInfo:nil];
                }
                break;
                case AFNetworkReachabilityStatusReachableViaWWAN: {
                    netStatus(LCNetStatusWWAN);
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"NETCHANGE" object:@(LCNetStatusWWAN) userInfo:nil];
                }
                case AFNetworkReachabilityStatusReachableViaWiFi: {
                    netStatus(LCNetStatusWiFi);
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"NETCHANGE" object:@(LCNetStatusWiFi) userInfo:nil];
                }
                break;
            }
        }];
    });
}
@end
