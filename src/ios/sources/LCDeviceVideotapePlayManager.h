#import "LCDeviceVideoManager.h"
#import "LCCloudVideotapeInfo.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCVideotapeDownloadInfo : NSObject
@property (nonatomic) NSInteger index;
@property (strong, nonatomic) NSString *recordId;
@property (strong, nonatomic) NSString *deviceId;
@property (strong, nonatomic) NSString *channelId;
@property (nonatomic) LCVideotapeDownloadState donwloadStatus;
@property (strong, nonatomic) NSString *localPath;
@property (nonatomic) NSInteger recieve;
@end
@interface LCDeviceVideotapePlayManager : LCDeviceVideoManager
+ (instancetype)manager;
@property (strong, nonatomic) LCCloudVideotapeInfo *cloudVideotapeInfo;
@property (strong, nonatomic) LCLocalVideotapeInfo *localVideotapeInfo;
@property (strong,nonatomic) NSDate * currentPlayOffest;
@property (strong,nonatomic) NSString * currentVideotapeId;
@property (strong, nonatomic) NSMutableDictionary<NSString *, LCVideotapeDownloadInfo *> *downloadQueue;
- (void)startDeviceDownload;
- (LCVideotapeDownloadInfo *)currentDownloadInfo;
- (void)cancleDownload:(NSString *)recordId;
@end
NS_ASSUME_NONNULL_END
