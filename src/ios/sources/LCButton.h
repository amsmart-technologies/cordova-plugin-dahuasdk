#import <UIKit/UIKit.h>
#import "UIView+BorderColor.h"
#import <Masonry/Masonry.h>
NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    LCButtonTypeCustom,
    LCButtonTypePrimary,
    LCButtonTypeLink,
    LCButtonTypeCode,
    LCButtonTypeMinor,
    LCButtonTypeShadow,
    LCButtonTypeVertical,
    LCButtonTypeCheckBox
} LCButtonType;
@interface LCButton : UIButton
typedef void(^LCButtonTouchUpInsideBlock)(LCButton * btn);
@property (nonatomic) NSUInteger lcBtnType;
@property (copy, nonatomic) LCButtonTouchUpInsideBlock touchUpInsideblock;
+ (instancetype)lcButtonWithType:(LCButtonType)type;
- (void)lcButtonSetType:(LCButtonType)type;
- (void)setBorderWithStyle:(LC_BORDER_DRAW_STYLE)style borderColor:(UIColor *)color borderWidth:(CGFloat)width;
- (void)setEnabled:(BOOL)enabled;
@end
NS_ASSUME_NONNULL_END
