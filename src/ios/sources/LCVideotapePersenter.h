#import "LCUIKit.h"
#import "LCVideotapeListViewController.h"
#import "LCDeviceVideoManager.h"
#import "LCCloudVideotapeInfo.h"
#import "LCBasicPresenter.h"
NS_ASSUME_NONNULL_BEGIN
@interface LCVideotapePersenter : LCBasicPresenter<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic) BOOL isEdit;
@property (nonatomic) BOOL isSelectAll;
@property (nonatomic) BOOL isCloudMode;
@property (nonatomic,strong)NSMutableArray <LCCloudVideotapeInfo *> * cloudVideoArray;
@property (nonatomic,strong)NSMutableArray<LCLocalVideotapeInfo *> * localVideoArray;
@property (nonatomic,strong)LCDeviceVideoManager * videoManager;
@property (nonatomic,retain)LCVideotapeListViewController * videoListPage;
@property (nonatomic, strong) NSDate *currentDate;
-(void)refreshCloudVideoListWithDate:(nullable NSDate *)date;
-(void)refreshLocalVideoListWithDate:(nullable NSDate *)date;
-(void)loadMoreCloudVideoListWithDate:(nonnull NSDate *)date;
-(void)loadMoreLocalVideoListWithDate:(nonnull NSDate *)date;
-(void)deleteCloudViewotape;
@end
NS_ASSUME_NONNULL_END
