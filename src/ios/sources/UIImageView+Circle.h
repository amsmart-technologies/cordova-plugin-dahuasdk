#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN
typedef enum : NSUInteger {
    LCIMGCirclePlayStyleCircle,
    LCIMGCirclePlayStyleOnce,
    LCIMGCirclePlayStyleNature,
} LCIMGCirclePlayStyle;
@interface UIImageView (Circle)
@property (nonatomic) NSArray <NSString *> * image;
@property (strong, nonatomic) NSTimer * timer;
@property (nonatomic) LCIMGCirclePlayStyle style;
@property (nonatomic) NSUInteger  index;
-(void)loadGifImageWith:(NSArray <NSString *>*)images TimeInterval:(NSTimeInterval)interval Style:(LCIMGCirclePlayStyle)style;
-(void)releaseImgs;
@end
NS_ASSUME_NONNULL_END
