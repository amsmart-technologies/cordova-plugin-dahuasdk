#import <UIKit/UIKit.h>
@interface UIDevice (LeChange)
+ (void)lc_setRotateToSatusBarOrientation;
+ (void)lc_setOrientation:(UIInterfaceOrientation)orientation;
+ (NSString *)lc_orientationDescprition:(UIInterfaceOrientation)orientation;
+ (long long)lc_freeDiskSpaceInBytes;
#pragma mark - ip mask gate dns
+ (NSString *)lc_getIPAddress;
+ (NSString *)lc_getMaskAddress;
+ (NSString *)lc_getRouterAddress;
+ (NSString *)lc_getDNSAddress;
@end
