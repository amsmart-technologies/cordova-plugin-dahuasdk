#import "LCStore.h"
#import "DHFileManager.h"
@interface LCStore() {
    NSMutableDictionary *_storeDic;
    NSString *_localPath;
}
@end
@implementation LCStore
- (instancetype)initWithLocalPath:(NSString *)path {
    if (self = [super init]) {
        _localPath = path;
        NSLog(@"🍎🍎🍎 %@:: Init with local path - %@", NSStringFromClass([self class]), path);
        _storeDic = [NSMutableDictionary dictionaryWithContentsOfFile:_localPath];;
        if (_storeDic == nil) {
            _storeDic = [NSMutableDictionary new];
        }
    }
    return self;
}
- (instancetype)init {
    if (self = [super init]) {
        NSString *configfilepath = [DHFileManager configFilePath];
        NSLog(@"🍎🍎🍎 %@:: Init with config file path - %@", NSStringFromClass([self class]), configfilepath);
        _storeDic = [NSMutableDictionary dictionaryWithContentsOfFile:configfilepath];;
        if (_storeDic == nil) {
            _storeDic = [NSMutableDictionary new];
        }
    }
    return self;
}
- (NSDictionary *)dicStore {
    return [_storeDic copy];
}
#pragma mark - 数据存储
- (id)getObjByKey:(NSString *)key {
    return _storeDic[key];
}
- (void)saveObj:(id)value withKey:(NSString *)key {
    @synchronized(self)
    {
        if (key == nil) {
            return;
        }
        if (_storeDic[key]!= nil && [value isKindOfClass:[NSNumber class]] && [_storeDic[key] isKindOfClass:[NSNumber class]]) {
            NSNumber *num1 = (NSNumber *)value;
            NSNumber *num2 = (NSNumber *)_storeDic[key];
            if ([num1 isEqualToNumber:num2]) {
                return ;
            }
        }
        _storeDic[key] = value;
        [self saveDictionary];
    }
}
- (BOOL)saveDictionary {
    return [_storeDic writeToFile:_localPath atomically:YES];
}
- (void)saveAppended:(NSDictionary *)dictionary {
    [_storeDic addEntriesFromDictionary:dictionary];
    for (id key in _storeDic.allKeys) {
        [self saveObj:_storeDic[key] withKey:key];
    }
}
@end
