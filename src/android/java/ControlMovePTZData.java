package cordova.plugin.dahuasdk;

import java.io.Serializable;

public class ControlMovePTZData implements Serializable {
    public ControlMovePTZData.RequestData data = new ControlMovePTZData.RequestData();

    public static class RequestData implements Serializable {
        public String token;
        public String deviceId;
        public String channelId = "0";
        public String operation;
        public long duration;
    }
}
