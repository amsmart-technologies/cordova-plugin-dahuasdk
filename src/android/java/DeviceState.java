package cordova.plugin.dahuasdk;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;

import static cordova.plugin.dahuasdk.DeviceState.OFFLINE;
import static cordova.plugin.dahuasdk.DeviceState.ONLINE;
import static cordova.plugin.dahuasdk.DeviceState.SLEEP;
import static cordova.plugin.dahuasdk.DeviceState.UPGRADE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Retention(SOURCE)
@StringDef({ONLINE, OFFLINE, SLEEP, UPGRADE, ""})
public @interface DeviceState {
    String ONLINE = "online";
    String OFFLINE = "offline";
    String SLEEP = "sleep";
    String UPGRADE = "upgrading";
}
