package cordova.plugin.dahuasdk;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.UUID;

public class DataInfo implements Cloneable, Serializable {
    private static final long serialVersionUID = 1L;
    protected String uuid = UUID.randomUUID().toString();

    public Hashtable<String, Object> getExtandAttributeTable() {
        if (extandAttributeTable == null) {
            extandAttributeTable = new Hashtable<>();
        }
        return extandAttributeTable;
    }

    protected Hashtable<String, Object> extandAttributeTable = null;


    public Object getExtandAttributeValue(String name) {
        if (extandAttributeTable == null
                || !extandAttributeTable.containsKey(name)) {
            return null;
        }

        return extandAttributeTable.get(name);
    }

    public void setExtandAttributeValue(String name, Object value) {
        if (extandAttributeTable == null) {
            extandAttributeTable = new Hashtable<>();
        }
        if (value != null) {
            extandAttributeTable.put(name, value);
        }
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return uuid;
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        DataInfo dataInfo = null;

        dataInfo = (DataInfo) super.clone();

        if (extandAttributeTable != null) {
            dataInfo.extandAttributeTable = new Hashtable<>();
            dataInfo.extandAttributeTable.putAll(extandAttributeTable);

        }

        return dataInfo;
    }

}
