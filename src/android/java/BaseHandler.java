package cordova.plugin.dahuasdk;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class BaseHandler extends Handler {

    private final static String TAG = "LeChange.BaseHandler";

    private AtomicBoolean isCancled = new AtomicBoolean(false);

    public BaseHandler() {
        super();
    }

    public BaseHandler(Looper looper) {
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        if (!isCancled.get()) {
            if (msg.what == HandleMessageCode.HMC_EXCEPTION) {
                Log.d(TAG, "base hander throw exception. what =" + msg.what);
                if (BusinessAuthUtil.isAuthFailed(msg.arg1)) {
                    authError(msg);
                    return;
                }
            }
            handleBusiness(msg);
        }
    }

    public abstract void handleBusiness(Message msg);

    public void authError(Message msg) {

    }

    public void cancle() {
        isCancled.set(true);
    }

    public boolean isCanceled() {
        return isCancled.get();
    }
}
