package cordova.plugin.dahuasdk;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.LockSupport;

public class TaskPoolHelper {
    private final static String TAG = "lcop_TaskPoolHelper";

    public static abstract class RunnableTask implements Runnable {
        public String mTaskID;

        public RunnableTask(String taskID) {
            this.mTaskID = taskID;
        }
    }

    private static ArrayBlockingQueue<RunnableTask> mQueueTask = new ArrayBlockingQueue<RunnableTask>(50);
    private static List<String> mFilteTask = new ArrayList<String>();
    private static Thread mQueueThread;
    private static RunnableTask mRealTask;
    private static Thread mRealThread;

    static {
        mQueueThread = new Thread() {
            @Override
            public void run() {
                super.run();
                while (true) {
                    try {
                        RunnableTask task = mQueueTask.take();
                        task.run();
                        mFilteTask.remove(task.mTaskID);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        mQueueThread.start();

        mRealThread = new Thread() {
            @Override
            public void run() {
                super.run();
                while (true) {
                    if (mRealTask == null) {
                        LockSupport.park();
                    } else {
                        RunnableTask task = mRealTask;
                        mRealTask = null;
                        task.run();
                    }
                }
            }
        };
        mRealThread.start();
    }

    public static void addTask(RunnableTask task) {
        if (task.mTaskID.equals("real")) {
            mRealTask = task;
            LockSupport.unpark(mRealThread);
            return;
        }
        if (mFilteTask.contains(task.mTaskID)) {
            return;
        }
        try {
            mQueueTask.add(task);
            mFilteTask.add(task.mTaskID);
        } catch (IllegalStateException e) {
            mQueueTask.clear();
            mFilteTask.clear();
        }
    }

    public static void clearTask() {
        mQueueTask.clear();
        mFilteTask.clear();
    }

}
