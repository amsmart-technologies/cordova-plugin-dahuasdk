package cordova.plugin.dahuasdk;

import android.content.Context;

public interface IBaseView {
    Context getContextInfo();

    boolean isViewActive();

    void showToastInfo(String msg);

    void showToastInfo(int msgId);

    void showToastInfo(int msgId, String errorDesc);

    void showProgressDialog();

    void cancelProgressDialog();

    void onMessageEvent(BaseEvent event);

    void unInit();

    void initPresenter();

    void finish();
}
