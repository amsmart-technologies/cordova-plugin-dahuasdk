package cordova.plugin.dahuasdk;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public class HiddenWifiActivity<T extends HiddenWifiConstract.Presenter> extends BaseManagerFragmentActivity<T> implements HiddenWifiConstract.View {

    public static String ERROR_PARAMS = "error_params";

    private TextView mNext;

    private ClearEditText mWifiName;

    private ClearPasswordEditText mWifiPsw;

    @Override
    protected View initTitle() {
        CommonTitle title = findViewById(getResources("id", "device_hidden_title"));
        title.initView(getResources("drawable", "mobile_common_title_back"), 0, getResources("string", "dahua_device_manager_wifi_title"));
        title.setOnTitleClickListener(new CommonTitle.OnTitleClickListener() {
            @Override
            public void onCommonTitleClick(int id) {
                if (id == CommonTitle.ID_LEFT) {
                    HiddenWifiActivity.this.finish();
                }
            }
        });
        return title;
    }

    @Override
    protected void initLayout() {
        setContentView(getResources("layout", "activity_device_hidden_wifi"));
    }

    protected void initView() {
        super.initView();

        mWifiName = findViewById(getResources("id", "wifi_name"));
        mWifiPsw = findViewById(getResources("id", "wifi_psw"));

        mNext = findViewById(getResources("id", "next"));
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(mWifiPsw.getWindowToken(), 0);
                mWifiPsw.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPresenter.wifiOperate();
                    }
                }, 100);
            }
        });
        findViewById(getResources("id", "tv_5g_tip")).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HiddenWifiActivity.this, ErrorTipActivity.class);
                intent.putExtra(ERROR_PARAMS, 1);
                startActivity(intent);
            }
        });

        mWifiName.addTextChangedListener(new SimpleTextChangedListener() {
            @Override
            public void afterTextChanged(Editable s) {
              mNext.setEnabled(s.length()>0);
            }
        });

    }

    @Override
    public void initPresenter() {
        mPresenter = (T) new HiddenWifiPresenter<>(this);
    }

    @Override
    protected void initData() {
        mPresenter.dispatchIntentData(getIntent());
    }


    @Override
    public String getWifiSSID() {
        return mWifiName.getText().toString().trim();
    }

    @Override
    public String getWifiPassword() {
        return mWifiPsw.getText().toString().trim();
    }

    @Override
    public void onWifiOperateSucceed(CurWifiInfo curWifiInfo) {
        finish();
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
