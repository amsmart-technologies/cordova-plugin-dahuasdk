package cordova.plugin.dahuasdk;

public enum Direction {
    Up, Down, Left, Right, Left_up, Left_down, Right_up, Right_down
}
