package cordova.plugin.dahuasdk;

public class BusinessErrorCode {
    private final static int BEC_COMMON_BASE = 0;
    private final static int BEC_DEVICE_BASE = 3000;
    public final static int BEC_COMMON_UNKNOWN = BEC_COMMON_BASE + 1;
    public final static int BEC_COMMON_NULL_POINT = BEC_COMMON_BASE + 9;
    public final static int BEC_COMMON_TIME_OUT = BEC_COMMON_BASE + 11;
    public static final int BEC_DEVICE_ADD_AP_UPPER_LIMIT = BEC_DEVICE_BASE + 65;
}