package cordova.plugin.dahuasdk;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.lechange.opensdk.listener.LCOpenSDK_EventListener;
import com.lechange.opensdk.listener.LCOpenSDK_TalkerListener;
import com.lechange.opensdk.media.LCOpenSDK_ParamReal;
import com.lechange.opensdk.media.LCOpenSDK_ParamTalk;
import com.lechange.opensdk.media.LCOpenSDK_PlayWindow;
import com.lechange.opensdk.media.LCOpenSDK_Talk;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class DeviceOnlineMediaPlayActivity extends AppCompatActivity implements View.OnClickListener, IGetDeviceInfoCallBack.IDeviceCacheCallBack {
    private static final String TAG = DeviceOnlineMediaPlayActivity.class.getSimpleName();

    private LCOpenSDK_PlayWindow mPlayWin = new LCOpenSDK_PlayWindow();
    private DeviceDetailListData.ResponseData.DeviceListBean deviceListBean;
    private Bundle bundle;
    private FrameLayout frLiveWindow, frLiveWindowContent;
    private TextView tvNoVideo, tvDeviceName, tvCloudVideo, tvLocalVideo, tvLoadingMsg, tvRefuse, tvAnswer;
    private RecyclerView rcvVideoList;
    private LinearLayout llVideoContent, llVideo, llSpeak, llScreenShot, llCloudStage, llFullScreen, llSound, llPlayStyle, llPlayPause, llDetail, llBack, llVideo1, llSpeak1, llScreenShot1, llCloudStage1;
    private ImageView ivPalyPause, ivPlayStyle, ivSound, ivCloudStage, ivScreenShot, ivSpeak, ivVideo, ivCloudStage1, ivScreenShot1, ivSpeak1, ivVideo1;
    private ProgressBar pbLoading;
    private RelativeLayout rlLoading;
    private LcCloudRudderView rudderView;
    private boolean cloudvideo = true;
    private boolean isShwoRudder = false;
    private AudioTalkerListener audioTalkerListener = new AudioTalkerListener();

    private VideoMode videoMode = VideoMode.MODE_HD;
    private SoundStatus soundStatus = SoundStatus.PLAY;
    private SpeakStatus speakStatus = SpeakStatus.STOP;
    private RecordStatus recordStatus = RecordStatus.STOP;
    private PlayStatus playStatus = PlayStatus.ERROR;
    private LinearLayoutManager linearLayoutManager;
    private MediaPlayRecordAdapter mediaPlayRecordAdapter;
    private List<RecordsData> recordsDataList = new ArrayList<>();
    private List<RecordsData> cloudRecordsDataList;
    private List<RecordsData> localRecordsDataList;
    private String cloudRecordsDataTip = "";
    private String localRecordsDataTip = "";
    private DeviceRecordService deviceRecordService = ClassInstanceManager.newInstance().getDeviceRecordService();
    private Direction mPTZPreDirection = null;
    private int mCurrentOrientation;
    private LinearLayout llController;
    private LinearLayout llCallAction;
    private FrameLayout frRecord;
    private RelativeLayout rlTitle;
    private ImageView ivChangeScreen;
    private EncryptKeyInputDialog encryptKeyInputDialog;
    private String encryptKey;
    private boolean supportPTZ;
    private boolean playbackEnabled = false;
    private boolean callBellEvent = false;
    private int imageSize = -1;//视频播放分辨率
    private int bateMode;
    private boolean showHD = true;//显示HD和SD的切换
    private MediaPlayer mediaPlayer;
    private boolean callAnswered = false;
    private String videoPath = null;

    public enum PlayStatus {
        PLAY, PAUSE, ERROR
    }

    public enum LoadStatus {
        LOADING, LOAD_SUCCESS, LOAD_ERROR
    }

    public enum SoundStatus {
        PLAY, STOP, NO_SUPPORT
    }

    public enum SpeakStatus {
        PLAY, STOP, NO_SUPPORT, OPENING
    }

    public enum VideoMode {
        MODE_HD, MODE_SD
    }

    public enum RecordStatus {
        START, STOP
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MediaPlayHelper.initContext(getApplicationContext());
        mCurrentOrientation = Configuration.ORIENTATION_PORTRAIT;
        setContentView(getResources("layout", "activity_device_online_media_play"));
        initView();
        initData();
        operatePTZ();
        checkOverlayPermission();
    }

    private void checkOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true);
            setTurnScreenOn(true);
            KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
            if (keyguardManager != null) {
                keyguardManager.requestDismissKeyguard(this, null);
            }
        } else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean canDrawOverlays = Settings.canDrawOverlays(this);
            if (!canDrawOverlays) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        switchScreenDirection();
    }

    private void operatePTZ() {
        rudderView.setRudderListener(new LcCloudRudderView.RudderListener() {
            @Override
            public void onSteeringWheelChangedBegin() {

            }

            @Override
            public void onSteeringWheelChangedContinue(Direction direction) {
                if (direction == null && mPTZPreDirection != null) {
                    controlPTZ(mPTZPreDirection, 200, true);
                    mPTZPreDirection = null;
                } else if (direction != mPTZPreDirection) {
                    mPTZPreDirection = direction;
                    controlPTZ(mPTZPreDirection, 30000, false);
                }
            }

            @Override
            public void onSteeringWheelChangedSingle(Direction direction) {
                controlPTZ(direction, 200, false);
            }

            @Override
            public void onSteeringWheelChangedEnd() {
            }
        });
    }

    private void controlPTZ(Direction em, long time, boolean stop) {
        String operation = "";
        if (em == Direction.Left) {
            operation = "2";
        } else if (em == Direction.Right) {
            operation = "3";
        } else if (em == Direction.Up) {
            operation = "0";
        } else if (em == Direction.Down) {
            operation = "1";
        }
        if (stop) {
            operation = "10";
        }
        ControlMovePTZData controlMovePTZData = new ControlMovePTZData();
        controlMovePTZData.data.deviceId = deviceListBean.deviceId;
        controlMovePTZData.data.channelId = deviceListBean.channelList.get(deviceListBean.checkedChannel).channelId;
        controlMovePTZData.data.operation = operation;
        controlMovePTZData.data.duration = time;
        deviceRecordService.controlMovePTZ(controlMovePTZData);
    }

    private void initCloudRecord() {
        if ((cloudRecordsDataTip != null && !cloudRecordsDataTip.isEmpty()) || cloudRecordsDataList != null) {
            if (cloudRecordsDataList != null) {
                showRecordList(cloudRecordsDataList);
            } else {
                showRecordListTip(cloudRecordsDataTip);
            }
        } else {
            CloudRecordsData cloudRecordsData = new CloudRecordsData();
            cloudRecordsData.data.deviceId = deviceListBean.deviceId;
            cloudRecordsData.data.channelId = deviceListBean.channelList.get(deviceListBean.checkedChannel).channelId;
            cloudRecordsData.data.beginTime = DateHelper.dateFormat(new Date(System.currentTimeMillis())) + " 00:00:00";
            cloudRecordsData.data.endTime = DateHelper.dateFormat(new Date(System.currentTimeMillis())) + " 23:59:59";
            cloudRecordsData.data.count = 6;
            deviceRecordService.getCloudRecords(cloudRecordsData, new IGetDeviceInfoCallBack.IDeviceCloudRecordCallBack() {
                @Override
                public void deviceCloudRecord(CloudRecordsData.Response result) {
                    List<CloudRecordsData.ResponseData.RecordsBean> cloudRecords = result.data.records;
                    if (cloudRecords != null && cloudRecords.size() > 0) {
                        cloudRecordsDataList = new ArrayList<>();
                        for (CloudRecordsData.ResponseData.RecordsBean recordsBean : cloudRecords) {
                            RecordsData recordsData = RecordsData.parseCloudData(recordsBean);
                            cloudRecordsDataList.add(recordsData);
                        }
                        showRecordList(cloudRecordsDataList);
                    } else {
                        cloudRecordsDataTip = getResources().getString(getResources("string", "dahua_device_more_video"));
                        showRecordListTip(cloudRecordsDataTip);
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    cloudRecordsDataTip = throwable.getMessage();
                    showRecordListTip(cloudRecordsDataTip);
                }
            });
        }
    }

    private void showRecordList(List<RecordsData> list) {
        rcvVideoList.setVisibility(View.VISIBLE);
        tvNoVideo.setVisibility(View.GONE);
        recordsDataList.clear();
        for (RecordsData a : list) {
            recordsDataList.add(a);
        }
        if (mediaPlayRecordAdapter == null) {
            mediaPlayRecordAdapter = new MediaPlayRecordAdapter(recordsDataList, DeviceOnlineMediaPlayActivity.this);
            rcvVideoList.setAdapter(mediaPlayRecordAdapter);
        } else {
            mediaPlayRecordAdapter.notifyDataSetChanged();
        }
        mediaPlayRecordAdapter.setLoadMoreClickListener(new MediaPlayRecordAdapter.LoadMoreClickListener() {
            @Override
            public void loadMore() {
                gotoRecordList();
            }
        });
        mediaPlayRecordAdapter.setOnItemClickListener(new MediaPlayRecordAdapter.OnItemClickListener() {
            @Override
            public void click(int recordType, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(MethodConst.ParamConst.deviceDetail, deviceListBean);
                bundle.putSerializable(MethodConst.ParamConst.recordData, recordsDataList.get(position));
                bundle.putInt(MethodConst.ParamConst.recordType, recordsDataList.get(position).recordType == 0 ? MethodConst.ParamConst.recordTypeCloud : MethodConst.ParamConst.recordTypeLocal);
                Intent intent = new Intent(DeviceOnlineMediaPlayActivity.this, DeviceRecordPlayActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void gotoRecordList() {
        if (cloudvideo) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(MethodConst.ParamConst.deviceDetail, deviceListBean);
            bundle.putInt(MethodConst.ParamConst.recordType, MethodConst.ParamConst.recordTypeCloud);
            Intent intent = new Intent(DeviceOnlineMediaPlayActivity.this, DeviceRecordListActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Bundle bundle = new Bundle();
            bundle.putSerializable(MethodConst.ParamConst.deviceDetail, deviceListBean);
            bundle.putInt(MethodConst.ParamConst.recordType, MethodConst.ParamConst.recordTypeLocal);
            Intent intent = new Intent(DeviceOnlineMediaPlayActivity.this, DeviceRecordListActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    private void showRecordListTip(String txt) {
        rcvVideoList.setVisibility(View.GONE);
        tvNoVideo.setVisibility(View.VISIBLE);
        tvNoVideo.setText(txt);
    }

    private void initLocalRecord() {
        if ((localRecordsDataTip != null && !localRecordsDataTip.isEmpty()) || localRecordsDataList != null) {
            if (localRecordsDataList != null) {
                showRecordList(localRecordsDataList);
            } else {
                showRecordListTip(localRecordsDataTip);
            }
        } else {
            LocalRecordsData localRecordsData = new LocalRecordsData();
            localRecordsData.data.deviceId = deviceListBean.deviceId;
            localRecordsData.data.channelId = deviceListBean.channelList.get(deviceListBean.checkedChannel).channelId;
            localRecordsData.data.beginTime = DateHelper.dateFormat(new Date(System.currentTimeMillis())) + " 00:00:00";
            localRecordsData.data.endTime = DateHelper.dateFormat(new Date(System.currentTimeMillis())) + " 23:59:59";
            localRecordsData.data.type = "All";
            localRecordsData.data.queryRange = "1-6";
            deviceRecordService.queryLocalRecords(localRecordsData, new IGetDeviceInfoCallBack.IDeviceLocalRecordCallBack() {
                @Override
                public void deviceLocalRecord(LocalRecordsData.Response result) {
                    List<LocalRecordsData.ResponseData.RecordsBean> localRecords = result.data.records;
                    if (localRecords != null && localRecords.size() > 0) {
                        localRecordsDataList = new ArrayList<>();
                        for (LocalRecordsData.ResponseData.RecordsBean recordsBean : localRecords) {
                            RecordsData recordsData = RecordsData.parseLocalData(recordsBean);
                            localRecordsDataList.add(recordsData);
                        }
                        showRecordList(localRecordsDataList);
                    } else {
                        localRecordsDataTip = getResources().getString(getResources("string", "dahua_device_more_video"));
                        showRecordListTip(localRecordsDataTip);
                    }
                }

                @Override
                public void onError(Throwable throwable) {
                    localRecordsDataTip = throwable.getMessage();
                    showRecordListTip(localRecordsDataTip);
                }
            });
        }
    }

    private void initView() {
        frLiveWindow = findViewById(getResources("id", "fr_live_window"));
        frLiveWindowContent = findViewById(getResources("id", "fr_live_window_content"));
        tvCloudVideo = findViewById(getResources("id", "tv_cloud_video"));
        tvLocalVideo = findViewById(getResources("id", "tv_local_video"));
        llBack = findViewById(getResources("id", "ll_back"));
        tvDeviceName = findViewById(getResources("id", "tv_device_name"));
        llDetail = findViewById(getResources("id", "ll_detail"));
        llDetail.setVisibility(View.GONE);
        llPlayPause = findViewById(getResources("id", "ll_paly_pause"));
        llPlayStyle = findViewById(getResources("id", "ll_play_style"));
        llSound = findViewById(getResources("id", "ll_sound"));
        llFullScreen = findViewById(getResources("id", "ll_fullscreen"));
        llCloudStage = findViewById(getResources("id", "ll_cloudstage"));
        llScreenShot = findViewById(getResources("id", "ll_screenshot"));
        llSpeak = findViewById(getResources("id", "ll_speak"));
        llVideo = findViewById(getResources("id", "ll_video"));
        llVideoContent = findViewById(getResources("id", "ll_video_content"));
        rcvVideoList = findViewById(getResources("id", "rcv_video_list"));
        tvNoVideo = findViewById(getResources("id", "tv_no_video"));
        tvRefuse = findViewById(getResources("id", "tv_refuse"));
        tvRefuse.setOnClickListener(this);
        tvAnswer = findViewById(getResources("id", "tv_answer"));
        tvAnswer.setOnClickListener(this);
        rudderView = findViewById(getResources("id", "rudder"));
        ivPalyPause = findViewById(getResources("id", "iv_paly_pause"));
        ivPlayStyle = findViewById(getResources("id", "iv_play_style"));
        ivSound = findViewById(getResources("id", "iv_sound"));
        ivCloudStage = findViewById(getResources("id", "iv_cloudStage"));
        ivScreenShot = findViewById(getResources("id", "iv_screen_shot"));
        ivSpeak = findViewById(getResources("id", "iv_speak"));
        ivVideo = findViewById(getResources("id", "iv_video"));

        llCloudStage1 = findViewById(getResources("id", "ll_cloudstage1"));
        llScreenShot1 = findViewById(getResources("id", "ll_screenshot1"));
        llSpeak1 = findViewById(getResources("id", "ll_speak1"));
        llVideo1 = findViewById(getResources("id", "ll_video1"));
        ivCloudStage1 = findViewById(getResources("id", "iv_cloudStage1"));
        ivScreenShot1 = findViewById(getResources("id", "iv_screen_shot1"));
        ivSpeak1 = findViewById(getResources("id", "iv_speak1"));
        ivVideo1 = findViewById(getResources("id", "iv_video1"));

        rlLoading = findViewById(getResources("id", "rl_loading"));
        pbLoading = findViewById(getResources("id", "pb_loading"));
        tvLoadingMsg = findViewById(getResources("id", "tv_loading_msg"));
        llController = findViewById(getResources("id", "ll_controller"));
        llCallAction = findViewById(getResources("id", "ll_action_bar_bottom"));
        frRecord = findViewById(getResources("id", "fr_record"));
        rlTitle = findViewById(getResources("id", "rl_title"));
        ivChangeScreen = findViewById(getResources("id", "iv_change_screen"));
        linearLayoutManager = new LinearLayoutManager(DeviceOnlineMediaPlayActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcvVideoList.setLayoutManager(linearLayoutManager);
        initCommonClickListener();
        switchScreenDirection();
        mPlayWin.initPlayWindow(this, frLiveWindowContent, 0, false);
        setWindowListener(mPlayWin);
        mPlayWin.openTouchListener();
    }

    private void switchScreenDirection() {
        if (mCurrentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            RelativeLayout.LayoutParams mLayoutParams = new RelativeLayout.LayoutParams(frLiveWindow.getLayoutParams());
            DisplayMetrics metric = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metric);
            mLayoutParams.width = metric.widthPixels;
            mLayoutParams.height = metric.widthPixels * 9 / 16;
            mLayoutParams.setMargins(0, 0, 0, 0);
            mLayoutParams.addRule(RelativeLayout.BELOW, getResources("id", "rl_title"));
            frLiveWindow.setLayoutParams(mLayoutParams);
            MediaPlayHelper.quitFullScreen(DeviceOnlineMediaPlayActivity.this);
            llController.setVisibility(View.VISIBLE);
            llCallAction.setVisibility(View.GONE);
            rlTitle.setVisibility(View.VISIBLE);
            llSpeak1.setVisibility(View.GONE);
            llCloudStage1.setVisibility(View.GONE);
            llVideo1.setVisibility(View.GONE);
            llScreenShot1.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(frRecord
                    .getLayoutParams());
            layoutParams.addRule(RelativeLayout.BELOW, getResources("id", "ll_controller"));
            frRecord.setLayoutParams(layoutParams);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(rudderView.getLayoutParams());
            layoutParams3.gravity = Gravity.CENTER;
            rudderView.setLayoutParams(layoutParams3);
            switchCloudRudder(false);
            if (callBellEvent) {
                llVideoContent.setVisibility(View.GONE);
                llController.setVisibility(View.GONE);
                llCallAction.setVisibility(View.VISIBLE);
            }
        } else if (mCurrentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            DisplayMetrics metric = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metric);
            RelativeLayout.LayoutParams mLayoutParams = new RelativeLayout.LayoutParams(metric.widthPixels, metric.heightPixels);
            mLayoutParams.setMargins(0, 0, 0, 0);
            mLayoutParams.removeRule(RelativeLayout.BELOW);
            frLiveWindow.setLayoutParams(mLayoutParams);
            MediaPlayHelper.setFullScreen(DeviceOnlineMediaPlayActivity.this);
            llController.setVisibility(View.GONE);
            llCallAction.setVisibility(View.GONE);
            rlTitle.setVisibility(View.GONE);
            llSpeak1.setVisibility(View.VISIBLE);
            llCloudStage1.setVisibility(View.VISIBLE);
            llVideo1.setVisibility(View.VISIBLE);
            llScreenShot1.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(frRecord
                    .getLayoutParams());
            layoutParams.removeRule(RelativeLayout.BELOW);
            frRecord.setLayoutParams(layoutParams);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(rudderView.getLayoutParams());
            layoutParams3.gravity = Gravity.CENTER_VERTICAL;
            rudderView.setLayoutParams(layoutParams3);
            switchCloudRudder(false);
        }
    }

    private void initData() {
        bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }
        requestPermission();
        deviceListBean = (DeviceDetailListData.ResponseData.DeviceListBean) bundle.getSerializable(MethodConst.ParamConst.deviceDetail);
        playbackEnabled = bundle.getBoolean("playbackEnabled", false);
        callBellEvent = bundle.getBoolean("callBellEvent", false);
        switchVideoList(true);
        // getDeviceLocalCache();
        tvDeviceName.setText(deviceListBean.channelList.get(deviceListBean.checkedChannel).channelName);
        if (playbackEnabled) {
            llVideoContent.setVisibility(View.VISIBLE);
        } else {
            llVideoContent.setVisibility(View.GONE);
        }
        if (callBellEvent) {
            llVideoContent.setVisibility(View.GONE);
            llController.setVisibility(View.GONE);
            llCallAction.setVisibility(View.VISIBLE);
            tvAnswer.setEnabled(false);
            tvAnswer.setAlpha(0.5f);
            try {
                int resID = getResources("raw", "doorbell");
                mediaPlayer = MediaPlayer.create(getApplicationContext(), resID);
                mediaPlayer.setLooping(true);
                mediaPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    if (!callAnswered) {
                        runOnUiThread(() -> tvRefuse.performClick());
                    }
                }
            }, 60 * 1000);
        }
        if (deviceListBean.channelList.get(deviceListBean.checkedChannel).resolutions != null && deviceListBean.channelList.get(deviceListBean.checkedChannel).resolutions.size() > 0) {
            showHD = false;
            bateMode = deviceListBean.channelList.get(deviceListBean.checkedChannel).resolutions.get(0).streamType;
            imageSize = deviceListBean.channelList.get(deviceListBean.checkedChannel).resolutions.get(0).imageSize;
        } else {
            showHD = true;
        }
    }

    public void requestPermission() {
        boolean isMinSDKM = Build.VERSION.SDK_INT < 23;
        boolean isGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        if (isMinSDKM || isGranted) {
            return;
        }
        requestRecordAudioPermission();
    }

    private void requestRecordAudioPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void getDeviceLocalCache() {
        DeviceLocalCacheData deviceLocalCacheData = new DeviceLocalCacheData();
        deviceLocalCacheData.setDeviceId(deviceListBean.deviceId);
        if (deviceListBean.channelList != null && deviceListBean.channelList.size() > 0) {
            deviceLocalCacheData.setChannelId(deviceListBean.channelList.get(deviceListBean.checkedChannel).channelId);
        }
        DeviceLocalCacheService deviceLocalCacheService = ClassInstanceManager.newInstance().getDeviceLocalCacheService();
        deviceLocalCacheService.findLocalCache(deviceLocalCacheData, this);
    }

    @Override
    public void deviceCache(DeviceLocalCacheData deviceLocalCacheData) {
        BitmapDrawable bitmapDrawable = MediaPlayHelper.picDrawable(deviceLocalCacheData.getPicPath());
        if (bitmapDrawable != null) {
            rlLoading.setBackground(bitmapDrawable);
        }
    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        loadingStatus(LoadStatus.LOADING, getResources().getString(getResources("string", "dahua_device_video_play_loading")), deviceListBean.deviceId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stop();
        recordStatus = RecordStatus.STOP;
    }

    private void initAbility(boolean loadSuccess) {
        String deviceAbility = deviceListBean.deviceAbility;
        String channelAbility = deviceListBean.channelList.get(deviceListBean.checkedChannel).channelAbility;
        supportPTZ = (deviceListBean.checkedChannel == 0 ? (deviceListBean.channelList.size() > 1 ? channelAbility.contains("PT") : deviceAbility.contains("PT")) : channelAbility.contains("PT")) && loadSuccess;
        cloudStageClickListener(supportPTZ);
        speakClickListener((channelAbility.contains("AudioTalkV1") || deviceAbility.contains("AudioTalk")) && loadSuccess);
    }

    private void switchVideoList(boolean b) {
        this.cloudvideo = b;
        tvCloudVideo.setSelected(cloudvideo);
        tvLocalVideo.setSelected(!cloudvideo);
        if (cloudvideo) {
            initCloudRecord();
        } else {
            initLocalRecord();
        }
    }

    private void initCommonClickListener() {
        llBack.setOnClickListener(this);
        llDetail.setOnClickListener(this);
        tvCloudVideo.setOnClickListener(this);
        tvLocalVideo.setOnClickListener(this);
        tvNoVideo.setOnClickListener(this);
        llFullScreen.setOnClickListener(this);
    }

    private void featuresClickListener(boolean loadSuccess) {
        llPlayPause.setOnClickListener(loadSuccess ? this : null);
        llPlayStyle.setOnClickListener(loadSuccess ? this : null);

        llScreenShot.setOnClickListener(loadSuccess ? this : null);
        llScreenShot1.setOnClickListener(loadSuccess ? this : null);
        llVideo.setOnClickListener(loadSuccess ? this : null);
        llVideo1.setOnClickListener(loadSuccess ? this : null);
        llSound.setOnClickListener(loadSuccess ? this : null);
        ivPalyPause.setImageDrawable(loadSuccess ? getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_pause")) : getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_pause_disable")));
        ivPlayStyle.setImageDrawable(videoMode == VideoMode.MODE_HD ?
                (loadSuccess ? getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_hd")) : getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_hd_disable"))) :
                (loadSuccess ? getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_sd")) : getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_hd_disable"))));
        ivScreenShot.setImageDrawable(loadSuccess
                ? getDrawable(getResources("drawable", "lc_demo_photo_capture_selector"))
                : getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_screenshot_disable")));
        ivVideo.setImageDrawable(loadSuccess
                ? getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_video"))
                : getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_video_disable")));

        ivScreenShot1.setImageDrawable(loadSuccess
                ? getDrawable(getResources("mipmap", "live_video_icon_h_screenshot"))
                : getDrawable(getResources("mipmap", "live_video_icon_h_screenshot_disable")));
        ivVideo1.setImageDrawable(loadSuccess
                ? getDrawable(getResources("mipmap", "live_video_icon_h_video_off"))
                : getDrawable(getResources("mipmap", "live_video_icon_h_video_off_disable")));
        ivSound.setImageDrawable(loadSuccess ? getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_sound_off")) : getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_sound_off_disable")));
        if (soundStatus != SoundStatus.PLAY) {
            return;
        }
        if (openAudio()) {
            soundStatus = SoundStatus.PLAY;
            ivSound.setImageDrawable(getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_sound_on")));
        }
    }

    private void cloudStageClickListener(boolean isSupport) {
        llCloudStage.setOnClickListener(isSupport ? this : null);
        ivCloudStage.setImageDrawable(isSupport
                ? getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_cloudstage"))
                : getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_cloudstage_disable")));
        llCloudStage1.setOnClickListener(isSupport ? this : null);
        ivCloudStage1.setImageDrawable(isSupport
                ? getDrawable(getResources("mipmap", "live_video_icon_h_cloudterrace_off"))
                : getDrawable(getResources("mipmap", "live_video_icon_h_cloudterrace_off_disable")));
    }

    private void speakClickListener(boolean isSupport) {
        ivSpeak.setOnClickListener(isSupport ? this : null);
        ivSpeak.setImageDrawable(isSupport
                ? getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_speak"))
                : getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_speak_disable")));
        ivSpeak1.setOnClickListener(isSupport ? this : null);
        ivSpeak1.setImageDrawable(isSupport
                ? getDrawable(getResources("mipmap", "live_video_icon_h_talk_off"))
                : getDrawable(getResources("mipmap", "live_video_icon_h_talk_off_disable")));
    }

    private void setWindowListener(LCOpenSDK_PlayWindow playWin) {
        playWin.setWindowListener(new LCOpenSDK_EventListener() {
            @Override
            public void onZoomBegin(int index) {
                super.onZoomBegin(index);
                Log.d(TAG, "onZoomBegin: index= " + index);
            }

            @Override
            public void onZooming(int index, float dScale) {
                super.onZooming(index, dScale);
                Log.d(TAG, "onZooming: index= " + index + " , dScale= " + dScale);
                mPlayWin.doScale(dScale);
            }

            @Override
            public void onZoomEnd(int index, ZoomType zoomType) {
                super.onZoomEnd(index, zoomType);
                Log.d(TAG, "onZoomEnd: index= " + index + " , zoomType= " + zoomType);
            }

            @Override
            public void onControlClick(int index, float dx, float dy) {
                super.onControlClick(index, dx, dy);
                Log.d(TAG, "onControlClick: index= " + index + " , dx= " + dx + " , dy= " + dy);
            }

            @Override
            public void onWindowDBClick(int index, float dx, float dy) {
                super.onWindowDBClick(index, dx, dy);
                Log.d(TAG, "onWindowDBClick: index= " + index + " , dx= " + dx + " , dy= " + dy);
            }

            @Override
            public boolean onSlipBegin(int index, Direction direction, float dx, float dy) {
                Log.d(TAG, "onSlipBegin: index= " + index + " , direction= " + direction + " , dx= " + dx + " , dy= " + dy);
                return super.onSlipBegin(index, direction, dx, dy);
            }

            @Override
            public void onSlipping(int index, Direction direction, float prex, float prey, float dx, float dy) {
                super.onSlipping(index, direction, prex, prey, dx, dy);
                Log.d(TAG, "onSlipping: index= " + index + " , direction= " + direction + " , prex= " + prex + " , prey= " + prey + " , dx= " + dx + " , dy= " + dy);
            }

            @Override
            public void onSlipEnd(int index, Direction direction, float dx, float dy) {
                super.onSlipEnd(index, direction, dx, dy);
                Log.d(TAG, "onSlipEnd: index= " + index + " , direction= " + direction + " , dx= " + dx + " , dy= " + dy);
            }

            @Override
            public void onWindowLongPressBegin(int index, Direction direction, float dx, float dy) {
                super.onWindowLongPressBegin(index, direction, dx, dy);
                Log.d(TAG, "onWindowLongPressBegin: index= " + index + " , direction= " + direction + " , dx= " + dx + " , dy= " + dy);
            }

            @Override
            public void onWindowLongPressEnd(int index) {
                super.onWindowLongPressEnd(index);
                Log.d(TAG, "onWindowLongPressEnd: index= " + index);
            }

            @Override
            public void onPlayerResult(int index, String code, int resultSource) {
                super.onPlayerResult(index, code, resultSource);
                Log.d(TAG, "onPlayerResult: index= " + index + " , code= " + code + " , resultSource= " + resultSource);
                boolean failed = false;
                if (resultSource == 99) {
                    failed = true;
                } else {
                    if (callBellEvent) {
                        closeAudio();
                    }
                    if (resultSource == 5 && (!(code.equals("1000") || code.equals("0")))) {
                        failed = true;
                        if (code.equals("1000005")) {
                            inputEncryptKey();
                        }
                    } else if (resultSource == 0 && (code.equals("0") || code.equals("1") || code.equals("3") || code.equals("7"))) {
                        failed = true;
                        if (code.equals("7")) {
                            inputEncryptKey();
                        }
                    }
                }
                if (failed) {
                    loadingStatus(LoadStatus.LOAD_ERROR, getResources().getString(getResources("string", "dahua_device_video_play_error")) + ":" + code + "." + resultSource, "");
                    playStatus = PlayStatus.ERROR;
                }
            }

            @Override
            public void onResolutionChanged(int index, int width, int height) {
                super.onResolutionChanged(index, width, height);
                Log.d(TAG, "onResolutionChanged: index= " + index + " , width= " + width + " , height= " + height);
            }

            @Override
            public void onPlayBegan(int index) {
                super.onPlayBegan(index);
                Log.d(TAG, "onPlayBegan: index= " + index);
                loadingStatus(LoadStatus.LOAD_SUCCESS, "", "");
                playStatus = PlayStatus.PLAY;
                if (callBellEvent) {
                    runOnUiThread(() -> tvAnswer.setEnabled(true));
                    runOnUiThread(() -> tvAnswer.setAlpha(1f));
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            closeAudio();
                        }
                    }, 2000);
                }
            }

            @Override
            public void onReceiveData(int index, int len) {
                super.onReceiveData(index, len);
                Log.d(TAG, "onReceiveData: index= " + index + " , len= " + len);
            }

            @Override
            public void onStreamCallback(int index, byte[] bytes, int len) {
                super.onStreamCallback(index, bytes, len);
                Log.d(TAG, "onStreamCallback: index= " + index + " , len= " + len);
            }

            @Override
            public void onPlayFinished(int index) {
                super.onPlayFinished(index);
                Log.d(TAG, "onPlayFinished: index= " + index);
            }

            @Override
            public void onPlayerTime(int index, long time) {
                super.onPlayerTime(index, time);
                Log.d(TAG, "onPlayerTime: index= " + index + " , time= " + time);
            }
        });
    }

    private void inputEncryptKey() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (encryptKeyInputDialog == null) {
                    encryptKeyInputDialog = new EncryptKeyInputDialog(DeviceOnlineMediaPlayActivity.this);
                }
                encryptKeyInputDialog.show();
                encryptKeyInputDialog.setOnClick(new EncryptKeyInputDialog.OnClick() {
                    @Override
                    public void onSure(String txt) {
                        encryptKey = txt;
                        loadingStatus(LoadStatus.LOADING, getResources().getString(getResources("string", "dahua_device_video_play_change")), txt);
                    }
                });
            }
        });
    }

    private void loadingStatus(final LoadStatus loadStatus, final String msg, final String psk) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (loadStatus == LoadStatus.LOADING) {
                    stop();
                    play(psk);
                    rlLoading.setVisibility(View.VISIBLE);
                    pbLoading.setVisibility(View.VISIBLE);
                    tvLoadingMsg.setText(msg);
                } else if (loadStatus == LoadStatus.LOAD_SUCCESS) {
                    rlLoading.setVisibility(View.GONE);
                    rudderView.enable(true);
                    initAbility(true);
                    featuresClickListener(true);
                } else {
                    stop();
                    rlLoading.setVisibility(View.VISIBLE);
                    pbLoading.setVisibility(View.GONE);
                    tvLoadingMsg.setText(msg);
                    initAbility(false);
                    featuresClickListener(false);
                }
            }
        });
    }

    public void play(String psk) {
        if (showHD) {
            bateMode = videoMode == VideoMode.MODE_HD ? 0 : 1;
        }
        boolean isOpenAudio = !callBellEvent;
        LCOpenSDK_ParamReal paramReal = new LCOpenSDK_ParamReal(
                LCDeviceEngine.newInstance().accessToken,
                deviceListBean.deviceId,
                Integer.parseInt(deviceListBean.channelList.get(deviceListBean.checkedChannel).channelId),
                psk,
                deviceListBean.playToken,
                bateMode,
                true,
                isOpenAudio,
                imageSize,
                deviceListBean.productId
        );
        mPlayWin.playRtspReal(paramReal);
    }

    public void stop() {
        captureLastPic();
        stopRecord();
        closeAudio();
        stopTalk();
        rudderView.enable(false);
        mPlayWin.stopRtspReal(true);
    }

    private void captureLastPic() {
        if (playStatus == PlayStatus.ERROR) {
            return;
        }
        String capturePath;
        try {
            capturePath = capture(false);
        } catch (Throwable e) {
            capturePath = null;
        }
        if (capturePath == null) {
            return;
        }
        DeviceLocalCacheService deviceLocalCacheService = ClassInstanceManager.newInstance().getDeviceLocalCacheService();
        DeviceLocalCacheData deviceLocalCacheData = new DeviceLocalCacheData();
        deviceLocalCacheData.setPicPath(capturePath);
        deviceLocalCacheData.setDeviceName(deviceListBean.deviceName);
        deviceLocalCacheData.setDeviceId(deviceListBean.deviceId);
        if (deviceListBean.channelList != null && deviceListBean.channelList.size() > 0) {
            deviceLocalCacheData.setChannelId(deviceListBean.channelList.get(deviceListBean.checkedChannel).channelId);
            deviceLocalCacheData.setChannelName(deviceListBean.channelList.get(deviceListBean.checkedChannel).channelName);
        }
        deviceLocalCacheService.addLocalCache(deviceLocalCacheData);
    }

    public boolean startRecord() {
        String channelName = null;
        if (deviceListBean.channelList != null && deviceListBean.channelList.size() > 0) {
            channelName = deviceListBean.channelList.get(deviceListBean.checkedChannel).channelName;
        } else {
            channelName = deviceListBean.deviceName;
        }
        channelName = channelName.replace("-", "");
        videoPath = MediaPlayHelper.getCaptureAndVideoPath(MediaPlayHelper.DHFilesType.DHVideo, channelName);
        MediaScannerConnection.scanFile(this, new String[]{videoPath}, null, null);
        int ret = mPlayWin.startRecord(videoPath, 1, 0x7FFFFFFF);
        return ret == 0;
    }

    public boolean stopRecord() {
        return mPlayWin.stopRecord() == 0;
    }

    public String capture(boolean notify) {
        String captureFilePath = null;
        String channelName = null;
        if (deviceListBean.channelList != null && deviceListBean.channelList.size() > 0) {
            channelName = deviceListBean.channelList.get(deviceListBean.checkedChannel).channelName;
        } else {
            channelName = deviceListBean.deviceName;
        }
        channelName = channelName.replace("-", "");
        captureFilePath = MediaPlayHelper.getCaptureAndVideoPath(notify ? MediaPlayHelper.DHFilesType.DHImage : MediaPlayHelper.DHFilesType.DHImageCache, channelName);
        int ret = mPlayWin.snapShot(captureFilePath);
        if (ret == 0) {
            if (notify) {
                MediaPlayHelper.updatePhotoAlbum(captureFilePath);
            }
        } else {
            captureFilePath = null;
        }
        return captureFilePath;
    }

    public boolean openAudio() {
        return mPlayWin.playAudio() == 0;
    }

    public boolean closeAudio() {
        return mPlayWin.stopAudio() == 0;
    }

    public void startTalk() {
        closeAudio();
        soundStatus = SoundStatus.STOP;
        speakStatus = SpeakStatus.OPENING;
        ivSound.setImageDrawable(getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_sound_off")));
        LCOpenSDK_Talk.setListener(audioTalkerListener);
        int channelId = -1;

        if (Integer.parseInt(deviceListBean.channelNum) > 1) {
            channelId = Integer.parseInt(deviceListBean.channelList.get(deviceListBean.checkedChannel).channelId);
            Log.d(TAG, "deviceCatalog = " + deviceListBean.catalog + ", routing device talk...");
        }
        Log.d(TAG, "playTalk, channelId = " + channelId);
        LCOpenSDK_ParamTalk paramTalk = new LCOpenSDK_ParamTalk(
                LCDeviceEngine.newInstance().accessToken,
                deviceListBean.deviceId,
                channelId,
                TextUtils.isEmpty(encryptKey) ? deviceListBean.deviceId : encryptKey,
                deviceListBean.playToken,
                true,
                "talk",
                "",
                false
        );
        LCOpenSDK_Talk.playTalk(paramTalk);
    }

    public void stopTalk() {
        speakStatus = SpeakStatus.STOP;
        ivSpeak.setImageDrawable(getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_speak")));
        ivSpeak1.setImageDrawable(getDrawable(getResources("mipmap", "live_video_icon_h_talk_off")));
        LCOpenSDK_Talk.stopTalk();
        LCOpenSDK_Talk.setListener(null);
    }

    class AudioTalkerListener extends LCOpenSDK_TalkerListener {
        public AudioTalkerListener() {
            super();
        }

        @Override
        public void onTalkResult(String error, int type) {
            super.onTalkResult(error, type);
            boolean talkResult = false;
            if (type == 99 || error.equals("-1000") || error.equals("0") || error.equals("1") || error.equals("3")) {
                talkResult = false;
            } else if (error.equals("4")) {
                talkResult = true;
            }
            final boolean finalTalkResult = talkResult;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!finalTalkResult) {
                        stopTalk();
                        Toast.makeText(DeviceOnlineMediaPlayActivity.this, getResources().getString(getResources("string", "dahua_device_talk_open_failed")), Toast.LENGTH_SHORT).show();
                        speakStatus = SpeakStatus.STOP;
                        ivSpeak.setImageDrawable(getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_speak")));
                        ivSpeak1.setImageDrawable(getDrawable(getResources("mipmap", "live_video_icon_h_talk_off")));
                    } else {
                        Toast.makeText(DeviceOnlineMediaPlayActivity.this, getResources().getString(getResources("string", "dahua_device_talk_open_success")), Toast.LENGTH_SHORT).show();
                        speakStatus = SpeakStatus.PLAY;
                        ivSpeak.setImageDrawable(getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_speak_ing")));
                        ivSpeak1.setImageDrawable(getDrawable(getResources("mipmap", "live_video_icon_h_talk_on")));
                    }
                }
            });
        }

        @Override
        public void onTalkPlayReady() {
            super.onTalkPlayReady();
        }

        @Override
        public void onAudioRecord(byte[] bytes, int i, int i1, int i2, int i3) {
            super.onAudioRecord(bytes, i, i1, i2, i3);
        }

        @Override
        public void onAudioReceive(byte[] bytes, int i, int i1, int i2, int i3) {
            super.onAudioReceive(bytes, i, i1, i2, i3);
        }

        @Override
        public void onDataLength(int i) {
            super.onDataLength(i);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == getResources("id", "ll_back")) {
            setResult(0);
            finish();
        } else if (id == getResources("id", "ll_fullscreen")) {
            if (mCurrentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                mCurrentOrientation = Configuration.ORIENTATION_LANDSCAPE;
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                mCurrentOrientation = Configuration.ORIENTATION_PORTRAIT;
            }
            ivChangeScreen.setImageDrawable(mCurrentOrientation == Configuration.ORIENTATION_LANDSCAPE ?
                    getResources().getDrawable(getResources("mipmap", "live_btn_smallscreen")) :
                    getResources().getDrawable(getResources("mipmap", "video_fullscreen")));
        } else if (id == getResources("id", "ll_detail")) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(MethodConst.ParamConst.deviceDetail, deviceListBean);
            Intent intent = new Intent(DeviceOnlineMediaPlayActivity.this, DeviceDetailActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, 0);
        } else if (id == getResources("id", "ll_paly_pause")) {
            if (playStatus == PlayStatus.PLAY) {
                stop();
                initAbility(false);
                featuresClickListener(false);
                llPlayPause.setOnClickListener(this);
            } else {
                getDeviceLocalCache();
                loadingStatus(LoadStatus.LOADING, getResources().getString(getResources("string", "dahua_device_loading")), TextUtils.isEmpty(encryptKey) ? deviceListBean.deviceId : encryptKey);
            }
            playStatus = (playStatus == PlayStatus.PLAY) ? PlayStatus.PAUSE : PlayStatus.PLAY;
            ivPalyPause.setImageDrawable(playStatus == PlayStatus.PLAY ? getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_pause")) : getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_play")));
        } else if (id == getResources("id", "ll_play_style")) {
            videoMode = (videoMode == VideoMode.MODE_HD) ? VideoMode.MODE_SD : VideoMode.MODE_HD;
            loadingStatus(LoadStatus.LOADING, getResources().getString(getResources("string", "dahua_device_video_play_change")), TextUtils.isEmpty(encryptKey) ? deviceListBean.deviceId : encryptKey);
            ivPlayStyle.setImageDrawable(videoMode == VideoMode.MODE_HD ? getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_hd")) : getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_sd")));
        } else if (id == getResources("id", "ll_sound")) {
            if (soundStatus == SoundStatus.NO_SUPPORT) {
                return;
            }
            boolean result = false;
            if (soundStatus == SoundStatus.PLAY) {
                result = closeAudio();
            } else {
                result = openAudio();
            }
            if (!result) {
                return;
            }
            soundStatus = (soundStatus == SoundStatus.PLAY) ? SoundStatus.STOP : SoundStatus.PLAY;
            ivSound.setImageDrawable(soundStatus == SoundStatus.PLAY ? getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_sound_on")) : getDrawable(getResources("mipmap", "lc_demo_live_video_icon_h_sound_off")));
        } else if (id == getResources("id", "ll_cloudstage") || id == getResources("id", "ll_cloudstage1")) {
            switchCloudRudder(!isShwoRudder);
        } else if (id == getResources("id", "ll_screenshot") || id == getResources("id", "ll_screenshot1")) {
            if (capture(true) != null) {
                Toast.makeText(this, getResources().getString(getResources("string", "dahua_device_capture_success")), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getResources().getString(getResources("string", "dahua_device_capture_failed")), Toast.LENGTH_SHORT).show();
            }
        } else if (id == getResources("id", "iv_speak") || id == getResources("id", "iv_speak1")) {
            if (speakStatus == SpeakStatus.NO_SUPPORT || speakStatus == SpeakStatus.OPENING) {
                return;
            }
            if (speakStatus == SpeakStatus.STOP) {
                startTalk();
            } else {
                stopTalk();
            }
        } else if (id == getResources("id", "ll_video") || id == getResources("id", "ll_video1")) {
            if (recordStatus == RecordStatus.STOP) {
                if (startRecord()) {
                    Toast.makeText(this, getResources().getString(getResources("string", "dahua_device_record_started")), Toast.LENGTH_SHORT).show();
                } else {
                    return;
                }
            } else {
                if (stopRecord()) {
                    Toast.makeText(this, getResources().getString(getResources("string", "dahua_device_record_stopped")), Toast.LENGTH_SHORT).show();
                    MediaPlayHelper.updatePhotoVideo(videoPath);
                } else {
                    return;
                }
            }
            recordStatus = (recordStatus == RecordStatus.START) ? RecordStatus.STOP : RecordStatus.START;
            ivVideo.setImageDrawable(recordStatus == RecordStatus.START
                    ? getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_video_ing"))
                    : getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_video")));
            ivVideo1.setImageDrawable(recordStatus == RecordStatus.START
                    ? getDrawable(getResources("mipmap", "live_video_icon_h_video_on"))
                    : getDrawable(getResources("mipmap", "live_video_icon_h_video_off")));
        } else if (id == getResources("id", "tv_cloud_video")) {
            switchVideoList(true);
        } else if (id == getResources("id", "tv_local_video")) {
            switchVideoList(false);
        } else if (id == getResources("id", "tv_no_video")) {
            gotoRecordList();
        } else if (id == getResources("id", "tv_refuse")) {
            // refuse/hangup call
            if (callAnswered) {
                new Thread(() -> {
                    try {
                        String token = LCDeviceEngine.newInstance().accessToken;
                        String deviceId = deviceListBean.deviceId;
                        JsonObject result = OpenApiManager.doorbellCallHangUp(token, deviceId);
                        Log.d(TAG, result.toString());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }).start();
            } else {
                new Thread(() -> {
                    try {
                        String token = LCDeviceEngine.newInstance().accessToken;
                        String deviceId = deviceListBean.deviceId;
                        JsonObject result = OpenApiManager.doorbellCallRefuse(token, deviceId);
                        Log.d(TAG, result.toString());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }).start();
            }
            mediaPlayer.stop();
            setResult(0);
            finish();
        } else if (id == getResources("id", "tv_answer")) {
            // answer call
            new Thread(() -> {
                try {
                    String token = LCDeviceEngine.newInstance().accessToken;
                    String deviceId = deviceListBean.deviceId;
                    JsonObject result = OpenApiManager.doorbellCallAnswer(token, deviceId);
                    callAnswered = true;
                    Log.d(TAG, result.toString());
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }).start();
            mediaPlayer.stop();
            tvAnswer.setVisibility(View.GONE);
            tvRefuse.setText("End call");
            startTalk();
        }
    }

    private void switchCloudRudder(boolean isShow) {
        this.isShwoRudder = isShow;
        if (isShow) {
            ivCloudStage.setImageDrawable(getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_cloudstage_click")));
            ivCloudStage1.setImageDrawable(getDrawable(getResources("mipmap", "live_video_icon_h_cloudterrace_on")));
            llVideoContent.setVisibility(View.GONE);
            rudderView.setVisibility(View.VISIBLE);
        } else {
            ivCloudStage.setImageDrawable(supportPTZ
                    ? getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_cloudstage"))
                    : getDrawable(getResources("mipmap", "lc_demo_livepreview_icon_cloudstage_disable")));
            ivCloudStage1.setImageDrawable(supportPTZ
                    ? getDrawable(getResources("mipmap", "live_video_icon_h_cloudterrace_off"))
                    : getDrawable(getResources("mipmap", "live_video_icon_h_cloudterrace_off_disable")));
            if (playbackEnabled) {
                llVideoContent.setVisibility(mCurrentOrientation == Configuration.ORIENTATION_PORTRAIT ? View.VISIBLE : View.GONE);
            } else {
                llVideoContent.setVisibility(View.GONE);
            }
            rudderView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayWin.uninitPlayWindow();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && data != null) {
            boolean unBind = data.getBooleanExtra(DeviceConstant.IntentKey.DHDEVICE_UNBIND, false);
            if (unBind) {
                setResult(0);
                finish();
            }
        }
        if (resultCode == 100 && data != null) {
            String name = data.getStringExtra(DeviceConstant.IntentKey.DHDEVICE_NEW_NAME);
            tvDeviceName.setText(name);
            deviceListBean.channelList.get(deviceListBean.checkedChannel).channelName = name;
        }
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
