package cordova.plugin.dahuasdk;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.company.NetSDK.CB_fSDKLogCallBack;
import com.company.NetSDK.DEVICE_NET_INFO_EX;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SearchDeviceManager {
    private static final String TAG = "SearchDeviceService";

    private static volatile SearchDeviceManager sInstance;
    private volatile ConcurrentHashMap<String, DeviceNetInfo> mDeviceNetInfos = new ConcurrentHashMap<>();
    private SearchDeviceService.SearchDeviceBinder searchDevice;
    private ISearchDeviceListener mListener;
    boolean mIsConnected;
    private LogCallBack mLogCallBack;
    private Context context;

    private SearchDeviceManager() {
        mListener = new SearchDeviceImpl();
        mDeviceNetInfos = new ConcurrentHashMap<>();
        mLogCallBack = new LogCallBack();
    }

    public static SearchDeviceManager getInstance() {
        if (sInstance == null) {
            synchronized (SearchDeviceManager.class) {
                if (sInstance == null) {
                    sInstance = new SearchDeviceManager();
                }
            }
        }
        return sInstance;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public synchronized void connnectService() {
        if (!mIsConnected) {
            Intent intent = new Intent(context, SearchDeviceService.class);
            mIsConnected = context.bindService(intent, mBinderPoolConnection, Context.BIND_AUTO_CREATE);
        }
        if (!mIsExist) mIsExist = true;
    }

    private ServiceConnection mBinderPoolConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            Log.d(TAG, "onServiceConnected");
            searchDevice = (SearchDeviceService.SearchDeviceBinder) arg1;
            if (searchDevice != null) {
                try {
                    searchDevice.linkToDeath(mBinderPoolDeathRecipient, 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            registerListener(mListener);
            startSearch();
        }
    };

    private IBinder.DeathRecipient mBinderPoolDeathRecipient = new IBinder.DeathRecipient() {
        @Override
        public void binderDied() {
            Log.d(TAG, "binderDied");
            if (searchDevice != null) {
                searchDevice.unlinkToDeath(mBinderPoolDeathRecipient, 0);
                searchDevice = null;
            }
            connnectService();
        }
    };

    public void registerListener(ISearchDeviceListener listener) {
        if (searchDevice != null) {
            searchDevice.registerListener(listener);
        }
    }

    public void unRegisterListener(ISearchDeviceListener listener) {
        if (searchDevice != null) {
            searchDevice.unRegisterListener(listener);
        }
    }

    private void stopSearch() {
        if (searchDevice != null) {
            searchDevice.stopSearchDevicesAsync();
            if (mIsConnected) {
                context.unbindService(mBinderPoolConnection);
                mIsConnected = false;
            }

        }
    }

    public synchronized DEVICE_NET_INFO_EX getDeviceNetInfo(String snCode) {
        if (TextUtils.isEmpty(snCode))
            return null;
        if (mDeviceNetInfos != null && mDeviceNetInfos.get(snCode) != null) {
            return mDeviceNetInfos.get(snCode).getDevNetInfoEx();
        }
        return null;
    }

    public synchronized void startSearch() {
        removeInvalidDevice();
        if (searchDevice != null) {
            connnectService();
            searchDevice.startSearchDevices();
        }
    }

    public synchronized void clearDevice() {
        if (mDeviceNetInfos != null) {
            mDeviceNetInfos.clear();
            Log.d(TAG, "clear");
        }
    }

    public synchronized void removeInvalidDevice() {
        if (mDeviceNetInfos != null) {
            Log.d(TAG, "removeInvalidDevice： " + mDeviceNetInfos);
            for (Map.Entry<String, DeviceNetInfo> entry : mDeviceNetInfos.entrySet()) {
                if (entry.getValue() != null) {
                    if (!entry.getValue().isValid()) {
                        mDeviceNetInfos.remove(entry.getKey());
                        Log.d(TAG, "remove： " + entry.getKey());
                    } else {
                        entry.getValue().setValid(false);
                    }
                }
            }
            Log.d(TAG, "removeInvalidDevice： " + mDeviceNetInfos);
        }
    }

    volatile boolean mIsExist = false;

    public synchronized void checkSearchDeviceServiceDestory() {
        stopSearch();
        unRegisterListener(mListener);
        clearDevice();
        mIsExist = false;
    }

    public synchronized boolean checkSearchDeviceServiceIsExist() {
        return mIsExist;
    }

    private class SearchDeviceImpl implements ISearchDeviceListener {
        @Override
        public void onDeviceSearched(String sncode, DEVICE_NET_INFO_EX info) {
            if (mDeviceNetInfos != null) {
                DeviceNetInfo deviceNetInfo = new DeviceNetInfo(info);
                mDeviceNetInfos.put(sncode, deviceNetInfo);
                Log.d(TAG, "onDeviceSearched： " + mDeviceNetInfos);
            }
        }
    }

    public interface ISearchDeviceListener {
        void onDeviceSearched(String sncode, DEVICE_NET_INFO_EX info);
    }

    private class LogCallBack implements CB_fSDKLogCallBack {
        @Override
        public int invoke(byte[] bytes, int length) {
            String netSDKLog = new String(bytes).trim();
            Log.d(TAG, netSDKLog);
            String type = "";
            String content = "";
            try {
                JSONObject jsonObject = new JSONObject(netSDKLog);
                type = jsonObject.optString("type");
                content = jsonObject.optString("log");
                Log.d(TAG, "type : " + type + " content : " + content);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return 0;
        }
    }
}
