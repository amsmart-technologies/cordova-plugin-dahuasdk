package cordova.plugin.dahuasdk;

public class HandleMessageCode {
    private final static int HMC_EXCEPTION_BASE = 0;
    public final static int HMC_SUCCESS = HMC_EXCEPTION_BASE + 1;
    public final static int HMC_EXCEPTION = HMC_EXCEPTION_BASE + 2;
    public final static int HMC_BATCH_MIDDLE_RESULT = HMC_EXCEPTION_BASE + 3;
}