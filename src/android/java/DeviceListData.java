package cordova.plugin.dahuasdk;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.List;

public class DeviceListData implements Serializable {
    public DeviceListData.RequestData data = new DeviceListData.RequestData();

    public static class Response {
        public DeviceListData.ResponseData data;

        public void parseData(JsonObject json) {
            Gson gson = new Gson();
            this.data = gson.fromJson(json.toString(), DeviceListData.ResponseData.class);
        }
    }

    public static class ResponseData implements Serializable {
        public String count;
        public List<DeviceListElement> devices;

        @Override
        public String toString() {
            Gson g = new Gson();
            return g.toJson(this);
        }

        public static class DeviceListElement implements Serializable {
            public String bindId;
            public String deviceId;
            public List<ChannelsElement> channels;
            public List<AplistElement> aplist;

            @Override
            public String toString() {
                Gson g = new Gson();
                return g.toJson(this);
            }

            public static class ChannelsElement implements Serializable {
                public String channelName;
                public String channelId;

                @Override
                public String toString() {
                    Gson g = new Gson();
                    return g.toJson(this);
                }
            }

            public static class AplistElement implements Serializable {
                public String apId;
                public String apName;
                public String apType;

                @Override
                public String toString() {
                    Gson g = new Gson();
                    return g.toJson(this);
                }
            }
        }

    }

    public static class RequestData implements Serializable {
        public String token;
        public String limit = "8";
        public String type = "bind";
        public String needApInfo = "false";
        public long baseBindId = -1;
        public long openBindId = -1;

        @Override
        public String toString() {
            Gson g = new Gson();
            return g.toJson(this);
        }
    }
}
