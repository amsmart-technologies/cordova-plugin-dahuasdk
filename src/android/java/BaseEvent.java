package cordova.plugin.dahuasdk;

public class BaseEvent {
    private String code;

    public BaseEvent(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
