package cordova.plugin.dahuasdk;

import android.os.Handler;

import java.lang.ref.WeakReference;

public abstract class BusinessRunnable implements Runnable {

    private WeakReference<Handler> mHandler;

    public BusinessRunnable(Handler handle) {
        mHandler = new WeakReference<>(handle);
        Handler handler = getHander();
        ThreadPool.submit(this);
    }

    public Handler getHander() {
        return mHandler.get();
    }

    @Override
    public void run() {
        try {
            doBusiness();
        } catch (BusinessException e) {
            Handler handler = getHander();
            if (handler != null) {
                handler.obtainMessage(HandleMessageCode.HMC_EXCEPTION, e.errorCode, e.errorCode, e).sendToTarget();
            }

        } catch (Exception e) {
            Handler handler = getHander();
            if (handler != null) {
                handler.obtainMessage(HandleMessageCode.HMC_EXCEPTION, BusinessErrorCode.BEC_COMMON_UNKNOWN, BusinessErrorCode.BEC_COMMON_UNKNOWN, new BusinessException(e)).sendToTarget();
            }
        }
    }

    public abstract void doBusiness() throws BusinessException;
}