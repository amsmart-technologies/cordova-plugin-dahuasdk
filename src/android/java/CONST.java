package cordova.plugin.dahuasdk;

public class CONST {
    public static String METHOD_ACCESSTOKEN = "accessToken";
    public static String METHOD_UNBINDDEVICEINFO = "unBindDeviceInfo";
    public static String METHOD_GUIDEINFOGET = "deviceAddingProcessGuideInfoGet";
    public static String METHOD_GUIDEINFOCHECK = "deviceAddingProcessGuideInfoCheck";
    public static String METHOD_BINDDEVICE = "bindDevice";
    public static String METHOD_MODIFYDEVICENAME = "modifyDeviceName";
    public static String METHOD_CURRENT_DEVICE_WIFI = "currentDeviceWifi";
    public static String METHOD_WIFI_AROUND = "wifiAround";
    public static String METHOD_TIMEZONE_QUERY = "timeZoneQueryByDay";
    public static String METHOD_TIMEZONE_CONFIG = "timeZoneConfigByDay";
    public static String METHOD_CONTROL_DEVICE_WIFI = "controlDeviceWifi";
    public static String METHOD_DEVICE_OPEN_DETAIL_LIST = "deviceOpenDetailList";
    public static String LIST_SUB_ACCOUNT = "listSubAccount";
    public static String DELETE_SUB_ACCOUNT = "deleteSubAccount";
    public static String LIST_SUB_ACCOUNT_DEVICE = "listSubAccountDevice";
    public static String CREATE_SUB_ACCOUNT = "createSubAccount";
    public static String GET_OPENID_BYACCOUNT = "getOpenIdByAccount";
    public static String OPEN_DEVICE_CHANNEL_INFO = "queryOpenDeviceChannelInfo";
    public static String SUB_ACCOUNT_TOKEN = "subAccountToken";
    public static String ADD_POLICY = "addPolicy";
    public static String SET_MESSAGE_CALLBACK = "setMessageCallback";
    public static String GET_MESSAGE_CALLBACK = "getMessageCallback";
    public static String GET_ALARM_MESSAGE = "getAlarmMessage";
    public static String DELETE_ALARM_MESSAGE = "deleteAlarmMessage";
    public static String QUERY_CLOUDRECORD_CALLNUM = "queryCloudRecordCallNum";
    public static String OPEN_CLOUDRECORD = "openCloudRecord";
    public static String GET_STORAGE_STRATEGY = "getDeviceCloud";
    public static String DEVICESDCARDSTATUS = "deviceSdcardStatus";
    public static String DEVICESTORAGE = "deviceStorage";
    public static String RECOVERSDCARD = "recoverSDCard";
    public static String SETDEVICESNAP = "setDeviceSnapEnhanced";
    public static String QUERYLOCALRECORDPLAN = "queryLocalRecordPlan";
    public static String SETLOCALRECORDPLANRULES = "setLocalRecordPlanRules";
    public static String SUBACCOUNTDEVICELIST = "subAccountDeviceList";
    public static String RESTARTDEVICE = "restartDevice";
    public static String MODIFYDEVICEPWD = "modifyDevicePwd";
    public static String UPGRADEPROCESSDEVICE = "upgradeProcessDevice";
    public static String LISTDEVICEDETAILSBYPAGE = "listDeviceDetailsByPage";
    public static String HOST = "";
    public static String APPID = "";
    public static String SECRET = "";

    public static void makeEnv(String url, String appId, String secretKey) {
        APPID = appId;
        SECRET = secretKey;
        HOST = url;
    }
}
