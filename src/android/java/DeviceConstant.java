package cordova.plugin.dahuasdk;

public class DeviceConstant {
    public interface IntentKey {
        String DEVICE_CURRENT_WIFI_INFO = "DEVICE_CURRENT_WIFI_INFO";
        String DEVICE_WIFI_CONFIG_INFO = "DEVICE_WIFI_CONFIG_INFO";
        String DHDEVICE_INFO = "DHDEVICE_INFO";
        String DHDEVICE_UNBIND = "DHDEVICE_UNBIND";
        String DHDEVICE_NEW_NAME = "DHDEVICE_NEW_NAME";
    }

    public interface IntentCode {
        int DEVICE_SETTING_WIFI_OPERATE = 208;
        int DEVICE_SETTING_WIFI_LIST = 209;
    }
}
