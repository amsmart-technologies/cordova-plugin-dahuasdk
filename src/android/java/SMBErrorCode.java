package cordova.plugin.dahuasdk;

public class SMBErrorCode {
    public static final int SUCCESS = 10000;
    public static final int REQUEST_AUTHORITY_ERROR = 10001;
    public static final int REQUEST_INVALID_TOKEN = 10003;
    public static final int REQUEST_LOGIN_EXPIRED = 10005;
    public static final int REQUEST_SIGNATURE_FORMAT_ERROR = 10015;
}