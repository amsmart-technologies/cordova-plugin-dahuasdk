package cordova.plugin.dahuasdk;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CommonTitle extends RelativeLayout {

    public static final int ID_LEFT = 0;

    public static final int ID_LEFT_2 = 1;

    public static final int ID_RIGHT = 2;

    public static final int ID_RIGHT_2 = 3;

    public static final int ID_CENTER = 4;

    private TextView mTitleLeftTv;

    private LinearLayout mTitleLeftLl;

    private TextView mTitleLeft2Tv;

    private LinearLayout mTitleLeft2Ll;

    private TextView mTitleRightTv;

    private LinearLayout mTitleRightLl;

    private TextView mTitleRight2Tv;

    private LinearLayout mTitleRight2Ll;

    private TextView mTitleCenterTv;

    private LinearLayout mTitleCenterLl;

    private OnTitleClickListener mListener;

    private View mBottomV;

    private boolean mIsSupportChangeCenterWidth = false;

    public CommonTitle(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(getResources("layout", "mobile_common_widget_common_title"), this);
        initView();
        setListeners();
        setVisibleLeft2(View.GONE);
        setVisibleRight2(View.GONE);
    }

    private void initView() {
        mBottomV = findViewById(getResources("id", "v_bottom_line"));
        mTitleLeftLl = findViewById(getResources("id", "ll_title_left"));
        mTitleLeft2Ll = findViewById(getResources("id", "ll_title_left2"));
        mTitleRightLl = findViewById(getResources("id", "ll_title_right"));
        mTitleRight2Ll = findViewById(getResources("id", "ll_title_right2"));
        mTitleCenterLl = findViewById(getResources("id", "ll_title_center"));

        mTitleLeftTv = findViewById(getResources("id", "tv_title_left"));
        mTitleLeft2Tv = findViewById(getResources("id", "tv_title_left2"));
        mTitleRightTv = findViewById(getResources("id", "tv_title_right"));
        mTitleRight2Tv = findViewById(getResources("id", "tv_title_right2"));
        mTitleCenterTv = findViewById(getResources("id", "tv_title_center"));

        mTitleLeftTv.setTextColor(Color.parseColor("#FFFFFF"));
        mTitleLeft2Tv.setTextColor(Color.parseColor("#FFFFFF"));
        mTitleRightTv.setTextColor(Color.parseColor("#FFFFFF"));
        mTitleRight2Tv.setTextColor(Color.parseColor("#FFFFFF"));
        mTitleCenterTv.setTextColor(Color.parseColor("#2C2C2C"));

        mTitleLeftTv.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources()
                .getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_mid")));
        mTitleLeft2Tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_mid")));
        mTitleRightTv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_mid")));
        mTitleRight2Tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_mid")));
        mTitleCenterTv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_large")));

        DisplayMetrics dm = getContext().getResources().getDisplayMetrics();
        int commonTitleWidth = dm.widthPixels;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            commonTitleWidth = dm.widthPixels;
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            commonTitleWidth = dm.heightPixels;
        }

        int centerTitleWidth = commonTitleWidth - 4 * dp2px(dm, 48);

        mTitleCenterTv.setWidth(centerTitleWidth);
    }

    private int dp2px(DisplayMetrics dm, float dp) {
        final float scale = dm.density;
        return (int) (dp * scale + 0.5f);
    }

    private void setListeners() {
        mTitleLeftLl.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onCommonTitleClick(ID_LEFT);
            }

        });

        mTitleLeft2Ll.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onCommonTitleClick(ID_LEFT_2);
            }

        });

        mTitleRightLl.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onCommonTitleClick(ID_RIGHT);
            }

        });

        mTitleRight2Ll.setOnClickListener(v -> {
            if (mListener != null)
                mListener.onCommonTitleClick(ID_RIGHT_2);
        });

        mTitleCenterLl.setOnClickListener(v -> {
            if (mListener != null)
                mListener.onCommonTitleClick(ID_CENTER);
        });
    }

    public void initView(int tvLeftResId, int tvRightResId, int tvCenterResId) {
        setTitleLeftView(tvLeftResId, 0, 0);
        setTitleRightView(tvRightResId, 0, 0);
        setTitleCenterView(tvCenterResId, 0, 0);
    }

    public TextView getTextViewRight() {
        return mTitleRightTv;
    }

    public TextView getTextViewRight2() {
        return mTitleRight2Tv;
    }

    public TextView getTextViewCenter() {
        return mTitleCenterTv;
    }

    public void setTitleSelected(boolean selected, int id) {
        View v = findViewByID(id);
        if (v != null) {
            v.setSelected(selected);
        }
    }

    public void setEnabled(boolean enabled, int id) {
        View parent = findParentViewById(id);
        if (parent != null) {

            View v = findViewByID(id);
            if (v != null) {

                parent.setEnabled(enabled);
                v.setEnabled(enabled);
            }
        }

    }

    public boolean isEnable(int id) {
        View parent = findParentViewById(id);
        if (parent != null) {

            View v = findViewByID(id);
            if (v != null) {
                return v.isEnabled();
            }
        }

        return false;
    }

    private View findParentViewById(int id) {
        switch (id) {
            case ID_LEFT:
                return mTitleLeftLl;
            case ID_LEFT_2:
                return mTitleLeft2Ll;
            case ID_RIGHT:
                return mTitleRightLl;
            case ID_RIGHT_2:
                return mTitleRight2Ll;
            case ID_CENTER:
                return mTitleCenterLl;
            default:
                return null;
        }
    }

    private View findViewByID(int id) {
        switch (id) {
            case ID_LEFT:
                return mTitleLeftTv;
            case ID_LEFT_2:
                return mTitleLeft2Tv;
            case ID_RIGHT:
                return mTitleRightTv;
            case ID_RIGHT_2:
                return mTitleRight2Tv;
            case ID_CENTER:
                return mTitleCenterTv;
            default:
                return null;
        }
    }

    public void setTitleLeftView(int resId, int colorId, int textSizeDimenId) {
        setTitleLeft(resId);
        setTextColorLeft(colorId);
        setTextSizeLeft(textSizeDimenId);
    }

    public void setTitleRightView(int resId, int colorId, int textSizeDimenId) {
        setTitleRight(resId);
        setTextColorRight(colorId);
        setTextSizeRight(textSizeDimenId);
    }

    public void setTitleCenterView(int resId, int colorId, int textSizeDimenId) {
        setTitleCenter(resId);
        setTextColorCenter(colorId);
        setTextSizeCenter(textSizeDimenId);
    }

    public void setTitleLeft(int leftResId) {
        if (mTitleLeftTv != null) {
            if (leftResId != 0) {
                if (mTitleLeftLl != null && mTitleLeftLl.getVisibility() != View.VISIBLE)
                    mTitleLeftLl.setVisibility(VISIBLE);
                Drawable drawable = null;
                try {
                    drawable = getResources().getDrawable(leftResId);
                } catch (Exception e) {
                    // e.printStackTrace();
                } finally {
                    if (drawable != null) {
                        mTitleLeftTv.setBackgroundResource(leftResId);
                        mTitleLeftTv.setText(null);
                    } else {
                        mTitleLeftTv.setText(leftResId);
                        mTitleLeftTv.setBackgroundResource(0);
                    }
                }
            } else {
                if (mTitleLeftLl != null)
                    mTitleLeftLl.setVisibility(GONE);
            }
        }
    }

    public void setTitleLeft2(int left2ResId) {
        if (mTitleLeft2Tv != null) {
            if (left2ResId != 0) {
                if (mTitleLeft2Ll != null && mTitleLeft2Ll.getVisibility() != View.VISIBLE)
                    mTitleLeft2Ll.setVisibility(VISIBLE);
                Drawable drawable = null;
                try {
                    drawable = getResources().getDrawable(left2ResId);
                } catch (Exception e) {
                    // e.printStackTrace();
                } finally {
                    if (drawable != null) {
                        mTitleLeft2Tv.setBackgroundResource(left2ResId);
                        mTitleLeft2Tv.setText(null);
                    } else {
                        mTitleLeft2Tv.setText(left2ResId);
                        mTitleLeft2Tv.setBackgroundResource(0);
                    }
                }
            } else {
                if (mTitleLeft2Ll != null)
                    mTitleLeft2Ll.setVisibility(INVISIBLE);
            }
        }
    }

    public void setTitleRight(int rightResId) {
        if (mTitleRightTv != null) {
            if (rightResId != 0) {
                if (mTitleRightLl != null && mTitleRightLl.getVisibility() != View.VISIBLE)
                    mTitleRightLl.setVisibility(VISIBLE);
                Drawable drawable = null;
                try {
                    drawable = getResources().getDrawable(rightResId);
                } catch (Exception e) {
                    // e.printStackTrace();
                } finally {
                    if (drawable != null) {
                        mTitleRightTv.setBackgroundResource(rightResId);
                        mTitleRightTv.setText(null);
                    } else {
                        mTitleRightTv.setText(rightResId);
                        mTitleRightTv.setBackgroundResource(0);
                    }

                }
            } else {
                if (mTitleRightLl != null)
                    mTitleRightLl.setVisibility(INVISIBLE);
            }
        }
    }

    public void setTitleRight2(int right2ResId) {
        if (mTitleRight2Tv != null) {
            if (right2ResId != 0) {
                if (mTitleRight2Ll != null && mTitleRight2Ll.getVisibility() != View.VISIBLE)
                    mTitleRight2Ll.setVisibility(VISIBLE);
                Drawable drawable = null;
                try {
                    drawable = getResources().getDrawable(right2ResId);
                } catch (Exception e) {
                    // e.printStackTrace();
                } finally {
                    if (drawable != null) {
                        mTitleRight2Tv.setBackgroundResource(right2ResId);
                        mTitleRight2Tv.setText(null);
                    } else {
                        mTitleRight2Tv.setText(right2ResId);
                        mTitleRight2Tv.setBackgroundResource(0);
                    }
                }
            } else {
                if (mTitleRight2Ll != null)
                    mTitleRight2Ll.setVisibility(INVISIBLE);
            }
        }
    }

    public void setTitleCenter(int centerResId) {
        if (mTitleCenterTv != null) {
            if (centerResId != 0) {
                if (mTitleCenterLl != null && mTitleCenterLl.getVisibility() != View.VISIBLE)
                    mTitleCenterLl.setVisibility(VISIBLE);
                Drawable drawable = null;
                try {
                    drawable = getResources().getDrawable(centerResId);
                } catch (Exception e) {
                    // e.printStackTrace();
                } finally {
                    if (drawable != null) {
                        mTitleCenterTv.setBackgroundResource(centerResId);
                        mTitleCenterTv.setText(null);
                    } else {
                        mTitleCenterTv.setText(centerResId);
                        mTitleCenterTv.setBackgroundResource(0);
                        setTitleCenterWidth();
                    }
                }
            } else {
                if (mTitleCenterLl != null)
                    mTitleCenterLl.setVisibility(INVISIBLE);
            }
        }
    }

    public void setTextColorLeft(int colorId) {
        if (mTitleLeftTv != null) {
            mTitleLeftTv.setTextColor(colorId != 0 ? getResources().getColor(colorId) : Color.parseColor("#3E3E3E"));
        }
    }

    public void setTextColorLeft2(int colorId) {
        if (mTitleLeft2Tv != null) {
            mTitleLeft2Tv.setTextColor(colorId != 0 ? getResources().getColor(colorId) : Color.parseColor("#3E3E3E"));
        }
    }

    public void setTextColorRight(int colorId) {
        if (mTitleRightTv != null) {
            mTitleRightTv.setTextColor(colorId != 0 ? getResources().getColorStateList(colorId) : getResources().getColorStateList(getResources("color", "mobile_common_title_text_color_selector")));
        }
    }

    public void setTextColorRight2(int colorId) {
        if (mTitleRight2Tv != null) {
            mTitleRight2Tv.setTextColor(colorId != 0 ? getResources().getColor(colorId) : Color.parseColor("#FFFFFF"));
        }
    }

    public void setTextColorCenter(int colorId) {
        if (mTitleCenterTv != null) {
            mTitleCenterTv.setTextColor(colorId != 0 ? getResources().getColor(colorId) : Color.parseColor("#2C2C2C"));
        }
    }

    public void setTextSizeLeft(int textSizeDimenId) {
        if (mTitleLeftTv != null) {
            mTitleLeftTv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    textSizeDimenId != 0 ? getResources().getDimensionPixelSize(textSizeDimenId) : getResources()
                            .getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_mid")));
        }
    }

    public void setTextSizeLeft2(int textSizeDimenId) {
        if (mTitleLeft2Tv != null) {
            mTitleLeft2Tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    textSizeDimenId != 0 ? getResources().getDimensionPixelSize(textSizeDimenId) : getResources()
                            .getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_mid")));
        }
    }

    public void setTextSizeRight(int textSizeDimenId) {
        if (mTitleRightTv != null) {
            mTitleRightTv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    textSizeDimenId != 0 ? getResources().getDimensionPixelSize(textSizeDimenId) : getResources()
                            .getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_mid")));
        }
    }

    public void setTextSizeRight2(int textSizeDimenId) {
        if (mTitleRight2Tv != null) {
            mTitleRight2Tv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    textSizeDimenId != 0 ? getResources().getDimensionPixelSize(textSizeDimenId) : getResources()
                            .getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_mid")));
        }
    }

    public void setTextSizeCenter(int textSizeDimenId) {
        if (mTitleCenterTv != null) {
            mTitleCenterTv.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    textSizeDimenId != 0 ? getResources().getDimensionPixelSize(textSizeDimenId) : getResources()
                            .getDimensionPixelSize(getResources("dimen", "mobile_common_common_title_text_size_large")));
        }
    }

    public void setIconLeft(int resId) {
        if (mTitleLeftLl != null) {
            if (mTitleLeftLl.getVisibility() != View.VISIBLE) {
                mTitleLeftLl.setVisibility(View.VISIBLE);
            }
            setTitleLeft(resId);
        }
    }

    public void setIconLeft2(int resId) {
        if (mTitleLeft2Ll != null) {
            if (mTitleLeft2Ll.getVisibility() != View.VISIBLE) {
                mTitleLeft2Ll.setVisibility(View.VISIBLE);
            }
            setTitleLeft2(resId);
        }
    }

    public void setIconRight(int resId) {
        if (mTitleRightLl != null) {
            if (mTitleRightLl.getVisibility() != View.VISIBLE) {
                mTitleRightLl.setVisibility(View.VISIBLE);
            }
            setTitleRight(resId);
        }
    }

    public void setIconRight2(int resId) {
        if (mTitleRight2Ll != null) {
            if (mTitleRight2Ll.getVisibility() != View.VISIBLE) {
                mTitleRight2Ll.setVisibility(View.VISIBLE);
            }
            setTitleRight2(resId);
        }
    }

    public void setIconCenter(int resId) {
        if (mTitleCenterLl != null) {
            if (mTitleCenterLl.getVisibility() != View.VISIBLE) {
                mTitleCenterLl.setVisibility(View.VISIBLE);
            }
            setTitleCenter(resId);
        }
    }


    public void setTitleTextLeft(int resId) {
        if (mTitleLeftLl != null) {
            if (mTitleLeftLl.getVisibility() != View.VISIBLE) {
                mTitleLeftLl.setVisibility(View.VISIBLE);
            }
            setTitleLeft(resId);
        }
    }

    public void setTitleTextLeft2(int resId) {
        if (mTitleLeft2Ll != null) {
            if (mTitleLeft2Ll.getVisibility() != View.VISIBLE) {
                mTitleLeft2Ll.setVisibility(View.VISIBLE);
            }
            setTitleLeft(resId);
        }
    }

    public void setTitleTextRight(int resId) {
        if (mTitleRightLl != null) {
            if (mTitleRightLl.getVisibility() != View.VISIBLE) {
                mTitleRightLl.setVisibility(View.VISIBLE);
            }
            setTitleRight(resId);
        }
    }

    public void setTitleTextRight2(int resId) {
        if (mTitleRight2Ll != null) {
            if (mTitleRight2Ll.getVisibility() != View.VISIBLE) {
                mTitleRight2Ll.setVisibility(View.VISIBLE);
            }
            setTitleRight2(resId);
        }
    }

    public void setTitleTextCenter(int resId) {
        if (mTitleCenterLl != null) {
            if (mTitleCenterLl.getVisibility() != View.VISIBLE) {
                mTitleCenterLl.setVisibility(View.VISIBLE);
            }
            setTitleCenter(resId);
        }
    }

    public void setTitleTextLeft(String titleTextLeft) {
        if (mTitleLeftTv != null) {
            if (mTitleLeftTv.getVisibility() != View.VISIBLE) {
                mTitleLeftTv.setVisibility(View.VISIBLE);
            }
            mTitleLeftTv.setText(titleTextLeft);
            mTitleLeftTv.setBackgroundResource(0);
        }
    }

    public void setTitleTextLeft2(String titleTextLeft2) {
        if (mTitleLeft2Tv != null) {
            if (mTitleLeft2Tv.getVisibility() != View.VISIBLE) {
                mTitleLeft2Tv.setVisibility(View.VISIBLE);
            }
            mTitleLeft2Tv.setText(titleTextLeft2);
            mTitleLeft2Tv.setBackgroundResource(0);
        }
    }

    public void setTitleTextRight(String titleTextRight) {
        if (mTitleRightTv != null) {
            if (mTitleRightTv.getVisibility() != View.VISIBLE) {
                mTitleRightTv.setVisibility(View.VISIBLE);
            }
            mTitleRightTv.setText(titleTextRight);
            mTitleRightTv.setBackgroundResource(0);
        }
    }

    public void setTitleTextRight2(String titleTextRight2) {
        if (mTitleRight2Tv != null) {
            if (mTitleRight2Tv.getVisibility() != View.VISIBLE) {
                mTitleRight2Tv.setVisibility(View.VISIBLE);
            }
            mTitleRight2Tv.setText(titleTextRight2);
            mTitleRight2Tv.setBackgroundResource(0);
        }
    }

    public void setTitleTextCenter(String titleTextCenter) {
        if (mTitleCenterLl != null) {
            if (mTitleCenterLl.getVisibility() != View.VISIBLE) {
                mTitleCenterLl.setVisibility(View.VISIBLE);
            }
            mTitleCenterTv.setText(titleTextCenter);
            mTitleCenterTv.setBackgroundResource(0);
            setTitleCenterWidth();
        }
    }

    public void setChangeCenterWidthForWebView(boolean isSupportChangeCenterWidth) {
        mIsSupportChangeCenterWidth = isSupportChangeCenterWidth;
    }

    @Deprecated
    private void setTitleCenterWidth() {

    }

    public void setVisibleLeft(int flag) {
        if (mTitleLeftLl != null) {
            mTitleLeftLl.setVisibility(flag);
        }
    }

    public void setVisibleLeft2(int flag) {
        if (mTitleLeft2Ll != null) {
            mTitleLeft2Ll.setVisibility(flag);
        }
    }

    public void setVisibleRight(int flag) {
        if (mTitleRightLl != null) {
            mTitleRightLl.setVisibility(flag);
        }
    }

    public void setEnableRight(boolean enable) {
        if (mTitleRightLl != null) {
            mTitleRightLl.setEnabled(enable);
        }
    }

    public boolean getEnableRight() {
        return mTitleRightLl.isEnabled();
    }

    public void setVisibleRight2(int flag) {
        if (mTitleRight2Ll != null) {
            mTitleRight2Ll.setVisibility(flag);
        }
    }

    public void setVisibleCenter(int flag) {
        if (mTitleCenterLl != null) {
            mTitleCenterLl.setVisibility(flag);
        }
    }

    public void setVisibleBottom(int flag) {
        if (mBottomV != null) {
            mBottomV.setVisibility(flag);
        }
    }

    public boolean isIconLeftVisible() {
        if (mTitleLeftLl != null) {
            return mTitleLeftLl.getVisibility() == VISIBLE;
        }
        return false;
    }

    public void setOnTitleClickListener(OnTitleClickListener listener) {
        mListener = listener;
    }

    public interface OnTitleClickListener {
        public void onCommonTitleClick(int id);
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
