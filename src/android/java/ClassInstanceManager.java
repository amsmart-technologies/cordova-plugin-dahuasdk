package cordova.plugin.dahuasdk;

import android.content.Context;

public class ClassInstanceManager {
    private volatile static ClassInstanceManager classInstanceManager;
    private DeviceLocalCacheManager deviceLocalCacheManager;
    private DeviceDetailService deviceDetailService;
    private DeviceListService deviceListService;
    private DeviceRecordService deviceRecordService;
    private DeviceLocalCacheService deviceLocalCacheService;
    private Context context;

    public static ClassInstanceManager newInstance() {
        if (classInstanceManager == null) {
            synchronized (ClassInstanceManager.class) {
                if (classInstanceManager == null) {
                    classInstanceManager = new ClassInstanceManager();
                }
            }
        }
        return classInstanceManager;
    }

    public static ClassInstanceManager newInstance(Context context) {
        if (classInstanceManager == null) {
            synchronized (ClassInstanceManager.class) {
                if (classInstanceManager == null) {
                    classInstanceManager = new ClassInstanceManager();
                    classInstanceManager.init(context);
                }
            }
        }
        return classInstanceManager;
    }

    public void init(Context context) {
        this.context = context;
        deviceLocalCacheManager = new DeviceLocalCacheManager();
        deviceLocalCacheManager.init(this.context);
        deviceDetailService = new DeviceDetailService();
        deviceListService = new DeviceListService();
        deviceRecordService = new DeviceRecordService();
        deviceLocalCacheService = new DeviceLocalCacheService();
    }

    public int getResources(String name, String type) {
        return this.context.getResources().getIdentifier(name, type, this.context.getPackageName());
    }

    public DeviceLocalCacheManager getDeviceLocalCacheManager() {
        return deviceLocalCacheManager;
    }

    public DeviceDetailService getDeviceDetailService() {
        return deviceDetailService;
    }

    public DeviceListService getDeviceListService() {
        return deviceListService;
    }

    public DeviceRecordService getDeviceRecordService() {
        return deviceRecordService;
    }

    public DeviceLocalCacheService getDeviceLocalCacheService() {
        return deviceLocalCacheService;
    }

}
