package cordova.plugin.dahuasdk;

import android.content.Intent;
import android.os.Handler;

public class DeviceWifiPasswordPresenter<T extends DeviceWifiPasswordConstract.View> extends BasePresenter<T> implements DeviceWifiPasswordConstract.Presenter {

    public String WIFI_SAVE_PREFIX =  "DAHUA_WIFI_ADD_";
    public String WIFI_CHECKBOX_SAVE_PREFIX =  "DAHUA_WIFI_CHECKBOX_ADD_";

    protected String mDeviceId;
    protected WifiInfo mWifiInfo;
    private DHBaseHandler mWifiOperateHandler;
    protected boolean support5G = false;

    public DeviceWifiPasswordPresenter(T view) {
        super(view);
        initModel();
    }

    protected void initModel() {

    }

    @Override
    public void dispatchIntentData(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            mDeviceId = intent.getStringExtra(LCConfiguration.Device_ID);
            mWifiInfo = (WifiInfo) intent.getSerializableExtra(DeviceConstant.IntentKey.DEVICE_WIFI_CONFIG_INFO);
            support5G = intent.getBooleanExtra(LCConfiguration.SUPPORT_5G, false);
        }
    }

    @Override
    public boolean getSupport5G() {
        return support5G;
    }

    @Override
    public void updateWifiCache() {
        if (mView.get().isSavePwdChecked()) {
           PreferencesHelper.getInstance(mView.get().getContextInfo()).set(WIFI_SAVE_PREFIX + mWifiInfo.getSsid(), mView.get().getWifiPassword());
           PreferencesHelper.getInstance(mView.get().getContextInfo()).set(WIFI_CHECKBOX_SAVE_PREFIX + mWifiInfo.getSsid(), true);
        } else {
           PreferencesHelper.getInstance(mView.get().getContextInfo()).set(WIFI_SAVE_PREFIX + mWifiInfo.getSsid(), "");
           PreferencesHelper.getInstance(mView.get().getContextInfo()).set(WIFI_CHECKBOX_SAVE_PREFIX + mWifiInfo.getSsid(), false);
        }
    }

    @Override
    public String getSavedWifiPassword() {
       return PreferencesHelper.getInstance(mView.get().getContextInfo()).getString(WIFI_SAVE_PREFIX + mWifiInfo.getSsid());
    }

    @Override
    public boolean getSavedWifiCheckBoxStatus() {
       return PreferencesHelper.getInstance(mView.get().getContextInfo()).getBoolean(WIFI_CHECKBOX_SAVE_PREFIX + mWifiInfo.getSsid());
    }

    @Override
    public void wifiOperate() {
        new BusinessRunnable(mWifiOperateHandler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    OpenApiManager.controlDeviceWifi(LCDeviceEngine.newInstance().accessToken, mDeviceId, mWifiInfo.getSsid(), mWifiInfo.getBSSID(), true, mView.get().getWifiPassword());
                    mWifiOperateHandler.obtainMessage(HandleMessageCode.HMC_SUCCESS, true).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
        mView.get().showToastInfo(getResources("string", "dahua_device_manager_wifi_connetting_tip"));
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.get().onWifiOperateSucceed(null);
            }
        }, 3000);
    }

    @Override
    public String getWifiSSID() {
        if (mWifiInfo == null)
            return "";
        return mWifiInfo.getSsid();
    }

    @Override
    public void unInit() {

    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
