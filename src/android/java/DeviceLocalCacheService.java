package cordova.plugin.dahuasdk;

import android.os.Message;

public class DeviceLocalCacheService {

    public void addLocalCache(final DeviceLocalCacheData localCacheData) {
        new BusinessRunnable(null) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    DeviceLocalCacheManager deviceLocalCacheManager = ClassInstanceManager.newInstance().getDeviceLocalCacheManager();
                    deviceLocalCacheManager.addLocalCache(localCacheData);
                } catch (Throwable e) {
                    throw e;
                }
            }
        };
    }

    public void findLocalCache(final DeviceLocalCacheData localCacheData, final IGetDeviceInfoCallBack.IDeviceCacheCallBack deviceCacheCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (deviceCacheCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    deviceCacheCallBack.deviceCache((DeviceLocalCacheData) msg.obj);
                } else {
                    deviceCacheCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    DeviceLocalCacheData localCache = ClassInstanceManager.newInstance().getDeviceLocalCacheManager().findLocalCache(localCacheData);
                    if (localCache != null) {
                        handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, localCache).sendToTarget();
                    } else {
                        throw new BusinessException("null point");
                    }
                } catch (Throwable e) {
                    throw e;
                }
            }
        };
    }
}
