package cordova.plugin.dahuasdk;

import android.view.View;

public abstract class BaseManagerFragmentActivity<T extends IBasePresenter> extends BaseMvpFragmentActivity<T> {
    private BasePopWindow mLoadingPopWindow;
    private PopWindowFactory mPopWindowFactory;
    private View mTitle;

    protected abstract View initTitle();

    @Override
    protected void initView() {
        mTitle = initTitle();
    }

    protected boolean showUnSaveAlertDialog() {
        return false;
    }

    private void showAlertDialog() {
        LCAlertDialog.Builder builder = new LCAlertDialog.Builder(this);
        LCAlertDialog alertDialog = builder
                .setTitle(getResources("string", "dahua_device_manager_not_saved_tip"))
                .setConfirmButton(getResources("string", "dahua_device_manager_exit"),
                        (dialog, which, isChecked) -> finish()).setCancelButton(getResources("string", "dahua_device_cancel"), null)
                .create();
        alertDialog.show(getSupportFragmentManager(), "");
    }

    @Override
    public void showProgressDialog() {
        if (mTitle == null) {
            super.showProgressDialog();
        } else {
            if (mLoadingPopWindow != null) {
                mLoadingPopWindow.dismiss();
            }
            if (mPopWindowFactory == null)
                mPopWindowFactory = new PopWindowFactory();
            try {
                mLoadingPopWindow = mPopWindowFactory.createLoadingPopWindow(this, mTitle);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void cancelProgressDialog() {
        if (mLoadingPopWindow == null) {
            super.cancelProgressDialog();
        } else {
            mLoadingPopWindow.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mLoadingPopWindow != null) {
            mLoadingPopWindow.dismiss();
            mLoadingPopWindow = null;
            mPopWindowFactory = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (showUnSaveAlertDialog()) {
            showAlertDialog();
        } else {
            super.onBackPressed();
        }
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
