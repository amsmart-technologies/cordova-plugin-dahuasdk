package cordova.plugin.dahuasdk;

import java.io.Serializable;

public class RecordsData implements Serializable {
    public int recordType;
    public String recordId;
    public String beginTime;
    public String endTime;
    public String type;
    public boolean check;
    public long fileLength;
    public String channelID;
    public String deviceId;
    public String channelId;
    public String size;
    public String thumbUrl;
    public int encryptMode;
    public String recordRegionId;

    public static RecordsData parseCloudData(CloudRecordsData.ResponseData.RecordsBean recordsBean) {
        RecordsData recordsData = new RecordsData();
        recordsData.recordType = 0;
        recordsData.recordId = recordsBean.recordId;
        recordsData.beginTime = recordsBean.beginTime;
        recordsData.endTime = recordsBean.endTime;
        recordsData.type = recordsBean.type;
        recordsData.deviceId = recordsBean.deviceId;
        recordsData.channelId = recordsBean.channelId;
        recordsData.size = recordsBean.size;
        recordsData.thumbUrl = recordsBean.thumbUrl;
        recordsData.encryptMode = recordsBean.encryptMode;
        recordsData.recordRegionId = recordsBean.recordRegionId;
        return recordsData;
    }

    public static RecordsData parseLocalData(LocalRecordsData.ResponseData.RecordsBean recordsBean) {
        RecordsData recordsData = new RecordsData();
        recordsData.recordType = 1;
        recordsData.recordId = recordsBean.recordId;
        recordsData.beginTime = recordsBean.beginTime;
        recordsData.endTime = recordsBean.endTime;
        recordsData.type = recordsBean.type;
        recordsData.fileLength = recordsBean.fileLength;
        recordsData.channelID = recordsBean.channelID;
        return recordsData;
    }
}
