package cordova.plugin.dahuasdk;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.LruCache;

import com.lechange.opensdk.utils.LCOpenSDK_Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageHelper {

    private final static String TAG = "LCOpenSDK_Demo_ImageHelper";
    private static Options mDefaultOption;
    private static LruCache<String, Drawable> mImageCache = new LruCache<String, Drawable>(100);

    static {
        mDefaultOption = new Options();
        mDefaultOption.inSampleSize = 2;
        mDefaultOption.inPreferredConfig = Config.RGB_565;
    }

    public static void loadRealImage(final String url, final Handler handler) {
        downloadImage(url, "real", handler);
    }

    public static void loadCacheImage(final String url, final Handler handler) {
        String[] imageIDBuffer = url.split("[/?]");
        final String imageID = imageIDBuffer[imageIDBuffer.length - 2];
        Drawable drawable = mImageCache.get(imageID);
        if (drawable != null) {
            Message msg = new Message();
            msg.what = url.hashCode();
            msg.obj = drawable;
            handler.handleMessage(msg);
        } else {
            downloadImage(url, imageID, handler);
        }
    }

    public static void loadCacheImage(final String url, final String deviceId, String key, int position, final Handler handler) {
        String[] imageIDBuffer = url.split("[/?]");
        String imageID;
        if (imageIDBuffer.length - 2 > 0) {
            imageID = imageIDBuffer[imageIDBuffer.length - 2];
        } else {
            imageID = "";
        }
        Drawable drawable = mImageCache.get(imageID);
        if (drawable != null) {
            Message msg = new Message();
            msg.what = url.hashCode();
            msg.obj = drawable;
            msg.arg1 = position;
            handler.handleMessage(msg);
        } else {
            downloadImage(url, imageID, deviceId, key, position, handler);
        }
    }

    private static void downloadImage(final String url, final String imageID, final Handler handler) {
        TaskPoolHelper.addTask(new TaskPoolHelper.RunnableTask(imageID) {
            @Override
            public void run() {
                Drawable drawable = null;
                try {
                    URL resurl = new URL(url);

                    HttpURLConnection urlConn = (HttpURLConnection) resurl.openConnection();
                    urlConn.setConnectTimeout(5000);
                    urlConn.setReadTimeout(5000);
                    InputStream is = urlConn.getInputStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(is, null, mDefaultOption);
                    if (bitmap != null) {
                        drawable = new BitmapDrawable(bitmap);
                    }
                    mImageCache.put(imageID, drawable);
                    is.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.obtainMessage(url.hashCode(), drawable).sendToTarget();
            }
        });
    }

    private static void downloadImage(final String url, final String imageID, final String deviceId, final String key, final int position, final Handler handler) {
        TaskPoolHelper.addTask(new TaskPoolHelper.RunnableTask(imageID) {
            @Override
            public void run() {
                Drawable drawable = null;
                try {
                    URL resurl = new URL(url);
                    HttpURLConnection urlConn = (HttpURLConnection) resurl.openConnection();
                    urlConn.setConnectTimeout(5000);
                    urlConn.setReadTimeout(5000);
                    int code = urlConn.getResponseCode();
                    InputStream is = urlConn.getInputStream();
                    ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
                    byte[] buff = new byte[500];
                    int rc = 0;
                    int length = 0;
                    while ((rc = is.read(buff, 0, 500)) > 0) {
                        length += rc;
                        swapStream.write(buff, 0, rc);
                    }
                    byte[] srcBuf = swapStream.toByteArray();
                    byte[] dstBuf = new byte[500000];
                    int[] dstLen = new int[1];
                    dstLen[0] = 500000;
                    Bitmap bitmap;
                    int res = LCOpenSDK_Utils.decryptPic(LCDeviceEngine.newInstance().accessToken, srcBuf, length, deviceId, key, dstBuf, dstLen);
                    switch (res) {
                        case 0:
                            bitmap = BitmapFactory.decodeByteArray(dstBuf, 0, dstLen[0], mDefaultOption);
                            if (bitmap == null) {
                                String filepath = "sdcard/temp.jpg";
                                File file = new File(filepath);
                                OutputStream outputStream = new FileOutputStream(file);
                                outputStream.write(dstBuf);
                                outputStream.flush();
                                outputStream.close();
                                bitmap = BitmapFactory.decodeFile(filepath);
                            }
                            if (bitmap != null) {
                                drawable = new BitmapDrawable(bitmap);
                            }
                            break;
                        case 1:
                        case 3:
                            bitmap = BitmapFactory.decodeByteArray(srcBuf, 0, length, mDefaultOption);
                            if (bitmap != null) {
                                drawable = new BitmapDrawable(bitmap);
                            }
                            break;
                        default:
                            break;
                    }
                    mImageCache.put(imageID, drawable);
                    is.close();
                    swapStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.obtainMessage(url.hashCode(), position, 0, drawable).sendToTarget();
            }
        });
    }

    public static void clear() {
        TaskPoolHelper.clearTask();
        mImageCache.evictAll();
    }
}
