package cordova.plugin.dahuasdk;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;

import com.company.NetSDK.DEVICE_NET_INFO_EX;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils4AddDevice {
    public static final int NETWORK_NONE = -1;
    public static final int NETWORK_WIFI = 0;
    public static final int NETWORK_MOBILE = 1;

    public static String strRegCodeFilter(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String strEx = "[0-9A-Za-z]";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (!temp.matches(strEx)) {
                str = str.replace(temp, "");
                return strRegCodeFilter(str);
            }
        }
        return str;
    }

    public static String wifiPwdFilter(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String chinese1 = "[\u2E80-\uA4CF]";
        String chinese2 = "[\uF900-\uFAFF]";
        String chinese3 = "[\uFE30-\uFE4F]";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (temp.matches(chinese1) || temp.matches(chinese2) || temp.matches(chinese3)) {
                str = str.replace(temp, "");
                return wifiPwdFilter(str);
            }
        }
        return str;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo anInfo : info) {
                    if (anInfo.isConnected()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isWifi(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo curNetwork = connectivity.getActiveNetworkInfo();
            if (curNetwork != null && curNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            }
        }
        return false;
    }

    public static int getNetWorkState(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo curNetwork = connectivity.getActiveNetworkInfo();
            if (curNetwork != null && curNetwork.isConnected()) {
                if (curNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    return NETWORK_WIFI;
                } else if (curNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    return NETWORK_MOBILE;
                }
            }
        }
        return NETWORK_NONE;
    }

    public static boolean isWifiEnabled(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        boolean wifi = (wifiInfo.getSSID() == null) || wifiInfo.getSSID().equalsIgnoreCase("");
        return !wifi;
    }

    public static boolean isWifiConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (mConnectivityManager == null) {
                return false;
            }
            NetworkInfo mWiFiNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (mWiFiNetworkInfo != null) {
                return mWiFiNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    public static boolean checkString(String str) {
        String regEx = "[0-9A-Za-z]*";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        if (m.matches()) {
            return true;
        } else {
            return false;
        }
    }

    public static String filterInvalidString(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String numberAndAbc = "[a-zA-Z0-9]";
        StringBuilder buffer = new StringBuilder();
        int len = str.length();
        for (int i = 0; i < len; ++i) {
            String temp = str.substring(i, i + 1);
            if (temp.matches(numberAndAbc)) {
                buffer.append(temp);
            }
        }
        return buffer.toString();
    }

    public static String filterInvalidString4Type(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String numberAndAbc = "[a-zA-Z0-9-/\\\\]";
        StringBuilder buffer = new StringBuilder();
        int len = str.length();
        for (int i = 0; i < len; ++i) {
            String temp = str.substring(i, i + 1);
            if (temp.matches(numberAndAbc)) {
                buffer.append(temp);
            }
        }
        return buffer.toString();
    }

    public static boolean isDeviceTypeBox(String deviceMode) {
        if (deviceMode == null || TextUtils.isEmpty(deviceMode)) {
            return false;
        }
        if (deviceMode.equalsIgnoreCase("G10")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isTp1And(String deviceModelName) {
        return LCConfiguration.TYPE_TC1.equals(deviceModelName) || LCConfiguration.TYPE_TK1.equals(deviceModelName)
                || LCConfiguration.TYPE_TC3.equals(deviceModelName) || LCConfiguration.TYPE_TK3.equals(deviceModelName)
                || LCConfiguration.TYPE_TC4.equals(deviceModelName) || LCConfiguration.TYPE_TC5.equals(deviceModelName)
                || LCConfiguration.TYPE_TC5S.equals(deviceModelName) || LCConfiguration.TYPE_TP1.equals(deviceModelName)
                || LCConfiguration.TYPE_TP6.equals(deviceModelName) || LCConfiguration.TYPE_TP6C.equals(deviceModelName)
                || LCConfiguration.TYPE_TC6.equals(deviceModelName) || LCConfiguration.TYPE_TC6C.equals(deviceModelName)
                || LCConfiguration.TYPE_TP7.equals(deviceModelName);
    }

    public static String getAddDeviceHelpUrl(String host) {
        if (host.contains(":443")) {
            host = host.split(":")[0];
        }
        return "http://" + host + "/bindhelp.html";
    }

    public static boolean checkDeviceVersion(DEVICE_NET_INFO_EX deviceInfo) {
        if (deviceInfo == null)
            return false;
        int flag = (deviceInfo.bySpecialAbility >> 2) & 0x03;
        return 0x00 != flag && 0x03 != flag;
    }

    public static boolean checkEffectiveIP(DEVICE_NET_INFO_EX deviceInfo) {
        if (deviceInfo == null)
            return false;
        int flag = (deviceInfo.bySpecialAbility >> 2) & 0x03;
        return 0x02 == flag;
    }

}
