package cordova.plugin.dahuasdk;

import android.content.Context;
import android.text.TextUtils;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class StringUtils {
    public static String getRTSPAuthPassword(String password, String deviceSnCode) {
        byte[] key = MD5Helper.encode(new String(deviceSnCode).toUpperCase() + "DAHUAKEY").toLowerCase().getBytes();
        byte[] iv = new String("0a52uuEvqlOLc5TO").getBytes();
        byte[] psw = null;
        try {
            SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv));
            psw = cipher.doFinal(password.getBytes());
        } catch (Exception e) {
        }
        String baseString = psw == null ? "" : Base64Help.encode(psw);
        if (baseString.endsWith("\n")) {
            baseString = baseString.replace("\n", "");
        }
        return baseString;
    }

    public static String strFilter(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String strEx = "^[a-zA-Z0-9\\-\u4E00-\u9FA5\\_\\@\\,\\.]+";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (!temp.matches(strEx)) {
                str = str.replace(temp, "");
                return strFilter(str);
            }
        }
        return str;
    }


    public static String strPsswordFilter(String str) {
        String strEx = "^[a-zA-Z0-9\\!\\#\\$\\%\\(\\)\\*\\+\\,\\-\\.\\/\\<\\=\\>\\?\\@\\[\\\\\\]\\^\\_\\`\\{\\|\\}\\~]*";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (!temp.matches(strEx)) {
                str = str.replace(temp, "");
                return strPsswordFilter(str);
            }
        }
        return str;
    }

    public static boolean checkSafetyBaseline(String username, String password, Context context) {
        return checkPasswordWithUername(username, password) || checkRepeat(password) || checkCryptographicLibrary(password, context);
    }

    public static boolean checkPasswordWithUername(String username, String password) {
        if (TextUtils.isEmpty(username)) {
            return false;
        }
        String userNameLowerCase = username.toLowerCase();
        String passwordLowerCase = password.toLowerCase();
        String reverse = new StringBuilder(userNameLowerCase).reverse().toString();
        return TextUtils.equals(passwordLowerCase, reverse) || TextUtils.equals(passwordLowerCase, userNameLowerCase);
    }

    public static boolean checkCryptographicLibrary(String password, Context context) {
        List<String> library = getCryptographicLirary(context);
        return library.contains(password);
    }

    private static List<String> getCryptographicLirary(Context context) {
        List<String> array = new ArrayList<>();
        try {
            XmlPullParser pullParser = Xml.newPullParser();
            InputStream is = context.getAssets().open("password.xml");
            pullParser.setInput(is, "utf-8");
            int eventType = pullParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        array = new ArrayList<>();
                        break;
                    case XmlPullParser.START_TAG:
                        if ("string".equals(pullParser.getName())) {
                            String str = pullParser.getAttributeValue(0);
                            array.add(str);
                        }
                        break;
                }
                eventType = pullParser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    public static boolean checkRepeat(String password) {
        String rex = "(.)\\1+";
        List<String> arr = new ArrayList<>();
        Pattern pattern = Pattern.compile(rex);
        Matcher matcher = pattern.matcher(password);
        while (matcher.find()) {
            String str = matcher.group();
            arr.add(str);
        }
        if (arr.isEmpty()) {
            return false;
        }
        for (String str : arr) {
            if (str.length() >= 6) {
                return true;
            }
        }
        return false;
    }

    public static String snFilter(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String E1 = "[A-Z]";
        String E2 = "[a-z]";
        String E3 = "[0-9]";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (!temp.matches(E1) && !temp.matches(E2)
                    && !temp.matches(E3)) {
                str = str.replace(temp, "");
                return snFilter(str);
            }
        }
        return str;
    }

    public static String strPwdFilter(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        String strEx = "^[a-zA-Z0-9\\-\\_\\@]+";
        for (int i = 0; i < str.length(); i++) {
            String temp = str.substring(i, i + 1);
            if (!temp.matches(strEx)) {
                str = str.replace(temp, "");
                return strPwdFilter(str);
            }
        }
        return str;
    }

}
