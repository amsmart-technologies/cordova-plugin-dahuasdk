package cordova.plugin.dahuasdk;

public class BusinessAuthUtil {

    private static int[] errorCodesInt = new int[]{SMBErrorCode.REQUEST_AUTHORITY_ERROR, SMBErrorCode.REQUEST_INVALID_TOKEN, SMBErrorCode.REQUEST_LOGIN_EXPIRED, SMBErrorCode.REQUEST_SIGNATURE_FORMAT_ERROR};

    public static boolean isAuthFailed(int codeFromHandle) {
        for (int errorCode : errorCodesInt) {
            if (codeFromHandle == errorCode) {
                return true;
            }
        }
        return false;
    }
}
