package cordova.plugin.dahuasdk;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

public class LoadingDialog extends Dialog {
    ProgressBar av_anim;

    public LoadingDialog(Context context) {
        super(context, ClassInstanceManager.newInstance().getResources("custom_dialog", "style"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources("layout", "dialog_loading_view"));
        setCanceledOnTouchOutside(false);
        initView();
    }

    private void initView() {
        av_anim = (ProgressBar) findViewById(getResources("id", "av_anim"));
    }

    public void dismissLoading() {
        av_anim.setVisibility(View.GONE);
        dismiss();
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
