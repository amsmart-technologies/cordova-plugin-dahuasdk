package cordova.plugin.dahuasdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.lechange.opensdk.api.InitParams;
import com.lechange.opensdk.api.LCOpenSDK_Api;
import com.lechange.opensdk.device.LCOpenSDK_DeviceInit;

public class LCDeviceEngine {
    private boolean sdkHasInit = false;
    private volatile static LCDeviceEngine lcDeviceEngine;
    public String accessToken;
    public CommonParam commonParam;

    public static LCDeviceEngine newInstance() {
        if (lcDeviceEngine == null) {
            synchronized (LCDeviceEngine.class) {
                if (lcDeviceEngine == null) {
                    lcDeviceEngine = new LCDeviceEngine();
                }
            }
        }
        return lcDeviceEngine;
    }

    public boolean init(String token, CommonParam commonParam) throws Throwable {
        if (TextUtils.isEmpty(token)) {
            throw new Exception("accessToken must not null");
        }
        if (commonParam == null) {
            throw new Exception("commonParam must not null");
        }
        this.commonParam = commonParam;
        this.accessToken = token;
        this.sdkHasInit = false;
        commonParam.checkParam();
        InitParams initParams = new InitParams(commonParam.getContext(), CONST.HOST.replace("https://", ""), token);
        LCOpenSDK_Api.initOpenApi(initParams);
        LCOpenSDK_DeviceInit.getInstance();
        sdkHasInit = true;
        return true;
    }

    public void deviceOnlineChangeNet(Activity activity, DHDevice device, CurWifiInfo wifiInfo) {
        if (!sdkHasInit) {
            return;
        }
        Intent intent = new Intent(commonParam.getContext(), DeviceWifiListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("DHDEVICE_INFO", device);
        bundle.putSerializable("DEVICE_CURRENT_WIFI_INFO", wifiInfo);
        intent.putExtras(bundle);
        activity.startActivityForResult(intent, 209);
    }

}
