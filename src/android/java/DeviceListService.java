package cordova.plugin.dahuasdk;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class DeviceListService {

    public DeviceDetailListData.Response deviceBaseList(String token, String openid, int pageNo, int pageSize) throws BusinessException {
        try {
            return dealDeviceDetailList(token, openid, pageNo, pageSize);
        } catch (BusinessException e) {
            throw e;
        }
    }

    private DeviceDetailListData.Response dealDeviceDetailList(String token, String openid, int pageNo, int pageSize) throws BusinessException {
        DeviceDetailListData.Response result = new DeviceDetailListData.Response();
        result.data = new DeviceDetailListData.ResponseData();
        result.data.deviceList = new ArrayList<>();
        DeviceDetailListData.Response deviceOpenDetailList = getDeviceDetailListFromCloud(token, openid, pageNo, pageSize);
        if (deviceOpenDetailList.data != null && deviceOpenDetailList.data.deviceList != null && deviceOpenDetailList.data.deviceList.size() > 0) {
            result.data = new DeviceDetailListData.ResponseData();
            result.data.count = deviceOpenDetailList.data.deviceList.size();
            result.data.total = deviceOpenDetailList.data.total;
            result.data.deviceList = new ArrayList<>();
            for (DeviceDetailListData.ResponseData.DeviceListBean a : deviceOpenDetailList.data.deviceList) {
                Gson gson = new Gson();
                DeviceDetailListData.ResponseData.DeviceListBean b = gson.fromJson(gson.toJson(a), DeviceDetailListData.ResponseData.DeviceListBean.class);
                b.deviceSource = 1;
                result.data.deviceList.add(b);
            }
            result.openBindId = deviceOpenDetailList.openBindId;
        }
        return result;
    }

    private DeviceDetailListData.Response getDeviceDetailListFromCloud(String token, String openid, int pageNo, int pageSize) throws BusinessException {
        DeviceDetailListData.Response result = new DeviceDetailListData.Response();
        JsonObject devices = OpenApiManager.listSubAccountDevice(token, openid, pageNo, pageSize);
        JsonElement policy = devices.get("policy");
        JsonElement total = devices.get("total");
        if (policy != null) {
            JsonArray array = policy.getAsJsonArray();
            StringBuilder deviceIds = new StringBuilder();
            if (array.size() > 0) {
                for (JsonElement jsonElement : array) {
                    String deviceid = jsonElement.getAsJsonObject().get("deviceId").getAsString();
                    deviceIds.append(deviceid).append(",");
                }
            }
            String devicesIds = deviceIds.length() > 0 ? deviceIds.substring(0, deviceIds.length() - 1) : "";
            if (!devicesIds.equals("")) {
                JsonObject json = OpenApiManager.queryOpenDeviceChannelInfo(token, devicesIds);
                DeviceListData.Response deviceList = new DeviceListData.Response();
                deviceList.parseData(json);
                if (deviceList.data == null || deviceList.data.devices == null || deviceList.data.devices.size() == 0) {
                    return result;
                }
                DeviceDetailListData deviceDetailListData = new DeviceDetailListData();
                List<DeviceDetailListData.RequestData.DeviceListBean> deviceListBeans = new ArrayList<>();
                for (DeviceListData.ResponseData.DeviceListElement deviceListElement : deviceList.data.devices) {
                    DeviceDetailListData.RequestData.DeviceListBean deviceListBean = new DeviceDetailListData.RequestData.DeviceListBean();
                    deviceListBean.deviceId = deviceListElement.deviceId;
                    StringBuilder stringBuilder = new StringBuilder();
                    if (deviceListElement.channels.size() > 0) {
                        for (DeviceListData.ResponseData.DeviceListElement.ChannelsElement channelsElement : deviceListElement.channels) {
                            stringBuilder.append(channelsElement.channelId).append(",");
                        }
                        deviceListBean.channelList = stringBuilder.substring(0, stringBuilder.length() - 1);
                    }
                    deviceListBeans.add(deviceListBean);
                }
                deviceDetailListData.data.deviceList = deviceListBeans;
                result = OpenApiManager.deviceOpenDetailList(deviceDetailListData);
                if (total != null) {
                    result.data.total = total.getAsInt();
                }
            }
        }
        return result;
    }
}
