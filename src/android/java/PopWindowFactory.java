package cordova.plugin.dahuasdk;

import android.app.Activity;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

public class PopWindowFactory {

    private final String TAG=PopWindowFactory.this.getClass().getSimpleName();

    public enum PopWindowType {LOADING}

    public BasePopWindow createPopWindow(final Activity context, CommonTitle commonTitle, PopWindowType type) {
        BasePopWindow popupWindow = null;
        final WindowManager.LayoutParams params = context.getWindow().getAttributes();
        if (type != PopWindowType.LOADING) {
            params.alpha = 0.5f;
            context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            context.getWindow().setAttributes(params);
        }
        switch (type) {
            case LOADING:
                popupWindow = createLoadingPop(context, commonTitle);
                break;
            default:
                break;
        }

        if (popupWindow != null) {
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    params.alpha = 1.0f;
                    context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    context.getWindow().setAttributes(params);
                }
            });
        }
        return popupWindow;
    }

    public BasePopWindow createLoadingPopWindow(final Activity context, View title) {
        final WindowManager.LayoutParams params = context.getWindow().getAttributes();
        BasePopWindow popupWindow = createLoadingPop(context, (CommonTitle) title);
        if (popupWindow != null) {
            popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    params.alpha = 1.0f;
                    context.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                    context.getWindow().setAttributes(params);
                }
            });
        }
        return popupWindow;
    }

    private BasePopWindow createLoadingPop(final Activity context, final CommonTitle commonTitle) {
        View view = LayoutInflater.from(context).inflate(getResources("layout", "common_progressdialog_layout1"), null);
        final BasePopWindow popupWindow = new LoadingPopWindow(view, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        int[] location = new int[2];
        View decorView = context.getWindow().getDecorView();
        Rect rect = new Rect();
        decorView.getWindowVisibleDisplayFrame(rect);
        int screenHeight = rect.bottom;
        commonTitle.getLocationOnScreen(location);
        int height = screenHeight - location[1] - commonTitle.getMeasuredHeight();
        popupWindow.setHeight(height);
        popupWindow.showAsDropDown(commonTitle);
        return popupWindow;
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
