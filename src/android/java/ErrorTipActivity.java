package cordova.plugin.dahuasdk;

import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ErrorTipActivity extends BaseManagerFragmentActivity implements CommonTitle.OnTitleClickListener {

    public static String ERROR_PARAMS = "error_params";

    private ImageView imageView;
    private TextView textView;
    private TextView textView1;
    private TextView mHelpLinkTxt,mHelpPhoneTv;
    int errorcode;

    @Override
    protected void initLayout() {
        setContentView(getResources("layout", "activity_error_tip"));
    }

    @Override
    protected View initTitle() {
        CommonTitle title = (CommonTitle) findViewById(getResources("id", "error_tip_title"));
        title.initView(getResources("drawable", "mobile_common_title_back"), 0, getResources("string", "dahua_network_config"));
        title.setOnTitleClickListener(this);
        return title;
    }

    @Override
    protected void initView() {
        super.initView();
        imageView = findViewById(getResources("id", "tip_img"));
        textView = findViewById(getResources("id", "tip_txt"));
        textView1 = findViewById(getResources("id", "tip_txt_1"));
        mHelpLinkTxt = findViewById(getResources("id", "help_link"));
        mHelpLinkTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        mHelpPhoneTv = findViewById(getResources("id", "tv_help_phone"));
        mHelpPhoneTv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        mHelpPhoneTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        if (errorcode == 1) {
            imageView.setBackgroundResource(getResources("drawable", "adddevice_icon_wifiexplain"));
            textView1.setVisibility(View.VISIBLE);
            textView.setText(getResources("string", "dahua_device_tip_not_support_5g_1"));
        } else if (errorcode == 2) {
            imageView.setBackgroundResource(getResources("drawable", "adddevice_icon_wifiexplain_choosewifi"));
            textView.setText(getResources("string", "dahua_add_device_tip_wifi_name"));
        }
    }

    @Override
    protected void initData() {
        errorcode = getIntent().getIntExtra(ERROR_PARAMS, 0);

    }

    @Override
    public void onCommonTitleClick(int id) {
        switch (id) {
            case CommonTitle.ID_LEFT:
                ErrorTipActivity.this.finish();
                break;
        }
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
