package cordova.plugin.dahuasdk;

import android.content.Intent;

public interface IActivityResultDispatch {
    public interface OnActivityResultListener {
        void onActivityResult(int requestCode, int resultCode, Intent data);
    }

    public void addOnActivityResultListener(OnActivityResultListener listener);

    public void removeOnActivityResultListener(OnActivityResultListener listener);
}
