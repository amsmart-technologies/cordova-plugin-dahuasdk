package cordova.plugin.dahuasdk;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;

import com.company.NetSDK.DEVICE_NET_INFO_EX;
import com.dahua.mobile.utility.network.DHWifiUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lechange.common.configwifi.LCSmartConfig;
import com.lechange.opensdk.bluetooth.LCOpenSDK_BluetoothConfig;
import com.lechange.opensdk.configwifi.LCOpenSDK_ConfigWifi;
import com.lechange.opensdk.searchwifi.LCOpenSDK_SearchWiFi;
import com.lechange.opensdk.searchwifi.WlanInfo;
import com.lechange.opensdk.softap.LCOpenSDK_SoftAPConfig;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

import java.util.List;

public class DahuaSDK extends CordovaPlugin implements SearchDeviceManager.ISearchDeviceListener {
    private static final String TAG = DahuaSDK.class.getSimpleName();
    private CallbackContext callback;
    private SearchDeviceManager mServiceManager;
    private DHWifiUtil mDHWifiUtil;
    private boolean mIsHasInitbyIp;
    private String mDeviceSnCode;
    private PluginResult pluginResult;
    private ClassInstanceManager classInstanceManager;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        this.classInstanceManager = ClassInstanceManager.newInstance(cordova.getContext());
        Log.d(TAG, action);
        Log.d(TAG, args.toString());
        switch (action) {
            case "setEnv":
                this.setEnv(args, callbackContext);
                break;
            case "getToken":
                this.getToken(args, callbackContext);
                break;
            case "initialize":
                this.initialize(args, callbackContext);
                break;
            case "parseQRcode":
                this.parseQRcode(args, callbackContext);
                break;
            case "createSubAccount":
                this.createSubAccount(args, callbackContext);
                break;
            case "getOpenIdByAccount":
                this.getOpenIdByAccount(args, callbackContext);
                break;
            case "getSubAccountToken":
                this.getSubAccountToken(args, callbackContext);
                break;
            case "listSubAccount":
                this.listSubAccount(args, callbackContext);
                break;
            case "deleteSubAccount":
                this.deleteSubAccount(args, callbackContext);
                break;
            case "listSubAccountDevice":
                this.listSubAccountDevice(args, callbackContext);
                break;
            case "subAccountDeviceList":
                this.subAccountDeviceList(args, callbackContext);
                break;
            case "listDeviceDetailsByPage":
                this.listDeviceDetailsByPage(args, callbackContext);
                break;
            case "addDeviceToSubAccount":
                this.addDeviceToSubAccount(args, callbackContext);
                break;
            case "getDevicesList":
                this.getDevicesList(args, callbackContext);
                break;
            case "getSnapShot":
                this.getSnapShot(args, callbackContext);
                break;
            case "openDevicePlay":
                this.openDevicePlay(args, callbackContext);
                break;
            case "openDeviceDetail":
                this.openDeviceDetail(args, callbackContext);
                break;
            case "deviceInfoBeforeBind":
                this.deviceInfoBeforeBind(args, callbackContext);
                break;
            case "userDeviceBind":
                this.userDeviceBind(args, callbackContext);
                break;
            case "deviceLeadingInfo":
                this.deviceLeadingInfo(args, callbackContext);
                break;
            case "startSearchService":
                this.startSearchService(callbackContext);
                break;
            case "stopSearchService":
                this.stopSearchService(callbackContext);
                break;
            case "getDeviceNetInfo":
                this.getDeviceNetInfo(args, callbackContext);
                break;
            case "startDevInitByIp":
                this.startDevInitByIp(args, callbackContext);
                break;
            case "checkWifi":
                this.checkWifi(callbackContext);
                break;
            case "startSmartConfig":
                this.startSmartConfig(args, callbackContext);
                break;
            case "stopSmartConfig":
                this.stopSmartConfig(callbackContext);
                break;
            case "listenForSearchedDevices":
                this.listenForSearchedDevices(args, callbackContext);
                break;
            case "getWifiListSoftAp":
                this.getWifiListSoftAp(args, callbackContext);
                break;
            case "connectDeviceToWifi":
                this.connectDeviceToWifi(args, callbackContext);
                break;
            case "connectPhoneToWifi":
                this.connectPhoneToWifi(args, callbackContext);
                break;
            case "searchDeviceBLE":
                this.searchDeviceBLE(args, callbackContext);
                break;
            case "modifyDeviceName":
                this.modifyDeviceName(args, callbackContext);
                break;
            case "unbindDevice":
                this.unbindDevice(args, callbackContext);
                break;
            case "bindDeviceChannelInfo":
                this.bindDeviceChannelInfo(args, callbackContext);
                break;
            case "modifyDeviceAlarmStatus":
                this.modifyDeviceAlarmStatus(args, callbackContext);
                break;
            case "deviceVersionList":
                this.deviceVersionList(args, callbackContext);
                break;
            case "wifiAround":
                this.wifiAround(args, callbackContext);
                break;
            case "controlDeviceWifi":
                this.controlDeviceWifi(args, callbackContext);
                break;
            case "timeZoneQueryByDay":
                this.timeZoneQueryByDay(args, callbackContext);
                break;
            case "timeZoneConfigByDay":
                this.timeZoneConfigByDay(args, callbackContext);
                break;
            case "setMessageCallback":
                this.setMessageCallback(args, callbackContext);
                break;
            case "getMessageCallback":
                this.getMessageCallback(args, callbackContext);
                break;
            case "getAlarmMessage":
                this.getAlarmMessage(args, callbackContext);
                break;
            case "deleteAlarmMessage":
                this.deleteAlarmMessage(args, callbackContext);
                break;
            case "queryCloudRecordCallNum":
                this.queryCloudRecordCallNum(args, callbackContext);
                break;
            case "openCloudRecord":
                this.openCloudRecord(args, callbackContext);
                break;
            case "getDeviceCloud":
                this.getDeviceCloud(args, callbackContext);
                break;
            case "deviceSdcardStatus":
                this.deviceSdcardStatus(args, callbackContext);
                break;
            case "deviceStorage":
                this.deviceStorage(args, callbackContext);
                break;
            case "recoverSDCard":
                this.recoverSDCard(args, callbackContext);
                break;
            case "setDeviceSnap":
                this.setDeviceSnap(args, callbackContext);
                break;
            case "queryLocalRecordPlan":
                this.queryLocalRecordPlan(args, callbackContext);
                break;
            case "setLocalRecordPlanRules":
                this.setLocalRecordPlanRules(args, callbackContext);
                break;
            case "restartDevice":
                this.restartDevice(args, callbackContext);
                break;
            case "modifyDevicePwd":
                this.modifyDevicePwd(args, callbackContext);
                break;
            case "upgradeProcessDevice":
                this.upgradeProcessDevice(args, callbackContext);
                break;
            case "upgradeDevice":
                this.upgradeDevice(args, callbackContext);
                break;
            case "wakeUpDevice":
                this.wakeUpDevice(args, callbackContext);
                break;
            case "deviceOnline":
                this.deviceOnline(args, callbackContext);
                break;
            case "getDevicePowerInfo":
                this.getDevicePowerInfo(args, callbackContext);
                break;
            case "getDeviceCameraStatus":
                this.getDeviceCameraStatus(args, callbackContext);
                break;
            case "setDeviceCameraStatus":
                this.setDeviceCameraStatus(args, callbackContext);
                break;
            case "checkOverlayPermission":
                this.checkOverlayPermission(args, callbackContext);
                break;
            default:
                return false;
        }
        return true;
    }

    private void checkOverlayPermission(JSONArray args, CallbackContext callbackContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean canDrawOverlays = Settings.canDrawOverlays(cordova.getContext());
            if (!canDrawOverlays) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + cordova.getActivity().getPackageName()));
                cordova.getActivity().startActivity(intent);
            }
        }
        callbackContext.success();
    }

    private void setEnv(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String appId = args.getString(0);
                String appSecret = args.getString(1);
                String url = args.getString(2);
                CONST.makeEnv(url, appId, appSecret);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(cordova.getContext());
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("appId", appId);
                editor.putString("appSecret", appSecret);
                editor.putString("url", url);
                editor.apply();
                callbackContext.success();
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getToken(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String accessToken = OpenApiManager.getToken();
                callbackContext.success(accessToken);
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void initialize(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                CommonParam commonParam = new CommonParam();
                commonParam.setEnvirment(CONST.HOST);
                commonParam.setContext(cordova.getActivity().getApplication());
                commonParam.setAppId(CONST.APPID);
                commonParam.setAppSecret(CONST.SECRET);
                LCDeviceEngine.newInstance().init(token, commonParam);
                BluetoothPermissionHelper.requestBluetoothPermissions(cordova.getActivity());
                callbackContext.success("OK");
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void parseQRcode(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String scanStr = args.getString(0);
                cordova.plugin.dahuasdk.ScanResult scanResult = ScanResultFactory.scanResult(scanStr);
                callbackContext.success(new Gson().toJson(scanResult));
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void createSubAccount(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String account = args.getString(1);
                JsonObject subAccount = OpenApiManager.createSubAccount(token, account);
                callbackContext.success(subAccount.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getOpenIdByAccount(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String account = args.getString(1);
                JsonObject subAccount = OpenApiManager.getOpenIdByAccount(token, account);
                callbackContext.success(subAccount.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getSubAccountToken(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String openid = args.getString(1);
                String accessToken = OpenApiManager.getSubAccountToken(token, openid);
                callbackContext.success(accessToken);
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void listSubAccount(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                int pageNo = args.getInt(1);
                int pageSize = args.getInt(2);
                JsonObject subAccounts = OpenApiManager.listSubAccount(token, pageNo, pageSize);
                callbackContext.success(subAccounts.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void deleteSubAccount(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String openid = args.getString(1);
                JsonObject subAccounts = OpenApiManager.deleteSubAccount(token, openid);
                callbackContext.success(subAccounts.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void listSubAccountDevice(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String openid = args.getString(1);
                int pageNo = args.getInt(2);
                int pageSize = args.getInt(3);
                JsonObject devices = OpenApiManager.listSubAccountDevice(token, openid, pageNo, pageSize);
                callbackContext.success(devices.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void subAccountDeviceList(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                int pageNo = args.getInt(1);
                int pageSize = args.getInt(2);
                JsonObject devices = OpenApiManager.subAccountDeviceList(token, pageNo, pageSize);
                callbackContext.success(devices.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void listDeviceDetailsByPage(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                int page = args.getInt(1);
                int pageSize = args.getInt(2);
                JsonObject devices = OpenApiManager.listDeviceDetailsByPage(token, page, pageSize);
                callbackContext.success(devices.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void addDeviceToSubAccount(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String openid = args.getString(1);
                String policy = args.getString(2);
                JsonObject result = OpenApiManager.addPolicy(token, openid, policy);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getDevicesList(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String openid = args.getString(1);
                int pageNo = args.getInt(2);
                int pageSize = args.getInt(3);
                DeviceListService service = new DeviceListService();
                DeviceDetailListData.Response list = service.deviceBaseList(token, openid, pageNo, pageSize);
                callbackContext.success(list.data.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getSnapShot(JSONArray args, CallbackContext callbackContext) {
        try {
            String device = args.getString(0);
            String channelId = args.getString(1);
            Gson gson = new Gson();
            DeviceDetailListData.ResponseData.DeviceListBean deviceListBean = gson.fromJson(device, DeviceDetailListData.ResponseData.DeviceListBean.class);
            DeviceLocalCacheData deviceLocalCacheData = new DeviceLocalCacheData();
            deviceLocalCacheData.setDeviceId(deviceListBean.deviceId);
            deviceLocalCacheData.setChannelId(channelId);
            DeviceLocalCacheService deviceLocalCacheService = this.classInstanceManager.getDeviceLocalCacheService();
            deviceLocalCacheService.findLocalCache(deviceLocalCacheData, new IGetDeviceInfoCallBack.IDeviceCacheCallBack() {
                @Override
                public void deviceCache(DeviceLocalCacheData deviceLocalCacheData) {
                    String base64Image = MediaPlayHelper.picDrawableString(deviceLocalCacheData.getPicPath());
                    callbackContext.success(base64Image);
                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                    callbackContext.success("null");
                }
            });
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }

    }

    private void openDevicePlay(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            this.callback = callbackContext;
            try {
                String device = args.getString(0);
                String channelId = args.getString(1);
                boolean playbackEnabled = args.getBoolean(2);
                boolean callBellEvent = args.getBoolean(3);
                Gson gson = new Gson();
                DeviceDetailListData.ResponseData.DeviceListBean deviceListBean = gson.fromJson(device, DeviceDetailListData.ResponseData.DeviceListBean.class);
                int index = 0;
                for (int i = 0; i < deviceListBean.channelList.size(); i++) {
                    if (deviceListBean.channelList.get(i).channelId.equals(channelId)) {
                        index = i;
                    }
                }
                deviceListBean.checkedChannel = index;
                if (deviceListBean.deviceStatus.equals("online") && deviceListBean.channelList.get(index).channelStatus.equals("online")) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(MethodConst.ParamConst.deviceDetail, deviceListBean);
                    bundle.putBoolean("playbackEnabled", playbackEnabled);
                    bundle.putBoolean("callBellEvent", callBellEvent);
                    Intent intent = new Intent(cordova.getContext(), DeviceOnlineMediaPlayActivity.class);
                    intent.putExtras(bundle);
                    cordova.setActivityResultCallback(this);
                    cordova.getActivity().startActivityForResult(intent, 1);
                } else {
                    callbackContext.error("DEVICE_NOT_ONLINE");
                }
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void openDeviceDetail(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            this.callback = callbackContext;
            try {
                String device = args.getString(0);
                Gson gson = new Gson();
                DeviceDetailListData.ResponseData.DeviceListBean deviceListBean = gson.fromJson(device, DeviceDetailListData.ResponseData.DeviceListBean.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(MethodConst.ParamConst.deviceDetail, deviceListBean);
                bundle.putString(MethodConst.ParamConst.fromList, MethodConst.ParamConst.fromList);
                Intent intent = new Intent(cordova.getContext(), DeviceDetailActivity.class);
                intent.putExtras(bundle);
                cordova.setActivityResultCallback(this);
                cordova.getActivity().startActivityForResult(intent, 1);
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void deviceInfoBeforeBind(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String deviceCodeModel = args.getString(2);
                String deviceModelName = args.getString(3);
                String ncCode = args.getString(4);
                String productId = args.getString(5);
                JsonObject result = OpenApiManager.deviceInfoBeforeBind(token, deviceId, deviceCodeModel, deviceModelName, ncCode, productId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void userDeviceBind(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String device = args.getString(1);
                String code = args.getString(2);
                JsonObject result = OpenApiManager.userDeviceBind(token, device, code);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void deviceLeadingInfo(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceModel = args.getString(1);
                JsonObject result = OpenApiManager.deviceLeadingInfo(token, deviceModel);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void startSearchService(CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                if (mServiceManager == null) {
                    mServiceManager = SearchDeviceManager.getInstance();
                    mServiceManager.setContext(cordova.getContext());
                }
                mServiceManager.connnectService();
                callbackContext.success("OK");
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void stopSearchService(CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                if (mServiceManager != null) {
                    mServiceManager.checkSearchDeviceServiceDestory();
                }
                callbackContext.success("OK");
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getDeviceNetInfo(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                if (mServiceManager != null) {
                    String snCode = args.getString(0);
                    DEVICE_NET_INFO_EX info = mServiceManager.getDeviceNetInfo(snCode);
                    callbackContext.success(new Gson().toJson(info));
                } else {
                    callbackContext.success(new Gson().toJson(null));
                }
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void startDevInitByIp(JSONArray args, CallbackContext callbackContext) {
        try {
            if (mServiceManager != null) {
                String deviceSerialNumber = args.getString(0);
                String deviceInitPassword = args.getString(1);
                DEVICE_NET_INFO_EX info = mServiceManager.getDeviceNetInfo(deviceSerialNumber);
                @SuppressLint("HandlerLeak")
                LCBusinessHandler initDevHandler = new LCBusinessHandler() {
                    @Override
                    public void handleBusiness(Message msg) {
                        if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                            boolean inited = (boolean) msg.obj;
                            if (inited) {
                                callbackContext.success("OK");
                            } else {
                                if (mIsHasInitbyIp) {
                                    callbackContext.error("INITED_BY_IP");
                                } else {
                                    DEVICE_NET_INFO_EX deviceNetInfoEx = mServiceManager.getDeviceNetInfo(deviceSerialNumber);
                                    initDevAccountByIp(deviceNetInfoEx, deviceInitPassword, callbackContext);
                                }
                            }
                        }
                    }
                };
                DeviceAddModel.newInstance().initDev(info, deviceInitPassword, initDevHandler);
            } else {
                callbackContext.success(new Gson().toJson(null));
            }
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }

    private void initDevAccountByIp(DEVICE_NET_INFO_EX deviceNetInfo, final String password, CallbackContext callbackContext) {
        @SuppressLint("HandlerLeak")
        LCBusinessHandler initDevHandler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                mIsHasInitbyIp = true;
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    boolean inited = (boolean) msg.obj;
                    if (inited) {
                        callbackContext.success("OK");
                    } else {
                        callbackContext.error("NOT_INITED");
                    }
                }
            }
        };
        DeviceAddModel.newInstance().initDevByIp(deviceNetInfo, password, initDevHandler);
    }

    private void checkWifi(CallbackContext callbackContext) {
        mDHWifiUtil = new DHWifiUtil(cordova.getContext());
        if (Utils4AddDevice.isWifi(cordova.getContext())) {
            callbackContext.success(getCurWifiName());
        } else {
            callbackContext.success("null");
        }
    }

    private String getCurWifiName() {
        String curWifiName = mDHWifiUtil.getCurrentWifiInfo().getSSID().replaceAll("\"", "");
        if (LCConfiguration.UNKNOWN_SSID.equals(curWifiName)) {
            curWifiName = "";
        }
        return curWifiName;
    }

    private void startSmartConfig(JSONArray args, CallbackContext callbackContext) {
        try {
            ScanResult scanResult = mDHWifiUtil.getScanResult();
            String encryptionCap = "";
            if (scanResult != null) {
                encryptionCap = scanResult.capabilities == null ? "" : scanResult.capabilities;
            }
            String deviceSn = args.getString(0);
            mDeviceSnCode = deviceSn;
            String ssid = args.getString(1);
            String ssid_pwd = args.getString(2);
            /**
             * cfgType: Hálózati terjesztési mód, 1 - multicast, 2 - broadcast, 4-sonic wave, amely bitenként vagy működéssel kombinálva használható;
             * voiceConfigFilePath: A hanghullámokkal párosított hangfájl generációs útvonala, amelyet a felső réteg hívója ad át;
             * enableBgMusic: Akár háttérhangot használunk hanghullám-párosításhoz, az alapértelmezett a kakukkhang;
             * frekvencia: a jelek küldéséhez és fogadásához párosított hanghullám alapsávú frekvenciája;
             * fsk_tx_mode: fsk küldési mód, 0 - új fsk küldési mód, 1 - régi fsk küldési mód, 2 - új és régi fsk hullám küldési mód;
             */
            int fskTxMode = 1;
            boolean enableBgMusic = true;
            LCOpenSDK_ConfigWifi.configWifiStart(deviceSn, ssid, ssid_pwd, encryptionCap, LCSmartConfig.ConfigType.LCConfigWifi_Type_ALL, enableBgMusic, 11000, fskTxMode);
            callbackContext.success("OK");
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }

    private void stopSmartConfig(CallbackContext callbackContext) {
        try {
            LCOpenSDK_ConfigWifi.configWifiStop();
            callbackContext.success("OK");
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }

    private void listenForSearchedDevices(JSONArray args, CallbackContext callbackContext) {
        try {
            String deviceSn = args.getString(0);
            mDeviceSnCode = deviceSn;
            this.callback = callbackContext;
            mServiceManager.registerListener(this);
            this.pluginResult = new PluginResult(PluginResult.Status.OK, "null");
            this.pluginResult.setKeepCallback(true);
            this.callback.sendPluginResult(pluginResult);
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }

    private void getWifiListSoftAp(JSONArray args, CallbackContext callbackContext) {
        try {
            mDHWifiUtil = new DHWifiUtil(cordova.getContext());
            String gatewayip = mDHWifiUtil.getGatewayIp();
            String pwd = args.getString(0);
            boolean isNotNeedLogin = args.getBoolean(1);
            @SuppressLint("HandlerLeak")
            LCBusinessHandler getWifiListHandler = new LCBusinessHandler() {
                @Override
                public void handleBusiness(Message msg) {
                    if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                        List<WlanInfo> mListData = (List<WlanInfo>) msg.obj;
                        callbackContext.success(new Gson().toJson(mListData));
                    } else if (msg.what == HandleMessageCode.HMC_BATCH_MIDDLE_RESULT) {
                        callbackContext.error("WRONG_PWD");
                    } else {
                        callbackContext.error("ERROR");
                    }
                }
            };
            LCOpenSDK_SearchWiFi.getSoftApWifiList(gatewayip, isNotNeedLogin ? "" : pwd, getWifiListHandler);
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }

    private void connectDeviceToWifi(JSONArray args, CallbackContext callbackContext) {
        try {
            String wifiName = args.getString(0);
            String wifiPwd = args.getString(1);
            String deviceId = args.getString(2);
            String devicePwd = args.getString(3);
            boolean hasSc = args.getBoolean(4);
            int wifiEncry = args.getInt(5);
            boolean isNeedInit = args.getBoolean(6);
            @SuppressLint("HandlerLeak")
            LCBusinessHandler connectWifiHandler = new LCBusinessHandler() {
                @Override
                public void handleBusiness(Message msg) {
                    boolean connectResult = msg.arg1 == 0;
                    if (connectResult) {
                        clearNetWork();
                        connectWifi(wifiName, wifiPwd);
                        callbackContext.success("OK");
                    } else {
                        callbackContext.error("NOT_CONNECTED");
                    }
                }
            };
            LCOpenSDK_SoftAPConfig.startSoftAPConfig(wifiName, wifiPwd, wifiEncry, mDHWifiUtil.getGatewayIp(), devicePwd, isNeedInit, connectWifiHandler, 30 * 1000);
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }

    private void connectPhoneToWifi(JSONArray args, CallbackContext callbackContext) {
        try {
            String wifiName = args.getString(0);
            String wifiPwd = args.getString(1);
            if (clearNetWork() && connectWifi(wifiName, wifiPwd)) {
                callbackContext.success("OK");
            } else {
                callbackContext.error("NOT_CONNECTED");
            }
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }

    public boolean clearNetWork() {
        ConnectivityManager connectivityManager = (ConnectivityManager) cordova.getContext().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return connectivityManager.bindProcessToNetwork(null);
        } else {
            return ConnectivityManager.setProcessDefaultNetwork(null);
        }
    }

    public boolean connectWifi(String wifiName, String wifiPwd) {
        DHWifiUtil wifiUtil = new DHWifiUtil(cordova.getContext());
        return wifiUtil.connectWifi(wifiName, wifiPwd);
    }

    @Override
    public void onDeviceSearched(String sncode, DEVICE_NET_INFO_EX info) {
        if (info != null && mDeviceSnCode.equalsIgnoreCase(sncode)) {
            mServiceManager.unRegisterListener(this);
            this.pluginResult = new PluginResult(PluginResult.Status.OK, new Gson().toJson(info));
            this.pluginResult.setKeepCallback(false);
            this.callback.sendPluginResult(pluginResult);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.callback.success("OK");
    }

    public void searchDeviceBLE(JSONArray args, CallbackContext callbackContext) {
        try {
            String wifiName = args.getString(0);
            String wifiPwd = args.getString(1);
            String deviceId = args.getString(2);
            String deviceProductId = args.getString(3);
            Handler resultHandler = new Handler(Looper.myLooper()) {
                @Override
                public void handleMessage(@NonNull Message msg) {
                    super.handleMessage(msg);
                    Log.d(TAG, "handleMessage : msg.what:" + msg.what);
                    switch (msg.what) {
                        case 0:
                            callbackContext.success("OK");
                            break;
                        case -1:
                            callbackContext.error("NOT_CONNECTED");
                            break;
                    }
                }
            };
            LCOpenSDK_BluetoothConfig.startBleConfig(cordova.getContext(), deviceId, deviceProductId, wifiName, wifiPwd, resultHandler);
        } catch (Throwable e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }
    }

    private void modifyDeviceName(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                String name = args.getString(3);
                boolean b = OpenApiManager.modifyDeviceName2(token, deviceId, channelId, name);
                if (b) {
                    callbackContext.success("OK");
                } else {
                    callbackContext.error("null");
                }
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void unbindDevice(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                boolean b = OpenApiManager.unBindDevice2(token, deviceId);
                if (b) {
                    callbackContext.success("OK");
                } else {
                    callbackContext.error("null");
                }
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void bindDeviceChannelInfo(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                JsonObject result = OpenApiManager.bindDeviceChannelInfo2(token, deviceId, channelId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void modifyDeviceAlarmStatus(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                boolean enable = args.getBoolean(3);
                boolean b = OpenApiManager.modifyDeviceAlarmStatus2(token, deviceId, channelId, enable);
                if (b) {
                    callbackContext.success("OK");
                } else {
                    callbackContext.error("null");
                }
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void deviceVersionList(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.deviceVersionList2(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void wifiAround(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.wifiAround2(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void controlDeviceWifi(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String ssid = args.getString(2);
                String bssid = args.getString(3);
                String password = args.getString(4);
                boolean b = OpenApiManager.controlDeviceWifi2(token, deviceId, ssid, bssid, true, password);
                if (b) {
                    callbackContext.success("OK");
                } else {
                    callbackContext.error("null");
                }
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void timeZoneQueryByDay(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.timeZoneQueryByDay(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void timeZoneConfigByDay(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String areaIndex = args.getString(2);
                String timeZone = args.getString(3);
                String beginSunTime = args.getString(4);
                String endSunTime = args.getString(5);
                JsonObject result = OpenApiManager.timeZoneConfigByDay(token, deviceId, areaIndex, timeZone, beginSunTime, endSunTime);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void setMessageCallback(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String status = args.getString(1);
                String callbackUrl = args.getString(2);
                String callbackFlag = args.getString(3);
                JsonObject result = OpenApiManager.setMessageCallback(token, status, callbackUrl, callbackFlag);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getMessageCallback(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                JsonObject result = OpenApiManager.getMessageCallback(token);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getAlarmMessage(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                String beginTime = args.getString(3);
                String endTime = args.getString(4);
                String count = args.getString(5);
                String nextAlarmId = args.getString(6);
                JsonObject result = OpenApiManager.getAlarmMessage(token, deviceId, channelId, beginTime, endTime, count, nextAlarmId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void deleteAlarmMessage(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String indexId = args.getString(2);
                String channelId = args.getString(3);
                JsonObject result = OpenApiManager.deleteAlarmMessage(token, deviceId, indexId, channelId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void queryCloudRecordCallNum(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                Long strategyId = args.getLong(1);
                JsonObject result = OpenApiManager.queryCloudRecordCallNum(token, strategyId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void openCloudRecord(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                Long strategyId = args.getLong(3);
                String deviceCloudId = args.getString(4);
                JsonObject result = OpenApiManager.openCloudRecord(token, deviceId, channelId, strategyId, deviceCloudId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getDeviceCloud(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                JsonObject result = OpenApiManager.getDeviceCloud(token, deviceId, channelId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void deviceSdcardStatus(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.deviceSdcardStatus(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void deviceStorage(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.deviceStorage(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void recoverSDCard(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.recoverSDCard(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void setDeviceSnap(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                JsonObject result = OpenApiManager.setDeviceSnap(token, deviceId, channelId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void queryLocalRecordPlan(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                JsonObject result = OpenApiManager.queryLocalRecordPlan(token, deviceId, channelId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void setLocalRecordPlanRules(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                String rules = args.getString(3);
                Gson gson = new Gson();
                DeviceRecordPlanData.RequestData data = gson.fromJson("{rules:" + rules + "}", DeviceRecordPlanData.RequestData.class);
                JsonObject result = OpenApiManager.setLocalRecordPlanRules(token, deviceId, channelId, data);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void restartDevice(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.restartDevice(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void modifyDevicePwd(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String oldPwd = args.getString(2);
                String newPwd = args.getString(3);
                JsonObject result = OpenApiManager.modifyDevicePwd(token, deviceId, oldPwd, newPwd);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void upgradeProcessDevice(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.upgradeProcessDevice(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void upgradeDevice(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.upgradeDevice(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void wakeUpDevice(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.wakeUpDevice(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void deviceOnline(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.deviceOnline(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getDevicePowerInfo(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                JsonObject result = OpenApiManager.getDevicePowerInfo(token, deviceId);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void getDeviceCameraStatus(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                String enableType = args.getString(3);
                JsonObject result = OpenApiManager.getDeviceCameraStatus(token, deviceId, channelId, enableType);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    private void setDeviceCameraStatus(JSONArray args, CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> {
            try {
                String token = args.getString(0);
                String deviceId = args.getString(1);
                String channelId = args.getString(2);
                String enableType = args.getString(3);
                boolean enable = args.getBoolean(4);
                JsonObject result = OpenApiManager.setDeviceCameraStatus(token, deviceId, channelId, enableType, enable);
                callbackContext.success(result.toString());
            } catch (Throwable e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            }
        });
    }

    public void handleDeviceCallEvent(Application application, String mac, String deviceId, String channelId) {
        try {
            this.classInstanceManager = ClassInstanceManager.newInstance(application.getApplicationContext());
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(application.getApplicationContext());
            String appId = sharedPreferences.getString("appId", "");
            String appSecret = sharedPreferences.getString("appSecret", "");
            String url = sharedPreferences.getString("url", "");
            CONST.makeEnv(url, appId, appSecret);
            String accessToken = OpenApiManager.getToken();
            CommonParam commonParam = new CommonParam();
            commonParam.setEnvirment(CONST.HOST);
            commonParam.setContext(application);
            commonParam.setAppId(CONST.APPID);
            commonParam.setAppSecret(CONST.SECRET);
            LCDeviceEngine.newInstance().init(accessToken, commonParam);
            JsonObject subAccount = OpenApiManager.getOpenIdByAccount(accessToken, mac.concat("@amsmart.hu"));
            String openid = subAccount.get("openid").getAsString();
            String subAccountToken = OpenApiManager.getSubAccountToken(accessToken, openid);
            DeviceDetailListData.ResponseData.DeviceListBean deviceListBean = OpenApiManager.subAccountDeviceInfo(subAccountToken, deviceId, channelId);
            if (deviceListBean != null) {
                int index = 0;
                for (int i = 0; i < deviceListBean.channelList.size(); i++) {
                    if (deviceListBean.channelList.get(i).channelId.equals(channelId)) {
                        index = i;
                    }
                }
                if (deviceListBean.deviceStatus.equals("online") && deviceListBean.channelList.get(index).channelStatus.equals("online")) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(MethodConst.ParamConst.deviceDetail, deviceListBean);
                    bundle.putBoolean("playbackEnabled", true);
                    bundle.putBoolean("callBellEvent", true);
                    Intent intent = new Intent(application.getApplicationContext(), DeviceOnlineMediaPlayActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.setAction(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    intent.putExtras(bundle);
                    application.getApplicationContext().startActivity(intent);
                } else {
                    throw new Error("DEVICE_NOT_ONLINE");
                }
            } else {
                throw new Error("DEVICE_NULL");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
