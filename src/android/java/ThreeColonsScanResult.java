package cordova.plugin.dahuasdk;

import static cordova.plugin.dahuasdk.PseudoJsonScanResult.filterInvalidString;
import static cordova.plugin.dahuasdk.PseudoJsonScanResult.filterInvalidString4Type;

/**
 * Xxx:xxx:xxx format qr code
 *
 * xxx：xxx:xxx格式二维码
 */
public class ThreeColonsScanResult extends ScanResult {

    /**
     * Create a new instance TwoColonsScanResult.
     * @param scanString
     *
     * 创建一个新的实例TwoColonsScanResult.
     * @param scanString
     */
    public ThreeColonsScanResult(String scanString,String split) {
        super(scanString);
        String[] resultStrings = scanString.split(split);
        this.setProductId(filterInvalidString(resultStrings[1]));
        this.setSn(filterInvalidString4Type(resultStrings[2]));
        this.setSc(filterInvalidString(resultStrings[3]));
    }

}
