package cordova.plugin.dahuasdk;

import android.content.DialogInterface;
import androidx.annotation.LayoutRes;

public interface IProgressDialogControlView {

    void showProgressDialog(@LayoutRes int layoutId);

    void dissmissProgressDialog();

    void cancleProgressDialog();

   void setProgressDialogCancelable(boolean flag);

   void setProgressDialogCancelListener(DialogInterface.OnCancelListener cancelListener);

}
