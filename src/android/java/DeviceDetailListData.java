package cordova.plugin.dahuasdk;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DeviceDetailListData implements Serializable {
    public DeviceDetailListData.RequestData data = new DeviceDetailListData.RequestData();

    public static class RequestData implements Serializable {
        public String token;
        public List<DeviceListBean> deviceList;

        @Override
        public String toString() {
            Gson g = new Gson();
            return g.toJson(this);
        }

        public static class DeviceListBean implements Serializable {
            public String deviceId;
            public String channelList;
            public String apList;

            @Override
            public String toString() {
                Gson g = new Gson();
                return g.toJson(this);
            }
        }
    }

    public static class Response {
        public DeviceDetailListData.ResponseData data;
        public long baseBindId = -1;
        public long openBindId = -1;

        public void parseData(JsonObject json) {
            Gson gson = new Gson();
            this.data = gson.fromJson(json.toString(), DeviceDetailListData.ResponseData.class);
        }
    }

    public static class ResponseData implements Serializable {
        public int count;
        public int total;

        public List<DeviceListBean> deviceList;

        @Override
        public String toString() {
            Gson g = new Gson();
            return g.toJson(this);
        }

        public static class DeviceListBean implements Serializable {
            public String deviceId;
            public String deviceStatus;
            public String deviceModel;
            public String catalog;
            public String brand;
            public String deviceVersion;
            public String deviceName;
            public String deviceAbility;
            public String accessType;
            public int checkedChannel;
            public String playToken;
            public String channelNum;
            public String permission;
            public int deviceSource;
            public List<ChannelsBean> channelList = new ArrayList<>();
            public List<AplistBean> aplist;
            public String productId = "";
            public boolean tlsEnable = false;
            public boolean multiFlag = false;

            @Override
            public String toString() {
                Gson g = new Gson();
                return g.toJson(this);
            }

            public static class ChannelsBean implements Serializable {
                public String channelId;
                public String deviceId;
                public String channelName;
                public String channelAbility;
                public String channelStatus;
                public String channelPicUrl;
                public String remindStatus;
                public String cameraStatus;
                public String storageStrategyStatus;
                public String shareStatus;
                public String shareFunctions;
                public String permission;
                public List<ResolutionBean> resolutions = new ArrayList<>();

                public static class ResolutionBean implements Serializable {
                    public String name;
                    public int imageSize;
                    public int streamType;

                    public ResolutionBean(String name, int imageSize, int streamType) {
                        this.name = name;
                        this.imageSize = imageSize;
                        this.streamType = streamType;
                    }

                    @Override
                    public String toString() {
                        return "ResolutionBean{" +
                                "name='" + name + '\'' +
                                ", imageSize=" + imageSize +
                                ", streamType=" + streamType +
                                '}';
                    }
                }

                @Override
                public String toString() {
                    Gson g = new Gson();
                    return g.toJson(this);
                }
            }

            public static class AplistBean implements Serializable {
                public String apId;
                public String apName;
                public String apType;
                public String apModel;
                public String ioType;
                public String apVersion;
                public String apStatus;
                public String apEnable;
                public String apCapacity;

                @Override
                public String toString() {
                    Gson g = new Gson();
                    return g.toJson(this);
                }
            }
        }
    }

}
