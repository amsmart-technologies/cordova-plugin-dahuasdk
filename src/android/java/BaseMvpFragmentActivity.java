package cordova.plugin.dahuasdk;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;

public abstract class BaseMvpFragmentActivity<T extends IBasePresenter> extends BaseFragmentActivity implements IBaseView {

    protected abstract void initLayout();

    protected abstract void initView();

    protected abstract void initData();

    protected T mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
        initData();
        initLayout();
        initView();
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) mPresenter.unInit();
        super.onDestroy();
    }

    @Override
    public Context getContextInfo() {
        return this;
    }

    @Override
    public boolean isViewActive() {
        return !isActivityDestory();
    }

    @Override
    public void showToastInfo(String msg) {
        toast(msg);
    }

    @Override
    public void showToastInfo(int msgId) {
        toast(msgId);
    }

    @Override
    public void showToastInfo(int msgId, String errorDesc) {
        if (!TextUtils.isEmpty(errorDesc)) {
            toast(errorDesc);
        } else {
            toast(msgId);
        }
    }

    @Override
    public void showProgressDialog() {
        showProgressDialog(getResources("layout", "mobile_common_progressdialog_layout"));
    }

    @Override
    public void onMessageEvent(BaseEvent event) {

    }

    @Override
    public void unInit() {

    }

    @Override
    public void initPresenter() {

    }

    public void cancelProgressDialog() {
        dissmissProgressDialog();
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
