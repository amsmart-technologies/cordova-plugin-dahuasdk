package cordova.plugin.dahuasdk;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.Serializable;

public class CurWifiInfo implements Serializable {
    public static class Response {
        public CurWifiInfo data;

        public void parseData(JsonObject json) {
            Gson gson = new Gson();
            this.data = gson.fromJson(json.toString(), CurWifiInfo.class);
        }
    }

    private boolean linkEnable;
    private String ssid;
    private int intensity;
    private String sigStrength;
    private String auth;

    public boolean isLinkEnable() {
        return linkEnable;
    }

    public void setLinkEnable(boolean linkEnable) {
        this.linkEnable = linkEnable;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public int getIntensity() {
        return intensity;
    }

    public void setIntensity(int intensity) {
        this.intensity = intensity;
    }

    public String getSigStrength() {
        return sigStrength;
    }

    public void setSigStrength(String sigStrength) {
        this.sigStrength = sigStrength;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
}
