package cordova.plugin.dahuasdk;

public class QRScanResult {
    private String sn = "";
    private String mode = "";
    private String regcode = "";
    private String rd = "";
    private String nc = "";
    private String sc = "";
    private String imeiCode = "";

    public QRScanResult(String scanString) {
    }

    public QRScanResult() {
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getRegcode() {
        return regcode;
    }

    public void setRegcode(String regcode) {
        this.regcode = regcode;
    }

    public String getRd() {
        return rd;
    }

    public void setRd(String rd) {
        this.rd = rd;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getNc() {
        return nc;
    }

    public void setNc(String nc) {
        this.nc = nc;
    }

    public String getSc() {
        return sc;
    }

    public void setSc(String sc) {
        this.sc = sc;
    }

    public String getImeiCode() {
        return imeiCode;
    }

    public void setImeiCode(String imeiCode) {
        this.imeiCode = imeiCode;
    }

    @Override
    public String toString() {
        return "ScanResult{" +
                "sn='" + sn + '\'' +
                ", mode='" + mode + '\'' +
                ", regcode='" + regcode + '\'' +
                ", nc='" + nc + '\'' +
                ", sc='" + sc + '\'' +
                ", imeiCode='" + imeiCode + '\'' +
                '}';
    }
}
