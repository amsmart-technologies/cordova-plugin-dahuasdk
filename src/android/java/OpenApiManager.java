package cordova.plugin.dahuasdk;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OpenApiManager {
    private static int TOKEN_TIME_OUT = 4 * 1000;
    private static int TIME_OUT = 60 * 1000;

    public static String getToken() throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        JsonObject jsonData = HttpSend.execute(paramsMap, CONST.METHOD_ACCESSTOKEN, TOKEN_TIME_OUT);
        return jsonData.get("accessToken").getAsString();
    }

    public static JsonObject listSubAccount(String token, int pageNo, int pageSize) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("pageNo", pageNo);
        paramsMap.put("pageSize", pageSize);
        return HttpSend.execute(paramsMap, CONST.LIST_SUB_ACCOUNT, TIME_OUT);
    }

    public static JsonObject deleteSubAccount(String token, String openid) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("openid", openid);
        return HttpSend.execute(paramsMap, CONST.DELETE_SUB_ACCOUNT, TIME_OUT);
    }

    public static JsonObject listSubAccountDevice(String token, String openid, int pageNo, int pageSize) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("openid", openid);
        paramsMap.put("pageNo", pageNo);
        paramsMap.put("pageSize", pageSize);
        return HttpSend.execute(paramsMap, CONST.LIST_SUB_ACCOUNT_DEVICE, TIME_OUT);
    }

    public static JsonObject subAccountDeviceList(String token, int pageNo, int pageSize) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("pageNo", pageNo);
        paramsMap.put("pageSize", pageSize);
        return HttpSend.execute(paramsMap, CONST.SUBACCOUNTDEVICELIST, TIME_OUT);
    }

    public static JsonObject listDeviceDetailsByPage(String token, int page, int pageSize) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("page", page);
        paramsMap.put("pageSize", pageSize);
        return HttpSend.execute(paramsMap, CONST.LISTDEVICEDETAILSBYPAGE, TIME_OUT);
    }

    public static JsonObject queryOpenDeviceChannelInfo(String token, String deviceIds) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceIds", deviceIds);
        return HttpSend.execute(paramsMap, CONST.OPEN_DEVICE_CHANNEL_INFO, TIME_OUT);
    }

    public static JsonObject createSubAccount(String token, String account) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("account", account);
        return HttpSend.execute(paramsMap, CONST.CREATE_SUB_ACCOUNT, TIME_OUT);
    }

    public static JsonObject getOpenIdByAccount(String token, String account) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("account", account);
        return HttpSend.execute(paramsMap, CONST.GET_OPENID_BYACCOUNT, TIME_OUT);
    }

    public static String getSubAccountToken(String token, String openid) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("openid", openid);
        JsonObject jsonData = HttpSend.execute(paramsMap, CONST.SUB_ACCOUNT_TOKEN, TOKEN_TIME_OUT);
        return jsonData.get("accessToken").getAsString();
    }

    public static JsonObject addPolicy(String token, String openid, String policy) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("openid", openid);
        paramsMap.put("policy", policy);
        return HttpSend.execute(paramsMap, CONST.ADD_POLICY, TIME_OUT);
    }

    public static JsonObject deviceInfoBeforeBind(String token, String deviceId, String deviceCodeModel, String deviceModelName, String ncCode, String productId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("deviceCodeModel", deviceCodeModel);
        paramsMap.put("deviceModelName", deviceModelName);
        paramsMap.put("ncCode", ncCode);
        paramsMap.put("productId", productId);
        return HttpSend.execute(paramsMap, CONST.METHOD_UNBINDDEVICEINFO, TIME_OUT);
    }

    public static JsonObject deviceLeadingInfo(String token, String deviceModel) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceModelName", deviceModel);
        return HttpSend.execute(paramsMap, CONST.METHOD_GUIDEINFOGET, TIME_OUT);
    }

    public static DeviceModelOrLeadingInfoCheckData.Response deviceModelOrLeadingInfoCheck(DeviceModelOrLeadingInfoCheckData deviceModelOrLeadingInfoCheckData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", deviceModelOrLeadingInfoCheckData.data.token);
        paramsMap.put("deviceModelName", deviceModelOrLeadingInfoCheckData.data.deviceModelName);
        paramsMap.put("updateTime", deviceModelOrLeadingInfoCheckData.data.updateTime);
        JsonObject json = HttpSend.execute(paramsMap, CONST.METHOD_GUIDEINFOCHECK, TIME_OUT);
        DeviceModelOrLeadingInfoCheckData.Response response = new DeviceModelOrLeadingInfoCheckData.Response();
        response.parseData(json);
        return response;
    }

    public static JsonObject userDeviceBind(String token, String deviceId, String code) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("code", code);
        return HttpSend.execute(paramsMap, CONST.METHOD_BINDDEVICE, TIME_OUT);
    }

    public static ModifyDeviceNameData.Response modifyDeviceName(ModifyDeviceNameData bindDeviceData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", bindDeviceData.data.token);
        paramsMap.put("deviceId", bindDeviceData.data.deviceId);
        paramsMap.put("channelId", bindDeviceData.data.channelId);
        paramsMap.put("name", bindDeviceData.data.name);
        JsonObject json = HttpSend.execute(paramsMap, CONST.METHOD_MODIFYDEVICENAME, TIME_OUT);
        ModifyDeviceNameData.Response response = new ModifyDeviceNameData.Response();
        response.parseData(json);
        return response;
    }

    public static CurWifiInfo currentDeviceWifi(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.METHOD_CURRENT_DEVICE_WIFI, TIME_OUT);
        CurWifiInfo.Response response = new CurWifiInfo.Response();
        response.parseData(json);
        return response.data;
    }

    public static WifiConfig wifiAround(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.METHOD_WIFI_AROUND, TIME_OUT);
        WifiConfig.Response response = new WifiConfig.Response();
        response.parseData(json);
        return response.data;
    }

    public static JsonObject wifiAround2(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.METHOD_WIFI_AROUND, TIME_OUT);
        return json;
    }

    public static JsonObject timeZoneQueryByDay(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.METHOD_TIMEZONE_QUERY, TIME_OUT);
        return json;
    }

    public static JsonObject timeZoneConfigByDay(String token, String deviceId, String areaIndex, String timeZone, String beginSunTime, String endSunTime) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("areaIndex", areaIndex);
        paramsMap.put("timeZone", timeZone);
        if (!beginSunTime.equals("null")) {
            paramsMap.put("beginSunTime", beginSunTime);
        }
        if (!endSunTime.equals("null")) {
            paramsMap.put("endSunTime", endSunTime);
        }
        JsonObject json = HttpSend.execute(paramsMap, CONST.METHOD_TIMEZONE_CONFIG, TIME_OUT);
        return json;
    }

    public static boolean controlDeviceWifi(String token, String deviceId, String ssid, String bssid, boolean linkEnable, String password) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("ssid", ssid);
        paramsMap.put("bssid", bssid);
        paramsMap.put("linkEnable", linkEnable);
        paramsMap.put("password", password);
        HttpSend.execute(paramsMap, CONST.METHOD_CONTROL_DEVICE_WIFI, TIME_OUT);
        return true;
    }

    public static boolean controlDeviceWifi2(String token, String deviceId, String ssid, String bssid, boolean linkEnable, String password) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("ssid", ssid);
        paramsMap.put("bssid", bssid);
        paramsMap.put("linkEnable", linkEnable);
        paramsMap.put("password", password);
        HttpSend.execute(paramsMap, CONST.METHOD_CONTROL_DEVICE_WIFI, TIME_OUT);
        return true;
    }

    public static DeviceDetailListData.Response deviceOpenDetailList(String token, DeviceDetailListData deviceDetailListData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceList", deviceDetailListData.data.deviceList);
        JsonObject json = HttpSend.execute(paramsMap, CONST.METHOD_DEVICE_OPEN_DETAIL_LIST, TIME_OUT);
        DeviceDetailListData.Response response = new DeviceDetailListData.Response();
        response.parseData(json);
        return response;
    }

    public static JsonObject setMessageCallback(String token, String status, String callbackUrl, String callbackFlag) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("status", status);
        paramsMap.put("callbackUrl", callbackUrl);
        paramsMap.put("callbackFlag", callbackFlag);
        JsonObject json = HttpSend.execute(paramsMap, CONST.SET_MESSAGE_CALLBACK, TIME_OUT);
        return json;
    }

    public static JsonObject getMessageCallback(String token) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        JsonObject json = HttpSend.execute(paramsMap, CONST.GET_MESSAGE_CALLBACK, TIME_OUT);
        return json;
    }

    public static JsonObject getAlarmMessage(String token, String deviceId, String channelId, String beginTime, String endTime, String count, String nextAlarmId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        paramsMap.put("beginTime", beginTime);
        paramsMap.put("endTime", endTime);
        if (!count.equals("null")) {
            paramsMap.put("count", count);
        }
        if (!nextAlarmId.equals("null")) {
            paramsMap.put("nextAlarmId", nextAlarmId);
        }
        JsonObject json = HttpSend.execute(paramsMap, CONST.GET_ALARM_MESSAGE, TIME_OUT);
        return json;
    }

    public static JsonObject deleteAlarmMessage(String token, String deviceId, String indexId, String channelId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("indexId", indexId);
        paramsMap.put("channelId", channelId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.DELETE_ALARM_MESSAGE, TIME_OUT);
        return json;
    }

    public static JsonObject queryCloudRecordCallNum(String token, Long strategyId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("strategyId", strategyId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.QUERY_CLOUDRECORD_CALLNUM, TIME_OUT);
        return json;
    }

    public static JsonObject openCloudRecord(String token, String deviceId, String channelId, Long strategyId, String deviceCloudId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        paramsMap.put("strategyId", strategyId);
        if (!deviceCloudId.equals("null")) {
            paramsMap.put("deviceCloudId", deviceCloudId);
        }
        JsonObject json = HttpSend.execute(paramsMap, CONST.OPEN_CLOUDRECORD, TIME_OUT);
        return json;
    }

    public static JsonObject getDeviceCloud(String token, String deviceId, String channelId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.GET_STORAGE_STRATEGY, TIME_OUT);
        return json;
    }

    public static JsonObject deviceSdcardStatus(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.DEVICESDCARDSTATUS, TIME_OUT);
        return json;
    }

    public static JsonObject deviceStorage(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.DEVICESTORAGE, TIME_OUT);
        return json;
    }

    public static JsonObject recoverSDCard(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.RECOVERSDCARD, TIME_OUT);
        return json;
    }

    public static JsonObject setDeviceSnap(String token, String deviceId, String channelId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.SETDEVICESNAP, TIME_OUT);
        return json;
    }

    public static JsonObject queryLocalRecordPlan(String token, String deviceId, String channelId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.QUERYLOCALRECORDPLAN, TIME_OUT);
        return json;
    }

    public static JsonObject setLocalRecordPlanRules(String token, String deviceId, String channelId, DeviceRecordPlanData.RequestData data) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        paramsMap.put("rules", data.rules);
        JsonObject json = HttpSend.execute(paramsMap, CONST.SETLOCALRECORDPLANRULES, TIME_OUT);
        return json;
    }

    public static DeviceListData.Response deviceOpenList(DeviceListData deviceListData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("bindId", deviceListData.data.openBindId);
        paramsMap.put("limit", deviceListData.data.limit);
        paramsMap.put("type", deviceListData.data.type);
        paramsMap.put("needApInfo", deviceListData.data.needApInfo);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_OPEN_LIST, TIME_OUT);
        DeviceListData.Response response = new DeviceListData.Response();
        response.parseData(json);
        return response;
    }

    public static DeviceDetailListData.Response deviceOpenDetailList(DeviceDetailListData deviceDetailListData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceList", deviceDetailListData.data.deviceList);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_OPEN_DETAIL_LIST, TIME_OUT);
        DeviceDetailListData.Response response = new DeviceDetailListData.Response();
        response.parseData(json);
        return response;
    }

    public static boolean unBindDevice(DeviceUnBindData deviceUnBindData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceId", deviceUnBindData.data.deviceId);
        HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_UN_BIND, TIME_OUT);
        return true;
    }

    public static boolean unBindDevice2(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_UN_BIND, TIME_OUT);
        return true;
    }

    public static DeviceVersionListData.Response deviceVersionList(DeviceVersionListData deviceVersionListData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceIds", deviceVersionListData.data.deviceIds);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_VERSION_LIST, TIME_OUT);
        DeviceVersionListData.Response response = new DeviceVersionListData.Response();
        response.parseData(json);
        return response;
    }

    public static JsonObject deviceVersionList2(String token, String deviceIds) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceIds", deviceIds);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_VERSION_LIST, TIME_OUT);
        return json;
    }

    public static boolean modifyDeviceName(DeviceModifyNameData deviceModifyNameData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceId", deviceModifyNameData.data.deviceId);
        paramsMap.put("channelId", deviceModifyNameData.data.channelId);
        paramsMap.put("name", deviceModifyNameData.data.name);
        HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_MODIFY_NAME, TIME_OUT);
        return true;
    }

    public static boolean modifyDeviceName2(String token, String deviceId, String channelId, String name) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        if (!channelId.equals("null")) {
            paramsMap.put("channelId", channelId);
        }
        paramsMap.put("name", name);
        HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_MODIFY_NAME, TIME_OUT);
        return true;
    }

    public static DeviceChannelInfoData.Response bindDeviceChannelInfo(DeviceChannelInfoData deviceChannelInfoData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceId", deviceChannelInfoData.data.deviceId);
        paramsMap.put("channelId", deviceChannelInfoData.data.channelId);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_CHANNEL_INFO, TIME_OUT);
        DeviceChannelInfoData.Response response = new DeviceChannelInfoData.Response();
        response.parseData(json);
        return response;
    }

    public static JsonObject bindDeviceChannelInfo2(String token, String deviceId, String channelId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_CHANNEL_INFO, TIME_OUT);
        return json;
    }

    public static boolean modifyDeviceAlarmStatus(DeviceAlarmStatusData deviceAlarmStatusData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceId", deviceAlarmStatusData.data.deviceId);
        paramsMap.put("channelId", deviceAlarmStatusData.data.channelId);
        paramsMap.put("enable", deviceAlarmStatusData.data.enable);
        HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_MODIFY_ALARM_STATUS, TIME_OUT);
        return true;
    }

    public static boolean modifyDeviceAlarmStatus2(String token, String deviceId, String channelId, boolean enable) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        paramsMap.put("enable", enable);
        HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_MODIFY_ALARM_STATUS, TIME_OUT);
        return true;
    }

    public static boolean upgradeDevice(String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceId", deviceId);
        HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_UPDATE, TIME_OUT);
        return true;
    }

    public static CloudRecordsData.Response getCloudRecords(CloudRecordsData cloudRecordsData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceId", cloudRecordsData.data.deviceId);
        paramsMap.put("channelId", cloudRecordsData.data.channelId);
        paramsMap.put("beginTime", cloudRecordsData.data.beginTime);
        paramsMap.put("endTime", cloudRecordsData.data.endTime);
        paramsMap.put("nextRecordId", cloudRecordsData.data.nextRecordId);
        paramsMap.put("count", cloudRecordsData.data.count);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_GET_CLOUND_RECORDS, TIME_OUT);
        CloudRecordsData.Response response = new CloudRecordsData.Response();
        response.parseData(json);
        return response;
    }

    public static LocalRecordsData.Response queryLocalRecords(LocalRecordsData localRecordsData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceId", localRecordsData.data.deviceId);
        paramsMap.put("channelId", localRecordsData.data.channelId);
        paramsMap.put("beginTime", localRecordsData.data.beginTime);
        paramsMap.put("endTime", localRecordsData.data.endTime);
        paramsMap.put("type", localRecordsData.data.type);
        paramsMap.put("queryRange", localRecordsData.data.queryRange);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_QUERY_LOCAL_RECORD, TIME_OUT);
        LocalRecordsData.Response response = new LocalRecordsData.Response();
        response.parseData(json);
        return response;
    }

    public static void controlMovePTZ(ControlMovePTZData controlMovePTZData) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("deviceId", controlMovePTZData.data.deviceId);
        paramsMap.put("channelId", controlMovePTZData.data.channelId);
        paramsMap.put("operation", controlMovePTZData.data.operation);
        paramsMap.put("duration", controlMovePTZData.data.duration);
        HttpSend.execute(paramsMap, MethodConst.METHOD_CONTROL_MOVE_PTZ, TIME_OUT);
    }

    public static boolean deleteCloudRecords(String recordRegionId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", LCDeviceEngine.newInstance().accessToken);
        paramsMap.put("recordRegionId", recordRegionId);
        HttpSend.execute(paramsMap, MethodConst.METHOD_DELETE_CLOUND_RECORDS, TIME_OUT);
        return true;
    }

    public static JsonObject restartDevice(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.RESTARTDEVICE, TIME_OUT);
        return json;
    }

    public static JsonObject modifyDevicePwd(String token, String deviceId, String oldPwd, String newPwd) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("oldPwd", oldPwd);
        paramsMap.put("newPwd", newPwd);
        JsonObject json = HttpSend.execute(paramsMap, CONST.MODIFYDEVICEPWD, TIME_OUT);
        return json;
    }

    public static JsonObject upgradeProcessDevice(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, CONST.UPGRADEPROCESSDEVICE, TIME_OUT);
        return json;
    }

    public static JsonObject upgradeDevice(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_UPDATE, TIME_OUT);
        return json;
    }

    public static JsonObject wakeUpDevice(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("url", "/device/wakeup");
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_WAKEUP, TIME_OUT);
        return json;
    }

    public static JsonObject deviceOnline(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_ONLINE, TIME_OUT);
        return json;
    }

    public static JsonObject getDevicePowerInfo(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_DEVICE_POWER, TIME_OUT);
        return json;
    }

    public static JsonObject doorbellCallAnswer(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_CALL_ANSWER, TIME_OUT);
        return json;
    }

    public static JsonObject doorbellCallRefuse(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_CALL_REFUSE, TIME_OUT);
        return json;
    }

    public static JsonObject doorbellCallHangUp(String token, String deviceId) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        JsonObject json = HttpSend.execute(paramsMap, MethodConst.METHOD_CALL_HANGUP, TIME_OUT);
        return json;
    }

    public static DeviceDetailListData.ResponseData.DeviceListBean subAccountDeviceInfo(String token, String deviceId, String channelId) throws BusinessException {
        DeviceDetailListData.ResponseData.DeviceListBean deviceListBean = null;
        List<DeviceDetailListData.ResponseData.DeviceListBean> deviceList = new ArrayList<>();
        DeviceDetailListData.Response response;
        int page = 0;
        do {
            page++;
            HashMap<String, Object> paramsMap = new HashMap<String, Object>();
            paramsMap.put("token", token);
            paramsMap.put("pageSize", 10);
            paramsMap.put("page", page);
            JsonObject json = HttpSend.execute(paramsMap, CONST.LISTDEVICEDETAILSBYPAGE, TIME_OUT);
            response = new DeviceDetailListData.Response();
            response.parseData(json);
            deviceList.addAll(response.data.deviceList);
        } while (response.data.count > 0);
        for (DeviceDetailListData.ResponseData.DeviceListBean deviceListBean1 : deviceList) {
            if (deviceListBean1.deviceId.equals(deviceId)) {
                deviceListBean = deviceListBean1;
            }
        }
        return deviceListBean;
    }

    public static JsonObject getDeviceCameraStatus(String token, String deviceId, String channelId, String enableType) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        paramsMap.put("enableType", enableType);
        JsonObject json = HttpSend.execute(paramsMap, "getDeviceCameraStatus", TIME_OUT);
        return json;
    }

    public static JsonObject setDeviceCameraStatus(String token, String deviceId, String channelId, String enableType, boolean enable) throws BusinessException {
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("token", token);
        paramsMap.put("deviceId", deviceId);
        paramsMap.put("channelId", channelId);
        paramsMap.put("enableType", enableType);
        paramsMap.put("enable", enable);
        JsonObject json = HttpSend.execute(paramsMap, "setDeviceCameraStatus", TIME_OUT);
        return json;
    }

}
