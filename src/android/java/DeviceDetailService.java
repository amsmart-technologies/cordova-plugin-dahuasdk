package cordova.plugin.dahuasdk;

import android.os.Message;

public class DeviceDetailService {

    public void deviceVersionList(final DeviceVersionListData deviceVersionListData, final IGetDeviceInfoCallBack.IDeviceVersionCallBack deviceVersionCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (deviceVersionCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    deviceVersionCallBack.deviceVersion((DeviceVersionListData.Response) msg.obj);
                } else {
                    deviceVersionCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    DeviceVersionListData.Response response = OpenApiManager.deviceVersionList(deviceVersionListData);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, response).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void modifyDeviceName(final DeviceModifyNameData deviceModifyNameData, final IGetDeviceInfoCallBack.IModifyDeviceCallBack modifyDeviceCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (modifyDeviceCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    //成功
                    modifyDeviceCallBack.deviceModify((boolean) msg.obj);
                } else {
                    //失败
                    modifyDeviceCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    boolean b = OpenApiManager.modifyDeviceName(deviceModifyNameData);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, b).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void unBindDevice(final DeviceUnBindData deviceUnBindData, final IGetDeviceInfoCallBack.IUnbindDeviceCallBack unbindDeviceCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (unbindDeviceCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    //成功
                    unbindDeviceCallBack.unBindDevice((boolean) msg.obj);
                } else {
                    //失败
                    unbindDeviceCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    boolean b = OpenApiManager.unBindDevice(deviceUnBindData);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, b).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void bindDeviceChannelInfo(final DeviceChannelInfoData deviceChannelInfoData, final IGetDeviceInfoCallBack.IDeviceChannelInfoCallBack deviceChannelInfoCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (deviceChannelInfoCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    //成功
                    deviceChannelInfoCallBack.deviceChannelInfo((DeviceChannelInfoData.Response) msg.obj);
                } else {
                    //失败
                    deviceChannelInfoCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    DeviceChannelInfoData.Response response = OpenApiManager.bindDeviceChannelInfo(deviceChannelInfoData);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, response).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void modifyDeviceAlarmStatus(final DeviceAlarmStatusData deviceAlarmStatusData, final IGetDeviceInfoCallBack.IDeviceAlarmStatusCallBack deviceAlarmStatusCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (deviceAlarmStatusCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    //成功
                    deviceAlarmStatusCallBack.deviceAlarmStatus((boolean) msg.obj);
                } else {
                    //失败
                    deviceAlarmStatusCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    boolean b = OpenApiManager.modifyDeviceAlarmStatus(deviceAlarmStatusData);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, b).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void upgradeDevice(final String deviceId, final IGetDeviceInfoCallBack.IDeviceUpdateCallBack deviceUpdateCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (deviceUpdateCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    //成功
                    deviceUpdateCallBack.deviceUpdate((boolean) msg.obj);
                } else {
                    //失败
                    deviceUpdateCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    boolean b = OpenApiManager.upgradeDevice(deviceId);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, b).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void currentDeviceWifi(final String deviceId, final IGetDeviceInfoCallBack.IDeviceCurrentWifiInfoCallBack deviceCurrentWifiInfoCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (deviceCurrentWifiInfoCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    //成功
                    deviceCurrentWifiInfoCallBack.deviceCurrentWifiInfo((CurWifiInfo) msg.obj);
                } else {
                    //失败
                    deviceCurrentWifiInfoCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    CurWifiInfo curWifiInfo = OpenApiManager.currentDeviceWifi(LCDeviceEngine.newInstance().accessToken, deviceId);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, curWifiInfo).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

}
