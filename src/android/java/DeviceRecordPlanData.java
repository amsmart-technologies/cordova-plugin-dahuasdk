package cordova.plugin.dahuasdk;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

public class DeviceRecordPlanData implements Serializable {
    public DeviceRecordPlanData.RequestData data = new DeviceRecordPlanData.RequestData();

    public static class RequestData implements Serializable {
        public List<Rule> rules;

        @Override
        public String toString() {
            Gson g = new Gson();
            return g.toJson(this);
        }

        public static class Rule implements Serializable {
            public String beginTime;
            public String endTime;
            public String period;

            @Override
            public String toString() {
                Gson g = new Gson();
                return g.toJson(this);
            }
        }
    }
}
