package cordova.plugin.dahuasdk;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class DeviceWifiListAdapter extends CommonAdapter<WifiInfo> {

    public DeviceWifiListAdapter(int layout, List<WifiInfo> list, Context context) {
        super(layout, list, context);
    }

    @Override
    public void convert(ViewHolder viewHolder, WifiInfo entity, final int position, ViewGroup parent) {
        TextView wifiSSID = (TextView) viewHolder.findViewById(getResources("id", "wifi_ssid"));
        ImageView wifiQualityIcon = (ImageView) viewHolder.findViewById(getResources("id", "wifi_quality_icon"));

        wifiSSID.setText(entity.getSsid());
        int quality = entity.getIntensity();

        if (quality < 2) {
            wifiQualityIcon.setImageResource("OPEN".equalsIgnoreCase(entity.getAuth())
                    ? getResources("drawable", "devicedetail_wifi_nosingal") : getResources("drawable", "devicedetail_wifi_nosingal_lock"));
        } else if (quality < 3) {
            wifiQualityIcon.setImageResource("OPEN".equalsIgnoreCase(entity.getAuth())
                    ? getResources("drawable", "devicedetail_wifi_1singal") : getResources("drawable", "devicedetail_wifi_1singal_lock"));
        } else if (quality < 4) {
            wifiQualityIcon.setImageResource("OPEN".equalsIgnoreCase(entity.getAuth())
                    ? getResources("drawable", "devicedetail_wifi_2singal") : getResources("drawable", "devicedetail_wifi_2singal_lock"));
        } else {
            wifiQualityIcon.setImageResource("OPEN".equalsIgnoreCase(entity.getAuth())
                    ? getResources("drawable", "devicedetail_wifi_3singal") : getResources("drawable", "devicedetail_wifi_3singal_lock"));
        }
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
