package cordova.plugin.dahuasdk;

public interface HiddenWifiConstract {
    interface Presenter extends IBasePresenter {
        void wifiOperate();
    }

    interface View extends IBaseView {
        String getWifiSSID();
        String getWifiPassword();
        void onWifiOperateSucceed(CurWifiInfo curWifiInfo);
    }
}
