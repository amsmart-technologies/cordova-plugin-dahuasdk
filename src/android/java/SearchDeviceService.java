package cordova.plugin.dahuasdk;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.company.NetSDK.CB_fSearchDevicesCB;
import com.company.NetSDK.DEVICE_NET_INFO_EX;
import com.company.NetSDK.INetSDK;

import java.util.concurrent.CopyOnWriteArrayList;

public class SearchDeviceService extends Service {
    public static final int SEND_START = 1;
    public static final String TAG = "SearchDeviceService";
    private long mLRet;
    private CopyOnWriteArrayList<SearchDeviceManager.ISearchDeviceListener> mListenerList;
    long lastReceiveTime = 0;
    private BroadcastReceiver mReceiver;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (!SearchDeviceManager.getInstance().mIsExist) return;
            if (System.currentTimeMillis() - lastReceiveTime > 2000) {
                handler.removeMessages(SEND_START);
                handler.removeCallbacks(searchRunnable);
                handler.postDelayed(searchRunnable, 500);
            } else {
                handler.removeMessages(SEND_START);
                handler.sendEmptyMessageDelayed(SEND_START, 500);
            }
        }
    };
    Runnable searchRunnable = new Runnable() {
        @Override
        public void run() {
            SearchDeviceManager.getInstance().removeInvalidDevice();
            if (mLRet != 0) {
                stopSearchDevices();
            }
            mLRet = INetSDK.StartSearchDevices(new CB_fSearchDevicesCB() {
                @Override
                public void invoke(DEVICE_NET_INFO_EX device_net_info_ex) {
                    lastReceiveTime = System.currentTimeMillis();
                    if (device_net_info_ex != null) {
                        String szSerialNo = new String(device_net_info_ex.szSerialNo).trim();
                        if (device_net_info_ex.iIPVersion == 4 && mListenerList != null) {
                            for (int i = 0; i < mListenerList.size(); i++) {
                                SearchDeviceManager.ISearchDeviceListener listener = mListenerList.get(i);
                                if (listener != null) {
                                    listener.onDeviceSearched(szSerialNo, device_net_info_ex);
                                }
                            }
                        }
                    }
                }
            });
            if (System.currentTimeMillis() - lastReceiveTime > 500) {
                handler.removeMessages(SEND_START);
                handler.removeCallbacks(searchRunnable);
                handler.postDelayed(searchRunnable, 2000);
            } else {
                handler.removeMessages(SEND_START);
                handler.sendEmptyMessageDelayed(SEND_START, 500);
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mListenerList = new CopyOnWriteArrayList<>();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LCConfiguration.CONNECTIVITY_CHAGET_ACTION);
        mReceiver = new Broadcast();
        registerReceiver(mReceiver, intentFilter);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new SearchDeviceBinder();
    }

    private void startSearchDevices() {
        handler.removeCallbacks(searchRunnable);
        handler.post(searchRunnable);
    }

    private void stopSearchDevicesAsync() {
        handler.removeCallbacks(searchRunnable);
        if (mLRet != 0) {
            new BusinessRunnable(null) {
                @Override
                public void doBusiness() throws BusinessException {
                    boolean result = INetSDK.StopSearchDevices(mLRet);
                }
            };
        }
    }

    private void stopSearchDevices() {
        handler.removeCallbacks(searchRunnable);
        if (mLRet != 0) {
            INetSDK.StopSearchDevices(mLRet);
        }
    }

    private void registerListener(SearchDeviceManager.ISearchDeviceListener listener) {
        if (mListenerList != null && !mListenerList.contains(listener)) {
            mListenerList.add(listener);
        }
    }

    private void unRegisterListener(SearchDeviceManager.ISearchDeviceListener listener) {
        if (mListenerList != null && mListenerList.contains(listener)) {
            mListenerList.remove(listener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        stopSearchDevicesAsync();
        if (mListenerList != null) {
            mListenerList.clear();
        }
        mListenerList = null;
    }

    public class SearchDeviceBinder extends Binder {

        public void startSearchDevices() {
            SearchDeviceService.this.startSearchDevices();
        }

        public void stopSearchDevicesAsync() {
            SearchDeviceService.this.stopSearchDevicesAsync();
        }

        public void stopSearchDevices() {
            SearchDeviceService.this.stopSearchDevices();
        }

        public void registerListener(SearchDeviceManager.ISearchDeviceListener listener) {
            SearchDeviceService.this.registerListener(listener);
        }

        public void unRegisterListener(SearchDeviceManager.ISearchDeviceListener listener) {
            SearchDeviceService.this.unRegisterListener(listener);
        }
    }

    private class Broadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LCConfiguration.CONNECTIVITY_CHAGET_ACTION.equals(intent.getAction())) {
                SearchDeviceManager.getInstance().clearDevice();
                startSearchDevices();
            }
        }
    }
}
