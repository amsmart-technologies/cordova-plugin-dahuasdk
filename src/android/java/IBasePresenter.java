package cordova.plugin.dahuasdk;

import android.content.Intent;

public interface IBasePresenter {
    void dispatchIntentData(Intent intent);
    void unInit();
}
