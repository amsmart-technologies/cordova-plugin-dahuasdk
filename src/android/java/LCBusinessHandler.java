package cordova.plugin.dahuasdk;

import android.os.Looper;
import android.os.Message;

public abstract class LCBusinessHandler extends BaseHandler {
	public LCBusinessHandler() {
		super();
	}

	public LCBusinessHandler(Looper looper) {
		super(looper);
	}

	@Override
	public void authError(Message msg) {
		super.authError(msg);
	}
}
