package cordova.plugin.dahuasdk;

import android.os.Message;

public class DeviceRecordService {

    public void getCloudRecords(final CloudRecordsData cloudRecordsData, final IGetDeviceInfoCallBack.IDeviceCloudRecordCallBack cloudRecordCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (cloudRecordCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    cloudRecordCallBack.deviceCloudRecord((CloudRecordsData.Response) msg.obj);
                } else {
                    cloudRecordCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    CloudRecordsData.Response response = OpenApiManager.getCloudRecords(cloudRecordsData);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, response).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void queryLocalRecords(final LocalRecordsData localRecordsData, final IGetDeviceInfoCallBack.IDeviceLocalRecordCallBack deviceLocalRecordCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (deviceLocalRecordCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    deviceLocalRecordCallBack.deviceLocalRecord((LocalRecordsData.Response) msg.obj);
                } else {
                    deviceLocalRecordCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    LocalRecordsData.Response response = OpenApiManager.queryLocalRecords(localRecordsData);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, response).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void controlMovePTZ(final ControlMovePTZData controlMovePTZData) {
        new BusinessRunnable(new LCBusinessHandler() {

            @Override
            public void handleBusiness(Message msg) {

            }
        }) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    OpenApiManager.controlMovePTZ(controlMovePTZData);
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }

    public void deleteCloudRecords(final DeleteCloudRecordsData deleteCloudRecordsData, final IGetDeviceInfoCallBack.IDeviceDeleteRecordCallBack deviceDeleteRecordCallBack) {
        final LCBusinessHandler handler = new LCBusinessHandler() {
            @Override
            public void handleBusiness(Message msg) {
                if (deviceDeleteRecordCallBack == null) {
                    return;
                }
                if (msg.what == HandleMessageCode.HMC_SUCCESS) {
                    deviceDeleteRecordCallBack.deleteDeviceRecord();
                } else {
                    deviceDeleteRecordCallBack.onError(BusinessErrorTip.throwError(msg));
                }
            }
        };
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                try {
                    for (String id : deleteCloudRecordsData.data.recordRegionIds) {
                        OpenApiManager.deleteCloudRecords(id);
                    }
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, null).sendToTarget();
                } catch (BusinessException e) {
                    throw e;
                }
            }
        };
    }
}
