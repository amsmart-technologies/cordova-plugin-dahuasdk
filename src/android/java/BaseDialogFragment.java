package cordova.plugin.dahuasdk;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class BaseDialogFragment extends DialogFragment {

    private Toast mToast;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int show(FragmentTransaction transaction, String tag) {
        return show(transaction, tag, true);
    }

    int mBackStackId;

    public int show(FragmentTransaction transaction, String tag, boolean allowStateLoss) {
        if (this.isAdded()) {
            transaction.remove(this);
        }
        transaction.add(this, tag);
        mBackStackId = allowStateLoss ? transaction.commitAllowingStateLoss() : transaction.commit();
        return mBackStackId;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        show(manager.beginTransaction(), tag, true);
    }

    protected void toast(int res) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            String content = "";
            try {
                content = getActivity().getString(res);
            } catch (Resources.NotFoundException e) {
                Log.d("toast", "resource id not found!!!");
            }
            toast(content);
        }
    }

    protected void toast(String content) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            if (mToast == null) {
                mToast = Toast.makeText(getActivity(), content, Toast.LENGTH_SHORT);
            } else {
                mToast.setText(content);
                mToast.setDuration(Toast.LENGTH_SHORT);
            }
            mToast.show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void dismissAllowingStateLoss() {
        if (getActivity() == null || getFragmentManager() == null || getFragmentManager().isDestroyed()) {
            return;
        }
        super.dismissAllowingStateLoss();
    }

    @Override
    public void dismiss() {
        if (getActivity() == null || getFragmentManager() == null || getFragmentManager().isDestroyed()) {
            return;
        }
        super.dismissAllowingStateLoss();
    }

}
