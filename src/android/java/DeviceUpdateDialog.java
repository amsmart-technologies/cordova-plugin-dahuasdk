package cordova.plugin.dahuasdk;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DeviceUpdateDialog extends Dialog {
    private TextView tv_title;
    private TextView tv_msg;
    private TextView btn_ok;
    private TextView btn_cancel;

    public DeviceUpdateDialog(Context context) {
        super(context, ClassInstanceManager.newInstance().getResources("sign_dialog", "style"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources("layout", "dialog_device_update"));
        tv_title = findViewById(getResources("id", "tv_title"));
        tv_msg = findViewById(getResources("id", "tv_msg"));
        btn_ok = findViewById(getResources("id", "btn_ok"));
        btn_cancel = findViewById(getResources("id", "btn_cancel"));
        btn_ok.setOnClickListener(view -> {
            if (mOnOkClickLisenter != null) {
                mOnOkClickLisenter.OnOK();
            }
            dismiss();
        });
        btn_cancel.setOnClickListener(view -> dismiss());
    }

    public interface OnOkClickLisenter {
        void OnOK();
    }

    private OnOkClickLisenter mOnOkClickLisenter;

    public void setOnOkClickLisenter(OnOkClickLisenter lisenter) {
        this.mOnOkClickLisenter = lisenter;
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }

}
