package cordova.plugin.dahuasdk;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

public class EncryptKeyInputDialog extends Dialog {

    private TextView tvTitle;
    private ClearEditText encryptKey;
    private TextView tvCancel;
    private TextView tvSure;
    private OnClick onClick;

    public EncryptKeyInputDialog(Context context) {
        super(context, ClassInstanceManager.newInstance().getResources("custom_dialog", "style"));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources("layout", "dialog_encryptkey_input"));
        setCanceledOnTouchOutside(false);
        initView();
    }

    private void initView() {
        tvTitle = findViewById(getResources("id", "tv_title"));
        encryptKey = findViewById(getResources("id", "encrypt_key"));
        tvCancel = findViewById(getResources("id", "tv_cancel"));
        tvSure = findViewById(getResources("id", "tv_sure"));
        tvCancel.setOnClickListener(v -> dismissLoading());
        tvSure.setOnClickListener(v -> {
            String key = encryptKey.getText().toString().trim();
            if (onClick != null && !TextUtils.isEmpty(key)) {
                onClick.onSure(key);
                dismissLoading();
            }
        });
    }

    public void dismissLoading() {
        dismiss();
    }

    public interface OnClick {
        void onSure(String txt);
    }

    public void setOnClick(OnClick onClick) {
        this.onClick = onClick;
    }

    private int getResources(String type, String name) {
        return ClassInstanceManager.newInstance().getResources(name, type);
    }
}
