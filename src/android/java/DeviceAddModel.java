package cordova.plugin.dahuasdk;

import android.os.Handler;
import android.util.Log;

import com.company.NetSDK.DEVICE_NET_INFO_EX;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.NET_IN_INIT_DEVICE_ACCOUNT;
import com.company.NetSDK.NET_OUT_INIT_DEVICE_ACCOUNT;

public class DeviceAddModel {
    private static final int NET_ERROR_DEVICE_ALREADY_INIT = 1017;
    private volatile static DeviceAddModel deviceAddModel;
    boolean loop = true;

    private DeviceAddModel() {

    }

    public static DeviceAddModel newInstance() {
        if (deviceAddModel == null) {
            synchronized (DeviceAddModel.class) {
                if (deviceAddModel == null) {
                    deviceAddModel = new DeviceAddModel();
                }
            }
        }
        return deviceAddModel;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public void initDev(final DEVICE_NET_INFO_EX device_net_info_ex, final String pwd, final Handler handler) {
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                if (device_net_info_ex != null) {
                    final NET_IN_INIT_DEVICE_ACCOUNT in = new NET_IN_INIT_DEVICE_ACCOUNT();
                    in.byPwdResetWay = device_net_info_ex.byPwdResetWay;
                    in.byInitStatus = device_net_info_ex.byInitStatus;
                    System.arraycopy(device_net_info_ex.szMac, 0, in.szMac, 0, device_net_info_ex.szMac.length);
                    System.arraycopy(pwd.getBytes(), 0, in.szPwd, 0, pwd.getBytes().length);
                    System.arraycopy("admin".getBytes(), 0, in.szUserName, 0, "admin".getBytes().length);
                    NET_OUT_INIT_DEVICE_ACCOUNT out = new NET_OUT_INIT_DEVICE_ACCOUNT();
                    boolean init = false;
                    for (int i = 0; i < 3; i++) {
                        init = INetSDK.InitDevAccount(in, out, 5 * 1000, null);
                        int error = (INetSDK.GetLastError() & 0x7fffffff);

                        if (init) {
                            break;
                        } else {
                            Log.d("InitPresenter", "initDev:error:" + error);
                            if (error == NET_ERROR_DEVICE_ALREADY_INIT) {    // 设备已初始化
                                init = true;
                                break;
                            }
                        }
                    }
                    Log.d("InitPresenter", "init : " + init);
                    handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, init).sendToTarget();
                }
                handler.obtainMessage(HandleMessageCode.HMC_EXCEPTION).sendToTarget();
            }
        };
    }

    public void initDevByIp(final DEVICE_NET_INFO_EX device_net_info_ex, final String pwd, final Handler handler) {
        new BusinessRunnable(handler) {
            @Override
            public void doBusiness() throws BusinessException {
                NET_IN_INIT_DEVICE_ACCOUNT inInit = new NET_IN_INIT_DEVICE_ACCOUNT();
                String username = "admin";
                String deviceMacAdd = new String(device_net_info_ex.szMac).trim();
                byte pwdResetWay = device_net_info_ex.byPwdResetWay;
                String deviceIp = new String(device_net_info_ex.szIP).trim();
                String cellphone = "";
                String mail = "";

                System.arraycopy(deviceMacAdd.getBytes(), 0, inInit.szMac, 0, deviceMacAdd.getBytes().length);
                System.arraycopy(username.getBytes(), 0, inInit.szUserName, 0, username.getBytes().length);
                System.arraycopy(pwd.getBytes(), 0, inInit.szPwd, 0, pwd.getBytes().length);
                System.arraycopy(cellphone.getBytes(), 0, inInit.szCellPhone, 0, cellphone.getBytes().length);
                System.arraycopy(mail.getBytes(), 0, inInit.szMail, 0, mail.getBytes().length);

                inInit.byPwdResetWay = pwdResetWay;

                NET_OUT_INIT_DEVICE_ACCOUNT outInit = new NET_OUT_INIT_DEVICE_ACCOUNT();
                boolean init = false;
                for (int i = 0; i < 2; i++) {
                    init = INetSDK.InitDevAccountByIP(inInit, outInit, 5000, null, deviceIp);
                    int error = (INetSDK.GetLastError() & 0x7fffffff);

                    if (init) {
                        break;
                    } else {
                        Log.d("InitPresenter", "initDev:error:" + error);
                        if (error == NET_ERROR_DEVICE_ALREADY_INIT) {
                            init = true;
                            break;
                        }
                    }
                }
                Log.d("InitPresenter", "init : " + init);
                handler.obtainMessage(HandleMessageCode.HMC_SUCCESS, init).sendToTarget();
            }
        };
    }

}
