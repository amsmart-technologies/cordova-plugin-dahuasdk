package cordova.plugin.dahuasdk;

public interface IGetDeviceInfoCallBack {
    public interface IDeviceListCallBack {
        void deviceList(DeviceDetailListData.Response responseData);

        void onError(Throwable throwable);
    }

    public interface IDeviceVersionCallBack {
        void deviceVersion(DeviceVersionListData.Response responseData);

        void onError(Throwable throwable);
    }

    public interface IModifyDeviceCallBack {
        void deviceModify(boolean result);

        void onError(Throwable throwable);
    }

    public interface IUnbindDeviceCallBack {
        void unBindDevice(boolean result);

        void onError(Throwable throwable);
    }

    public interface IDeviceChannelInfoCallBack {
        void deviceChannelInfo(DeviceChannelInfoData.Response result);

        void onError(Throwable throwable);
    }

    public interface IDeviceAlarmStatusCallBack {
        void deviceAlarmStatus(boolean result);

        void onError(Throwable throwable);
    }

    public interface IDeviceUpdateCallBack {
        void deviceUpdate(boolean result);

        void onError(Throwable throwable);
    }

    public interface IDeviceCloudRecordCallBack {
        void deviceCloudRecord(CloudRecordsData.Response result);

        void onError(Throwable throwable);
    }

    public interface IDeviceLocalRecordCallBack {
        void deviceLocalRecord(LocalRecordsData.Response result);

        void onError(Throwable throwable);
    }

    public interface IDeviceDeleteRecordCallBack {
        void deleteDeviceRecord();

        void onError(Throwable throwable);
    }

    public interface IDeviceCacheCallBack {
        void deviceCache(DeviceLocalCacheData deviceLocalCacheData);

        void onError(Throwable throwable);
    }

    public interface IDeviceCurrentWifiInfoCallBack {
        void deviceCurrentWifiInfo(CurWifiInfo curWifiInfo);

        void onError(Throwable throwable);
    }

    public interface IModifyDeviceName {
        void deviceName(String newName);
    }

}
