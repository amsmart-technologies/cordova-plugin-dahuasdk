package cordova.plugin.dahuasdk;

public class BusinessException extends Exception {
    public int errorCode = 1;
    public String errorDescription = "UNKNOWN_ERROR";

    public BusinessException() {
    }

    public BusinessException(String e) {
        super(e);
        this.errorDescription = e;
    }

    public BusinessException(Throwable cause) {
        super(cause);
        this.errorDescription = cause.getMessage();
    }

    public BusinessException(int errorCode) {
        this.errorCode = errorCode;
    }
}
