package cordova.plugin.dahuasdk;

public class MethodConst {
    public static String METHOD_DEVICE_BASE_LIST = "deviceBaseList";
    public static String METHOD_DEVICE_OPEN_LIST = "deviceOpenList";
    public static String METHOD_DEVICE_OPEN_DETAIL_LIST = "deviceOpenDetailList";
    public static String METHOD_DEVICE_BASE_DETAIL_LIST = "deviceBaseDetailList";
    public static String METHOD_DEVICE_UN_BIND = "unBindDevice";
    public static String METHOD_DEVICE_VERSION_LIST = "deviceVersionList";
    public static String METHOD_DEVICE_MODIFY_NAME = "modifyDeviceName";
    public static String METHOD_DEVICE_CHANNEL_INFO = "bindDeviceChannelInfo";
    public static String METHOD_DEVICE_MODIFY_ALARM_STATUS = "modifyDeviceAlarmStatus";
    public static String METHOD_DEVICE_UPDATE = "upgradeDevice";
    public static String METHOD_DEVICE_WAKEUP = "wakeUpDevice";
    public static String METHOD_DEVICE_ONLINE = "deviceOnline";
    public static String METHOD_DEVICE_POWER = "getDevicePowerInfo";
    public static String METHOD_CALL_ANSWER = "doorbellCallAnswer";
    public static String METHOD_CALL_REFUSE = "doorbellCallRefuse";
    public static String METHOD_CALL_HANGUP = "doorbellCallHangUp";
    public static String METHOD_GET_CLOUND_RECORDS = "getCloudRecords";
    public static String METHOD_QUERY_LOCAL_RECORD = "queryLocalRecords";
    public static String METHOD_CONTROL_MOVE_PTZ = "controlMovePTZ";
    public static String METHOD_QUERY_CLOUND_RECORDS = "queryCloudRecords";
    public static String METHOD_DELETE_CLOUND_RECORDS = "deleteCloudRecords";

    public interface ParamConst {
        public String deviceDetail = "deviceDetail";
        public String recordType = "recordType";
        public String recordData = "recordsData";
        public int recordTypeLocal = 2;
        public int recordTypeCloud = 1;
        public String fromList = "list";
    }
}
