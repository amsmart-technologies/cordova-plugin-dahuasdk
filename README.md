# Cordova Plugin Dahua SDK
- Cordova plugin for [LCOpenSDK](https://github.com/LCOpenSDK)
- Official website [easy4ip](https://open.easy4ip.com/#home)
- Api docs [Wiki](https://open.easy4ip.com/book/en)