var exec = require('cordova/exec');
module.exports = {
	setEnv: function (appId, appSecret, url, success, error) {
		exec(success, error, "DahuaSDK", "setEnv", [appId, appSecret, url]);
	},
	getToken: function (success, error) {
		exec(success, error, "DahuaSDK", "getToken", []);
	},
	initialize: function (token, success, error) {
		exec(success, error, "DahuaSDK", "initialize", [token]);
	},
	parseQRcode: function (scanStr, success, error) {
		exec(success, error, "DahuaSDK", "parseQRcode", [scanStr]);
	},
	createSubAccount: function (token, account, success, error) {
		exec(success, error, "DahuaSDK", "createSubAccount", [token, account]);
	},
	getOpenIdByAccount: function (token, account, success, error) {
		exec(success, error, "DahuaSDK", "getOpenIdByAccount", [token, account]);
	},
	getSubAccountToken: function (token, openid, success, error) {
		exec(success, error, "DahuaSDK", "getSubAccountToken", [token, openid]);
	},
	listSubAccount: function (token, pageNo, pageSize, success, error) {
		exec(success, error, "DahuaSDK", "listSubAccount", [token, pageNo, pageSize]);
	},
	deleteSubAccount: function (token, openid, success, error) {
		exec(success, error, "DahuaSDK", "deleteSubAccount", [token, openid]);
	},
	listSubAccountDevice: function (token, openid, pageNo, pageSize, success, error) {
		exec(success, error, "DahuaSDK", "listSubAccountDevice", [token, openid, pageNo, pageSize]);
	},
	subAccountDeviceList: function (token, pageNo, pageSize, success, error) {
		exec(success, error, "DahuaSDK", "subAccountDeviceList", [token, pageNo, pageSize]);
	},
	listDeviceDetailsByPage: function (token, page, pageSize, success, error) {
		exec(success, error, "DahuaSDK", "listDeviceDetailsByPage", [token, page, pageSize]);
	},
	addDeviceToSubAccount: function (token, openid, policy, success, error) {
		exec(success, error, "DahuaSDK", "addDeviceToSubAccount", [token, openid, policy]);
	},
	getDevicesList: function (token, openid, pageNo, pageSize, success, error) {
		exec(success, error, "DahuaSDK", "getDevicesList", [token, openid, pageNo, pageSize]);
	},
	getSnapShot: function (device, channelId, success, error) {
		exec(success, error, "DahuaSDK", "getSnapShot", [device, channelId]);
	},
	openDevicePlay: function (device, channelId, playbackEnabled, callBellEvent, success, error) {
		exec(success, error, "DahuaSDK", "openDevicePlay", [device, channelId, playbackEnabled, callBellEvent]);
	},
	openDeviceDetail: function (device, success, error) {
		exec(success, error, "DahuaSDK", "openDeviceDetail", [device]);
	},
	deviceInfoBeforeBind: function (token, deviceId, deviceCodeModel, deviceModelName, ncCode, productId, success, error) {
		exec(success, error, "DahuaSDK", "deviceInfoBeforeBind", [token, deviceId, deviceCodeModel, deviceModelName, ncCode, productId]);
	},
	userDeviceBind: function (token, sn, code, success, error) {
		exec(success, error, "DahuaSDK", "userDeviceBind", [token, sn, code]);
	},
	deviceLeadingInfo: function (token, deviceModel, success, error) {
		exec(success, error, "DahuaSDK", "deviceLeadingInfo", [token, deviceModel]);
	},
	startSearchService: function (success, error) {
		exec(success, error, "DahuaSDK", "startSearchService", []);
	},
	stopSearchService: function (success, error) {
		exec(success, error, "DahuaSDK", "stopSearchService", []);
	},
	getDeviceNetInfo: function (snCode, success, error) {
		exec(success, error, "DahuaSDK", "getDeviceNetInfo", [snCode]);
	},
	startDevInitByIp: function (deviceSerialNumber, deviceInitPassword, success, error) {
		exec(success, error, "DahuaSDK", "startDevInitByIp", [deviceSerialNumber, deviceInitPassword]);
	},
	checkWifi: function (success, error) {
		exec(success, error, "DahuaSDK", "checkWifi", []);
	},
	startSmartConfig: function (deviceSn, ssid, ssid_pwd, success, error) {
		exec(success, error, "DahuaSDK", "startSmartConfig", [deviceSn, ssid, ssid_pwd]);
	},
	stopSmartConfig: function (success, error) {
		exec(success, error, "DahuaSDK", "stopSmartConfig", []);
	},
	listenForSearchedDevices: function (deviceId, success, error) {
		exec(success, error, "DahuaSDK", "listenForSearchedDevices", [deviceId]);
	},
	getWifiListSoftAp: function (pwd, isNotNeedLogin, hasSc, success, error) {
		exec(success, error, "DahuaSDK", "getWifiListSoftAp", [pwd, isNotNeedLogin, hasSc]);
	},
	connectDeviceToWifi: function (wifiName, wifiPwd, deviceId, devicePwd, hasSc, wifiEncry, isNeedInit, success, error) {
		exec(success, error, "DahuaSDK", "connectDeviceToWifi", [wifiName, wifiPwd, deviceId, devicePwd, hasSc, wifiEncry, isNeedInit]);
	},
	connectPhoneToWifi: function (wifiName, wifiPwd, success, error) {
		exec(success, error, "DahuaSDK", "connectPhoneToWifi", [wifiName, wifiPwd]);
	},
	modifyDeviceName: function (token, deviceId, channelId, name, success, error) {
		exec(success, error, "DahuaSDK", "modifyDeviceName", [token, deviceId, channelId, name]);
	},
	searchDeviceBLE: function (wifiName, wifiPwd, deviceId, deviceProductId, success, error) {
		exec(success, error, "DahuaSDK", "searchDeviceBLE", [wifiName, wifiPwd, deviceId, deviceProductId]);
	},
	unbindDevice: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "unbindDevice", [token, deviceId]);
	},
	bindDeviceChannelInfo: function (token, deviceId, channelId, success, error) {
		exec(success, error, "DahuaSDK", "bindDeviceChannelInfo", [token, deviceId, channelId]);
	},
	modifyDeviceAlarmStatus: function (token, deviceId, channelId, enable, success, error) {
		exec(success, error, "DahuaSDK", "modifyDeviceAlarmStatus", [token, deviceId, channelId, enable]);
	},
	deviceVersionList: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "deviceVersionList", [token, deviceId]);
	},
	wifiAround: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "wifiAround", [token, deviceId]);
	},
	controlDeviceWifi: function (token, deviceId, ssid, bssid, password, success, error) {
		exec(success, error, "DahuaSDK", "controlDeviceWifi", [token, deviceId, ssid, bssid, password]);
	},
	timeZoneQueryByDay: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "timeZoneQueryByDay", [token, deviceId]);
	},
	timeZoneConfigByDay: function (token, deviceId, areaIndex, timeZone, beginSunTime, endSunTime, success, error) {
		exec(success, error, "DahuaSDK", "timeZoneConfigByDay", [token, deviceId, areaIndex, timeZone, beginSunTime, endSunTime]);
	},
	setMessageCallback: function (token, status, callbackUrl, callbackFlag, success, error) {
		exec(success, error, "DahuaSDK", "setMessageCallback", [token, status, callbackUrl, callbackFlag]);
	},
	getMessageCallback: function (token, success, error) {
		exec(success, error, "DahuaSDK", "getMessageCallback", [token]);
	},
	getAlarmMessage: function (token, deviceId, channelId, beginTime, endTime, count, nextAlarmId, success, error) {
		exec(success, error, "DahuaSDK", "getAlarmMessage", [token, deviceId, channelId, beginTime, endTime, count, nextAlarmId]);
	},
	deleteAlarmMessage: function (token, deviceId, indexId, channelId, success, error) {
		exec(success, error, "DahuaSDK", "deleteAlarmMessage", [token, deviceId, indexId, channelId]);
	},
	queryCloudRecordCallNum: function (token, strategyId, success, error) {
		exec(success, error, "DahuaSDK", "queryCloudRecordCallNum", [token, strategyId]);
	},
	openCloudRecord: function (token, deviceId, channelId, strategyId, deviceCloudId, success, error) {
		exec(success, error, "DahuaSDK", "openCloudRecord", [token, deviceId, channelId, strategyId, deviceCloudId]);
	},
	getDeviceCloud: function (token, deviceId, channelId, success, error) {
		exec(success, error, "DahuaSDK", "getDeviceCloud", [token, deviceId, channelId]);
	},
	deviceSdcardStatus: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "deviceSdcardStatus", [token, deviceId]);
	},
	deviceStorage: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "deviceStorage", [token, deviceId]);
	},
	recoverSDCard: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "recoverSDCard", [token, deviceId]);
	},
	setDeviceSnap: function (token, deviceId, channelId, success, error) {
		exec(success, error, "DahuaSDK", "setDeviceSnap", [token, deviceId, channelId]);
	},
	queryLocalRecordPlan: function (token, deviceId, channelId, success, error) {
		exec(success, error, "DahuaSDK", "queryLocalRecordPlan", [token, deviceId, channelId]);
	},
	setLocalRecordPlanRules: function (token, deviceId, channelId, rules, success, error) {
		exec(success, error, "DahuaSDK", "setLocalRecordPlanRules", [token, deviceId, channelId, rules]);
	},
	restartDevice: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "restartDevice", [token, deviceId]);
	},
	modifyDevicePwd: function (token, deviceId, oldPwd, newPwd, success, error) {
		exec(success, error, "DahuaSDK", "modifyDevicePwd", [token, deviceId, oldPwd, newPwd]);
	},
	upgradeProcessDevice: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "upgradeProcessDevice", [token, deviceId]);
	},
	upgradeDevice: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "upgradeDevice", [token, deviceId]);
	},
	wakeUpDevice: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "wakeUpDevice", [token, deviceId]);
	},
	deviceOnline: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "deviceOnline", [token, deviceId]);
	},
	getDevicePowerInfo: function (token, deviceId, success, error) {
		exec(success, error, "DahuaSDK", "getDevicePowerInfo", [token, deviceId]);
	},
	getDeviceCameraStatus: function (token, deviceId, channelId, enableType, success, error) {
		exec(success, error, "DahuaSDK", "getDeviceCameraStatus", [token, deviceId, channelId, enableType]);
	},
	setDeviceCameraStatus: function (token, deviceId, channelId, enableType, enable, success, error) {
		exec(success, error, "DahuaSDK", "setDeviceCameraStatus", [token, deviceId, channelId, enableType, enable]);
	},
	checkOverlayPermission: function (success, error) {
		exec(success, error, "DahuaSDK", "checkOverlayPermission", []);
	},
};